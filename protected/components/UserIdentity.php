<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$user=User::model()->findByAttributes(array('username'=>$this->username));
		
		$pegawai = Pegawai::model()->findByAttributes(array('nip'=>str_replace(' ','',$this->username)));
		
		if($user!==null) 
		{

			if($user->password===$this->password)
			{	
				$lastLogin = time();
				$this->setState('lastLoginTime', $lastLogin);
				$this->setState('role','admin');
				$this->setState('role_id',$user->role_id);
				
				$lastLogin = new LoginHistory;
				date_default_timezone_set('Asia/Jakarta');
				$lastLogin->model = 'user';
				$lastLogin->user_id = $user->id;
				$lastLogin->waktu_login = date('Y-m-d H:i:s');
				$lastLogin->save();
				
				$this->errorCode=self::ERROR_NONE;
				
			} else {
				$this->errorCode=self::ERROR_PASSWORD_INVALID;
			}	
				
		} else if($pegawai!==null) {
			
			if($pegawai->password==$this->password)
			{
				$this->setState('role','pegawai');
				$this->setState('pegawai_id',$pegawai->id);
				
				$lastLogin = new LoginHistory;
				date_default_timezone_set('Asia/Jakarta');
				$lastLogin->model = 'pegawai';
				$lastLogin->user_id = $pegawai->id;
				$lastLogin->waktu_login = date('Y-m-d H:i:s');
				$lastLogin->save();
				
				$this->errorCode=self::ERROR_NONE;
			} else {
				$this->errorCode=self::ERROR_PASSWORD_INVALID;
			}
		
		} else {
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		} 
							
			
		return !$this->errorCode;
	}
}