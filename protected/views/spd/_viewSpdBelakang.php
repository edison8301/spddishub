<div>&nbsp;</div>

<h2 class="bigger">Data Perjalanan</h2>


<?php $this->widget('booster.widgets.TbButton',array(
	'label' => 'Tambah Data Perjalanan',
	'buttonType'=>'link',
	'icon'=>'plus',
	'context' => 'primary',
	'url'=>array('spdBelakang/create','id_spd'=>$model->id),
	'htmlOptions'=>array('class'=>'big-button'),
)); ?>



<?php $spdBelakangs = SpdBelakang::model()->findAllByAttributes(array('id_spd'=>$model->id)); ?>
<?php $i=1; foreach($spdBelakangs as $spdBelakang) { ?>

<hr>	

<?php $this->widget('booster.widgets.TbButton',array(
				'label' => 'Sunting Perjalanan '.$i,
				'buttonType'=>'link',
				'icon'=>'pencil',
				'context' => 'primary',
				'url'=>array('spdBelakang/update','id'=>$spdBelakang->id),
				'htmlOptions'=>array('class'=>'big-button'),
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
				'label' => 'Hapus Perjalanan '.$i,
				'buttonType'=>'link',
				'icon'=>'trash',
				'context' => 'primary',
				'url'=>array('spdBelakang/update','id'=>$spdBelakang->id),
				'htmlOptions'=>array(
					'class'=>'big-button',
					'submit'=>array('spdBelakang/delete','id'=>$spdBelakang->id),
					'confirm'=>'Apakah anda yakin ?'
				)
)); ?>

<div>&nbsp;</div>

<div>&nbsp;</div>


<table class="table table-hover table-bordered">
<thead>
<tr>
	<td colspan="5" style="font-weight:bold">
		Perjalanan <?php print $i; ?>
	</td>
</tr>
</thead>
<tr>
	<td width="15%" style="font-weight:bold">Tiba di</td>
	<td width="35%"><?php print $spdBelakang->tempat_tiba; ?></td>
	<td width="15%" style="font-weight:bold">Berangkat dari</td>
	<td width="35%"><?php print $spdBelakang->tempat_berangkat; ?></td>
</tr>
<tr>
	<td style="font-weight:bold">Tiba Pada tanggal</td>
	<td><?php print Bantu::tanggal($spdBelakang->tanggal_tiba); ?></td>
	<td style="font-weight:bold">ke</td>
	<td><?php print $spdBelakang->ke; ?></td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td style="font-weight:bold">Berangkat Pada Tanggal</td>
	<td><?php print Bantu::tanggal($spdBelakang->tanggal_berangkat); ?></td>
</tr>
<tr>
	<td style="font-weight:bold">Jabatan Kepala Tiba</td>
	<td><?php print $spdBelakang->jabatan_kepala_tiba; ?></td>
	<td style="font-weight:bold">Jabatan Kepala Berangkat</td>
	<td><?php print $spdBelakang->jabatan_kepala_berangkat; ?></td>
</tr>
<tr>
	<td style="font-weight:bold">Nama Kepala Tiba</td>
	<td><?php print $spdBelakang->nama_kepala_tiba; ?></td>
	<td style="font-weight:bold">Nama Kepala Berangkat</td>
	<td><?php print $spdBelakang->nama_kepala_berangkat; ?></td>
</tr>
<tr>
	<td style="font-weight:bold">Nip Kepala Tiba</td>
	<td><?php print $spdBelakang->nip_kepala_tiba; ?></td>
	<td style="font-weight:bold">Nip Kepala Berangkat</td>
	<td><?php print $spdBelakang->nip_kepala_berangkat; ?></td>
</tr>
</table>

<div>&nbsp;</div>


<?php $i++; } ?>