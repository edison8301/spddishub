<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'bordered striped',
		'attributes'=>array(
			'nomor_spd',
			array(
				'class'=>'CDataColumn',
				'name'=>'tanggal_spd',
				'header'=>'Tanggal SPD',
				'type'=>'raw',
				'value'=>Yii::app()->dateFormatter->format("dd-MM-yyyy",$model->tanggal_spd),
			),
			array(
				'label'=>'Pemberi Perintah',
				'type'=>'raw',
				'value'=>$model->getRelationField("pejabat","nama")
			),
			array(
				'label'=>'Atas Nama',
				'type'=>'raw',
				'value'=>$model->getRelationField("atas_nama","nama")
			),			
			'tujuan',
			array(
				'class'=>'CDataColumn',
				'name'=>'tgl_pergi',
				'header'=>'Tangal Pergi',
				'type'=>'raw',
				'value'=>Yii::app()->dateFormatter->format("dd-MM-yyyy",$model->tgl_pergi),
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'tgl_kembali',
				'header'=>'Tanggal Kembali',
				'type'=>'raw',
				'value'=>Yii::app()->dateFormatter->format("dd-MM-yyyy",$model->tgl_kembali),
			),
			array(
				'label'=>'Lama Perjalanan',
				'type'=>'raw',
				'value'=>''.CHtml::encode($model->lama).' hari'
			),
			'maksud',
			'kendaraan',
			'tempat_berangkat',
			'instansi',
			'koring',
		),
)); ?>