<?php
$this->breadcrumbs=array(
	'Spds'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Spd','url'=>array('index')),
array('label'=>'Create Spd','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('spd-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Daftar Surat Perjalanan Dinas</h1>

<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'link','type'=>'primary','label'=>'Rekap Perjalanan Dinas','htmlOptions'=>array('target'=>'_blank'), 'icon'=>'download-alt white', 'url'=>array('export/ExportSpdDetail'))); ?>

<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'link','type'=>'primary','label'=>'Rekap Detail Perjalanan Dinas','htmlOptions'=>array('target'=>'_blank'), 'icon'=>'download-alt white', 'url'=>array('export/ExportSpdDetail'))); ?>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'spd-grid',
'type'=> 'striped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'nomor_spt',
		array(
			'class'=>'CDataColumn',
			'name'=>'tanggal_spt',
			'header'=>'Tanggal SPT',
			'type'=>'raw',
			'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tanggal_spt)',
		),
		array(
			'class'=>'CDataColumn',
			'name'=>'id_pejabat',
			'header'=>'Penandatangan',
			'type'=>'raw',
			'value'=>'$data->pejabat->nama',
			'filter'=>CHtml::listData(Pejabat::model()->findAll(),'id','nama')
		),
		array(
			'class'=>'CDataColumn',
			'name'=>'id_pegawai',
			'header'=>'Pegawai',
			'type'=>'raw',
			'value'=>'$data->pegawai->nama',
			'filter'=>CHtml::listData(Pegawai::model()->findAll(),'id','nama')
		),
		'maksud',
		/*
		'tujuan',
		'tgl_pergi',
		'tgl_kembali',
		'lama',
		'kendaraan',
		'tempat_berangkat',
		'instansi',
		'biaya',
		'koring',
		*/
/*array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),*/
),
)); ?>
