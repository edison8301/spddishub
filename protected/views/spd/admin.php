<?php
$this->breadcrumbs=array(
	'Data SPD'=>$this->createUrl('spd/admin'),
);

$this->menu=array(
array('label'=>'List Spd','url'=>array('index')),
array('label'=>'Create Spd','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('spd-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Data Surat Perjalanan Dinas</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah SPD',
		'icon'=>'plus',
		'context'=>'primary',
		'url'=>array('create'),
		'htmlOptions'=>array('class'=>'big-button'),
)); ?>&nbsp;

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'spd-grid',
		'type'=> 'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			array(
				'class'=>'CDataColumn',
				'name'=>'nomor_spd',
				'header'=>'Nomor SPD',
				'type'=>'raw',
				'value'=>'$data->nomor_spd',
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'tgl_pergi',
				'header'=>'Tanggal Pergi',
				'type'=>'raw',
				'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tgl_pergi)',
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'tgl_kembali',
				'header'=>'Tanggal Kembali',
				'type'=>'raw',
				'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tgl_kembali)',
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'tujuan',
				'header'=>'Tempat Tujuan',
				'type'=>'raw',
				'value'=>'$data->tujuan',
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'tanggal_spd',
				'header'=>'Tanggal SPD',
				'type'=>'raw',
				'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tanggal_spd)',
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
			)
		),
)); ?>