<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'spd-form',
	'enableAjaxValidation'=>false,
)); ?>

<script type="text/javascript">
	$(document).ready(function(){
		$(".format-money").on("keyup", function(){
	    var _this = $(this);
	    var value = _this.val().replace(/\.| /g,"");
	    _this.val(accounting.formatMoney(value, "", 0, ".", ","))
	})
});
</script>

<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>


<?php echo $form->errorSummary($model); ?>

<div class="row">
	<div class="col-xs-6">
		<?php echo $form->textFieldGroup($model,'nomor_spd',array('class'=>'span6','maxlength'=>255,'value'=>$model->nomor_spt,'prepend'=>'<i class="glyphicon glyphicon-asterisk"></i>')); ?>
		<?php echo $form->datePickerGroup($model,'tanggal_spd',array(
				'widgetOptions' => array(
					'options' => array(
						'language' => 'id',
						'format'=>'yyyy-mm-dd',
						'autoclose'=>true
					),
				),
				'wrapperHtmlOptions' => array(
					'class' => 'col-sm-5',
				),
				'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
		)); ?>

		<?php echo $form->textFieldGroup($model,'tujuan',array('wrapperHtmlOptions'=>array('class'=>'span6'),'widgetOptions'=>array('htmlOptions'=>array('maxlength'=>255)),'prepend'=>'<i class="glyphicon glyphicon-map-marker"></i>')); ?>
		
		<?php echo $form->datePickerGroup($model,'tgl_pergi',array(
				'widgetOptions' => array(
					'options' => array(
						'language' => 'id',
						'format'=>'yyyy-mm-dd',
						'autoclose'=>true
					),
				),
				'wrapperHtmlOptions' => array(
					'class' => 'col-sm-5',
				),
				'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
		)); ?>
		
		<?php echo $form->datePickerGroup($model,'tgl_kembali',array(
				'widgetOptions' => array(
					'options' => array(
						'language' => 'id',
						'format'=>'yyyy-mm-dd',
						'autoclose'=>true
					),
				),
				'wrapperHtmlOptions' => array(
					'class' => 'col-sm-5',
				),
				'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
		)); ?>
		
		<?php echo $form->textareaGroup($model,'maksud',array('wrapperHtmlOptions'=>array('class'=>'span8',),'widgetOptions'=>array('htmlOptions'=>array('maxlength'=>255,'rows'=>3,)))); ?>
		
		
	</div>
	<div class="col-xs-6">
		<?php echo $form->textFieldGroup($model,'tempat_berangkat',array('widgetOptions'=>array('htmlOptions'=>array('value'=>$model->tempat_berangkat == null ? "Serang" : $model->tempat_berangkat)))); ?>
		
		<?php echo $form->textFieldGroup($model,'kendaraan',array('widgetOptions'=>array('htmlOptions'=>array('value'=>$model->kendaraan == null ? "Mobil Dinas" : $model->kendaraan)))); ?>
		
		<?php echo $form->textFieldGroup($model,'instansi',array('widgetOptions'=>array('htmlOptions'=>array('value'=>$model->instansi == null ? "Dishubkominfo" : $model->instansi)))); ?>
		
		<?php echo $form->dropDownListGroup($model,'koring',array('widgetOptions'=>array('data'=>CHtml::listData(Koring::model()->findAll(),'koring','koring')))); ?>
		
		<?php echo $form->hiddenField($model,'id_pejabat'); ?>
		<?php print CHtml::label('Pemberi Perintah','',array('class'=>'control-label')); ?>
		<div class="input-group">
			<span class="input-group-btn">
				<?php $this->widget('booster.widgets.TbButton', array(
					'label'=>'[..]',
					'context'=>'primary', 
					'htmlOptions'=>array(
						'onClick'=>'dialogPejabat(); return false;',
						'style'=>'margin-bottom:10px',
						//'class'=>'big-button',
					),
				)); ?>
			</span>
			<?php print CHtml::textField('nama_pejabat',$model->id_pejabat != null ? $model->getRelationField("pejabat","nama") : '',array('disabled'=>true,'placeholder'=>'Pilih Penandatangan','class'=>'form-control')); ?>
		</div>
		
		<?php echo $form->hiddenField($model,'id_atas_nama'); ?>
		<?php print CHtml::label('Atas Nama','',array('class'=>'control-label')); ?>
		<div class="input-group">
			<span class="input-group-btn">
				<?php $this->widget('booster.widgets.TbButton', array(
					'label'=>'[..]',
					'context'=>'primary', 
					'htmlOptions'=>array(
						'onClick'=>'dialogAtasNama(); return false;',
						'style'=>'margin-bottom:10px',
						//'class'=>'big-button',
					),
				)); ?>
			</span>
			<?php print CHtml::textField('atas_nama',$model->id_atas_nama != null ? $model->getRelationField("atas_nama","nama") : '',array('disabled'=>true,'placeholder'=>'Pilih Atas Nama','class'=>'form-control')); ?>
		</div>
	</div>
</div>
<div>&nbsp;</div>
<div class="row">	
<div class="col-xs-12">
<div class="form-actions well">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
			'htmlOptions'=>array('class'=>'big-button'),
	)); ?>
</div>
</div>
</div>
<?php $this->endWidget(); ?>

<div class="hidden">
<div id="wrapper-tambah-pejabat">
<?php //Dialog Pejabat
	$pejabat = new Pejabat;
	
	$this->widget('booster.widgets.TbGridView',array(
		'id'=>'dialogPejabat',
		'type'=> 'striped bordered',
		'afterAjaxUpdate'=>'function(id,data) { isi = $("#wrapper-tambah-pejabat").html(); $(".modal-content").html(isi);  }',
		'dataProvider'=>$pejabat->search(),
		'filter'=>$pejabat,
		'columns'=>array(
			'nip',
			'nama',
			'jabatan',
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "
								$(\"#Spd_id_pejabat\").val(\"$data->id\");
								$(\"#nama_pejabat\").val(\"$data->nama\");
								$(\".modal-window\").closeModal();",
					"class"=>"big-button",
					"id"=>"pilihPejabat",
				))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
				),
			),
		),
	));
?>
</div>
</div>

<div class="hidden">
<div id="wrapper-atas-nama">
<?php //Dialog Pejabat
	$atasNama = new Pejabat;
	
	$this->widget('booster.widgets.TbGridView',array(
		'id'=>'dialogAtasNama',
		'type'=> 'striped bordered',
		'afterAjaxUpdate'=>'function(id,data) { isi = $("#wrapper-atas-nama").html(); $(".modal-content").html(isi);  }',
		'dataProvider'=>$atasNama->search(),
		'filter'=>$atasNama,
		'columns'=>array(
			'nip',
			'nama',
			'jabatan',
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "
								$(\"#Spd_id_atas_nama\").val(\"$data->id\");
								$(\"#atas_nama\").val(\"$data->nama\");
								$(\".modal-window\").closeModal();",
					"class"=>"big-button",
					"id"=>"pilihPejabat",
				))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
				),
			),
		),
	));
?>
</div>
</div>

<script>
// Pejabat Modal
function dialogPejabat()
{
	$.modal({
		content: $("#wrapper-tambah-pejabat"),
		title: 'Pilih Penandatangan Dokumen',
		maxWidth: 700,
		buttons: {
			'Close': function(win) { win.closeModal(); }
		}
	});
}

function dialogAtasNama()
{
	$.modal({
		content: $("#wrapper-atas-nama"),
		title: 'Pilih Atas Nama',
		maxWidth: 700,
		buttons: {
			'Close': function(win) { win.closeModal(); }
		}
	});
}
</script>