<table border="<?php print $border; ?>" class="table">
<tr>
	<th>No SPT</th>
	<th>TGL SPT</th>
	<th>PEJABAT</th>
	<th>NIP PEJABAT</th>
	<th>JABATAN</th>
	<th>PEGAWAI</th>
	<th>NIP PEGAWAI</th>
	<th>JAB PEGAWAI</th>
	<th>PENGIKUT 1</th>
	<th>NIP PENGIKUT 1</th>
	<th>JAB PENGIKUT 1</th>
	<th>PENGIKUT 2</th>
	<th>NIP PENGIKUT 2</th>
	<th>JAB PENGIKUT 2</th>
	<th>PENGIKUT 3</th>
	<th>NIP PENGIKUT 3</th>
	<th>JAB PENGIKUT 3</th>
	<th>PENGIKUT 4</th>
	<th>NIP PENGIKUT 4</th>
	<th>JAB PENGIKUT 4</th>
	<th>PENGIKUT 5</th>
	<th>NIP PENGIKUT 5</th>
	<th>JAB PENGIKUT 5</th>
	<th>PENGIKUT 6</th>
	<th>NIP PENGIKUT 6</th>
	<th>JAB PENGIKUT 6</th>
	<th>MAKSUD</th>
	<th>TUJUAN</th>
	<th>TGL PERGI</th>
	<th>TGL KEMBALI</th>
</tr>
<?php if(isset($_GET['tampil'])) { ?>
<?php
		$criteria = new CDbCriteria;
		$criteria->order = 'tanggal_spt ASC';
		
		if(!empty($_GET['tanggal_awal']) AND !empty($_GET['tanggal_akhir']))
		{
			$criteria->condition = 'tanggal_spt >= :tanggal_awal AND tanggal_spt <= :tanggal_akhir';
			$criteria->params = array(':tanggal_awal'=>$_GET['tanggal_awal'],':tanggal_akhir'=>$_GET['tanggal_akhir']);
		}
?>

<?php $i=1; foreach(Spd::model()->findAll($criteria) as $data) { ?>
<tr>
	<td><?php print $data->nomor_spt; ?></td>
	<td><?php print $data->tanggal_spt; ?></td>
	<td><?php print $data->getRelationField("pejabat","nama"); ?></td>
	<td><?php print $data->getRelationField("pejabat","nip"); ?></td>
	<td><?php print $data->getRelationField("pejabat","jabatan"); ?></td>
	<td><?php print $data->getRelationField("pejabat","nama"); ?></td>
	<td><?php print $data->getRelationField("pejabat","nip"); ?></td>
	<td><?php print $data->getRelationField("pejabat","jabatan"); ?></td>
	<?php $i = 1; foreach(Pengikut::model()->findAllByAttributes(array('id_spd'=>$data->id,'aktif'=>1)) as $pengikut) { ?>
	<td><?php print $pengikut->getRelationField("pegawai","nama"); ?></td>
	<td><?php print $pengikut->getRelationField("pegawai","nip"); ?></td>
	<td><?php print $pengikut->getRelationField("pegawai","jabatan"); ?></td>
	<?php $i++; } ?>
	<?php for($j=$i;$j<=6;$j++) { ?>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<?php } ?>
	<td><?php print $data->maksud; ?></td>
	<td><?php print $data->tujuan; ?></td>
	<td><?php print $data->tgl_pergi; ?></td>
	<td><?php print $data->tgl_kembali; ?></td>
</tr>
<?php $i++; } ?>
<?php } ?>
</table>