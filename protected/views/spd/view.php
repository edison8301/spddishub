<?php
$this->breadcrumbs=array(
	'Data SPD'=>$this->createUrl('admin'),
	'Detail SPD'=>$this->createUrl('spd/view',array('id'=>$model->id)),
);

?>

<h1>Surat Perjalanan Dinas</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'label' => 'Sunting SPD',
		'buttonType'=>'link',
		'icon'=>'pencil',
		'context' => 'primary',
		'url'=>array('spd/update','id'=>$model->id),
		'htmlOptions'=>array('class'=>'big-button'),
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'label' => 'Kelola SPD',
		'buttonType'=>'link',
		'icon'=>'list',
		'context' => 'primary',
		'url'=>array('spd/admin'),
		'htmlOptions'=>array('class'=>'big-button'),
)); ?>


<?php $this->widget('booster.widgets.TbButtonGroup',array(
        'context' => 'primary',
        'buttons' => array(
            array(
                'label' => 'Setting',
				'icon' => 'wrench',
				'htmlOptions'=>array('class'=>'big-button'),
                'items' => array(
					array('label' => 'Setting Posisi SPT', 'url' => array('posisiCetak/setting','jenis_surat'=>'5')),
                    array('label' => 'Setting Posisi Visum Depan', 'url' => array('posisiCetak/setting','jenis_surat'=>'1')),
					array('label' => 'Setting Posisi Visum Belakang', 'url' => array('posisiCetak/setting','jenis_surat'=>'4')),
					array('label' => 'Setting Posisi LHPD', 'url' => array('posisiCetak/setting','jenis_surat'=>'3')),
					array('label' => 'Setting Posisi Kwitansi', 'url' => array('posisiCetak/setting','jenis_surat'=>'2')),
					'---',
                    array('label' => 'Setting Font', 'url' => array('setting/update','id'=>1,'id_spd'=>$model->id)),
                )
            ),
        ),
)); ?>
<div>&nbsp;</div>

<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
?>

<?php
Yii::app()->clientScript->registerScript(
   'myHideEffect',
   '$(".alert").animate({opacity: 1.0}, 3000).fadeOut("slow");',
   CClientScript::POS_READY
);
?>

<?php $this->widget('booster.widgets.TbTabs',array(
	'type' => 'tabs',
	//'justified' => true,
	'tabs' => array(
		array(
			'label' => 'Lembar 1', 
			'active'=>true,
			'content' => $this->renderPartial('_viewSpd',array('model'=>$model),true)
		),
		array(
			'label' => 'Lembar 2',
			'encodeLabel'=>false,
			'content' => $this->renderPartial('_viewSpdBelakang',array('model'=>$model),true)
		),	
	),
)); ?>

<div>&nbsp;</div>

<h2 class="bigger">Data Pegawai</h2>

<?php $this->widget('booster.widgets.TbButton',array(
		'label' => 'Tambah Pegawai',
		'buttonType'=>'link',
		'icon'=>'plus',
		'context' => 'primary',
		'htmlOptions' => array(
            'onClick'=>'dialogPegawai(); return false;',
			'class'=>'big-button',
        ),
)); ?>

<div>&nbsp;</div>

<?php $pengikuts = Pengikut::model()->findAllByAttributes(array('id_spd'=>$model->id)); ?>
<table class="table table-hover table-striped table-bordered">
	<thead>
		<tr>
			<th style="text-align:center;width:5%">No</th>
			<th>Nama Pegawai</th>
			<th style="text-align:center">Cetak SPT</th>
			<th style="text-align:center">Cetak SPD</th>
			<th width="7%" style="text-align:center">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php $i=1; foreach($pengikuts as $pengikut) { ?>
		<tr>
			<td style="text-align:center"><?php print $i++; ?></td>
			<td><?php print $pengikut->getRelationField('pegawai','nama'); ?></td>
			<td style="text-align:center">
				<?php print CHtml::Link('<i class="glyphicon glyphicon-print"></i> Cetak SPT (Blanko)',array('spd/cetakSptBlanko','id'=>$model->id,'id_pengikut'=>$pengikut->id_pegawai),array('target'=>'_blank','data-toggle'=>'tooltip','data-placement'=>'top','title'=>'Cetak SPT (Blanko)')); ?>
				<br>
				<?php print CHtml::Link('<i class="glyphicon glyphicon-print"></i> Cetak SPT (Lengkap)',array('spd/cetakSptLengkap','id'=>$model->id,'id_pengikut'=>$pengikut->id_pegawai),array('target'=>'_blank','data-toggle'=>'tooltip','data-placement'=>'top','title'=>'Cetak SPT (Blanko)')); ?>
			</td>
			<td style="text-align:center">
				<?php print CHtml::Link('<i class="glyphicon glyphicon-print"></i> Visum Depan (Blanko)',array('spd/cetakSpdDepanBlanko','id'=>$model->id,'id_pengikut'=>$pengikut->id_pegawai),array('target'=>'_blank','data-toggle'=>'tooltip','data-placement'=>'top','title'=>'Visum Depan (Blanko)')); ?><br>
				<?php print CHtml::Link('<i class="glyphicon glyphicon-print"></i> Visum Depan (Lengkap)',array('spd/cetakSpdDepanLengkap','id'=>$model->id,'id_pengikut'=>$pengikut->id_pegawai,'jenis_surat'=>1),array('target'=>'_blank','data-toggle'=>'tooltip','data-placement'=>'top','title'=>'Visum Depan (Lengkap)')); ?><br>
				<br>
				<?php print CHtml::Link('<i class="glyphicon glyphicon-print"></i> Visum Belakang (Blanko)',array('spd/cetakSpdBelakangBlanko','id'=>$model->id,'id_pengikut'=>$pengikut->id_pegawai),array('target'=>'_blank','data-toggle'=>'tooltip','data-placement'=>'top','title'=>'Visum Belakang (Blanko)')); ?><br>
				<?php print CHtml::Link('<i class="glyphicon glyphicon-print"></i> Visum Belakang (Lengkap)',array('spd/cetakSpdBelakangLengkap','id'=>$model->id,'id_pengikut'=>$pengikut->id_pegawai),array('target'=>'_blank','data-toggle'=>'tooltip','data-placement'=>'top','title'=>'Visum Belakang (Lengkap)')); ?>
			
			</td>
			<td style="text-align:center">
				<?php print CHtml::link('<i class="glyphicon glyphicon-trash"></i>','#',array('submit'=>array('pengikut/delete','id'=>$pengikut->id),'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'Hapus','confirm'=>'Apakah anda yakin ? Data LHP dan kwitansi dari pengikut tersebut akan ikut dihapus juga')); ?>
			</td>
		</tr>
	<?php } ?>
	</tbody>
</table>

<div>&nbsp;</div>

<h2 class="bigger">Data LHPD</h2>

<?php $this->widget('booster.widgets.TbButton',array(
		'label' => 'Tambah LHPD',
		'buttonType'=>'link',
		'icon'=>'plus',
		'context' => 'primary',
		'htmlOptions' => array(
            'onClick'=>'dialogLhp(); return false;',
			'class'=>'big-button',
        ),
)); ?>

<div>&nbsp;</div>

<table class="table table-hover table-striped table-bordered">
	<thead>
		<tr>
			<th>Nama Pegawai</th>
			<th style="text-align:center">Cetak LHP</th>
			<th style="text-align:center" width="10%">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php $lhps = Lhp::model()->findAllByAttributes(array('id_spd'=>$model->id)); ?>
	<?php foreach($lhps as $lhp) { ?>
		<tr>
			<td><?php print $lhp->getRelationField('pegawai','nama'); ?></td>
			<td style="text-align:center">
				<?php print CHtml::Link('<i class="glyphicon glyphicon-print"></i> Cetak LHP (Blanko)',array('lhp/cetakBlanko','id'=>$lhp->id,'id_pengikut'=>$lhp->id_pegawai),array('target'=>'_blank','data-toggle'=>'tooltip','data-placement'=>'top','title'=>'Cetak LHP (Blanko)')); ?>
				<br>
				<?php print CHtml::Link('<i class="glyphicon glyphicon-print"></i> Cetak LHP (Lengkap)',array('lhp/cetakPdf','id'=>$lhp->id),array('target'=>'_blank','data-toggle'=>'tooltip','data-placement'=>'top','title'=>'Cetak LHP (Lengkap)')); ?>
			</td>
			<td style="text-align:center">
				<?php print CHtml::Link('<i class="glyphicon glyphicon-eye-open"></i>',array('lhp/view','id'=>$lhp->id),array('data-toggle'=>'tooltip','data-placement'=>'top','title'=>'View')); ?>
				<?php print CHtml::Link('<i class="glyphicon glyphicon-pencil"></i>',array('lhp/update','id'=>$lhp->id),array('data-toggle'=>'tooltip','data-placement'=>'top','title'=>'Sunting')); ?>
				<?php print CHtml::link('<i class="glyphicon glyphicon-trash"></i>','#',array('submit'=>array('lhp/delete','id'=>$lhp->id),'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'Hapus','confirm'=>'Apakah anda yakin ?')); ?>
			</td>
		</tr>
	<?php } ?>
	</tbody>
</table>

<div>&nbsp;</div>

<h2 class="bigger">Data Kwitansi</h2>
<?php $this->widget('booster.widgets.TbButton',array(
		'label' => 'Tambah Kwitansi',
		'buttonType'=>'link',
		'icon'=>'plus',
		'context' => 'primary',
		'htmlOptions' => array(
            'onClick'=>'dialogKwitansi(); return false;',
			'class'=>'big-button',
        ),
)); ?>

<div>&nbsp;</div>

<table class="table table-hover table-striped table-bordered">
	<thead>
		<tr>
			<th>Nama Pegawai</th>
			<th style="text-align:center">Cetak Kwitansi</th>
			<th style="text-align:center" width="10%">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php $kwitansis = Kwitansi::model()->findAllByAttributes(array('id_spd'=>$model->id)); ?>
	<?php foreach($kwitansis as $kwitansi) { ?>
		<tr>
			<td><?php print $kwitansi->getRelationField('penerimaRelation','nama'); ?></td>
			<td style="text-align:center">
				<?php print CHtml::Link('<i class="glyphicon glyphicon-print"></i> Cetak Kwitansi (Blanko)',array('kwitansi/cetakBlanko','id'=>$kwitansi->id),array('target'=>'_blank','data-toggle'=>'tooltip','data-placement'=>'top','title'=>'Cetak Kwitansi')); ?><br>
				<?php print CHtml::Link('<i class="glyphicon glyphicon-print"></i> Cetak Kwitansi (Lengkap)',array('kwitansi/cetakLengkap','id'=>$kwitansi->id),array('target'=>'_blank','data-toggle'=>'tooltip','data-placement'=>'top','title'=>'Cetak Kwitansi')); ?>
			
			</td>
			<td style="text-align:center">
				<?php print CHtml::Link('<i class="glyphicon glyphicon-eye-open"></i>',array('kwitansi/view','id'=>$kwitansi->id),array('data-toggle'=>'tooltip','data-placement'=>'top','title'=>'View')); ?>
				<?php print CHtml::Link('<i class="glyphicon glyphicon-pencil"></i>',array('kwitansi/update','id'=>$kwitansi->id),array('data-toggle'=>'tooltip','data-placement'=>'top','title'=>'Sunting')); ?>
				<?php print CHtml::link('<i class="glyphicon glyphicon-trash"></i>','#',array('submit'=>array('kwitansi/delete','id'=>$kwitansi->id),'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'Hapus','confirm'=>'Apakah anda yakin ?')); ?>
			</td>	
		</tr>
	<?php } ?>
	</tbody>
</table>

<div style="overflow:auto" class="hidden modal-tambah-pegawai">
<div id="wrapper-tambah-pegawai">
<?php 
$tes = new Pegawai;
$this->widget('booster.widgets.TbGridView',array(
	'id'=>'tambahPegawai',
	'type'=>'striped bordered',
	//'ajaxUpdate'=>false,
	'afterAjaxUpdate'=>'function(id,data) { isi = $("#wrapper-tambah-pegawai").html(); $(".modal-content").html(isi);  }',
	'dataProvider'=>$tes->search(),
	'filter'=>$tes,
	'columns'=>array(
		'nip',
		'nama',
		array(
			'class'=>'CDataColumn',
			'name'=>'id_golongan',
			'header'=>'Golongan',
			'type'=>'raw',
			'value'=>'$data->golongan->nama',
			'filter'=>CHtml::listData(Golongan::model()->findAll(),'id','nama')
		),
		'jabatan',
		/*array(
			'class'=>'CDataColumn',
			'name'=>'tgl_lahir',
			'header'=>'Tanggal Lahir',
			'type'=>'raw',
			'value'=>'Bantu::tgl($data->tgl_lahir)',
			//'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tgl_lahir)',
			
		),*/
		array(
			'header'=>'Pilih',
			'type'=>'raw',
			'value'=>'CHtml::Link("<i class=\"glyphicon glyphicon-plus\"></i>",array("/pengikut/tambah","id_pegawai"=>$data->id,"id_spd"=>$_GET["id"]),array("data-toggle"=>"tooltip","data-placement"=>"top","title"=>"Pilih Pegawai"))',
			'htmlOptions'=>array(
				'style'=>'text-align:center',
				'width'=>'30px',
			),
		),
	),
)); ?>
</div><!-- wrapper-tambah-pegawai -->
</div>


<div class="hidden modal-tambah-lhp">
<div id="wrapper-tambah-lhp">
<?php $dataPegawai = new Pegawai;
$this->widget('booster.widgets.TbGridView',array(
	'id'=>'tambahLhp',
	'type'=>'striped bordered',
	'afterAjaxUpdate'=>'function(id,data) { isi = $("#wrapper-tambah-lhp").html(); $(".modal-content").html(isi);  }',
	'dataProvider'=>$dataPegawai->searchForLhpAndKwitansi(),
	'filter'=>$dataPegawai,
	'columns'=>array(
		'nip',
		'nama',
		array(
			'class'=>'CDataColumn',
			'name'=>'id_golongan',
			'header'=>'Golongan',
			'type'=>'raw',
			'value'=>'$data->golongan->nama',
			'filter'=>CHtml::listData(Golongan::model()->findAll(),'id','nama')
		),
		'jabatan',
		/*array(
			'class'=>'CDataColumn',
			'name'=>'tgl_lahir',
			'header'=>'Tanggal Lahir',
			'type'=>'raw',
			'value'=>'Bantu::tgl($data->tgl_lahir)',
			//'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tgl_lahir)',
		),*/
		array(
			'header'=>'Pilih',
			'type'=>'raw',
			'value'=>'CHtml::Link("<i class=\"glyphicon glyphicon-plus\"></i>",array("/lhp/create","id_pegawai"=>$data->id,"id_spd"=>$_GET["id"]),array("data-toggle"=>"tooltip","data-placement"=>"top","title"=>"Tambah LHP"))',
			'htmlOptions'=>array(
				'style'=>'text-align:center',
				'width'=>'30px',
			),
		),
	),
)); ?>
</div>
</div>

<div class="hidden modal-tambah-kwitansi">
<div id="wrapper-tambah-kwitansi">
<?php $dataPegawai = new Pegawai;
$this->widget('booster.widgets.TbGridView',array(
	'id'=>'tambahKwitansi',
	'type'=>'striped bordered',
	'afterAjaxUpdate'=>'function(id,data) { isi = $("#wrapper-tambah-kwitansi").html(); $(".modal-content").html(isi);  }',
	'dataProvider'=>$dataPegawai->searchForLhpAndKwitansi(),
	'filter'=>$dataPegawai,
	'columns'=>array(
		'nip',
		'nama',
		array(
			'class'=>'CDataColumn',
			'name'=>'id_golongan',
			'header'=>'Golongan',
			'type'=>'raw',
			'value'=>'$data->golongan->nama',
			'filter'=>CHtml::listData(Golongan::model()->findAll(),'id','nama')
		),
		'jabatan',
		/*array(
			'class'=>'CDataColumn',
			'name'=>'tgl_lahir',
			'header'=>'Tanggal Lahir',
			'type'=>'raw',
			'value'=>'Bantu::tgl($data->tgl_lahir)',
			//'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tgl_lahir)',
			
		),*/
		array(
			'header'=>'Pilih',
			'type'=>'raw',
			'value'=>'CHtml::Link("<i class=\"glyphicon glyphicon-plus\"></i>",array("/kwitansi/create","penerima"=>$data->id,"id_spd"=>$_GET["id"]),array("data-toggle"=>"tooltip","data-placement"=>"top","title"=>"Tambah Kwitansi"))',
			'htmlOptions'=>array(
				'style'=>'text-align:center',
				'width'=>'30px',
			),
		),
	),
)); ?>
</div>
</div>

<script>
// Modal Tambah Pegawai
function dialogPegawai()
{
	$.modal({
		content: $("#wrapper-tambah-pegawai"),
		title: 'Pilih Pegawai',
		width: 700,
		buttons: {
			'Close': function(win) { win.closeModal(); }
		}
	});
}
// Modal Tambah LHP
function dialogLhp()
{
	$.modal({
		content: $("#wrapper-tambah-lhp"),
		title: 'Pilih Pegawai',
		width: 700,
		buttons: {
			'Close': function(win) { win.closeModal(); }
		}
	});
}
// Modal Tambah Kwitansi
function dialogKwitansi()
{
	$.modal({
		content: $("#wrapper-tambah-kwitansi"),
		title: 'Pilih Pegawai',
		width: 700,
		buttons: {
			'Close': function(win) { win.closeModal(); }
		}
	});
}
</script>