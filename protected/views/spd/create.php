<?php
$this->breadcrumbs=array(
	'Data SPD'=>$this->createUrl('admin'),
	'Input SPD'=>$this->createUrl('spd/create'),
);

	$this->menu=array(
	array('label'=>'List Spd','url'=>array('index')),
	array('label'=>'Create Spd','url'=>array('create')),
	array('label'=>'View Spd','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Spd','url'=>array('admin')),
	);
	?>

<h1>Input Surat Perjalanan Dinas</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>