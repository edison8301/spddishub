<?php
$this->breadcrumbs=array(
	'Spds'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Spd','url'=>array('index')),
array('label'=>'Create Spd','url'=>array('create')),
array('label'=>'Update Spd','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Spd','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Spd','url'=>array('admin')),
);
?>

<h1>Surat Perjalanan Dinas</h1>

		<?php
			$this->widget(
				'bootstrap.widgets.TbButton',
				array(
				'label' => 'Cetak Lembar ke-1',
				'icon'=>'download-alt white',
				'type' => 'primary',
				'url'=>array('export/ExportSpdOto1','id'=>$model->id),
				'htmlOptions'=>array('target'=>'_blank')
				)
			);
			echo "&nbsp;";
			$this->widget(
				'bootstrap.widgets.TbButton',
				array(
				'label' => 'Cetak Lembar ke-2',
				'icon'=>'download-alt white',
				'type' => 'primary',
				'url'=>array('export/ExportSpdOto2','id'=>$model->id),
				'htmlOptions'=>array('target'=>'_blank')
				)
			);
			
		?>

<div>&nbsp;</div>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'nomor',
		'tanggal',
		array(
			'label'=>'Pejabat',
			'type'=>'raw',
			'value'=>CHtml::encode($model->pejabat->nama)
		),
		array(
			'label'=>'Pegawai',
			'type'=>'raw',
			'value'=>CHtml::encode($model->pegawai->nama)
		),
		'maksud',
		'tujuan',
		'tgl_pergi',
		'tgl_kembali',
		array(
			'label'=>'Lama',
			'type'=>'raw',
			'value'=>''.CHtml::encode($model->lama).' hari'
		),
		'kendaraan',
		'tempat_berangkat',
		'instansi',
		'biaya',
		'koring',
		array(
			'label'=>'Pengikut',
			'type'=>'raw',
			'value'=>''.CHtml::encode($model->pengikut).' Orang'
		),
),
)); ?>
