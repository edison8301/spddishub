<?php
$this->breadcrumbs=array(
	'Spds',
);

$this->menu=array(
array('label'=>'Create Spd','url'=>array('create')),
array('label'=>'Manage Spd','url'=>array('admin')),
);
?>

<h1>Spds</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
