<?php
$this->breadcrumbs=array(
	'Setting Posisi Cetak'=>$this->createUrl('setting',array('jenis_surat'=>$model->jenis_surat)),
	'Update Posisi Cetak'=>$this->createUrl('update',array('id'=>$model->id)),
);

	$this->menu=array(
	array('label'=>'List PosisiCetak','url'=>array('index')),
	array('label'=>'Create PosisiCetak','url'=>array('create')),
	array('label'=>'View PosisiCetak','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage PosisiCetak','url'=>array('admin')),
	);
	?>

	<h1>Update Posisi Cetak : <?php echo $model->kolom; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>