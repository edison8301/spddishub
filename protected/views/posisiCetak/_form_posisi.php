<h2><?php print $model->kolom; ?></h2>

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'posisi-cetak-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>
<?php echo $form->errorSummary($model); ?>
	
	<?php echo $form->textFieldGroup($model,'horisontal',array('class'=>'span5')); ?>

	<?php echo $form->textFieldGroup($model,'vertikal',array('class'=>'span5')); ?>
	
	<?php echo $form->textFieldGroup($model,'font_size',array('class'=>'span5')); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>'Simpan',
		)); ?>
</div>

<?php $this->endWidget(); ?>
