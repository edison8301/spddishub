<?php
$this->breadcrumbs=array(
	'Setting Posisi Cetak'=>$this->createUrl('setting',array('jenis_surat'=>$_GET['jenis_surat'])),
);
?>

<h1>Setting Posisi Cetak</h1>

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'posisi-cetak-grid',
'type'=>'striped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
	'kolom',
	array(
		'class' => 'booster.widgets.TbEditableColumn',
		'name' => 'horisontal',
		'sortable'=>true,
		'editable' => array(
			'url' => $this->createUrl('posisiCetak/directUpdate'),
			'placement' => 'top',
			'inputclass' => 'input-large',
		)
	),
	array(
		'class' => 'booster.widgets.TbEditableColumn',
		'name' => 'vertikal',
		'sortable'=>true,
		'editable' => array(
			'url' => $this->createUrl('posisiCetak/directUpdate'),
			'placement' => 'top',
			'inputclass' => 'input-large',
		)
	),
	array(
		'class' => 'booster.widgets.TbEditableColumn',
		'name' => 'font_size',
		'sortable'=>true,
		'editable' => array(
			'url' => $this->createUrl('posisiCetak/directUpdate'),
			'placement' => 'top',
			'inputclass' => 'input-large',
		)
	),
),
)); ?>
