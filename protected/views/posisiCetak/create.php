<?php
$this->breadcrumbs=array(
	'Create',
);

$this->menu=array(
array('label'=>'List PosisiCetak','url'=>array('index')),
array('label'=>'Manage PosisiCetak','url'=>array('admin')),
);
?>

<h1>Create Posisi Cetak</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>