<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenis_surat')); ?>:</b>
	<?php echo CHtml::encode($data->jenis_surat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kolom')); ?>:</b>
	<?php echo CHtml::encode($data->kolom); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('horisontal')); ?>:</b>
	<?php echo CHtml::encode($data->horisontal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vertikal')); ?>:</b>
	<?php echo CHtml::encode($data->vertikal); ?>
	<br />


</div>