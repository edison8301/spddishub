<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'posisi-cetak-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php //echo $form->textFieldRow($model,'jenis_surat',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'kolom',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldGroup($model,'horisontal',array('class'=>'span5')); ?>

	<?php echo $form->textFieldGroup($model,'vertikal',array('class'=>'span5')); ?>
	
	<?php echo $form->textFieldGroup($model,'font_size',array('class'=>'span5')); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Simpan' : 'Simpan',
			'icon'=>'ok',
		)); ?>
</div>

<?php $this->endWidget(); ?>
