<?php
$this->breadcrumbs=array(
	'Data Akomodasi'=>$this->createUrl('admin'),
	'Detail Akomodasi'=>$this->createUrl('view',array('id'=>$model->id)),
);

$this->menu=array(
array('label'=>'List Akomodasi','url'=>array('index')),
array('label'=>'Create Akomodasi','url'=>array('create')),
array('label'=>'Update Akomodasi','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Akomodasi','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Akomodasi','url'=>array('admin')),
);
?>

<h1>Detail Akomodasi</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'label' => 'Update Akomodasi',
		'buttonType'=>'link',
		'icon'=>'pencil',
		'context' => 'primary',
		'url'=>array('update','id'=>$model->id),
		'htmlOptions'=>array('class'=>'big-button'),
)); ?>

<?php $this->widget('booster.widgets.TbButton',array(
		'label' => 'Data Akomodasi',
		'buttonType'=>'link',
		'icon'=>'list',
		'context' => 'primary',
		'url'=>array('admin'),
		'htmlOptions'=>array('class'=>'big-button'),
)); ?>

<div> &nbsp; </div>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type'=>'striped bordered',
'attributes'=>array(
		'jarak',
		array(
			'label'=>'Golongan',
			'value'=>$model->getRelationField('golongan','nama')
		),
		array(
			'label'=>'Biaya',
			'value'=>Bantu::rp($model->biaya)
		),
),
)); ?>
