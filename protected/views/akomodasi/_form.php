<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'akomodasi-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'jarak',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span4','maxlength'=>255)))); ?>

	<?php echo $form->dropDownListGroup($model,'id_golongan',array('widgetOptions'=>array('data'=>CHtml::listData(Golongan::model()->findAll(),'id','nama')))); ?>

	<?php echo $form->textFieldGroup($model,'biaya',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span3','maxlength'=>255)))); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
			'htmlOptions'=>array('class'=>'big-button'),
		)); ?>
</div>

<?php $this->endWidget(); ?>
