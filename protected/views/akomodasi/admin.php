<?php
$this->breadcrumbs=array(
	'Data Akomodasi'=>$this->createUrl('admin'),
);

$this->menu=array(
array('label'=>'List Akomodasi','url'=>array('index')),
array('label'=>'Create Akomodasi','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('akomodasi-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Data Akomodasi</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'label' => 'Tambah Akomodasi',
		'buttonType'=>'link',
		'icon'=>'plus',
		'context' => 'primary',
		'url'=>array('create'),
		'htmlOptions'=>array('class'=>'big-button'),
)); ?>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'akomodasi-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			array(
				'class'=>'CDataColumn',
				'name'=>'jarak',
				'header'=>'Jarak',
				'type'=>'raw',
				'value'=>'$data->jarak',
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'id_golongan',
				'header'=>'Golongan',
				'type'=>'raw',
				'value'=>'$data->golongan->nama',
				'filter'=>CHtml::listData(Golongan::model()->findAll(),'id','nama')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'biaya',
				'header'=>'Biaya',
				'type'=>'raw',
				'value'=>'Bantu::rp($data->biaya)',
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
			),
		),
)); ?>
