<?php
$this->breadcrumbs=array(
	'Data Akomodasi'=>$this->createUrl('admin'),
	'Entry Akomodasi'=>$this->createUrl('create'),
);

$this->menu=array(
array('label'=>'List Akomodasi','url'=>array('index')),
array('label'=>'Manage Akomodasi','url'=>array('admin')),
);
?>

<h1>Entry Akomodasi</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>