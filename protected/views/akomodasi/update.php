<?php
$this->breadcrumbs=array(
	'Data Akomodasi'=>$this->createUrl('admin'),
	'Update Akomodasi'=>$this->createUrl('update',array('id'=>$model->id)),
);

	$this->menu=array(
	array('label'=>'List Akomodasi','url'=>array('index')),
	array('label'=>'Create Akomodasi','url'=>array('create')),
	array('label'=>'View Akomodasi','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Akomodasi','url'=>array('admin')),
	);
	?>

<h1>Update Akomodasi</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>