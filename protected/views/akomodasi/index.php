<?php
$this->breadcrumbs=array(
	'Akomodasis',
);

$this->menu=array(
array('label'=>'Create Akomodasi','url'=>array('create')),
array('label'=>'Manage Akomodasi','url'=>array('admin')),
);
?>

<h1>Akomodasis</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
