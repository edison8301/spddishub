<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'pengikut-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>
	
	<?php print $form->hiddenField($model,'id_spd',array('value'=>isset($_GET['id_spd']) ? $_GET['id_spd'] : $model->id_spd)); ?>
	<?php print CHtml::label('Nomor SPD',''); ?>
	<?php if(isset($_GET['id_spd']) AND $_GET['id_spd']!=null) { ?>
		<?php $spd = Spd::model()->findByPk($_GET['id_spd']); ?>
		<?php print CHtml::textField('nomor_spd',$spd->nomor_spd,array('disabled'=>true,'class'=>'form-control span5','placeholder'=>'Pilih SPD','style'=>'margin-bottom:10px;')); ?>
	<?php } else { ?>
	<div class="input-group">
		<span class="input-group-btn">
			<?php $this->widget('booster.widgets.TbButton', array(
				'label'=>'[..]',
				'context'=>'primary', 
				'htmlOptions'=>array(
					'onclick'=>'$("#dialogSpd").dialog("open"); return false;',
					'style'=>'margin-bottom:10px',
				),
			)); ?>
		</span>
		<?php print CHtml::textField('nomor_spd',$model->getRelationField("spd","nomor_spd") != '' ? $model->getRelationField("spd","nomor_spd") : '',array('disabled'=>true,'class'=>'form-control span5','placeholder'=>'Pilih SPD')); ?>
	</div>
	<?php } ?>
	
	<?php echo $form->hiddenField($model,'id_pegawai'); ?>
	<?php print CHtml::label('Nama Pegawai','',array('style'=>'margin-top:10px;')); ?>
	<div class="input-group">
		<span class="input-group-btn">
			<?php $this->widget('booster.widgets.TbButton', array(
				'label'=>'[..]',
				'context'=>'primary', 
				'htmlOptions'=>array(
					'onClick'=>'$("#dialogPegawai").dialog("open"); return false;',
					'style'=>'margin-bottom:10px',
				),
			)); ?>
		</span>
		<?php print CHtml::textField('nama_pegawai',$model->id_pegawai != null ? $model->getRelationField("pegawai","nama") : '',array('disabled'=>true,'class'=>'form-control','placeholder'=>'Pilih Pegawai')); ?>
	</div>
	
	<?php echo $form->switchGroup($model,'aktif',array(
		'widgetOptions' => array(
			'events'=>array(
				'switchChange'=>'js:function(event, state) {
				  console.log(this); // DOM element
				  console.log(event); // jQuery event
				  console.log(state); // true | false
				}'
			),
			'options'=>array(
				'onText'=>'Aktif',
				'offText'=>'Nonaktif',
				'offColor'=>'danger',
				'onColor'=>'success',
			),
		)
	)); ?>
		
<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Simpan' : 'Simpan',
			'icon'=>'ok',
		)); ?>
</div>

<?php $this->endWidget(); ?>

<?php //Dialog Pegawai
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogPegawai',
    'options'=>array(
        'title'=>'Pilih Pegawai',
        'autoOpen'=>false,
		'minWidth'=>700,
		'modal'=>true,
    ),
));
	$pegawai = new Pegawai;
	$this->widget('booster.widgets.TbGridView',array(
		'id'=>'pegawai-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$pegawai->search(),
		'filter'=>$pegawai,
		'columns'=>array(
			'nip',
			'nama',
			array(
				'class'=>'CDataColumn',
				'name'=>'id_golongan',
				'header'=>'Golongan',
				'type'=>'raw',
				'value'=>'$data->golongan->nama',
				'filter'=>CHtml::listData(Golongan::model()->findAll(),'id','nama')
			),
			'jabatan',
			array(
				'class'=>'CDataColumn',
				'name'=>'tgl_lahir',
				'header'=>'Tanggal Lahir',
				'type'=>'raw',
				'value'=>'Bantu::tgl($data->tgl_lahir)',
				//'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tgl_lahir)',
				
			),
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "$(\"#dialogPegawai\").dialog(\"close\");
							    $(\"#Pengikut_id_pegawai\").val(\"$data->id\");
								$(\"#nama_pegawai\").val(\"$data->nama\");",
					"class"=>"btn btn-primary btn-sm"
				))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
					
				),
			),
		),
));
	
$this->endWidget('zii.widgets.jui.CJuiDialog'); //=== end CJuiDialog for mark insurer
?>

<?php //Dialog SPD
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogSpd',
    'options'=>array(
        'title'=>'Pilih SPD',
        'autoOpen'=>false,
		'minWidth'=>700,
		'modal'=>true,
    ),
));
	$spd = new Spd;
	$this->widget('booster.widgets.TbGridView',array(
		'id'=>'spd-grid',
		'type'=> 'striped bordered',
		'dataProvider'=>$spd->dataSpd(),
		'filter'=>$spd,
		'columns'=>array(
			array(
				'class'=>'CDataColumn',
				'type'=>'raw',
				'header'=>'Cetak',
				'value'=>'CHtml::link("<center><i class=\"glyphicon glyphicon-print\"></i></center>",array("spd/viewSpd","id"=>"$data->id"))',
			),
			'nomor_spd',
			array(
				'class'=>'CDataColumn',
				'name'=>'tanggal_spd',
				'header'=>'Tanggal SPD',
				'type'=>'raw',
				'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tanggal_spd)',
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'id_pejabat',
				'header'=>'Penandatangan',
				'type'=>'raw',
				'value'=>'$data->getRelationField("pejabat","nama")',
				'filter'=>CHtml::listData(Pejabat::model()->findAll(),'id','nama')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'id_pegawai',
				'header'=>'Pegawai',
				'type'=>'raw',
				'value'=>'$data->getRelationField("pegawai","nama")',
				'filter'=>CHtml::listData(Pegawai::model()->findAll(),'id','nama')
			),
			'maksud',
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "$(\"#dialogSpd\").dialog(\"close\");
								$(\"#Pengikut_lama\").val(\"$data->lama\");
								$(\"#Pengikut_id_spd\").val(\"$data->id\");
							    $(\"#nomor_spd\").val(\"$data->nomor_spd\");
								$(\"#Lhp_id_spd\").val(\"$data->id\");",
					"class" => "btn btn-primary btn-sm",
				))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
				),
			),
		),
));
$this->endWidget('zii.widgets.jui.CJuiDialog'); //=== end CJuiDialog for mark insurer
?>