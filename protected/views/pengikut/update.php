<?php
$this->breadcrumbs=array(
	'Pengikuts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Pengikut','url'=>array('index')),
	array('label'=>'Create Pengikut','url'=>array('create')),
	array('label'=>'View Pengikut','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Pengikut','url'=>array('admin')),
	);
	?>

	<h1>Sunting Pengikut</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>