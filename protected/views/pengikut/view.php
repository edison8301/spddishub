<?php
$this->breadcrumbs=array(
	'Pengikuts'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Pengikut','url'=>array('index')),
array('label'=>'Create Pengikut','url'=>array('create')),
array('label'=>'Update Pengikut','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Pengikut','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Pengikut','url'=>array('admin')),
);
?>

<h1>Detail Pengikut</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'label' => 'Sunting Pengikut',
		'buttonType'=>'link',
		'icon'=>'pencil',
		'context' => 'primary',
		'url'=>array('pengikut/update','id'=>$model->id),
)); ?>

<?php $this->widget('booster.widgets.TbButton',array(
		'label' => 'Data Pengikut',
		'buttonType'=>'link',
		'icon'=>'list',
		'context' => 'primary',
		'url'=>array('pengikut/admin'),
)); ?>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type'=>'striped bordered',
'attributes'=>array(
		array(
			'label'=>'No. SPD',
			'value'=>$model->getRelationField('spd','nomor_spd')
		),
		array(
			'label'=>'Nama Pegawai',
			'value'=>$model->getRelationField('pegawai','nama')
		),
		array(
			'label'=>'Status',
			'value'=>$model->aktif == 0 ? 'Nonaktif' : 'Aktif',
		),
),
)); ?>
