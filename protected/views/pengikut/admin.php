<h1>Data Pengikut</h1>

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'pengikut-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'type'=>'striped bordered',
'columns'=>array(
		array(
			'class'=>'CDataColumn',
			'name'=>'id_spd',
			'header'=>'No. SPD',
			'type'=>'raw',
			'value'=>'$data->getRelationField("spd","nomor_spd")',
			'filter'=>CHtml::listData(Spd::model()->findAll(),'id','nomor')
		),
		array(
			'class'=>'CDataColumn',
			'name'=>'id_pegawai',
			'header'=>'Nama Pegawai',
			'type'=>'raw',
			'value'=>'$data->getRelationField("pegawai","nama")',
			'filter'=>CHtml::listData(Pegawai::model()->findAll(),'id','nama')
		),
		array(
			'class'=>'CDataColumn',
			'name'=>'aktif',
			'header'=>'Status',
			'type'=>'raw',
			'value'=>'$data->aktif == 1 ? "Aktif" : "Nonaktif"',
			'filter'=>array('1'=>'Aktif','0'=>'Nonaktif')
		),
array(
'class'=>'booster.widgets.TbButtonColumn',
),
),
)); ?>
