<?php
$this->breadcrumbs=array(
	'Pengikuts'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Pengikut','url'=>array('index')),
array('label'=>'Manage Pengikut','url'=>array('admin')),
);
?>

<h1>Tambah Pengikut</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>