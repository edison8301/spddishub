<?php
$this->breadcrumbs=array(
	'Data SPD'=>$this->createUrl('admin'),
	'Detail SPD'=>$this->createUrl('spd/view',array('id'=>$model->id_spd)),
	'Perjalanan'=>$this->createUrl('spdBelakang/view',array('id'=>$model->id))
);

?>

<h1>Detail SPD Belakang</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'label' => 'Detail SPD',
		'buttonType'=>'link',
		'icon'=>'arrow-left',
		'context' => 'primary',
		'url'=>array('spd/view','id'=>$model->id_spd),
		'htmlOptions'=>array('class'=>'big-button'),
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'label' => 'Sunting Perjalanan',
		'buttonType'=>'link',
		'icon'=>'pencil',
		'context' => 'primary',
		'url'=>array('spdBelakang/update','id'=>$model->id),
		'htmlOptions'=>array('class'=>'big-button'),
)); ?>&nbsp;


<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'id_spd',
		'urutan',
		'tempat_berangkat',
		'tanggal_berangkat',
		'nama_kepala_berangkat',
		'nip_kepala_berangkat',
		'jabatan_kepala_berangkat',
		'tempat_tiba',
		'tanggal_tiba',
		'nama_kepala_tiba',
		'nip_kepala_tiba',
		'jabatan_kepala_tiba',
),
)); ?>
