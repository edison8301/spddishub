<?php
$this->breadcrumbs=array(
	'SPD'=>$this->createUrl('spd/view',array('id'=>$model->id_spd)),
	'Sunting Perjalanan'=>$this->createUrl('spdBelakang/update',array('id'=>$model->id)),
);

	$this->menu=array(
	array('label'=>'List SpdBelakang','url'=>array('index')),
	array('label'=>'Create SpdBelakang','url'=>array('create')),
	array('label'=>'View SpdBelakang','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage SpdBelakang','url'=>array('admin')),
	);
	?>

	<h1>Sunting Perjalanan</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>