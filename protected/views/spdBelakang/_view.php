<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_spd')); ?>:</b>
	<?php echo CHtml::encode($data->id_spd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('urutan')); ?>:</b>
	<?php echo CHtml::encode($data->urutan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tempat_berangkat')); ?>:</b>
	<?php echo CHtml::encode($data->tempat_berangkat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_berangkat')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_berangkat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_kepala_berangkat')); ?>:</b>
	<?php echo CHtml::encode($data->nama_kepala_berangkat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nip_kepala_berangkat')); ?>:</b>
	<?php echo CHtml::encode($data->nip_kepala_berangkat); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('jabatan_kepala_berangkat')); ?>:</b>
	<?php echo CHtml::encode($data->jabatan_kepala_berangkat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tempat_tiba')); ?>:</b>
	<?php echo CHtml::encode($data->tempat_tiba); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_tiba')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_tiba); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_kepala_tiba')); ?>:</b>
	<?php echo CHtml::encode($data->nama_kepala_tiba); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nip_kepala_tiba')); ?>:</b>
	<?php echo CHtml::encode($data->nip_kepala_tiba); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jabatan_kepala_tiba')); ?>:</b>
	<?php echo CHtml::encode($data->jabatan_kepala_tiba); ?>
	<br />

	*/ ?>

</div>