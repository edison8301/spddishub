<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'spd-belakang-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<div class="row">
	<div class="col-xs-12">
		<?php echo $form->textFieldGroup($model,'urutan',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>
	</div>
</div>
<div class="row">
	<div class="col-xs-6">
	<?php echo $form->textFieldGroup($model,'tempat_tiba',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->datePickerGroup($model,'tanggal_tiba',array('widgetOptions'=>array('options'=>array('language' => 'id','format'=>'yyyy-mm-dd','autoclose'=>true),'htmlOptions'=>array('class'=>'span5')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>')); ?>
	
	<?php echo $form->textFieldGroup($model,'jabatan_kepala_tiba',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>
	
	<?php echo $form->textFieldGroup($model,'nama_kepala_tiba',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->textFieldGroup($model,'nip_kepala_tiba',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	
	</div>
	
	<div class="col-xs-6">
	<?php echo $form->textFieldGroup($model,'tempat_berangkat',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>
	
	<?php echo $form->textFieldGroup($model,'ke',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->datePickerGroup($model,'tanggal_berangkat',array('widgetOptions'=>array('options'=>array('language' => 'id','format'=>'yyyy-mm-dd','autoclose'=>true),'htmlOptions'=>array('class'=>'span5')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>')); ?>
	
	<?php echo $form->textFieldGroup($model,'jabatan_kepala_berangkat',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>
	
	<?php echo $form->textFieldGroup($model,'nama_kepala_berangkat',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->textFieldGroup($model,'nip_kepala_berangkat',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	
	</div>
</div>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>$model->isNewRecord ? 'Simpan' : 'Simpan',
			'htmlOptions'=>array('class'=>'big-button')
		)); ?>
</div>

<?php $this->endWidget(); ?>
