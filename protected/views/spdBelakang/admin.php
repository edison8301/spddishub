<h1>Manage Spd Belakangs</h1>

<p>
	You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
		&lt;&gt;</b>
	or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'spd-belakang-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
		'id_spd',
		'urutan',
		'tempat_berangkat',
		'tanggal_berangkat',
		'nama_kepala_berangkat',
		/*
		'nip_kepala_berangkat',
		'jabatan_kepala_berangkat',
		'tempat_tiba',
		'tanggal_tiba',
		'nama_kepala_tiba',
		'nip_kepala_tiba',
		'jabatan_kepala_tiba',
		*/
array(
'class'=>'booster.widgets.TbButtonColumn',
),
),
)); ?>
