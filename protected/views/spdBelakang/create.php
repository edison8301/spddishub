<?php
$this->breadcrumbs=array(
	'Spd'=>$this->createUrl('spd/view',array('id'=>$_GET['id_spd'])),
	'Tambah Perjalanan'=>$this->createUrl('spdBelakang/create',array('id'=>$_GET['id_spd'])),
);

$this->menu=array(
array('label'=>'List SpdBelakang','url'=>array('index')),
array('label'=>'Manage SpdBelakang','url'=>array('admin')),
);
?>

<h1>Tambah Perjalanan</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>