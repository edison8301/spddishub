<?php
$this->breadcrumbs=array(
	'Rills'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Rill','url'=>array('index')),
array('label'=>'Manage Rill','url'=>array('admin')),
);
?>

<h1>Entry Pernyataan Riil</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>