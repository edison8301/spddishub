<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'rill-form',
	'enableAjaxValidation'=>false,
)); ?>

<script type="text/javascript">
	$(document).ready(function(){
		$(".format-money").on("keyup", function(){
			var _this = $(this);
			var value = _this.val().replace(/\.| /g,"");
			_this.val(accounting.formatMoney(value, "", 0, ".", ","))
		});
		$("input").keyup(function(){
			var val1 = +$('#Riil_bbm').val().replace('.',"");
			var val2 = +$('#Riil_tol').val().replace('.',"");
			$("#Riil_jumlah").val(val1+val2);
		});
	});
</script>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php print $form->hiddenField($model,'id_spd'); ?>
	<?php print CHtml::label('Nomor SPD',''); ?>
	<div class="input-group">
		<span class="input-group-btn">
			<?php $this->widget('booster.widgets.TbButton', array(
				'label'=>'[..]',
				'context'=>'primary', 
				'htmlOptions'=>array(
					'onclick'=>'$("#dialogSpd").dialog("open"); return false;',
					'style'=>'margin-bottom:10px',
				),
			)); ?>
		</span>
		<?php print CHtml::textField('nomor_spd',$model->getRelationField("spd","nomor_spd") != '' ? $model->getRelationField("spd","nomor_spd") : '',array('disabled'=>true,'class'=>'span2 form-control','placeholder'=>'Pilih SPD')); ?>
	</div>
	
	<?php echo $form->textFieldGroup($model,'bbm',array('prepend'=>'Rp','widgetOptions'=>array('htmlOptions'=>array('class'=>'span10 format-money','maxlength'=>255)))); ?>

	<?php echo $form->textFieldGroup($model,'tol',array('prepend'=>'Rp','widgetOptions'=>array('htmlOptions'=>array('class'=>'span10 format-money','maxlength'=>255)))); ?>

	<?php echo $form->textFieldGroup($model,'jumlah',array('prepend'=>'Rp','widgetOptions'=>array('htmlOptions'=>array('class'=>'span10 format-money','maxlength'=>255)))); ?>

	<?php echo $form->datepickerGroup($model, 'tanggal',array('widgetOptions' => array('options'=>array('autoclose'=>true,'format'=>'yy-mm-dd')), 'prepend' => '<i class="glyphicon glyphicon-calendar"></i>')); ?>
	
	<?php print $form->hiddenField($model,'ppk'); ?>
	<?php print CHtml::label('Pilih PPK',''); ?>
	<div class="input-group">
		<span class="input-group-btn">
			<?php $this->widget('booster.widgets.TbButton', array(
				'label'=>'[..]',
				'context'=>'primary', 
				'htmlOptions'=>array(
					'onclick'=>'$("#dialogPpk").dialog("open"); return false;',
					'style'=>'margin-bottom:10px',
				),
			)); ?>
		</span>
		<?php print CHtml::textField('nama_ppk',$model->getRelationField("ppkRelation","nama") != '' ? $model->getRelationField("ppkRelation","nama") : '',array('disabled'=>true,'class'=>'span2 form-control','placeholder'=>'Pilih Pegawai')); ?>
	</div>
	
	<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

<?php //Dialog SPD
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogSpd',
    'options'=>array(
        'title'=>'Pilih SPD',
        'autoOpen'=>false,
		'minWidth'=>700,
		'modal'=>true,
    ),
));
	$spd = new Spd;
	$this->widget('booster.widgets.TbGridView',array(
		'id'=>'spd-grid',
		'type'=> 'striped bordered',
		'dataProvider'=>$spd->dataSpd(),
		'filter'=>$spd,
		'columns'=>array(
			array(
				'class'=>'CDataColumn',
				'type'=>'raw',
				'header'=>'Cetak',
				'value'=>'CHtml::link("<center><i class=\"glyphicon glyphicon-print\" ></i></center>",array("spd/viewSpd","id"=>"$data->id"))',
			),
			'nomor_spd',
			array(
				'class'=>'CDataColumn',
				'name'=>'tanggal_spd',
				'header'=>'Tanggal SPD',
				'type'=>'raw',
				'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tanggal_spd)',
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'id_pejabat',
				'header'=>'Penandatangan',
				'type'=>'raw',
				'value'=>'$data->getRelationField("pejabat","nama")',
				'filter'=>CHtml::listData(Pejabat::model()->findAll(),'id','nama')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'id_pegawai',
				'header'=>'Pegawai',
				'type'=>'raw',
				'value'=>'$data->getRelationField("pegawai","nama")',
				'filter'=>CHtml::listData(Pegawai::model()->findAll(),'id','nama')
			),
			'maksud',
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "$(\"#dialogSpd\").dialog(\"close\");
								$(\"#Riil_id_spd\").val(\"$data->id\");
							    $(\"#nomor_spd\").val(\"$data->nomor_spd\");
								
				"))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
				),
			),
		),
));

	
$this->endWidget('zii.widgets.jui.CJuiDialog'); //=== end CJuiDialog for mark insurer
?>

<?php //Dialog PPK
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogPpk',
    'options'=>array(
        'title'=>'Pilih PPK',
        'autoOpen'=>false,
		'minWidth'=>700,
		'modal'=>true,
    ),
));
	$ppk = new Pejabat;
	$this->widget('booster.widgets.TbGridView',array(
		'id'=>'ppk-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$ppk->search(),
		'filter'=>$ppk,
		'columns'=>array(
			'nip',
			'nama',
			'jabatan',
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "$(\"#dialogPpk\").dialog(\"close\");
							    $(\"#Riil_ppk\").val(\"$data->id\");
								$(\"#nama_ppk\").val(\"$data->nama\");",
					"class"=>"btn-primary"
				))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
					
				),
			),
		),
));
	
$this->endWidget('zii.widgets.jui.CJuiDialog'); //=== end CJuiDialog for mark insurer
?>