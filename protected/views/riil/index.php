<?php
$this->breadcrumbs=array(
	'Rills',
);

$this->menu=array(
array('label'=>'Create Rill','url'=>array('create')),
array('label'=>'Manage Rill','url'=>array('admin')),
);
?>

<h1>Rills</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
