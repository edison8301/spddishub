<?php
$this->breadcrumbs=array(
	'Rills'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Rill','url'=>array('index')),
array('label'=>'Create Rill','url'=>array('create')),
array('label'=>'Update Rill','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Rill','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Rill','url'=>array('admin')),
);
?>

<h1>Detail Riil</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Sunting Riil',
		'icon'=>'pencil',
		'context'=>'primary',
		'url'=>array('update','id'=>$model->id)
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Data Riil',
		'icon'=>'list',
		'context'=>'primary',
		'url'=>array('admin')
)); ?>&nbsp;

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type'=>'striped bordered',
'attributes'=>array(
		array(
			'label'=>'No. SPD',
			'value'=>$model->getRelationField('spd','nomor_spd')
		),
		'bbm',
		'tol',
		'jumlah',
		array(
			'label'=>'Tanggal',
			'value'=>Bantu::tanggal($model->tanggal)
		),
		'ppk',
),
)); ?>
