<?php
$this->breadcrumbs=array(
	'Rills'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Rill','url'=>array('index')),
array('label'=>'Create Rill','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('rill-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Data Pernyataan Riil</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah Pernyataan Riil',
		'icon'=>'plus',
		'context'=>'primary',
		'url'=>array('input')
)); ?>&nbsp;

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'rill-grid',
'type'=>'striped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		array(
			'class'=>'CDataColumn',
			'name'=>'id_spd',
			'header'=>'No. SPD',
			'type'=>'raw',
			'value'=>'$data->getRelationField("spd","nomor_spd")',
			'filter'=>CHtml::listData(Spd::model()->findAll(),'id','nomor_spd')
		),
		'bbm',
		'tol',
		'jumlah',
		array(
			'class'=>'CDataColumn',
			'name'=>'tanggal',
			'header'=>'Tanggal',
			'type'=>'raw',
			'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tanggal)',
		),
		/*
		'ppk',
		*/
array(
'class'=>'booster.widgets.TbButtonColumn',
),
),
)); ?>