<?php
$this->breadcrumbs=array(
	'Rills'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Rill','url'=>array('index')),
	array('label'=>'Create Rill','url'=>array('create')),
	array('label'=>'View Rill','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Rill','url'=>array('admin')),
	);
	?>

<h1>Input Riil</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>