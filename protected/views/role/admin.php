<?php
$this->breadcrumbs=array(
	'Data Role'=>$this->createUrl('admin'),
);

$this->menu=array(
array('label'=>'List Role','url'=>array('index')),
array('label'=>'Create Role','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('role-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Data User Role</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah User Role',
		'icon'=>'plus',
		'context'=>'primary',
		'url'=>array('create'),
		'htmlOptions'=>array('class'=>'big-button'),
)); ?>&nbsp;

<?php $this->renderPartial('_admin',array('role'=>$role)); ?>


