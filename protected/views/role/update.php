<?php
$this->breadcrumbs=array(
	'Data Role'=>$this->createUrl('admin'),
	'Sunting Role'=>$this->createUrl('update',array('id'=>$model->id)),
);

	$this->menu=array(
	array('label'=>'List Role','url'=>array('index')),
	array('label'=>'Create Role','url'=>array('create')),
	array('label'=>'View Role','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Role','url'=>array('admin')),
	);
	?>

<h1>Update Data Role</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>