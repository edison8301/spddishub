<?php
$this->breadcrumbs=array(
	'Data Role'=>$this->createUrl('admin'),
	'Entry Role'=>$this->createUrl('create'),
);

$this->menu=array(
array('label'=>'List Role','url'=>array('index')),
array('label'=>'Manage Role','url'=>array('admin')),
);
?>

<h1>Entry Role</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>


