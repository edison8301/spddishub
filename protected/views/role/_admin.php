<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'role-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$role->search(),
		'filter'=>$role,
		'columns'=>array(
			array(
				'header'=>'Akses',
				'type'=>'raw',
				'value'=>'CHtml::link("<i class=\"glyphicon glyphicon-lock\"></i>",array("role/access","id"=>$data->id))',
				'htmlOptions' => array('style' => 'text-align:center;width:5%;'),
			),
			'nama',
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'template'=>'{update} {delete}',
			),
		),
)); ?>