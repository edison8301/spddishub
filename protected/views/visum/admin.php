<?php
$this->breadcrumbs=array(
	'Visums'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Visum','url'=>array('index')),
array('label'=>'Create Visum','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('visum-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Manage Visums</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'visum-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		
		'nomor',
		'nip',
		'nama',
		'jabatan',
		'instansi',
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
'template'=>'{view} {delete}'
),
),
)); ?>
