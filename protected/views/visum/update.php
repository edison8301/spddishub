<?php
$this->breadcrumbs=array(
	'Visums'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Visum','url'=>array('index')),
	array('label'=>'Create Visum','url'=>array('create')),
	array('label'=>'View Visum','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Visum','url'=>array('admin')),
	);
	?>

<h1>Visum</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'visum-grid',
'type'=> 'striped bordered',
'dataProvider'=>$visum->search(),
'filter'=>$visum,
'columns'=>array(
		
		'nomor',
		'nip',
		'nama',
		'jabatan',
		'instansi',
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>