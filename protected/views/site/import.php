<?php 
$this->breadcrumbs=array(
	'Backup Database'=>$this->createUrl('site/backup'),
	'Restore Database'=>$this->createUrl('site/import'),
);
?>

<h1>Restore Database</h1>

<div>&nbsp;</div>

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
		'id'=>'import-form',
		'htmlOptions'=>array('enctype'=>'multipart/form-data'),
		//'action'=>'site/importProcess',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>false,
		),
)); ?>
	
	<?php echo $form->fileFieldGroup($model, 'file',array('wrapperHtmlOptions' => array('class' => 'col-sm-5',),)); ?>

	<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>'Restore Database',
			'htmlOptions'=>array('class'=>'big-button'),
	)); ?>
	</div>
	
<?php $this->endWidget(); ?>