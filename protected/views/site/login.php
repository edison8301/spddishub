<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
		'id'=>'login-form',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
)); ?>
			
	<p class="inline-small-label">
		<label for="login"><span class="big">Username</span></label>
		<?php echo $form->textField($model,'username',array('class'=>'full-width')); ?>
		<?php //echo $form->error($model,'username'); ?>
	</p>

	<p class="inline-small-label">
		<label for="login"><span class="big">Password</span></label>
		<?php echo $form->passwordField($model,'password',array('class'=>'full-width')); ?>
		<?php //echo $form->error($model,'password'); ?>
	</p>
		
	<div class="rememberMe">
		<?php echo $form->checkBox($model,'rememberMe',array('class'=>'mini-switch')); ?>
		<?php echo $form->label($model,'rememberMe',array('for'=>'keep-logged','class'=>'inline')); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>

	<div class="buttons">
		<button type="submit" class="float-right">Login</button>
	</div>

<?php $this->endWidget(); ?>

<div>&nbsp;</div>
<div>&nbsp;</div>