<?php
$this->breadcrumbs=array(
	'Refresentatifs',
);

$this->menu=array(
array('label'=>'Create Refresentatif','url'=>array('create')),
array('label'=>'Manage Refresentatif','url'=>array('admin')),
);
?>

<h1>Refresentatifs</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
