<?php
$this->breadcrumbs=array(
	'Refresentatifs'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Refresentatif','url'=>array('index')),
array('label'=>'Create Refresentatif','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('refresentatif-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Kelola Refresentatif</h1>

<?php
            $this->widget('bootstrap.widgets.TbButtonGroup', array(
            //'size'=>'small',
            'type'=>'primary', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'buttons'=>array(
            array('label'=>'Menu', 'items'=>array(
					array('label'=>'Tambah Refresentatif', 'icon'=>'plus', 'url'=>array('refresentatif/create')),
                )),
            ),
            ));
?>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'refresentatif-grid',
'type'=>'striped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'jarak',
		'biaya',
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
