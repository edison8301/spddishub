<?php
$this->breadcrumbs=array(
	'Role Accesses'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List RoleAccess','url'=>array('index')),
array('label'=>'Manage RoleAccess','url'=>array('admin')),
);
?>

<h1>Create RoleAccess</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>