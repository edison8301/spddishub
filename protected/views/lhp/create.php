<?php
$this->breadcrumbs=array(
	'Data LHP'=>$this->createUrl('admin'),
	'Input LHP'=>$this->createUrl('create'),
);

$this->menu=array(
array('label'=>'List Lhp','url'=>array('index')),
array('label'=>'Manage Lhp','url'=>array('admin')),
);
?>

<h1>Laporan Hasil Perjalanan Dinas</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
