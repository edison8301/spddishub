<?php 
$this->breadcrumbs=array(
	'Data LHP'=>$this->createUrl('admin'),
	'Detail LHP'=>$this->createUrl('view',array('id'=>$model->id)),
);
?>
<h1>Detail LHPD</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'label' => 'Lihat SPD',
		'buttonType'=>'link',
		'icon'=>'arrow-left',
		'context' => 'primary',
		'url'=>array('spd/view','id'=>$model->id_spd),
		'htmlOptions'=>array('class'=>'big-button'),
)); ?>

<?php $this->widget('booster.widgets.TbButton',array(
		'label' => 'Sunting LHP',
		'buttonType'=>'link',
		'icon'=>'pencil white',
		'context' => 'primary',
		'url'=>array('lhp/update','id'=>$model->id),
		'htmlOptions'=>array('class'=>'big-button'),
)); ?>
<?php $this->widget('booster.widgets.TbButtonGroup',array(
        'context' => 'primary',
        'buttons' => array(
            array(
                'label' => 'Cetak LHPD',
				'icon' => 'print',
				'htmlOptions'=>array('class'=>'big-button'),
                'items' => array(
					array('label' => 'Cetak Blanko', 'url' => array('lhp/cetakBlanko','id'=>$model->id,'id_pengikut'=>$model->id_pegawai),'linkOptions'=>array('target'=>'_blank')),
					array('label' => 'Cetak Lengkap', 'url' => array('lhp/cetakPdf','id'=>$model->id,'id_pengikut'=>$model->id_pegawai),'linkOptions'=>array('target'=>'_blank')),
					'---',
                    array('label' => 'Setting Posisi LHPD', 'url' => array('posisiCetak/setting','jenis_surat'=>'3')),
				)
            ),
        ),
)); ?>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			array(
				'label'=>'Nama Pegawai',
				'value'=>$model->getRelationField('pegawai','nama')
			),
			array(
				'label'=>'No. SPD',
				'value'=>$model->getRelationField('spd','nomor_spd')
			),
			array(
				'label'=>'Tanggal LHP',
				'type'=>'raw',
				'value'=>date('d-m-Y',strtotime($model->tanggal_lhp))
			),
			'hasil_kegiatan_1',
			'hasil_kegiatan_2',
			'hasil_kegiatan_3',
		),
)); ?>