<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'lhp-grid',
		'type'=> 'striped bordered',
		'dataProvider'=>$lhp->search(),
		'filter'=>$lhp,
		'columns'=>array(
			array(
				'class'=>'CDataColumn',
				'name'=>'id_pegawai',
				'header'=>'Nama Pegawai',
				//'type'=>'raw',
				'value'=>'$data->getRelationField("pegawai","nama")',
				//'filter'=>CHtml::listData(Pegawai::model()->findAll(),'id','nama')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'id_spd',
				'header'=>'No. SPD',
				//'type'=>'raw',
				'value'=>'$data->getRelationField("spd","nomor_spd")',
				//'filter'=>CHtml::listData(Spd::model()->findAll(),'id','nomor_spd')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'tanggal_lhp',
				'header'=>'Tanggal LHP',
				'type'=>'raw',
				'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tanggal_lhp)',
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
			),
		),
)); ?>