<?php
$this->breadcrumbs=array(
	'Data LHP'=>$this->createUrl('admin'),
	'Sunting LHP'=>$this->createUrl('update',array('id'=>$model->id)),
);

	$this->menu=array(
	array('label'=>'List Lhp','url'=>array('index')),
	array('label'=>'Create Lhp','url'=>array('create')),
	array('label'=>'View Lhp','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Lhp','url'=>array('admin')),
	);
	?>
<h1>Laporan Hasil Perjalanan Dinas</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>