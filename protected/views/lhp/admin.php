<?php
$this->breadcrumbs=array(
	'Data LHP'=>$this->createUrl('admin'),
);

$this->menu=array(
array('label'=>'List Lhp','url'=>array('index')),
array('label'=>'Manage Lhp','url'=>array('admin')),
);
?>

<h1>Data Laporan Hasil Perjalanan Dinas</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah LHP',
		'icon'=>'plus',
		'context'=>'primary',
		'url'=>array('create'),
		'htmlOptions'=>array('class'=>'big-button'),
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Setting Posisi Cetak LHP',
		'icon'=>'wrench',
		'context'=>'primary',
		'url'=>array('posisiCetak/setting','jenis_surat'=>'3'),
		'htmlOptions'=>array('class'=>'big-button'),
)); ?>&nbsp;

<?php echo $this->renderPartial('_admin', array('lhp'=>$lhp)); ?>



