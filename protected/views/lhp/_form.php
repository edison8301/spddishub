<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'lhp-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>
	
	
	<?php print $form->hiddenField($model,'id_spd',array('value'=>isset($_GET['id_spd']) ? $_GET['id_spd'] : $model->id_spd)); ?>
	<?php print CHtml::label('Nomor SPD',''); ?>
	<?php if(isset($_GET['id_spd']) AND $_GET['id_spd']!=null) { ?>
		<?php $spd = Spd::model()->findByPk($_GET['id_spd']); ?>
		<?php print CHtml::textField('nomor_spd',$spd->nomor_spd,array('disabled'=>true,'class'=>'form-control span5','placeholder'=>'Pilih SPD','style'=>'margin-bottom:10px;')); ?>
	<?php } else { ?>
	<div class="input-group">
		<span class="input-group-btn">
			<?php $this->widget('booster.widgets.TbButton', array(
				'label'=>'[..]',
				'context'=>'primary', 
				'htmlOptions'=>array(
					'onclick'=>'dialogSpd(); return false;',
					'style'=>'margin-bottom:10px',
				),
			)); ?>
		</span>
		<?php print CHtml::textField('nomor_spd',$model->getRelationField("spd","nomor_spd") != '' ? $model->getRelationField("spd","nomor_spd") : '',array('disabled'=>true,'class'=>'form-control span5','placeholder'=>'Pilih SPD')); ?>
	</div>
	<?php } ?>

	<?php print $form->hiddenField($model,'id_pegawai',array('value'=>isset($_GET['id_pegawai']) ? $_GET['id_pegawai'] : $model->id_pegawai)); ?>
	<?php print CHtml::label('Nama Pegawai',''); ?>
	<?php if(isset($_GET['id_pegawai']) AND $_GET['id_pegawai']!=null) { ?>
		<?php $pegawai = Pegawai::model()->findByPk($_GET['id_pegawai']); ?>
		<?php print CHtml::textField('nama_pegawai',$pegawai->nama,array('disabled'=>true,'class'=>'form-control span5','placeholder'=>'Pilih Pegawai','style'=>'margin-bottom:10px;')); ?>
	<?php } else { ?>
	<div class="input-group">
		<span class="input-group-btn">
			<?php $this->widget('booster.widgets.TbButton', array(
				'label'=>'[..]',
				'context'=>'primary', 
				'htmlOptions'=>array(
					'onclick'=>'dialogPegawai(); return false;',
					'style'=>'margin-bottom:10px',
				),
			)); ?>
		</span>
		<?php print CHtml::textField('nama_pegawai',$model->getRelationField("pegawai","nama") != '' ? $model->getRelationField("pegawai","nama") : '',array('disabled'=>true,'class'=>'form-control span5','placeholder'=>'Pilih Pegawai')); ?>
	</div>
	<?php } ?>
	
	<?php echo $form->datepickerGroup($model, 'tanggal_lhp',array(
			'widgetOptions' => array(
				'options'=>array(
					'autoclose'=>true,
					'format'=>'yyyy-mm-dd'
				)
			), 
			'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
	)); ?>
	
	<?php echo $form->textareaGroup($model,'hasil_kegiatan_1',array('wrapperHtmlOptions'=>array('class'=>'span8',),'widgetOptions'=>array('htmlOptions'=>array('maxlength'=>255,'rows'=>3,)))); ?>
		
	<?php echo $form->textareaGroup($model,'hasil_kegiatan_2',array('wrapperHtmlOptions'=>array('class'=>'span8',),'widgetOptions'=>array('htmlOptions'=>array('maxlength'=>255,'rows'=>3,)))); ?>
	
	<?php echo $form->textareaGroup($model,'hasil_kegiatan_3',array('wrapperHtmlOptions'=>array('class'=>'span8',),'widgetOptions'=>array('htmlOptions'=>array('maxlength'=>255,'rows'=>3,)))); ?>
			
<div class="form-actions well">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
			'htmlOptions'=>array('class'=>'big-button'),
	)); ?>
</div>

<?php $this->endWidget(); ?>

<div class="hidden">
<div id="wrapper-tambah-spd">
<?php //Dialog SPD
	$spd = new Spd;
	$this->widget('booster.widgets.TbGridView',array(
		'id'=>'spd-grid',
		'type'=> 'striped bordered',
		'afterAjaxUpdate'=>'function(id,data) { isi = $("#wrapper-tambah-spd").html(); $(".modal-content").html(isi);  }',
		'dataProvider'=>$spd->dataSpd(),
		'filter'=>$spd,
		'columns'=>array(
			array(
				'class'=>'CDataColumn',
				'type'=>'raw',
				'header'=>'Cetak',
				'value'=>'CHtml::link("<center><i class=\"glyphicon glyphicon-print\"></i></center>",array("spd/view","id"=>"$data->id"))',
			),
			'nomor_spd',
			array(
				'class'=>'CDataColumn',
				'name'=>'tanggal_spd',
				'header'=>'Tanggal SPD',
				'type'=>'raw',
				'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tanggal_spd)',
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'id_pejabat',
				'header'=>'Penandatangan',
				'type'=>'raw',
				'value'=>'$data->getRelationField("pejabat","nama")',
				'filter'=>CHtml::listData(Pejabat::model()->findAll(),'id','nama')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'id_pegawai',
				'header'=>'Pegawai',
				'type'=>'raw',
				'value'=>'$data->getRelationField("pegawai","nama")',
				'filter'=>CHtml::listData(Pegawai::model()->findAll(),'id','nama')
			),
			'maksud',
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "$(\"#dialogSpd\").dialog(\"close\");
								$(\"#Kwitansi_lama\").val(\"$data->lama\");
								$(\"#Kwitansi_id_spd\").val(\"$data->id\");
							    $(\"#nomor_spd\").val(\"$data->nomor_spd\");
								$(\"#Lhp_id_spd\").val(\"$data->id\");",
					"class" => "big-button",
				))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
				),
			),
		),
));
?>
</div>
</div>

<div class="hidden">
<div id="wrapper-tambah-pegawai">
<?php //Dialog Pegawai
	$pegawai = new Pegawai;
	$this->widget('booster.widgets.TbGridView',array(
		'id'=>'pegawai-grid',
		'type'=>'striped bordered',
		'afterAjaxUpdate'=>'function(id,data) { isi = $("#wrapper-tambah-pegawai").html(); $(".modal-content").html(isi);  }',
		'dataProvider'=>$pegawai->search(),
		'filter'=>$pegawai,
		'columns'=>array(
			'nip',
			'nama',
			array(
				'class'=>'CDataColumn',
				'name'=>'id_golongan',
				'header'=>'Golongan',
				'type'=>'raw',
				'value'=>'$data->golongan->nama',
				'filter'=>CHtml::listData(Golongan::model()->findAll(),'id','nama')
			),
			'jabatan',
			array(
				'class'=>'CDataColumn',
				'name'=>'tgl_lahir',
				'header'=>'Tanggal Lahir',
				'type'=>'raw',
				'value'=>'Bantu::tgl($data->tgl_lahir)',
				//'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tgl_lahir)',
				
			),
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "$(\"#dialogPegawai\").dialog(\"close\");
							    $(\"#Lhp_id_pegawai\").val(\"$data->id\");
								$(\"#nama_pegawai\").val(\"$data->nama\");",
					"class"=>"big-button"
				))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
					
				),
			),
		),
));
?>
</div>
</div>

<script>
// Modal Tambah SPD
function dialogSpd()
{
	$.modal({
		content: $("#wrapper-tambah-spd"),
		title: 'Pilih SPD',
		width: 700,
		buttons: {
			'Close': function(win) { win.closeModal(); }
		}
	});
}
// Modal Tambah Pegawai
function dialogPegawai()
{
	$.modal({
		content: $("#wrapper-tambah-pegawai"),
		title: 'Pilih Pegawai',
		width: 700,
		buttons: {
			'Close': function(win) { win.closeModal(); }
		}
	});
}
</script>