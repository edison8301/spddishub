<?php 
$this->breadcrumbs=array(
	'Data Uang Harian'=>$this->createUrl('admin'),
	'Detail Uang Harian'=>$this->createUrl('view',array('id'=>$model->id)),
);
?>
<h1>Detail Uang Harian</h1>

<?php $this->widget('booster.widgets.TbButton',array('buttonType'=>'link','context'=>'primary','icon'=>'pencil white','label'=>'Sunting Uang Harian','url'=>array('uangHarian/update','id'=>$model->id),'htmlOptions'=>array('class'=>'big-button'),)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array('buttonType'=>'link','context'=>'primary','icon'=>'list white','label'=>'Data Uang Harian','url'=>array('uangHarian/admin'),'htmlOptions'=>array('class'=>'big-button'),)); ?>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			'jarak',
			array(
				'label'=>'Golongan',
				'value'=>$model->golongan->nama
			),
			array(
				'label'=>'Biaya',
				'value'=>Bantu::rp($model->biaya)
			),
		),
)); ?>
