<?php
$this->breadcrumbs=array(
	'Data Uang Harian'=>$this->createUrl('admin'),
	'Sunting Uang Harian'=>$this->createUrl('update',array('id'=>$model->id)),
);

	$this->menu=array(
	array('label'=>'List Ssh','url'=>array('index')),
	array('label'=>'Create Ssh','url'=>array('create')),
	array('label'=>'View Ssh','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Ssh','url'=>array('admin')),
	);
	?>

<h1>Update Uang Harian</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>