<?php
$this->breadcrumbs=array(
	'Data Uang Harian'=>$this->createUrl('admin'),
	'Entry Uang Harian'=>$this->createUrl('create'),
);

$this->menu=array(
array('label'=>'List Ssh','url'=>array('index')),
array('label'=>'Manage Ssh','url'=>array('admin')),
);
?>

<h1>Entry Uang Harian</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
