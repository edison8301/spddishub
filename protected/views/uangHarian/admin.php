<?php 
$this->breadcrumbs=array(
	'Data Uang Harian'=>$this->createUrl('admin'),
);
?>
<h1>Kelola Uang Harian</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah Uang Harian',
		'icon'=>'plus',
		'context'=>'primary',
		'url'=>array('create'),
		'htmlOptions'=>array('class'=>'big-button'),
)); ?>&nbsp;

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'ssh-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			array(
				'class'=>'CDataColumn',
				'name'=>'jarak',
				'header'=>'Jarak',
				'type'=>'raw',
				'value'=>'$data->jarak',
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'id_golongan',
				'header'=>'Golongan',
				'type'=>'raw',
				'value'=>'$data->golongan->nama',
				'filter'=>CHtml::listData(Golongan::model()->findAll(),'id','nama')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'biaya',
				'header'=>'Biaya',
				'type'=>'raw',
				'value'=>'Bantu::rp($data->biaya)',
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
			),
		),
)); ?>
