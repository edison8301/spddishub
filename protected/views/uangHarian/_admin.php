<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'uang-harian-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$uangHarian->search(),
		'filter'=>$uangHarian,
		'columns'=>array(
			'jarak',
			array(
				'class'=>'CDataColumn',
				'name'=>'id_golongan',
				'header'=>'Golongan',
				'type'=>'raw',
				'value'=>'$data->golongan->nama',
				'filter'=>CHtml::listData(Golongan::model()->findAll(),'id','nama')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'biaya',
				'header'=>'Biaya',
				'type'=>'raw',
				'value'=>'number_format($data->biaya, 0,",",".")',
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				//'template'=>'{update} {delete}',
			),
		),
)); ?>