<?php
$this->breadcrumbs=array(
	'Data Pejabat'=>$this->createUrl('admin'),
	'Sunting Penandatangan Dokumen'=>$this->createUrl('update',array('id'=>$model->id)),
);

	$this->menu=array(
	array('label'=>'List Pejabat','url'=>array('index')),
	array('label'=>'Create Pejabat','url'=>array('create')),
	array('label'=>'View Pejabat','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Pejabat','url'=>array('admin')),
	);
	?>

<h1>Sunting Penandatangan Dokumen</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
