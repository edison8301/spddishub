<?php
$this->breadcrumbs=array(
	'Data Pejabat'=>$this->createUrl('admin'),
	'Entry Penandatangan Dokumen'=>$this->createUrl('create'),
);

$this->menu=array(
array('label'=>'List Pejabat','url'=>array('index')),
array('label'=>'Manage Pejabat','url'=>array('admin')),
);
?>

<h1>Entry Penandatangan Dokumen</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>