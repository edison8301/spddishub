<?php
$this->breadcrumbs=array(
	'Pejabat',
);

$this->menu=array(
array('label'=>'Create Pejabat','url'=>array('create')),
array('label'=>'Manage Pejabat','url'=>array('admin')),
);
?>

<h1>Pejabat</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
