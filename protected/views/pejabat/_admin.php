<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'pejabat-grid',
		'type'=> 'striped bordered',
		'dataProvider'=>$pejabat->search(),
		'filter'=>$pejabat,
		'columns'=>array(
			array(
				'class'=>'CDataColumn',
				'name'=>'foto',
				'header'=>'Foto',
				'type'=>'raw',
				'value'=>'$data->getFoto()',
				'headerHtmlOptions'=>array('style'=>'width:10%')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'nip',
				'header'=>'NIP',
				'type'=>'raw',
				'value'=>'$data->getNip()',
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'nama',
				'header'=>'Nama',
				'type'=>'raw',
				'value'=>'$data->nama',
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'jabatan',
				'header'=>'Jabatan',
				'type'=>'raw',
				'value'=>'$data->jabatan',
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
			),
		),
)); ?>