<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'pejabat-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data')
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'nip',array('class'=>'span5','maxlength'=>255,'value'=>$model->nip == '' ? '' : $model->getNip())); ?>

	<?php echo $form->textFieldGroup($model,'nama',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldGroup($model,'jabatan',array('class'=>'span5','maxlength'=>255)); ?>
	
	<?php echo $form->labelEx($model,'foto'); ?>
	
	<?php 
		if($model->foto != '')
		{
			print CHtml::image(Yii::app()->request->baseUrl.'/uploads/pejabat/'.$model->foto,'',array('style'=>'width:150px;margin-right:10px;')); 
			$this->widget('booster.widgets.TbButton', array(
				'buttonType'=>'link',
				'context'=>'danger',
				'size'=>'mini',
				'icon'=>'remove white',
				'label'=>'',
				'url'=>array('/pejabat/hapusFoto','id'=>$model->id)
			));
			
		} else {
			print CHtml::image(Yii::app()->request->baseUrl.'/img/no-profile.jpg','',array('style'=>'width:150px'));
		}
	?>		

	<?php echo $form->fileField($model,'foto'); ?>
	
	<?php echo $form->error($model,'foto'); ?>
	
	<div>&nbsp;</div>
	
	<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
			'htmlOptions'=>array('class'=>'big-button'),
		)); ?>
	</div>

<?php $this->endWidget(); ?>
