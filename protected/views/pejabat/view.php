<?php
$this->breadcrumbs=array(
	'Data Pejabat'=>$this->createUrl('admin'),
	'Detail Pejabat'=>$this->createUrl('view',array('id'=>$model->id)),
);

$this->menu=array(
array('label'=>'List Pejabat','url'=>array('index')),
array('label'=>'Create Pejabat','url'=>array('create')),
array('label'=>'Update Pejabat','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Pejabat','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Pejabat','url'=>array('admin')),
);
?>

<h1>Detail Pejabat <?php echo $model->nama; ?></h1>

<?php $this->widget('booster.widgets.TbButton',array('buttonType'=>'link','context'=>'primary','icon'=>'pencil white','label'=>'Sunting TTD','url'=>array('pejabat/update','id'=>$model->id),'htmlOptions'=>array('class'=>'big-button'),)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array('buttonType'=>'link','context'=>'primary','icon'=>'list white','label'=>'Master TTD','url'=>array('pejabat/admin'),'htmlOptions'=>array('class'=>'big-button'),)); ?>

<div> &nbsp; </div>
<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			array(
				'label'=>'NIP',
				'value'=>$model->getNip(),
			),
			'nama',
			'jabatan',
			array(
				'label'=>'Foto',
				'type'=>'raw',
				'value'=>$model->getFoto()
			),
		),
)); ?>
