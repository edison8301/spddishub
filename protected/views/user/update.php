<?php
$this->breadcrumbs=array(
	'Data User'=>$this->createUrl('admin'),
	'Sunting User'=>$this->createUrl('update',array('id'=>$model->id)),
);

	$this->menu=array(
	array('label'=>'List User','url'=>array('index')),
	array('label'=>'Create User','url'=>array('create')),
	array('label'=>'View User','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage User','url'=>array('admin')),
	);
	?>

	<h1>Sunting User <?php echo $model->username; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>