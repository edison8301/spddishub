<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'username',array('class'=>'span5','maxlength'=>255)); ?>
	
	<?php if($model->isNewRecord) { ?>
		<?php echo $form->passwordFieldGroup($model,'password',array('class'=>'span5','maxlength'=>255)); ?>
	<?php } ?>
	
	<?php echo $form->dropDownListGroup($model,'role_id',array('widgetOptions'=>array('data'=>CHtml::listData(Role::model()->findAll(),'id','nama'),'htmlOptions'=>array('empty'=>'-- Pilih User Role --')))); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
			'htmlOptions'=>array('class'=>'big-button'),
		)); ?>
</div>

<?php $this->endWidget(); ?>
