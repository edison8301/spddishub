<?php
$this->breadcrumbs=array(
	'Data User'=>$this->createUrl('admin'),
	'Detail User'=>$this->createUrl('view',array('id'=>$model->id)),
);

$this->menu=array(
array('label'=>'List User','url'=>array('index')),
array('label'=>'Create User','url'=>array('create')),
array('label'=>'Update User','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete User','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage User','url'=>array('admin')),
);
?>

<h1>Detail User</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Sunting User',
		'icon'=>'plus',
		'context'=>'primary',
		'url'=>array('update','id'=>$model->id),
		'htmlOptions'=>array('class'=>'big-button'),
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Data User',
		'icon'=>'list',
		'context'=>'primary',
		'url'=>array('admin'),
		'htmlOptions'=>array('class'=>'big-button'),
)); ?>&nbsp;

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type'=>'striped bordered',
'attributes'=>array(
		'username',
		//'password',
		array(
			'label'=>'Role',
			'value'=>$model->role->nama
		),
),
)); ?>
