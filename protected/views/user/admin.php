<?php
$this->breadcrumbs=array(
	'Data User'=>$this->createUrl('admin'),
);

$this->menu=array(
array('label'=>'List User','url'=>array('index')),
array('label'=>'Create User','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('user-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<?php
            /*$this->widget('bootstrap.widgets.TbButtonGroup', array(
            //'size'=>'small',
            'type'=>'primary', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'buttons'=>array(
            array('label'=>'Menu', 'items'=>array(
					array('label'=>'Tambah User', 'icon'=>'plus', 'url'=>array('user/create')),
                )),
            ),
            ));*/
?>
<h1>Data User</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah User',
		'icon'=>'plus',
		'context'=>'primary',
		'url'=>array('create'),
		'htmlOptions'=>array('class'=>'big-button'),
)); ?>&nbsp;

<?php echo $this->renderPartial('_admin', array('user'=>$user)); ?>

