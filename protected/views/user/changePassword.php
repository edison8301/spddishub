<?php 
$this->breadcrumbs=array(
	'Data User'=>$this->createUrl('admin'),
	'Change Password'=>$this->createUrl('changePassword'),
);
?>

<h1>Change Password</h1>

<div>&nbsp;</div>

<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
            'id' => 'chnage-password-form',
            'enableClientValidation' => true,
            'clientOptions' => array(
            'validateOnSubmit' => true,
            ),
     ));
?>
	<?php echo $form->passwordFieldGroup($model,'old_password',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>
	<?php echo $form->passwordFieldGroup($model,'new_password',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>
	<?php echo $form->passwordFieldGroup($model,'repeat_password',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?><div>&nbsp;</div>
	
	<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
				'buttonType' => 'submit', 
				'context' => 'primary', 
				'label' => 'Simpan',
				'icon' => 'ok'
		)); ?>
	</div>
	
<?php $this->endWidget(); ?>