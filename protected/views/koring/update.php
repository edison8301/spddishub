<?php
$this->breadcrumbs=array(
	'Data Koring'=>$this->createUrl('admin'),
	'Sunting Koring'=>$this->createUrl('update',array('id'=>$model->id)),
);

	$this->menu=array(
	array('label'=>'List Kwitansi','url'=>array('index')),
	array('label'=>'Create Kwitansi','url'=>array('create')),
	array('label'=>'View Kwitansi','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Kwitansi','url'=>array('admin')),
	);
	?>

<h1>Sunting Koring</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>