<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'koring-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'koring',array('class'=>'span5','maxlength'=>255)); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok white',
			'label'=>$model->isNewRecord ? 'Simpan' : 'Simpan',
			'htmlOptions'=>array('class'=>'big-button'),
		)); ?>
</div>

<?php $this->endWidget(); ?>
