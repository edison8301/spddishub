<?php
$this->breadcrumbs=array(
	'Data Koring'=>$this->createUrl('admin'),
);

$this->menu=array(
array('label'=>'List Koring','url'=>array('index')),
array('label'=>'Manage Koring','url'=>array('admin')),
);
?>

<h1>Data Koring</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah Koring',
		'icon'=>'plus',
		'context'=>'primary',
		'url'=>array('input'),
		'htmlOptions'=>array('class'=>'big-button'),
)); ?>&nbsp;

<?php echo $this->renderPartial('_admin',array('model'=>$model)); ?>