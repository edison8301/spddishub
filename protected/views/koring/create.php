<?php
$this->breadcrumbs=array(
	'Data Pegawai'=>$this->createUrl('admin'),
	'Input Koring'=>$this->createUrl('create'),
);

$this->menu=array(
array('label'=>'List Koring','url'=>array('index')),
array('label'=>'Manage Koring','url'=>array('admin')),
);
?>

<h1>Input Koring</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>