<?php
$this->breadcrumbs=array(
	'Korings',
);

$this->menu=array(
array('label'=>'Create Koring','url'=>array('create')),
array('label'=>'Manage Koring','url'=>array('admin')),
);
?>

<h1>Korings</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
