<?php
$this->breadcrumbs=array(
	'Data Koring'=>$this->createUrl('admin'),
	'Detail Koring'=>$this->createUrl('view',array('id'=>$model->id)),
);

$this->menu=array(
array('label'=>'List Koring','url'=>array('index')),
array('label'=>'Create Koring','url'=>array('create')),
array('label'=>'Update Koring','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Koring','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Koring','url'=>array('admin')),
);
?>

<h1>Detail Koring</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Sunting Koring',
		'icon'=>'pencil',
		'context'=>'primary',
		'url'=>array('update','id'=>$model->id),
		'htmlOptions'=>array('class'=>'big-button'),
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Data Koring',
		'icon'=>'list',
		'context'=>'primary',
		'url'=>array('admin'),
		'htmlOptions'=>array('class'=>'big-button'),
)); ?>&nbsp;

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type'=>'striped bordered',
'attributes'=>array(
		'koring',
),
)); ?>
