<?php
$this->breadcrumbs=array(
	'Data Koring'=>$this->createUrl('admin'),
	'Input Koring'=>$this->createUrl('create'),
);

$this->menu=array(
array('label'=>'List Kwitansi','url'=>array('index')),
array('label'=>'Manage Kwitansi','url'=>array('admin')),
);
?>

<h1>Input Koring</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model,)); ?>