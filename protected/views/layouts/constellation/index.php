<?php

	// Get apache version
	function apache_version()
	{
		if (function_exists('apache_get_version'))
		{
			if (preg_match('|Apache\/(\d+)\.(\d+)\.(\d+)|', apache_get_version(), $version))
			{
				return $version[1].'.'.$version[2].'.'.$version[3];
			}
		}
		elseif (isset($_SERVER['SERVER_SOFTWARE']))
		{
			if (preg_match('|Apache\/(\d+)\.(\d+)\.(\d+)|', $_SERVER['SERVER_SOFTWARE'], $version))
			{
				return $version[1].'.'.$version[2].'.'.$version[3];
			}
		}
		
		return '(unknown)';
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if lt IE 8 ]><html lang="en" class="no-js ie ie7"><![endif]-->
<!--[if IE 8 ]><html lang="en" class="no-js ie"><![endif]-->
<!--[if (gt IE 8)|!(IE)]><!--><html lang="en" class="no-js"><!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<meta name="description" content="">
	<meta name="author" content="">
	
	<!-- Combined stylesheets load -->
	<!-- Load either 960.gs.fluid or 960.gs to toggle between fixed and fluid layout -->
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/constellation/css/mini.php?files=reset,common,form,standard,960.gs.fluid,simple-lists,block-lists,planning,table,calendars,wizard,gallery" rel="stylesheet" type="text/css">
	<script src="<?php print Yii::app()->request->baseUrl; ?>/js/accounting.min.js" type="text/javascript"></script>
    <script src="<?php print Yii::app()->request->baseUrl; ?>/js/accounting.js" type="text/javascript"></script>
	<!-- Favicon -->
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
	<link rel="icon" type="image/png" href="favicon-large.png">
	
</head>

<body>
	<!-- Header -->
	<!-- Server status -->
	<header><div class="container_12">
		
		<p id="skin-name" style="font-size:15px !important;color:#FFFA00 !important;padding-top:8px;font-weight:bold">Sistem Informasi Manajemen Surat Perjalanan Dinas<br>DISHUBKOMINFO KABUPATEN SERANG</p>
		<div class="server-info">Server: <strong>Apache <?php echo apache_version(); ?></strong></div>
		<div class="server-info">Php: <strong><?php echo phpversion(); ?></strong></div>
		
	</div></header>
	<!-- End server status -->

	<!-- Main nav -->
	<nav id="main-nav" style="padding-bottom:68px;">
		<ul class="container_12">
			<li class="home <?php Bantu::getActiveTab(array('site')); ?>"><a href="<?php print Yii::app()->createUrl('site/admin'); ?>" title="Home">Home</a></li>
			<li class="write <?php Bantu::getActiveTab(array('spd','kwitansi','lhp')); ?>"><a href="#" title="Master SPD">Master SPD</a>
				<ul>
					<li class="<?php Bantu::getActiveMenu('spd') ?>"><a href="<?php print Yii::app()->createUrl('spd/admin'); ?>" title="SPD">SPD</a></li>
					<li class="<?php Bantu::getActiveMenu('kwitansi') ?>"><a href="<?php print Yii::app()->createUrl('kwitansi/admin'); ?>" title="Kwitansi">Kwitansi</a></li>
					<li class="<?php Bantu::getActiveMenu('lhp') ?>"><a href="<?php print Yii::app()->createUrl('lhp/admin'); ?>" title="LHP">LHP</a></li>
				</ul>
			</li>
			<li class="write <?php Bantu::getActiveTab(array('pegawai','pejabat','koring','uangHarian','akomodasi')); ?>"><a href="#" title="Master Data">Master Data</a>
				<ul>
					<li class="<?php Bantu::getActiveMenu('pegawai') ?>"><a href="<?php print Yii::app()->createUrl('pegawai/admin'); ?>" title="Master Pegawai">Master Pegawai</a></li>
					<li class="<?php Bantu::getActiveMenu('pejabat') ?>"><a href="<?php print Yii::app()->createUrl('pejabat/admin'); ?>" title="Master TTD Doc">Master TTD Doc</a></li>
					<li class="<?php Bantu::getActiveMenu('koring') ?>"><a href="<?php print Yii::app()->createUrl('koring/admin'); ?>" title="Koring">Koring</a></li>
					<li class="<?php Bantu::getActiveMenu('uangHarian') ?>"><a href="<?php print Yii::app()->createUrl('uangHarian/admin'); ?>" title="Uang Harian">Uang Harian</a></li>
					<li class="<?php Bantu::getActiveMenu('akomodasi') ?>"><a href="<?php print Yii::app()->createUrl('akomodasi/admin'); ?>" title="Akomodasi">Akomodasi</a></li>
				</ul>
			</li>
			<li class="stats <?php Bantu::getActiveTab(array('perjalanan')); ?>"><a href="#" title="Rekapitulasi">Rekapitulasi</a>
				<ul>
					<li class="<?php Bantu::getActiveMenu('perjalanan','index') ?>"><a href="<?php print Yii::app()->createUrl('perjalanan/index'); ?>" title="Kalender Perjalanan">Kalender Perjalanan</a></li>
					<li class="<?php Bantu::getActiveMenu('perjalanan','rekap') ?>"><a href="<?php print Yii::app()->createUrl('perjalanan/rekap'); ?>" title="Rekap Perjalanan">Rekap Perjalanan</a></li>
					<li class="<?php Bantu::getActiveMenu('perjalanan','rekapBiaya') ?>"><a href="<?php print Yii::app()->createUrl('perjalanan/rekapBiaya'); ?>" title="Rekap Biaya Perjalanan">Rekap Biaya Perjalanan</a></li>
				</ul>
			</li>
			<li class="users <?php Bantu::getActiveTab(array('user','role')); ?>"><a href="#" title="User & Role">User & Role</a>
				<ul>
					<li class="<?php Bantu::getActiveMenu('user') ?>"><a href="<?php print Yii::app()->createUrl('user/admin'); ?>" title="User">User</a></li>
					<li class="<?php Bantu::getActiveMenu('user','changePassword') ?>"><a href="<?php print Yii::app()->createUrl('user/changePassword'); ?>" title="Role">Change Password</a></li>
					<li class="<?php Bantu::getActiveMenu('role') ?>"><a href="<?php print Yii::app()->createUrl('role/admin'); ?>" title="Role">Role</a></li>
				</ul>
			</li>
			<li class="backup <?php Bantu::getActiveTab(array('site','site'),'import'); ?>"><a href="#" title="Backup Database">Backup Database</a>
				<ul>
					<li class="<?php Bantu::getActiveMenu('site','backup') ?>"><a href="<?php print Yii::app()->createUrl('site/backup'); ?>" title="Backup Database">Backup Database</a></li>
					<li class="<?php Bantu::getActiveMenu('site','import') ?>"><a href="<?php print Yii::app()->createUrl('site/import'); ?>" title="Import Database">Restore Database</a></li>
				</ul>
			</li>
		</ul>
	</nav>
	<!-- End main nav -->
	<!-- Sub nav -->
	<div id="sub-nav"><div class="container_12">
	
	</div></div>
	<!-- End sub nav -->
	<!-- Status bar -->
	<div id="status-bar"><div class="container_12">
		<ul id="status-infos">
			<li class="spaced">Logged as: <strong><?php print Yii::app()->user->id; ?></strong></li>
			<li><a href="<?php print Yii::app()->createUrl('site/logout') ?>" class="button red" title="Logout"><span class="smaller">LOGOUT</span></a></li>
		</ul>
		<ul id="breadcrumb">
			<?php if(isset($this->breadcrumbs)) { ?>
				<?php foreach($this->breadcrumbs as $title=>$url) { ?>
					<li><a href="<?php print $url; ?>" title="<?php print $title; ?>"><?php print $title; ?></a></li>
				<?php } ?>
			<?php } ?>
		</ul>
	</div></div>
	<!-- End status bar -->
	
	<div id="header-shadow"></div>
	<!-- End header -->
	
	<div>&nbsp;</div>
	
	<section class="col-xs-12">
			<div class="block-border">
				<div class="block-content">
					<?php print $content; ?>
				</div>
			</div>
	</section>
	
	<footer>
		<div class="float-right">
			<a href="#top" class="button"><img src="<?php echo Yii::app()->request->baseUrl; ?>/css/constellation/images/icons/fugue/navigation-090.png" width="16" height="16"> Page top</a>
		</div>
	</footer>
	
	<!--
	
	Updated as v1.5:
	Libs are moved here to improve performance
	
	-->
	
	<!-- Combined JS load -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/css/constellation/js/mini.php?files=libs/jquery.hashchange,jquery.accessibleList,searchField,common,standard,jquery.tip,jquery.contextMenu,jquery.modal,list"></script>
	
</body>
</html>