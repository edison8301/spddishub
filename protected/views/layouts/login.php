<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	 <link rel="stylesheet" type="text/css" href="<?php print Yii::app()->request->baseUrl; ?>/css/admin/admin.css">
	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body class="bodybg">
	<div class="row-fluid">
		<div style="width:380px;margin-left:auto;margin-right:500px;margin-top:200px">
			<?php echo $content; ?>
		</div>
	</div>

</body>
</html>