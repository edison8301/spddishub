<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<div class="row">
	<div class="col-lg-3 col-md-3">
	<?php $this->widget('booster.widgets.TbMenu',array(
			'type'=>'inverse',
			'items' => array(
				array('label' => 'Dashboard','icon'=>'home', 'url' => array('site/index')),
				array('label' => 'Data Tower','icon'=>'search', 'url' => array('tower/admin')),
				array('label' => 'Tambah Tower','icon'=>'plus', 'url' => array('tower/create')),
				array('label' => 'Tambah Retribusi','icon'=>'file', 'url' => array('retribusi/createOto')),
				array('label' => 'Peta Tower','icon'=>'globe', 'url' => array('tower/peta')),
				array('label' => 'Data Perusahaan','icon'=>'list', 'url' => array('perusahaan/admin')),
				array('label' => 'User','icon'=>'user', 'url' => array('user/admin')),
				array('label'=>'Logout ('.Yii::app()->user->id.')','icon'=>'off','url'=>array('site/logout'),'visible'=>!Yii::app()->user->isGuest)
			)
	)); ?>
	</div>

	<div class="col-lg-9 col-md-9">
		<div class="row">
			<div class="col-lg-12">
				<?php foreach(Yii::app()->user->getFlashes() as $key => $message) { ?>
					<div class="alert alert-<?php print $key; ?>"><?php print $message; ?></div>
				<?php } ?>
				<?php Yii::app()->clientScript->registerScript('hideAlert',
						'$(".alert").animate({opacity: 1.0}, 3000).fadeOut("slow");',
						CClientScript::POS_READY
				); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<?php echo $content; ?>
			</div>
		</div>
	</div><!-- content -->
</div>



<?php $this->endContent(); ?>