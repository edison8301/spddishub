<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<div class="row">
	<div class="col-lg-3 col-md-3">
	<?php $this->widget('booster.widgets.TbMenu',array(
			'type'=>'inverse',
			'items' => array(
				array('label' => 'Dashboard','icon'=>'home', 'url' => array('/site/admin')),
				array('label' => 'Kalender Perjalanan','icon'=>'calendar', 'url' => array('/perjalanan')),
				array('label' => 'Rekap Perjalanan','icon'=>'align-left', 'url' => array('/perjalanan/rekap')),
				array('label' => 'Rekap Biaya Perjalanan','icon'=>'usd', 'url' => array('/perjalanan/rekapBiaya')),
				array('label' => 'SPD','icon'=>'book', 'url' => array('/spd/admin')),
				array('label' => 'Input SPD','icon'=>'plus', 'url' => array('/spd/create')),
				array('label' => 'LHP','icon'=>'tag', 'url' => array('/lhp/admin')),
				array('label' => 'Kwitansi','icon'=>'usd', 'url' => array('/kwitansi/admin')),
				array('label' => 'Master Data','icon'=>'credit-card', 'items' => array(
					array('label' => 'Master Pegawai','icon'=>'credit-card', 'url' => array('/pegawai/admin')),
					array('label' => 'Master TTD Doc','icon'=>'file', 'url' => array('/pejabat/admin')),
					array('label' => 'Koring','icon'=>'book', 'url' => array('/koring/admin')),
					array('label' => 'Uang Harian','icon'=>'usd', 'url' => array('/uangHarian/admin')),
					array('label' => 'Akomodasi','icon'=>'inbox', 'url' => array('/akomodasi/admin')),
				)),
				array('label' => 'User & Role','icon'=>'user', 'items' => array(
					array('label' => 'User','icon'=>'user', 'url' => array('/user/admin')),
					array('label' => 'Role','icon'=>'lock', 'url' => array('/role/admin')),
				)),
				array('label' => 'Backup Database','icon'=>'hdd', 'items' => array(
					array('label' => 'Backup Data','icon'=>'arrow-up', 'url' => array('/site/backup')),
					array('label' => 'Restore Data','icon'=>'inbox', 'url' => array('/site/import')),
				)),
				array('label' => 'Logout ('.Yii::app()->user->id.')','icon'=>'off', 'url' => array('site/logout')),
			)
	)); ?>
	</div>

	<div class="col-lg-9 col-md-9">
		<div class="row">
			<div class="col-lg-12">
				<?php foreach(Yii::app()->user->getFlashes() as $key => $message) { ?>
					<div class="alert alert-<?php print $key; ?>"><?php print $message; ?></div>
				<?php } ?>
				<?php Yii::app()->clientScript->registerScript('hideAlert',
						'$(".alert").animate({opacity: 1.0}, 3000).fadeOut("slow");',
						CClientScript::POS_READY
				); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<?php echo $content; ?>
			</div>
		</div>
	</div><!-- content -->
</div>



<?php $this->endContent(); ?>