<?php
$this->breadcrumbs=array(
	'Login Histories'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List LoginHistory','url'=>array('index')),
array('label'=>'Manage LoginHistory','url'=>array('admin')),
);
?>

<h1>Create LoginHistory</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>