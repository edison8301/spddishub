<?php
$this->breadcrumbs=array(
	'Login Histories',
);

$this->menu=array(
array('label'=>'Create LoginHistory','url'=>array('create')),
array('label'=>'Manage LoginHistory','url'=>array('admin')),
);
?>

<h1>Login Histories</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
