<?php
$this->breadcrumbs=array(
	'Login Histories'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List LoginHistory','url'=>array('index')),
array('label'=>'Create LoginHistory','url'=>array('create')),
array('label'=>'Update LoginHistory','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete LoginHistory','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage LoginHistory','url'=>array('admin')),
);
?>

<h1>View LoginHistory #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'user_id',
		'waktu_login',
),
)); ?>
