<?php
$this->breadcrumbs=array(
	'Login Histories'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List LoginHistory','url'=>array('index')),
	array('label'=>'Create LoginHistory','url'=>array('create')),
	array('label'=>'View LoginHistory','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage LoginHistory','url'=>array('admin')),
	);
	?>

	<h1>Update LoginHistory <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>