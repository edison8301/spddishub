<?php
$this->breadcrumbs=array(
	'Kalender Perjalanan'=>$this->createUrl('index'),
	'Rekap Perjalanan'=>$this->createUrl('rekap'),
);

$this->menu=array(
array('label'=>'Create Perjalanan','url'=>array('create')),
array('label'=>'Manage Perjalanan','url'=>array('admin')),
);
?>
<?php /*<h1>Perjalanans</h1>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
));*/ ?>
<h1>Rekap Perjalanan: <?php print date('Y'); ?></h1>

<div>&nbsp;</div>

<div style="overflow:auto">

<div style="margin-bottom:10px;">

<?php /* $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'type'=>'primary',
		'icon'=>'chevron-left white',
		'url'=>array('perjalanan/index','month'=>Bantu::getPrevMonth())
)); ?>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'type'=>'primary',
		'icon'=>'chevron-right white',
		'url'=>array('perjalanan/index','month'=>Bantu::getNextMonth())
)); */ ?>

</div>


<table class="table table-bordered">
	<thead>	
		<tr>
			<?php $jumlahHari = date('t',strtotime(Bantu::getMonth())); ?>
			<th colspan="2" style="background:#317EAC;color:#FFFFFF;font-weight:bold;vertical-align:middle;text-align:center" class="lebarKolomNama" style="width:15%">Pegawai</th>
			<th colspan="12" style="background:#317EAC;color:#FFFFFF;font-weight:bold;vertical-align:middle;text-align:center"><?php print date('Y'); ?></th>
			<th rowspan="2" style="background:#317EAC;color:#FFFFFF;font-weight:bold;vertical-align:middle;text-align:center">TOTAL</th>
		
		</tr>
		<tr>
			<th style="background:#317EAC;color:#FFFFFF;font-weight:bold;text-align:center;width:10%" width="10%">Foto</th>
			<th style="background:#317EAC;color:#FFFFFF;font-weight:bold;text-align:center" width="10%">Nama</th>
			<th style="background:#317EAC;color:#FFFFFF;font-weight:bold;text-align:center">
				Jan
			</th>
			<th style="background:#317EAC;color:#FFFFFF;font-weight:bold;text-align:center">
				Feb
			</th>
			<th style="background:#317EAC;color:#FFFFFF;font-weight:bold;text-align:center">
				Mar
			</th>
			<th style="background:#317EAC;color:#FFFFFF;font-weight:bold;text-align:center">
				Apr
			</th>	
			<th style="background:#317EAC;color:#FFFFFF;font-weight:bold;text-align:center">
				Mei
			</th>	
			<th style="background:#317EAC;color:#FFFFFF;font-weight:bold;text-align:center">
				Jun
			</th>	
			<th style="background:#317EAC;color:#FFFFFF;font-weight:bold;text-align:center">
				Jul
			</th>	
			<th style="background:#317EAC;color:#FFFFFF;font-weight:bold;text-align:center">
				Agt
			</th>
			<th style="background:#317EAC;color:#FFFFFF;font-weight:bold;text-align:center">
				Sep
			</th>			
			<th style="background:#317EAC;color:#FFFFFF;font-weight:bold;text-align:center">
				Okt
			</th>	
			<th style="background:#317EAC;color:#FFFFFF;font-weight:bold;text-align:center">
				Nov
			</th>	
			<th style="background:#317EAC;color:#FFFFFF;font-weight:bold;text-align:center">
				Des
			</th>	
			
		</tr>
	</thead>
	<tbody>
		<?php
			$criteria = new CDbCriteria;
			$criteria->order = 'nama ASC';
		?>
		<?php foreach (Pegawai::model()->findAll($criteria) as $pegawai) { ?>
		<?php $total = 0; ?>
		<tr>
			<td>
				<?php echo $pegawai->getFoto(); ?>
			</td>
			<td style="font-weight:bold" class="lebarKolomNama">
				<?php echo $pegawai->nama; ?>
			</td>
			<?php for($i=1;$i<=12;$i++) { ?>
			
			<?php 
				$bulan = $i; 
				if($bulan <= 9) $bulan = '0'.$bulan;
				
				$tahun = date('Y');
				$jumlah = $pegawai->getJumlahPerjalananByBulanByTahun($bulan,$tahun); 
				$total += $jumlah;
			?>
			<?php if($jumlah >= 16) { ?>
			<td style="text-align:center" class="alert-danger"><?php print $jumlah; ?></td>
			<?php } else if($jumlah >= 11 AND $jumlah <= 15) { ?>
			<td style="text-align:center" class="alert-warning"><?php print $jumlah; ?></td>
			<?php } else { ?>
			<td style="text-align:center" class="alert-success"><?php print $jumlah; ?></td>
			<?php } ?>
			<?php } ?>
			<td style="text-align:center;font-weight:bold"><?php print $total; ?></td>
			
		</tr>
		<?php } ?>
	</tbody>
</table>
</div>