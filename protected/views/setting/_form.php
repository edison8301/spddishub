<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'setting-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'title',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>
	
	<?php if($model->isNewRecord) { ?>
		<?php echo $form->textFieldGroup($model,'key',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>
	<?php } ?>
		
	<?php echo $form->textAreaGroup($model,'value', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>6, 'cols'=>50, 'class'=>'span8')))); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>$model->isNewRecord ? 'Simpan' : 'Simpan',
			'htmlOptions'=>array('class'=>'big-button'),
		)); ?>
</div>

<?php $this->endWidget(); ?>
