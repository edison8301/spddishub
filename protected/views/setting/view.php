<?php
$this->breadcrumbs=array(
	'Settings'=>array('index'),
	$model->title,
);

$this->menu=array(
array('label'=>'List Setting','url'=>array('index')),
array('label'=>'Create Setting','url'=>array('create')),
array('label'=>'Update Setting','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Setting','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Setting','url'=>array('admin')),
);
?>

<h1>View Setting</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Update',
		'icon'=>'pencil',
		'context'=>'primary',
		'url'=>array('/setting/update','id'=>$model->id)
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Add',
		'context'=>'primary',
		'icon'=>'plus white',
		'url'=>array('/setting/create')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Manage',
		'context'=>'primary',
		'icon'=>'list white',
		'url'=>array('/setting/admin')
)); ?>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			'id',
			'title',
			'key',
			'value',
		),
)); ?>
