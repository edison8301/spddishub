<?php
$this->breadcrumbs=array(
	//'SPD'=>$this->createUrl('spd/view',array('id'=>$_GET['id_spd'])),
	'Settings Font'=>$this->createUrl('setting/update',array('id'=>'1')),
);

$this->menu=array(
	array('label'=>'List Setting','url'=>array('index')),
	array('label'=>'Create Setting','url'=>array('create')),
	array('label'=>'View Setting','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Setting','url'=>array('admin')),
	);
?>

<h1>Update Setting</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>