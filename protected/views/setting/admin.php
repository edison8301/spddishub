<h1>Kelola Setting</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Add',
		'context'=>'primary',
		'icon'=>'plus white',
		'url'=>array('/setting/create')
)); ?>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'setting-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
				),
			),
			'title',
			'key',
			'value',
			array(
				'class'=>'booster.widgets.TbButtonColumn',
			),
		),
)); ?>