<?php
$this->breadcrumbs=array(
	'Data Kwitansi'=>$this->createUrl('admin'),
	'Input Kwitansi'=>$this->createUrl('create'),
);

$this->menu=array(
array('label'=>'List Kwitansi','url'=>array('index')),
array('label'=>'Manage Kwitansi','url'=>array('admin')),
);
?>

<h1>Tanda Bukti Pembayaran</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
	
