<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'kwitansi-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$kwitansi->search(),
		'filter'=>$kwitansi,
		'columns'=>array(
			array(
				'class'=>'CDataColumn',
				'name'=>'nomor',
				'header'=>'Nomor',
				'type'=>'raw',
				'value'=>'$data->nomor',
			),
			
			array(
				'class'=>'CDataColumn',
				'name'=>'id_spd',
				'header'=>'No. SPD',
				//'type'=>'raw',
				'value'=>'$data->getRelationField("spd","nomor_spd")',
				//'filter'=>CHtml::listData(Spd::model()->findAll(),'id','nomor_spd')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'tanggal',
				'header'=>'Tanggal Kwitansi',
				'type'=>'raw',
				'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tanggal)',
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'penerima',
				'header'=>'Penerima',
				'type'=>'raw',
				'value'=>'$data->getRelationField("penerimaRelation","nama")',
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'total',
				'header'=>'Total',
				'type'=>'raw',
				'value'=>'number_format($data->total, 0,",",".")',
			),
			
			array(
				'class'=>'booster.widgets.TbButtonColumn',
			)
			
		),
)); ?>