<?php
$this->breadcrumbs=array(
	'Data Kwitansi'=>$this->createUrl('admin'),
);

$this->menu=array(
array('label'=>'List Kwitansi','url'=>array('index')),
array('label'=>'Manage Kwitansi','url'=>array('admin')),
);
?>

<h1>Data Tanda Bukti Pembayaran</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah Kwitansi',
		'icon'=>'plus',
		'context'=>'primary',
		'url'=>array('create'),
		'htmlOptions'=>array('class'=>'big-button'),
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Setting Posisi Cetak Kwitansi',
		'icon'=>'wrench',
		'context'=>'primary',
		'url'=>array('posisiCetak/setting','jenis_surat'=>'2'),
		'htmlOptions'=>array('class'=>'big-button'),
)); ?>&nbsp;

<div style="overflow:auto">
	<?php echo $this->renderPartial('_admin',array('kwitansi'=>$kwitansi)); ?>
</div>


