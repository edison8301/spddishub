<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'kwitansi-form',
	'enableAjaxValidation'=>false,
)); ?>
<script type="text/javascript">
	$(document).ready(function(){
		$(".format-money").on("keyup", function(){
			var _this = $(this);
			var value = _this.val().replace(/\.| /g,"");
			_this.val(accounting.formatMoney(value, "", 0, ".", ","))
		});	
		
		$(".format-money").trigger("keyup");
		
		$("#Kwitansi_uang_harian").on("keyup",function() {
			var lama = $('#lama').val();
			var uang_harian = $('#Kwitansi_uang_harian').val().replace('.',"");
			var total_uang_harian = lama*uang_harian;
			$('#total_uang_harian').val(total_uang_harian);
			$('#total_uang_harian').trigger("keyup");
		});
	});
</script>

<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>
<?php echo $form->errorSummary($model); ?>

<div class="row">
	<div class="col-xs-6">
		<?php echo $form->textFieldGroup($model,'nomor',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span9','maxlength'=>255,'placeholder'=>'Nomor Kwitansi')))); ?>
		
		<?php echo $form->datepickerGroup($model, 'tanggal',array('widgetOptions' => array('options'=>array('autoclose'=>true,'format'=>'yyyy-mm-dd')), 'prepend' => '<i class="glyphicon glyphicon-calendar"></i>')); ?>
	
		<?php print $form->hiddenField($model,'penerima',array('value'=>isset($_GET['penerima']) ? $_GET['penerima'] : $model->penerima)); ?>
		<?php print CHtml::label('Nama Penerima',''); ?>
		<?php if(isset($_GET['penerima']) AND $_GET['penerima']!=null) { ?>
			<?php $pegawai = Pegawai::model()->findByPk($_GET['penerima']); ?>
			<?php print CHtml::textField('nama_penerima',$pegawai->nama,array('disabled'=>true,'class'=>'form-control span5','placeholder'=>'Pilih Penerima','style'=>'margin-bottom:10px;')); ?>
		<?php } else { ?>
		<?php print CHtml::label('Pilih Penerima',''); ?>
		<div class="input-group">
			<span class="input-group-btn">
				<?php $this->widget('booster.widgets.TbButton', array(
					'label'=>'[..]',
					'context'=>'primary', 
					'htmlOptions'=>array(
						'onclick'=>'dialogPenerima(); return false;',
						'style'=>'margin-bottom:10px',
					),
				)); ?>
			</span>
			<?php print CHtml::textField('nama_penerima',$model->getRelationField("penerimaRelation","nama")!='' ? $model->getRelationField("penerimaRelation","nama") : '',array('disabled'=>true,'class'=>'form-control span8','placeholder'=>'Pilih Penerima')); ?>
		</div>
		<?php } ?>
		
		<?php print $form->hiddenField($model,'id_spd',array('value'=>isset($_GET['id_spd']) ? $_GET['id_spd'] : $model->id_spd)); ?>
		<?php print CHtml::label('Nomor SPD',''); ?>
		<?php if(isset($_GET['id_spd']) AND $_GET['id_spd']!=null) { ?>
			<?php $spd = Spd::model()->findByPk($_GET['id_spd']); ?>
			<?php print CHtml::textField('nomor_spd',$spd->nomor_spd,array('disabled'=>true,'class'=>'form-control span5','placeholder'=>'Pilih SPD','style'=>'margin-bottom:10px;')); ?>
		<?php } else { ?>
		<div class="input-group">
			<span class="input-group-btn">
				<?php $this->widget('booster.widgets.TbButton', array(
					'label'=>'[..]',
					'context'=>'primary', 
					'htmlOptions'=>array(
						'onclick'=>'dialogSpd(); return false;',
						'style'=>'margin-bottom:10px',
					),
				)); ?>
			</span>
			<?php print CHtml::textField('nomor_spd',$model->getRelationField("spd","nomor_spd") != '' ? $model->getRelationField("spd","nomor_spd") : '',array('disabled'=>true,'class'=>'form-control span8','placeholder'=>'Pilih SPD')); ?>
		</div>
		<?php } ?>
	
		<?php print CHtml::label('Koring - Jenis Perjalanan Dinas',''); ?>
		<?php if(isset($_GET['id_spd']) AND $_GET['id_spd']!=null) { ?>
			<?php print CHtml::textField('koring_jenis_pd',$spd->koring,array('disabled'=>true,'class'=>'form-control','style'=>'margin-bottom:10px;')); ?>
		<?php } else { ?>
			<?php print CHtml::textField('koring_jenis_pd',$model->getRelationField("spd","koring") != '' ? $model->getRelationField("spd","koring") : '',array('disabled'=>true,'class'=>'form-control','style'=>'margin-bottom:10px;')); ?>
		<?php } ?>
		
		<?php print CHtml::label('Lama (Hari)',''); ?>
		<?php if(isset($_GET['id_spd']) AND $_GET['id_spd']!=null) { ?>
			<?php print CHtml::textField('lama',$spd->lama,array('disabled'=>true,'class'=>'form-control','style'=>'margin-bottom:10px;')); ?>
		<?php } else { ?>
			<?php print CHtml::textField('lama',$model->getRelationField("spd","lama") != '' ? $model->getRelationField("spd","lama") : '',array('disabled'=>true,'class'=>'form-control','style'=>'margin-bottom:10px;')); ?>
		<?php }	?>
		
		<div class="input-group">
			<?php echo $form->textFieldGroup($model,'uang_harian',array('prepend'=>'Rp','widgetOptions'=>array('htmlOptions'=>array('class'=>'format-money span12' ,'maxlength'=>11,'placeholder'=>'Uang Harian')))); ?>
			<span class="input-group-btn">
				<?php $this->widget('booster.widgets.TbButton', array(
					'label'=>'[..]',
					'context'=>'primary', 
					'htmlOptions'=>array(
						'onClick'=>'dialogUangHarian(); return false;',
						'value'=>'add',
						//'style'=>'margin-top:10px',
					),
				)); ?>
			</span>
		</div>
	
		<?php print CHtml::label('Total Uang Harian',''); ?>
		<?php print CHtml::textField('total_uang_harian',$model->getRelationField("spd","lama") != '' ? $model->getRelationField("spd","lama")*$model->uang_harian : '',array('disabled'=>true,'class'=>'format-money form-control col-xs-12','style'=>'margin-bottom:10px;')); ?>
	
		<div class="input-group">
			<?php echo $form->textFieldGroup($model,'akomodasi',array('prepend'=>'Rp','widgetOptions'=>array('htmlOptions'=>array('class'=>'span10 format-money','maxlength'=>11,'placeholder'=>'Biaya Akomodasi')))); ?>
			<span class="input-group-btn">
				<?php $this->widget('booster.widgets.TbButton', array(
					'label'=>'[..]',
					'context'=>'primary', 
					'htmlOptions'=>array(
						'onclick'=>'dialogAkomodasi(); return false;',
						//'style'=>'margin-top:10px',
					),
				)); ?>
			</span>
		</div>
	
		<div class="input-group">
			<?php echo $form->textFieldGroup($model,'refresentatif',array('prepend'=>'Rp','widgetOptions'=>array('htmlOptions'=>array('class'=>'span10 format-money','maxlength'=>11,'placeholder'=>'Biaya Refresentatif')))); ?>
			<span class="input-group-btn">
				<?php $this->widget('booster.widgets.TbButton', array(
					'label'=>'[..]',
					'context'=>'primary', 
					'htmlOptions'=>array(
						'onClick'=>'dialogRefresentatif(); return false;',
						//'style'=>'margin-top:10px',
					),
				)); ?>
			</span>
		</div>
	</div>
	<div class="col-xs-6">
		<?php echo $form->textFieldGroup($model,'bbm',array('prepend'=>'Rp','widgetOptions'=>array('htmlOptions'=>array('class'=>'span12 format-money','maxlength'=>255,'placeholder'=>'Biaya BBM')))); ?>

		<?php echo $form->textFieldGroup($model,'tol',array('prepend'=>'Rp','widgetOptions'=>array('htmlOptions'=>array('class'=>'span12 format-money','maxlength'=>255,'placeholder'=>'Biaya TOL')))); ?>

		<?php echo $form->textFieldGroup($model,'tiket',array('prepend'=>'Rp','widgetOptions'=>array('htmlOptions'=>array('class'=>'span12 format-money','maxlength'=>255,'placeholder'=>'Biaya Tiket Pesawat')))); ?>
		
		<?php echo $form->textFieldGroup($model,'program',array('prepend'=>'<i class="glyphicon glyphicon-book"></i>','widgetOptions'=>array('htmlOptions'=>array('class'=>'span12','maxlength'=>255)))); ?>
		
		<?php echo $form->textFieldGroup($model,'kegiatan',array('prepend'=>'<i class="glyphicon glyphicon-book"></i>','widgetOptions'=>array('htmlOptions'=>array('class'=>'span12','maxlength'=>255)))); ?>
		
		<?php print $form->hiddenField($model,'bendahara'); ?>
		<?php print CHtml::label('Pilih Bendahara',''); ?>
		<div class="input-group">
			<span class="input-group-btn">
				<?php $this->widget('booster.widgets.TbButton', array(
					'label'=>'[..]',
					'context'=>'primary', 
					'htmlOptions'=>array(
						'onclick'=>'dialogBendahara(); return false;',
						'style'=>'margin-bottom:10px',
					),
				)); ?>
			</span>
			<?php print CHtml::textField('nama_bendahara',$model->getRelationField("bendaharaRelation","nama") != '' ? $model->getRelationField("bendaharaRelation","nama") : '',array('disabled'=>true,'class'=>'form-control span8','placeholder'=>'Pilih Pegawai')); ?>
		</div>
	
		<?php print $form->hiddenField($model,'pptk'); ?>
		<?php print CHtml::label('Pilih PPTK',''); ?>
		<div class="input-group">
			<span class="input-group-btn">
				<?php $this->widget('booster.widgets.TbButton', array(
					'label'=>'[..]',
					'context'=>'primary', 
					'htmlOptions'=>array(
						'onclick'=>'dialogPptk(); return false;',
						'style'=>'margin-bottom:10px',
					),
				)); ?>
			</span>	
			<?php print CHtml::textField('nama_pptk',$model->getRelationField("pptkRelation","nama") != '' ? $model->getRelationField("pptkRelation","nama") : '',array('disabled'=>true,'class'=>'form-control span8','placeholder'=>'Pilih Pegawai')); ?>
		</div>
	
		<?php print $form->hiddenField($model,'pa'); ?>
		<?php print CHtml::label('Pilih PA/KPA',''); ?>
		<div class="input-group">
			<span class="input-group-btn">
				<?php $this->widget('booster.widgets.TbButton', array(
					'label'=>'[..]',
					'context'=>'primary', 
					'htmlOptions'=>array(
						'onclick'=>'dialogPa(); return false;',
						'style'=>'margin-bottom:10px',
					),
				)); ?>
			</span>
			<?php print CHtml::textField('nama_pa',$model->getRelationField("paRelation","nama") != '' ? $model->getRelationField("paRelation","nama") : '',array('disabled'=>true,'class'=>'span8 form-control','placeholder'=>'Pilih Pegawai')); ?>
		</div>
	</div>
</div>

<div class="form-actions well">
	<?php $this->widget('booster.widgets.TbButton', array(
		'buttonType'=>'submit',
		'context'=>'primary',
		'icon'=>'ok white',
		'label'=>'Simpan',
		'htmlOptions'=>array('class'=>'big-button'),
	)); ?>
</div>
<?php $this->endWidget(); ?>

<div class="hidden">
<div id="wrapper-tambah-spd">
<?php //Dialog SPD
	$spd = new Spd;
	$this->widget('booster.widgets.TbGridView',array(
		'id'=>'spd-grid',
		'type'=> 'striped bordered',
		'afterAjaxUpdate'=>'function(id,data) { isi = $("#wrapper-tambah-spd").html(); $(".modal-content").html(isi);  }',
		'dataProvider'=>$spd->dataSpd(),
		'filter'=>$spd,
		'columns'=>array(
			array(
				'class'=>'CDataColumn',
				'type'=>'raw',
				'header'=>'Cetak',
				'value'=>'CHtml::link("<center><i class=\"glyphicon glyphicon-print\"></i></center>",array("spd/view","id"=>"$data->id"))',
			),
			'nomor_spd',
			array(
				'class'=>'CDataColumn',
				'name'=>'tanggal_spd',
				'header'=>'Tanggal SPD',
				'type'=>'raw',
				'value'=>'Bantu::tgl($data->tanggal_spd)',
				//'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tanggal_spd)',
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'id_pejabat',
				'header'=>'Penandatangan',
				'type'=>'raw',
				'value'=>'$data->getRelationField("pejabat","nama")',
				'filter'=>CHtml::listData(Pejabat::model()->findAll(),'id','nama')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'id_pegawai',
				'header'=>'Pegawai',
				'type'=>'raw',
				'value'=>'$data->getRelationField("pegawai","nama")',
				'filter'=>CHtml::listData(Pegawai::model()->findAll(),'id','nama')
			),
			'maksud',
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "
								$(\"#Kwitansi_lama\").val(\"$data->lama\");
								$(\"#Kwitansi_id_spd\").val(\"$data->id\");
							    $(\"#nomor_spd\").val(\"$data->nomor_spd\");
								$(\"#koring_jenis_pd\").val(\"$data->koring\");
								$(\"#lama\").val(\"$data->lama\");
								$(\"#Kwitansi_uang_harian\").trigger(\"keyup\");
								$(\".format-money\").trigger(\"keyup\");
								$(\".modal-window\").closeModal();",
					"class"=>"big-button",
				))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
				),
			),
		),
));
?>
</div>
</div>

<div class="hidden">
<div id="wrapper-tambah-penerima">
<?php //Dialog Penerima
	$penerima = new Pegawai;
	$this->widget('booster.widgets.TbGridView',array(
		'id'=>'penerima-grid',
		'type'=>'striped bordered',
		'afterAjaxUpdate'=>'function(id,data) { isi = $("#wrapper-tambah-penerima").html(); $(".modal-content").html(isi);  }',
		'dataProvider'=>$penerima->search(),
		'filter'=>$penerima,
		'columns'=>array(
			'nip',
			'nama',
			array(
				'class'=>'CDataColumn',
				'name'=>'id_golongan',
				'header'=>'Golongan',
				'type'=>'raw',
				'value'=>'$data->getRelationField("golongan","nama")',
				'filter'=>CHtml::listData(Golongan::model()->findAll(),'id','nama')
			),
			'jabatan',
			array(
				'class'=>'CDataColumn',
				'name'=>'tgl_lahir',
				'header'=>'Tanggal Lahir',
				'type'=>'raw',
				'value'=>'Bantu::tgl($data->tgl_lahir)',
				//'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tgl_lahir)',
			),
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "
							    $(\"#Kwitansi_penerima\").val(\"$data->id\");
								$(\"#nama_penerima\").val(\"$data->nama\");
								$(\".modal-window\").closeModal();",
					"class"=>"big-button"
				))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
					
				),
			),
		),
));
?>
</div>
</div>

<div class="hidden">
<div id="wrapper-tambah-bendahara">
<?php //Dialog Bendahara
	$bendahara = new Pejabat;
	$this->widget('booster.widgets.TbGridView',array(
		'id'=>'bendahara-grid',
		'type'=>'striped bordered',
		'afterAjaxUpdate'=>'function(id,data) { isi = $("#wrapper-tambah-bendahara").html(); $(".modal-content").html(isi);  }',
		'dataProvider'=>$bendahara->search(),
		'filter'=>$bendahara,
		'columns'=>array(
			'nip',
			'nama',
			'jabatan',
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "
							    $(\"#Kwitansi_bendahara\").val(\"$data->id\");
								$(\"#nama_bendahara\").val(\"$data->nama\");
								$(\".modal-window\").closeModal();",
					"class"=>"big-button"
				))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
					
				),
			),
		),
));
?>
</div>
</div>

<div class="hidden">
<div id="wrapper-tambah-pptk">
<?php //Dialog PPTK
	$pptk = new Pejabat;
	$this->widget('booster.widgets.TbGridView',array(
		'id'=>'pptk-grid',
		'type'=>'striped bordered',
		'afterAjaxUpdate'=>'function(id,data) { isi = $("#wrapper-tambah-pptk").html(); $(".modal-content").html(isi);  }',
		'dataProvider'=>$pptk->search(),
		'filter'=>$pptk,
		'columns'=>array(
			'nip',
			'nama',
			'jabatan',
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "
							    $(\"#Kwitansi_pptk\").val(\"$data->id\");
								$(\"#nama_pptk\").val(\"$data->nama\");
								$(\".modal-window\").closeModal();",
					"class"=>"big-button"
				))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
					
				),
			),
		),
));
?>
</div>
</div>

<div class="hidden">
<div id="wrapper-tambah-pa">
<?php //Dialog PA
	$pa = new Pejabat;
	$this->widget('booster.widgets.TbGridView',array(
		'id'=>'pa-grid',
		'type'=>'striped bordered',
		'afterAjaxUpdate'=>'function(id,data) { isi = $("#wrapper-tambah-pa").html(); $(".modal-content").html(isi);  }',
		'dataProvider'=>$pa->search(),
		'filter'=>$pa,
		'columns'=>array(
			'nip',
			'nama',
			'jabatan',
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "
							    $(\"#Kwitansi_pa\").val(\"$data->id\");
								$(\"#nama_pa\").val(\"$data->nama\");
								$(\".modal-window\").closeModal();",
					"class"=>"big-button"
				))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
					
				),
			),
		),
));
?>
</div>
</div>

<div class="hidden">
<div id="wrapper-tambah-akomodasi">
<?php //Dialog Akomodasi
	$akomodasi = new Akomodasi;
	$this->widget('booster.widgets.TbGridView',array(
		'id'=>'akomodasi-grid',
		'type'=>'striped bordered',
		'afterAjaxUpdate'=>'function(id,data) { isi = $("#wrapper-tambah-akomodasi").html(); $(".modal-content").html(isi);  }',
		'dataProvider'=>$akomodasi->search(),
		'filter'=>$akomodasi,
		'columns'=>array(
			'jarak',
			array(
				'class'=>'CDataColumn',
				'name'=>'id_golongan',
				'header'=>'Golongan',
				'type'=>'raw',
				'value'=>'$data->getRelationField("golongan","nama")',
				'filter'=>CHtml::listData(Golongan::model()->findAll(),'id','nama')
			),
			'biaya',
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "
							    $(\"#Kwitansi_akomodasi\").val(\"$data->biaya\");
								$(\".format-money\").trigger(\"keyup\");
								$(\".modal-window\").closeModal();",
					"class"=>"big-button",
				))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
				),
			),
		),
	));
?>
</div>
</div>

<div class="hidden">
<div id="wrapper-tambah-refresentatif">
<?php //Refresentatif Dialog
	$refresentatif = new Refresentatif;
	$this->widget('booster.widgets.TbGridView',array(
		'id'=>'refresentatif-grid',
		'type'=>'striped bordered',
		'afterAjaxUpdate'=>'function(id,data) { isi = $("#wrapper-tambah-refresentatif").html(); $(".modal-content").html(isi);  }',
		'dataProvider'=>$refresentatif->search(),
		'filter'=>$refresentatif,
		'columns'=>array(
			'jarak',
			'biaya',
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "
							    $(\"#Kwitansi_refresentatif\").val(\"$data->biaya\");
                                $(\".format-money\").trigger(\"keyup\");
								$(\".modal-window\").closeModal();",
					"class"=>"big-button"
				))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
				),
			),
		),
	));
?>
</div>
</div>

<div class="hidden">
<div id="wrapper-tambah-uangHarian">
<?php //Uang Harian
	$uangHarian = new uangHarian;
	$this->widget('booster.widgets.TbGridView',array(
		'id'=>'uang-harian-grid',
		'type'=>'striped bordered',
		'afterAjaxUpdate'=>'function(id,data) { isi = $("#wrapper-tambah-uangHarian").html(); $(".modal-content").html(isi);  }',
		'dataProvider'=>$uangHarian->search(),
		'filter'=>$uangHarian,
		'columns'=>array(
			'jarak',
			array(
				'class'=>'CDataColumn',
				'name'=>'id_golongan',
				'header'=>'Golongan',
				'type'=>'raw',
				'value'=>'$data->golongan->nama',
				'filter'=>CHtml::listData(Golongan::model()->findAll(),'id','nama')
			),
			'biaya',
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "
							    $(\"#Kwitansi_uang_harian\").val(\"$data->biaya\");
								$(\"#Kwitansi_uang_harian\").trigger(\"keyup\");
								$(\".format-money\").trigger(\"keyup\");
								$(\".modal-window\").closeModal();",
					"class"=>"big-button",
				))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
				),
			),
		),
	));
?>
</div>
</div>

<script>
// Dialog SPD
function dialogSpd()
{
	$.modal({
		content: $("#wrapper-tambah-spd"),
		title: 'Pilih SPD',
		width: 700,
		buttons: {
			'Close': function(win) { win.closeModal(); }
		}
	});
}
// Dialog Penerima
function dialogPenerima()
{
	$.modal({
		content: $("#wrapper-tambah-penerima"),
		title: 'Pilih Penerima',
		width: 700,
		buttons: {
			'Close': function(win) { win.closeModal(); }
		}
	});
}
// Dialog Bendahara
function dialogBendahara()
{
	$.modal({
		content: $("#wrapper-tambah-bendahara"),
		title: 'Pilih Bendahara',
		width: 700,
		buttons: {
			'Close': function(win) { win.closeModal(); }
		}
	});
}
// Dialog PPTK
function dialogPptk()
{
	$.modal({
		content: $("#wrapper-tambah-pptk"),
		title: 'Pilih PPTK',
		width: 700,
		buttons: {
			'Close': function(win) { win.closeModal(); }
		}
	});
}
// Dialog PA
function dialogPa()
{
	$.modal({
		content: $("#wrapper-tambah-pa"),
		title: 'Pilih PA/KPA',
		width: 700,
		buttons: {
			'Close': function(win) { win.closeModal(); }
		}
	});
}
// Dialog SPD
function dialogAkomodasi()
{
	$.modal({
		content: $("#wrapper-tambah-akomodasi"),
		title: 'Pilih Akomodasi',
		width: 700,
		buttons: {
			'Close': function(win) { win.closeModal(); }
		}
	});
}
// Dialog SPD
function dialogRefresentatif()
{
	$.modal({
		content: $("#wrapper-tambah-refresentatif"),
		title: 'Pilih Refresentatif',
		width: 700,
		buttons: {
			'Close': function(win) { win.closeModal(); }
		}
	});
}
// Dialog UangHarian
function dialogUangHarian()
{
	$.modal({
		content: $("#wrapper-tambah-uangHarian"),
		title: 'Pilih Uang Harian',
		width: 700,
		buttons: {
			'Close': function(win) { win.closeModal(); }
		}
	});
}
</script>