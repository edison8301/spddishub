<?php $this->breadcrumbs=array(
	'Data Kwitansi'=>$this->createUrl('admin'),
	'Detail Kwitansi'=>$this->createUrl('update',array('id'=>$model->id)),
); ?>
<h1>Detail Kwitansi</h1>
<?php $this->widget('booster.widgets.TbButton',array(
		'label' => 'Lihat SPD',
		'buttonType'=>'link',
		'icon'=>'arrow-left',
		'context' => 'primary',
		'url'=>array('spd/view','id'=>$model->id_spd),
		'htmlOptions'=>array('class'=>'big-button')
)); ?>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'label'=>'Sunting',
		'icon'=>'pencil',
		'url'=>array('kwitansi/update','id'=>$model->id),
		'htmlOptions'=>array('class'=>'big-button')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'label'=>'Kelola',
		'icon'=>'list',
		'url'=>array('kwitansi/admin'),
		'htmlOptions'=>array('class'=>'big-button')
)); ?>
<?php $this->widget('booster.widgets.TbButtonGroup',array(
        'context' => 'primary',
        'buttons' => array(
            array(
                'label' => 'Cetak Kwitansi',
				'icon' => 'print',
				'htmlOptions'=>array('class'=>'big-button'),
                'items' => array(
					array('label' => 'Cetak Blanko', 'url' => array('kwitansi/cetakBlanko','id'=>$model->id),'linkOptions'=>array('target'=>'_blank')),
					array('label' => 'Cetak Lengkap', 'url' => array('kwitansi/cetakLengkap','id'=>$model->id),'linkOptions'=>array('target'=>'_blank')),
					'---',
                    array('label' => 'Setting Posisi Kwitansi', 'url' => array('posisiCetak/setting','jenis_surat'=>'2')),
				)
            ),
        ),
)); ?>


<div>&nbsp;</div>

<div class="">	
	<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			'nomor',
			array(
				'label'=>'Tanggal Kwitansi',
				'value'=>Bantu::tanggal($model->tanggal)
			),
			array(
				'label'=>'Nomor SPD',
				'type'=>'raw',
				'value'=>CHtml::encode($model->getRelationField("spd","nomor_spd"))
			),
			'program',
			'kegiatan',
			array(
				'label'=>'Akomodasi',
				'type'=>'raw',
				'value'=>'Rp '.number_format($model->akomodasi, 2,",",".")
			),
			array(
				'label'=>'Refresentatif',
				'type'=>'raw',
				'value'=>'Rp '.number_format($model->refresentatif, 2,",",".")
			),
			array(
				'label'=>'Uang Harian',
				'type'=>'raw',
				'value'=>'Rp '.number_format($model->uang_harian, 2,",",".")
			),
			array(
				'label'=>'Bendahara',
				'type'=>'raw',
				'value'=>$model->getRelationField("bendaharaRelation","nama")
			),
			array(
				'label'=>'PPTK',
				'type'=>'raw',
				'value'=>$model->getRelationField("pptkRelation","nama")
			),
			array(
				'label'=>'PA/KPA',
				'type'=>'raw',
				'value'=>$model->getRelationField("paRelation","nama")
			),
			array(
				'label'=>'BBM',
				'type'=>'raw',
				'value'=>'Rp '.number_format($model->bbm, 2,",",".")
			),
			array(
				'label'=>'Tol',
				'type'=>'raw',
				'value'=>'Rp '.number_format($model->tol, 2,",",".")
			),
			array(
				'label'=>'Tiket',
				'type'=>'raw',
				'value'=>'Rp '.number_format($model->tiket, 2,",",".")
			),
			array(
				'label'=>'Total',
				'type'=>'raw',
				'value'=>'Rp <b>'.number_format($model->total, 2,",",".").'</b>'
			),
		),
)); ?>
</div>