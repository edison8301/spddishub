<?php
$this->breadcrumbs=array(
	'Data Pegawai'=>$this->createUrl('admin'),
);

$this->menu=array(
array('label'=>'List Pegawai','url'=>array('index')),
array('label'=>'Create Pegawai','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('pegawai-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<?php
          /*  $this->widget('bootstrap.widgets.TbButtonGroup', array(
            //'size'=>'small',
            'type'=>'primary', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'buttons'=>array(
            array('label'=>'Menu', 'items'=>array(
					array('label'=>'Unduh Ke PDF','linkOptions'=>array('target'=>'_blank'), 'icon'=>'download-alt', 'url'=>array('export/exportPegawai')),
                )),
            ),
            ));*/
    ?>
<h1>Data Pegawai</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah Pegawai',
		'icon'=>'plus',
		'context'=>'primary',
		'url'=>array('create'),
		'htmlOptions'=>array('class'=>'big-button'),
)); ?>&nbsp;

<?php //echo $this->renderPartial('_form', array('model'=>$model)); ?>

<?php print $this->renderPartial('_admin',array('pegawai'=>$pegawai)); ?>

