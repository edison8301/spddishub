<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'pegawai-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data')
)); ?>

<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'nip',array('class'=>'span5','maxlength'=>255,'placeholder'=>'NIP pegawai 18 karakter','value'=>$model->nip == '' ? '' : $model->getNip())); ?>

	<?php echo $form->textFieldGroup($model,'nama',array('class'=>'span5','maxlength'=>255,'placeholder'=>'Nama pegawai')); ?>
	
	<?php echo $form->datepickerGroup($model, 'tgl_lahir',array('widgetOptions' => array('options'=>array('autoclose'=>true,'format'=>'yyyy-mm-dd')), 'prepend' => '<i class="glyphicon glyphicon-calendar"></i>')); ?>
	
	<?php echo $form->dropDownListGroup($model,'id_golongan',array('widgetOptions'=>array('data'=>CHtml::listData(Golongan::model()->findAll(),'id','nama'),'htmlOptions'=>array('class'=>'span5','empty'=>'-- Pilih Golongan --')))); ?>

	<?php echo $form->textFieldGroup($model,'jabatan',array('class'=>'span5','maxlength'=>255,'placeholder'=>'Jabatan pegawai')); ?>
	
	<?php echo $form->passwordFieldGroup($model,'password',array('class'=>'span5','maxlength'=>255,'placeholder'=>'Password untuk pegawai login')); ?>

	<?php echo $form->labelEx($model,'foto'); ?>
	
	<?php 
		if($model->foto != '')
		{
			print CHtml::image(Yii::app()->request->baseUrl.'/uploads/pegawai/'.$model->foto,'',array('style'=>'width:150px;margin-right:10px;')); 
			$this->widget('booster.widgets.TbButton', array(
				'buttonType'=>'link',
				'context'=>'danger',
				'size'=>'mini',
				'icon'=>'remove white',
				'label'=>'',
				'url'=>array('/pegawai/hapusFoto','id'=>$model->id)
			));
			
		} else {
			print CHtml::image(Yii::app()->request->baseUrl.'/img/no-profile.jpg','',array('style'=>'width:150px'));
		}
	?>		

	<?php echo $form->fileField($model,'foto'); ?>
	
	<?php echo $form->error($model,'foto'); ?>
	
	<?php //echo $form->textFieldRow($model,'tgl_lahir',array('class'=>'span5','maxlength'=>255)); ?>

	<?php //echo $form->datepickerRow($model,'tgl_lahir',array('prepend'=>'<i class="icon-calendar"></i>','options'=>array('autoclose'=>true,'format' => 'yyyy-mm-dd'))); ?>

	<div>&nbsp;</div>


<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
			'htmlOptions'=>array('class'=>'big-button'),
		)); ?>
</div>

<?php $this->endWidget(); ?>
