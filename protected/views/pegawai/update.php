<?php
$this->breadcrumbs=array(
	'Data Pegawai'=>$this->createUrl('admin'),
	'Sunting Pegawai'=>$this->createUrl('update',array('id'=>$model->id)),
);

	$this->menu=array(
	array('label'=>'List Pegawai','url'=>array('index')),
	array('label'=>'Create Pegawai','url'=>array('create')),
	array('label'=>'View Pegawai','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Pegawai','url'=>array('admin')),
	);
	?>

<h1>Sunting Pegawai</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>