<?php
$this->breadcrumbs=array(
	'Data Pegawai'=>$this->createUrl('admin'),
	'Entry Pegawai'=>$this->createUrl('create'),
);

$this->menu=array(
array('label'=>'List Pegawai','url'=>array('index')),
array('label'=>'Manage Pegawai','url'=>array('admin')),
);
?>


<h1>Entry Pegawai</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
