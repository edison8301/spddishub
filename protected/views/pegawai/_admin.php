<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'pegawai-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$pegawai->search(),
		'filter'=>$pegawai,
		'columns'=>array(
			
			array(
				'class'=>'CDataColumn',
				'name'=>'foto',
				'header'=>'Foto',
				'type'=>'raw',
				'value'=>'$data->getFoto()',
				'headerHtmlOptions'=>array('class'=>'img-responsive')
			),
			
			array(
				'class'=>'CDataColumn',
				'name'=>'nip',
				'header'=>'NIP',
				'type'=>'raw',
				'value'=>'$data->getNip()',
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'nama',
				'header'=>'Nama',
				'type'=>'raw',
				'value'=>'$data->nama',
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'id_golongan',
				'header'=>'Golongan',
				'type'=>'raw',
				'value'=>'$data->getRelationField("golongan","nama")',
				'filter'=>CHtml::listData(Golongan::model()->findAll(),'id','nama')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'jabatan',
				'header'=>'Jabatan',
				'type'=>'raw',
				'value'=>'$data->jabatan',
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'tgl_lahir',
				'header'=>'Tanggal Lahir',
				'type'=>'raw',
				'value'=>'$data->getTglLahir()',
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'template'=>'{view} {update}{delete}'
			),
		),
)); ?>