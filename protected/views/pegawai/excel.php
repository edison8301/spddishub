<?php



?>

<h1>Export Excel Pegawai</h1>

<div class="form-actions">
	


<?php $this->widget('bootstrap.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tampilkan Data Export',
		'type'=>'primary',
		'icon'=>'search white',
		'url'=>array('pegawai/excel','tampil'=>1)
)); ?>

<?php if(isset($_GET['tampil']) AND $_GET['tampil']==1) { ?>
<?php $this->widget('bootstrap.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Export Data',
		'type'=>'primary',
		'icon'=>'download-alt white',
		'url'=>array('pegawai/exportExcel')
)); ?>
<?php } ?>

</div>

<div>&nbsp;</div>

<table class="table">
<tr>
	<th>No</th>
	<th>NIP</th>
	<th>Nama</th>
	<th>Golongan</th>
	<th>Jabatan</th>
	<th>Tanggal Lahir</th>
</tr>
<?php if(isset($_GET['tampil']) AND $_GET['tampil']==1) { ?>
<?php $i=1; foreach(Pegawai::model()->findAll() as $data) { ?>
<tr>
	<td><?php print $i; ?></td>
	<td><?php print $data->getNip(); ?></td>
	<td><?php print $data->nama; ?></td>
	<td><?php print $data->golongan->nama; ?></td>
	<td><?php print $data->jabatan; ?></td>
	<td><?php print $data->tgl_lahir; ?></td>
</tr>

<?php $i++; } ?>
<?php } ?>
</table>