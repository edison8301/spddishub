<?php
$this->breadcrumbs=array(
	'Data Pegawai'=>$this->createUrl('admin'),
	'Detail Pegawai'=>$this->createUrl('view',array('id'=>$model->id)),
);

$this->menu=array(
array('label'=>'List Pegawai','url'=>array('index')),
array('label'=>'Create Pegawai','url'=>array('create')),
array('label'=>'Update Pegawai','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Pegawai','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Pegawai','url'=>array('admin')),
);
?>

<h1>Detail Pegawai <?php echo $model->nama; ?></h1>

<?php $this->widget('booster.widgets.TbButton',array('buttonType'=>'link','context'=>'primary','icon'=>'pencil white','label'=>'Sunting Pegawai','url'=>array('pegawai/update','id'=>$model->id),'htmlOptions'=>array('class'=>'big-button'),)); ?>&nbsp;
<?php //$this->widget('booster.widgets.TbButton',array('buttonType'=>'link','type'=>'primary','icon'=>'download-alt white','label'=>'Export Pegawai','url'=>array('pegawai/exportDataPegawai','id'=>$model->id))); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array('buttonType'=>'link','context'=>'primary','icon'=>'list white','label'=>'Master Pegawai','url'=>array('pegawai/admin'),'htmlOptions'=>array('class'=>'big-button'),)); ?>


<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type'=>'striped bordered',
'attributes'=>array(
		'nip',
		'nama',
		array(
			'label'=>'Golongan',
			'value'=>$model->golongan->nama
		),
		'jabatan',
		array(
			'label'=>'Tanggal Lahir',
			'value'=>Yii::app()->dateFormatter->format("dd-MM-yyyy",$model->tgl_lahir)
		),
		array(
			'label'=>'Foto',
			'type'=>'raw',
			'value'=>$model->getFoto()
		),
),
)); 

?>
