<?php
$this->breadcrumbs=array(
	'Golongan'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Golongan','url'=>array('index')),
array('label'=>'Create Golongan','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('golongan-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Kelola Golongan</h1>
<?php
            $this->widget('bootstrap.widgets.TbButtonGroup', array(
            //'size'=>'small',
            'type'=>'primary', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'buttons'=>array(
            array('label'=>'Menu', 'items'=>array(
					array('label'=>'Tambah Golongan', 'icon'=>'plus', 'url'=>array('golongan/create')),
				)),
            ),
            ));
?>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'golongan-grid',
'type'=>'striped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'nama',
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
