<?php
$this->breadcrumbs=array(
	'Golongan'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Golongan','url'=>array('index')),
	array('label'=>'Create Golongan','url'=>array('create')),
	array('label'=>'View Golongan','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Golongan','url'=>array('admin')),
	);
	?>

	<h1>Sunting Golongan</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>