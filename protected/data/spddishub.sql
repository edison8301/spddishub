-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 29 Mei 2015 pada 08.50
-- Versi Server: 5.5.42-37.1
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `spddishu_data`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `access`
--

CREATE TABLE IF NOT EXISTS `access` (
  `id` int(11) NOT NULL,
  `nama_controller` varchar(255) NOT NULL,
  `nama_action` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `access`
--

INSERT INTO `access` (`id`, `nama_controller`, `nama_action`) VALUES
(1, 'akomodasi', 'create'),
(2, 'akomodasi', 'update'),
(3, 'akomodasi', 'delete'),
(4, 'pegawai', 'create'),
(5, 'pegawai', 'update'),
(6, 'pegawai', 'delete'),
(7, 'user', 'create'),
(8, 'user', 'update'),
(9, 'kwitansi', 'input'),
(10, 'kwitansi', 'update'),
(11, 'lhp', 'input'),
(12, 'lhp', 'update'),
(13, 'lhp', 'delete'),
(14, 'visum', 'create'),
(15, 'visum', 'update'),
(16, 'visum', 'delete'),
(17, 'kwitansi', 'delete'),
(18, 'ssh', 'create'),
(19, 'ssh', 'update'),
(20, 'ssh', 'delete'),
(21, 'user', 'delete'),
(22, 'role', 'create'),
(23, 'role', 'update'),
(24, 'role', 'delete'),
(25, 'pejabat', 'create'),
(26, 'pejabat', 'update'),
(27, 'pejabat', 'delete'),
(28, 'role', 'access'),
(29, 'spd', 'inputSpt'),
(30, 'spd', 'updateSpt'),
(31, 'spd', 'cetakSpd'),
(32, 'rill', 'input'),
(33, 'rill', 'update'),
(34, 'rill', 'delete'),
(45, 'spd', 'inputSpd'),
(46, 'spd', 'updateSpd'),
(47, 'spd', 'cetakSpt'),
(48, 'lhp', 'cetak'),
(49, 'kwitansi', 'cetak');

-- --------------------------------------------------------

--
-- Struktur dari tabel `akomodasi`
--

CREATE TABLE IF NOT EXISTS `akomodasi` (
  `id` int(11) NOT NULL,
  `jarak` varchar(255) DEFAULT NULL,
  `id_golongan` int(11) DEFAULT NULL,
  `biaya` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `akomodasi`
--

INSERT INTO `akomodasi` (`id`, `jarak`, `id_golongan`, `biaya`) VALUES
(1, 'DKI Jakarta', 1, '350000'),
(2, 'DKI Jakarta', 2, '400000'),
(3, 'DKI Jakarta', 3, '500000'),
(4, 'DKI Jakarta', 4, '650000'),
(5, 'Pulau Jawa', 1, '300000'),
(6, 'Pulau Jawa', 2, '400000'),
(7, 'Pulau Jawa', 3, '500000'),
(8, 'Pulau Jawa', 4, '830000'),
(9, 'Luar Pulau Jawa', 1, '500000'),
(10, 'Luar Pulau Jawa', 2, '500000'),
(11, 'Luar Pulau Jawa', 4, '800000'),
(12, 'Luar Pulau Jawa', 4, '1100000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `golongan`
--

CREATE TABLE IF NOT EXISTS `golongan` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `golongan`
--

INSERT INTO `golongan` (`id`, `nama`) VALUES
(1, 'Juru Muda, I/a'),
(2, 'Juru Muda Tk I, I/b'),
(3, 'Juru, I/c'),
(4, 'Juru Tingkat I, I/d'),
(5, 'Pengatur Muda, II/a'),
(6, 'Pengatur Muda Tk I, II/b'),
(7, 'Pengatur, II/c'),
(8, 'Pengatur Tk I, II/d'),
(9, 'Penata Muda, III/a'),
(10, 'Penata Muda Tk I, III/b'),
(11, 'Penata, III/c'),
(12, 'Penata Tk I, III/d'),
(13, 'Pembina, IV/a'),
(14, 'Pembina Tk I, IV/b'),
(15, 'Pembina Utama Muda, IV/c'),
(16, 'Pembina Utama Madya, IV/d');

-- --------------------------------------------------------

--
-- Struktur dari tabel `koring`
--

CREATE TABLE IF NOT EXISTS `koring` (
  `id` int(11) NOT NULL,
  `koring` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `koring`
--

INSERT INTO `koring` (`id`, `koring`) VALUES
(1, '1.07.1.07.01.01.19.5.2.2.15.01'),
(2, '1.07.1.07.01.01.19.5.2.2.15.02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kwitansi`
--

CREATE TABLE IF NOT EXISTS `kwitansi` (
  `id` int(11) NOT NULL,
  `nomor` varchar(255) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `penerima` int(11) DEFAULT NULL,
  `program` varchar(255) DEFAULT NULL,
  `kegiatan` varchar(255) DEFAULT NULL,
  `id_spd` int(11) DEFAULT NULL,
  `lama` int(11) DEFAULT NULL,
  `akomodasi` varchar(255) DEFAULT NULL,
  `refresentatif` varchar(255) DEFAULT NULL,
  `uang_harian` varchar(255) DEFAULT NULL,
  `bendahara` int(11) DEFAULT NULL,
  `pptk` int(11) DEFAULT NULL,
  `pa` int(11) DEFAULT NULL,
  `bbm` varchar(255) DEFAULT NULL,
  `tol` varchar(255) DEFAULT NULL,
  `tiket` varchar(255) DEFAULT NULL,
  `total` varchar(255) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `kwitansi`
--

INSERT INTO `kwitansi` (`id`, `nomor`, `tanggal`, `penerima`, `program`, `kegiatan`, `id_spd`, `lama`, `akomodasi`, `refresentatif`, `uang_harian`, `bendahara`, `pptk`, `pa`, `bbm`, `tol`, `tiket`, `total`) VALUES
(13, 'test', NULL, 10, '', '', 46, 2, '350000', '24000', '24000', 3, 3, 6, '12000', '12000', '12000', '458000'),
(21, '132/3423/adsf', NULL, 14, '', '', 44, NULL, '400000', '100000', '150000', 2, 1, 1, '0', '0', '0', '1250000'),
(15, 'KW/01', NULL, 1, '', '', 46, 2, '350000', '100000', '150000', 3, 4, 5, '120000', '300000', '150000', '1320000'),
(16, 'KW/02', NULL, 1, '', '', 46, NULL, '350000', '100000', '150000', 3, 4, 3, '0', '0', '0', '600000'),
(19, '111', NULL, 13, 'Test Program', 'Test Kegiatan', 53, NULL, '300000', '100000', '275000', 1, 1, 1, '100000', '100000', '100000', '975000'),
(20, '123123', NULL, 17, 'asnd', 'lansd', 54, NULL, '300000', '100000', '150000', 1, 1, 2, '1000', '100000', '100000', '751000'),
(22, '', NULL, NULL, '', '', NULL, NULL, '0', '0', '0', NULL, NULL, NULL, '0', '0', '0', '0'),
(23, 'KW/2015/V/01', '2015-05-18', 123, '', '', 61, NULL, '400000', '100000', '200000', 1, 1, 1, '0', '0', '0', '900000'),
(25, 'KW/2015/V/03', '2015-05-28', 18, '', '', 62, NULL, '500000', '100000', '225000', 1, 2, 2, '0', '0', '0', '1050000'),
(27, 'KW/2015/V/03', '2015-05-30', 15, 'Program Kita', 'Kegiatan Kita', 65, NULL, '400000', '125000', '250000', 2, 2, 3, '150000', '200000', '50000', '1425000'),
(28, '', NULL, 16, '', '', 65, NULL, '400000', '0', '250000', NULL, NULL, NULL, '0', '0', '0', '900000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `lhp`
--

CREATE TABLE IF NOT EXISTS `lhp` (
  `id` int(11) NOT NULL,
  `id_pegawai` int(11) DEFAULT NULL,
  `id_spd` int(11) DEFAULT NULL,
  `tanggal_lhp` date DEFAULT NULL,
  `hasil_kegiatan_1` text,
  `hasil_kegiatan_2` text,
  `hasil_kegiatan_3` text
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `lhp`
--

INSERT INTO `lhp` (`id`, `id_pegawai`, `id_spd`, `tanggal_lhp`, `hasil_kegiatan_1`, `hasil_kegiatan_2`, `hasil_kegiatan_3`) VALUES
(1, NULL, 46, NULL, NULL, NULL, NULL),
(4, 13, 53, '2015-04-17', 'klasmd', 'aklsmd', 'asklmd'),
(5, 17, 54, '2015-04-17', 'asd', 'asd', 'asd'),
(6, 14, 44, '2015-05-18', 'Hasil kegiatan 1', 'Hasil kegiatan 2', 'Hasil kegiatan 3'),
(8, 123, 61, '2015-05-29', 'Hasil kegiatan 1', 'Hasil kegiatan 2', 'Hasil kegiatan 3'),
(9, 18, 62, '2015-05-27', 'Hasil kegiatan 1', 'Hasil kegiatan 2', 'Hasil kegiatan 3'),
(10, 21, 62, '2015-05-27', 'Hasil kegiatan 1', '', ''),
(12, 15, 65, '2015-05-29', 'Hasil kegiatan 1', 'Hasil kegiatan 2', 'Hasil kegiatan 3\r\n');

-- --------------------------------------------------------

--
-- Struktur dari tabel `login_history`
--

CREATE TABLE IF NOT EXISTS `login_history` (
  `id` int(11) NOT NULL,
  `model` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `waktu_login` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `login_history`
--

INSERT INTO `login_history` (`id`, `model`, `user_id`, `waktu_login`) VALUES
(1, 'pegawai', 11, '2015-03-30 13:51:57'),
(2, 'user', 1, '2015-03-30 13:52:22'),
(3, 'user', 1, '2015-03-31 09:25:30'),
(4, 'user', 1, '2015-03-31 11:07:14'),
(5, 'user', 1, '2015-04-01 09:24:43'),
(6, 'user', 1, '2015-04-03 15:02:01'),
(7, 'user', 1, '2015-04-03 15:03:14'),
(8, 'user', 1, '2015-04-08 10:45:21'),
(9, 'user', 1, '2015-04-08 11:27:01'),
(10, 'user', 1, '2015-04-08 16:38:06'),
(11, 'user', 1, '2015-04-09 08:49:11'),
(12, 'user', 1, '2015-04-09 10:04:00'),
(13, 'user', 1, '2015-04-09 10:26:22'),
(14, 'user', 1, '2015-04-09 10:41:26'),
(15, 'user', 1, '2015-04-09 14:16:22'),
(16, 'user', 1, '2015-04-09 15:50:58'),
(17, 'user', 1, '2015-04-09 15:54:28'),
(18, 'user', 1, '2015-04-10 09:47:03'),
(19, 'user', 1, '2015-04-10 10:14:03'),
(20, 'user', 1, '2015-04-10 16:13:55'),
(21, 'user', 1, '2015-04-13 09:04:50'),
(22, 'user', 1, '2015-04-13 09:11:15'),
(23, 'user', 1, '2015-04-13 09:22:41'),
(24, 'user', 1, '2015-04-13 12:43:40'),
(25, 'user', 1, '2015-04-13 14:18:49'),
(26, 'user', 1, '2015-04-14 09:11:14'),
(27, 'user', 1, '2015-04-14 09:13:42'),
(28, 'user', 1, '2015-04-14 15:53:06'),
(29, 'user', 1, '2015-04-15 09:17:14'),
(30, 'user', 1, '2015-04-16 12:22:06'),
(31, 'user', 1, '2015-04-16 12:33:24'),
(32, 'user', 1, '2015-05-18 06:42:39'),
(33, 'user', 1, '2015-05-18 07:01:49'),
(34, 'user', 1, '2015-05-18 10:46:18'),
(35, 'user', 1, '2015-05-18 10:54:09'),
(36, 'user', 1, '2015-05-18 11:02:00'),
(37, 'user', 1, '2015-05-18 12:02:10'),
(38, 'user', 1, '2015-05-27 14:00:45'),
(39, 'user', 1, '2015-05-27 20:07:57'),
(40, 'user', 1, '2015-05-28 16:01:23'),
(41, 'user', 1, '2015-05-29 01:34:10'),
(42, 'user', 1, '2015-05-29 12:38:39'),
(43, 'user', 1, '2015-05-29 12:39:26'),
(44, 'user', 1, '2015-05-29 13:08:50'),
(45, 'user', 1, '2015-05-29 13:53:25'),
(46, 'user', 1, '2015-05-29 16:18:12'),
(47, 'user', 1, '2015-05-29 16:19:17'),
(48, 'user', 1, '2015-05-29 20:49:02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE IF NOT EXISTS `pegawai` (
  `id` int(11) NOT NULL,
  `nip` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `id_golongan` int(11) DEFAULT NULL,
  `jabatan` varchar(255) DEFAULT NULL,
  `tgl_lahir` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`id`, `nip`, `password`, `nama`, `id_golongan`, `jabatan`, `tgl_lahir`, `foto`) VALUES
(14, '196609161991081001', 'bismillah', 'Drs. ODI BUDIONO, MM', 15, 'Kepala Dinas', '1966-09-16', NULL),
(15, '196612081986031005', 'bismillah', 'H. RUDIYANTO, S.IP', 14, 'Sekretaris Dinas', '1966-12-08', NULL),
(16, '196207061986031017', 'bismillah', 'Drs. EPI SAPARUDIN', 13, 'Kabid Kominfo', '1962-07-06', NULL),
(17, '196709091991031005', 'bismillah', 'BENNY YUARSA, ATD.M.Si', 13, 'Kabid Perhubungan Laut', '1967-09-09', NULL),
(18, '196804201992081001', 'bismillah', 'ENTOL M. RUDY AFFANDI, S.Sos. MM', 13, 'Kabid Kes Teksar Dan Prasarana Darat', '1968-04-20', NULL),
(19, '196907021993011003', 'bismillah', 'H. AGUS HERLAMBANG, ATD.MT. ', 12, 'Kabid LLAJ', '1969-07-02', NULL),
(20, '197007191998031006', 'bismillah', 'H. M. EDY F. LUTFIE,ST.MM.', 13, 'Kasubag Keuangan', '1970-07-19', NULL),
(21, '197111211996021001', 'bismillah', 'DAMAR WIGHUTOMO, S.SIT.M.Si', 12, 'Kasi Angkutan Jalan', '1971-11-21', NULL),
(22, '196012121986031031', 'bismillah', 'H. AL'' AMIN, SH.', 12, ' Penguji Kendaraan Bermotor Penyelia', '1960-12-12', NULL),
(23, '196612121991021001', 'bismillah', 'H.YUSAN W, S.Sos, M.Si', 12, 'Kasi Lalu Lintas & Kepelabuhanan', '1966-12-12', NULL),
(24, '196903101997031008', 'bismillah', 'JUDI, SE, M.Si', 12, 'Kasi Kesteksar Darat', '1969-03-10', NULL),
(25, '196712291998031004', 'bismillah', 'RD.BAMBANG SUCIPTO.PH.SH.', 12, 'Penguji Kendaraan Bermotor ', '1967-12-29', NULL),
(26, '197604081998031007', 'bismillah', 'AGUS PRIYATNO,SE.M.Si', 12, 'Kasubag Program Dan Evaluasi', '1976-04-08', NULL),
(27, '197808302006041006', 'bismillah', 'AGUS YASA WIRAWAN,ST,MM', 12, 'Kasi Aplikasi Telematika', '1978-09-30', NULL),
(28, '196406081991031010', 'Bismillah', 'LUTFI, S.Sos', 11, 'Kasi Keselamatan Pelayaran', '1964-06-08', NULL),
(29, '197508102005012013', 'bismillah', 'NAPSIYAH.SE.', 11, 'Pelaksana Dishub Kominfo', '1975-08-10', NULL),
(30, '198309182006042009', 'bismillah', 'AYU MIRA K, S.Sos.M.Si', 11, 'Kasi Postel', '1983-03-18', NULL),
(31, '197808082000032001', 'bismillah', 'DIANA EVANIA, A.Md.SE.', 11, 'Kasi Lalu Lintas Jalan', '1979-08-08', NULL),
(32, '197809272006041006', 'bismillah', 'ASEP SAEFULLAH, S.Sos. M.Si', 11, ' Kasubag Umum dan Kepegawaian', '1978-09-27', NULL),
(33, '197309082005011012', 'bismillah', 'IMAM TAUFIK.SE.', 11, 'Kasi ASDP', '1973-09-08', NULL),
(34, '196902061993121001', 'bismillah', 'SUTRISNO, SH', 11, 'Penguji Kendaraan Bermotor Pelnyelia', '1969-02-06', NULL),
(35, '196506272003121001', 'bismillah', 'BUDI SANTOSA, SH', 11, 'Penguji Kendaraan Bermotor Pelnyelia', '1965-06-27', NULL),
(36, '196101031991101001', 'bismillah', 'SARIFUDIN', 10, 'Kasi PJU', '1961-01-03', NULL),
(37, '196704102000031006', 'bismillah', 'H. NAZILI, S.Sos', 10, 'Kasi Diseminasi Informasi', '1967-04-10', NULL),
(38, '197805272009021001', 'bismillah', 'OPAN BAEHAKI, ST', 10, 'Kasi Termiknal dan Parkir', '1978-05-27', NULL),
(39, '197706292009021001', 'bismillah', 'EKO WAHYU WINANTO, SE', 10, 'Pelaksana seksi lalu lintas jalan', '1977-06-29', NULL),
(40, '196002111981031004', 'bismillah', 'RUSDI RAHMAT', 10, 'Pelaksana  Kes.Pel', '1960-02-11', NULL),
(41, '196409182006041005', 'bismillah', 'SAMANI, S.IP', 10, ' Kasi Dalops', '1964-09-18', NULL),
(42, '196310212006041001', 'bismillah', 'MASKUR, S.IP', 10, 'Pelaksana Seksi Dal Ops', '1963-10-21', NULL),
(43, '198005212010011013', 'bismillah', 'CHRISTIANSYAH PAGUA AMRAN,ST.M.Si', 10, 'Pelaksana Subag Program dan evaluasi', '1980-05-21', NULL),
(44, '198301232010011009', 'bismillah', 'ARIS HABIBI,ST.M.Si', 10, 'Pelaksana Subag Program dan evaluasi', '1983-01-23', NULL),
(45, '195808271994011001', 'bismillah', 'H. RD. ACHMAD YUSEP', 10, 'Pelaksana Seksi kes  Teksar Darat', '1958-09-27', NULL),
(46, '196404131984021002', 'bismillah', 'A. HALIMI', 10, 'Pelaksana  Kes.Pel', '1964-04-13', NULL),
(47, '196703082007011010', 'bismillah', 'HERYANTO, S.IP', 10, 'Pelaksana Subag Umum & Kepegawaian', '1967-03-08', NULL),
(48, '197602202011011001', 'bismillah', 'DONNY BRILLIANT, ST', 9, 'Pelaksana Seksi  Terminal dan parkir', '1976-02-20', NULL),
(49, '198105092011012003', 'bismillah', 'AMBAR ASMA WIJAYATI, SE', 9, 'Pelaksana Subag Program dan evaluasi', '1981-05-09', NULL),
(50, '197506012008011004', 'bismillah', 'BOYATNO, S.IP', 9, 'Pelaksana Seksi Lala Laut  &  Kepelabuhan ', '1975-06-01', NULL),
(51, '197307052008011017', 'bismillah', 'ABDUL HAMID, S.IP', 9, 'Pelaksana Seksi Dal Ops', '1973-07-05', NULL),
(52, '196201012006041010', 'bismillah', 'UJANG HASANUDIN, S.IP', 9, 'Pelaksana  Kes.Pel', '1962-01-01', NULL),
(53, '197504062008011008', 'bismillah', 'RONI SUSANTO, S.IP', 9, 'Pelaksana Subag Keuangan', '1975-04-06', NULL),
(54, '197211202003121002', 'bismillah', 'ALEXIUS SUHARYONO.Amd.', 9, 'Pelaksana  Kes.Pel', '1972-11-20', NULL),
(55, '197007152007011019', 'bismillah', 'CECEP HERMAWAN, S.Sos', 9, 'Pelaksana Seksi Terminal dan Parkir', '1970-07-15', NULL),
(56, '197711102008011004', 'bismillah', 'SANDI JAYA, SE', 9, 'Pelaksana Seksi Apilkasi Telematika', '1977-11-10', NULL),
(57, '198109072008011006', 'bismillah', 'ZUL MELAZ ARDIANSYAH, S.Sos', 9, 'Pelaksana Seksi PJU', '1981-09-07', NULL),
(58, '196203081981051001', 'bismillah', 'KOSWARA', 9, 'Pelaksana Seksi Dal Ops', '1962-03-08', NULL),
(59, '196109141985011002', 'bismillah', 'H. HASAN', 9, 'Pelaksana Seksi PJU', '1961-09-14', NULL),
(60, '196301021992101001', 'bismillah', 'UN MULYADI', 9, 'Pelaksana Seksi Terminal dan Parkir', '1963-01-02', NULL),
(61, '197902132010011005', 'bismillah', 'IRWAN ISMAWANDANI, SE', 9, 'Pelaksana Subag Umum & Kepegawaian', '1979-02-13', NULL),
(62, '197805212011011001', 'bismillah', 'YAYAN DANA ATMAJA, SE', 9, 'Pelaksana Seksi ASDP', '1978-05-21', NULL),
(63, '197508052009021001', 'bismillah', 'RASIMAN, SE', 9, 'Pelaksana Seksi Terminal dan parkir', '1975-08-05', NULL),
(64, '198401092015021001', 'bismillah', 'HENDRA PRAHARAN S.KOM', 9, 'Pelaksana Seksi Aptel', '1984-01-09', NULL),
(65, '196802081999012001', 'bismillah', 'AFIAH AFIFI', 8, 'Pelaksana Subag Keuangan', '1968-02-08', NULL),
(66, '196312282007011009', 'bismillah', 'WAWAN IRAWAN, A.Md', 8, 'Pelaksana Seksi ASDP', '1963-12-28', NULL),
(67, '196503081991031007', 'bismillah', 'ZAKARIA', 8, 'Pelaksana Seksi Terminal dan parkir', '1965-03-08', NULL),
(68, '198004122010011010', 'bismillah', 'LUKMANUL HAKIM,A.Md', 8, 'Pelaksana Seksi Lala Laut  &  Kepelabuhan ', '1980-04-12', NULL),
(69, '198302112010012007', 'bismillah', 'PURWANTI,A.Md', 8, 'Pelaksana Seksi Desiminasi imformasi', '1983-02-11', NULL),
(70, '198701082010011001', 'bismillah', 'RHOMY BUDIANWAR,A.Md', 8, 'Pelaksana Seksi Postel', '1987-01-08', NULL),
(71, '198312142010012016', 'bismillah', 'ANGGI PURNAMA RATIH, A.Md', 8, 'Pelaksana Seksi kes  Teksar Darat', '1983-12-14', NULL),
(72, '196712312006041083', 'bismillah', 'BEBEN KUSNARA', 7, 'Penguji Kendaraan Bermotor Pelaksana', '1967-12-31', NULL),
(73, '196207031988121001', 'bismillah', 'LAMRI', 7, 'Pelaksana Seksi Terminal dan parkir', '1962-07-03', NULL),
(74, '196010072006041003', 'bismillah', 'HARYAWAN', 7, 'Pelaksana Subag Keuangan', '1960-10-07', NULL),
(75, '196205102006042001', 'bismillah', 'ISMAWARTI', 7, 'Pelaksana Subag Umum', '1962-05-10', NULL),
(76, '196211252006041002', 'bismillah', 'ZULKIFLI', 7, 'Pelaksana Seksi Dal Ops', '1962-11-25', NULL),
(77, '196803182006041002', 'bismillah', 'A N I S', 7, 'Pelaksana Seksi Dal Ops', '1968-03-18', NULL),
(78, '196503232006041006', 'bismillah', 'WAN AHMAD SUHAILI', 7, 'Pelaksana Seksi Dal Ops', '1965-03-23', NULL),
(79, '197105182008011005', 'bismillah', 'SUNARSO, A.Md', 7, 'Pelaksana Seksi Dal Ops', '1971-05-18', NULL),
(80, '197710132008011006', 'bismillah', 'ADE SUSAN, A.Md', 7, 'Pelaksana Seksi Dal Ops', '1977-10-13', NULL),
(81, '196906052007011029', 'biamillah', 'ZAINUS SOLIHIN', 6, 'Pelaksana Seksi Postel', '1969-06-05', NULL),
(82, '197203032007011014', 'bismillah', 'RACHMAT', 6, 'Pelaksana Seksi Dal Ops', '1972-03-03', NULL),
(83, '197006102007011024', 'bismillah', 'SAMSURI', 6, 'Pelaksana Seksi Terminal dan parkir', '1970-06-10', NULL),
(84, '196802092007011011', 'bismillah', 'WAWAN DERMAWAN', 6, 'Pelaksana Seksi Dal Ops', '1968-02-09', NULL),
(85, '197210022007011011', 'bismillah', 'ONI SAHRONI', 6, 'Pelaksana Seksi Dal Ops', '1972-10-02', NULL),
(86, '196411212007011001', 'bismillah', 'JUANDI', 6, 'Pelaksana Seksi Dal Ops', '1964-11-21', NULL),
(87, '196612172007011007', 'bismillah', 'MAMAN SUPRIATNA', 6, 'Pelaksana seksi lalu lintas jalan', '1966-12-17', NULL),
(88, '196801132007011009', 'bismillah', 'DADAN SUDRAJAT', 6, 'Pelaksana Seksi Terminal dan parkir', '1968-01-13', NULL),
(89, '196309152007011006', 'bismillah', 'RADEN HIDAYAT', 6, 'Pelaksana Seksi Dal Ops', '1963-09-15', NULL),
(90, '196406092007011005', 'bismillah', 'HAMAMI', 6, 'Pelaksana Seksi Dal Ops', '1964-06-09', NULL),
(91, '196508192007011005', 'bismillah', 'GUN GUN GUNAWAN', 6, 'Pelaksana Seksi Dal Ops', '1965-08-19', NULL),
(92, '196408202007011004', 'bismillah', 'H. HERMAN', 6, 'Pelaksana seksi Angkutan jalan', '1964-08-20', NULL),
(93, '196508012007011011', 'bismillah', 'SURYANA', 6, 'Pelaksana Seksi Dal Ops', '1965-08-01', NULL),
(94, '196505112007011004', 'bismillah', 'YAYAN SOPYAN', 6, 'Pelaksana Seksi PJU', '1965-05-11', NULL),
(95, '196607022007011010', 'bismillah', 'TB. TETENG MUGNI', 6, 'Pelaksana Seksi PJU', '1966-07-02', NULL),
(96, '197007222007011011', 'bismillah', 'YULISAR AKHRIL', 6, 'Pelaksana Seksi Terminal dan parkir', '1970-07-22', NULL),
(97, '197008182007011015', 'bismillah', 'AGUS SANTONI', 6, 'Pelaksana Seksi Dal Ops', '1970-08-18', NULL),
(98, '196707292007011012', 'bismillah', 'MUHIMIN', 6, 'Pelaksana Seksi Dal Ops', '1967-07-29', NULL),
(99, '197410052008011005', 'bismillah', 'SIWAN PRIHATNA', 6, 'Pelaksana Seksi Terminal dan parkir', '1974-10-05', NULL),
(100, '197609252008011005', 'bismillah', 'HANAFI', 6, 'Pelaksana Seksi Dal Ops', '1976-09-25', NULL),
(101, '197603242008011003', 'bismillah', 'ROBI HADIAN', 6, 'Pelaksana Seksi Terminal dan parkir', '1976-03-24', NULL),
(102, '197708292008012003', 'bismillah', 'TINA AGUSTINA', 6, 'Pelaksana Subag Keuangan', '1977-08-29', NULL),
(103, '197201012008011012', 'bismillah', 'ENTIS SUJANA', 6, 'Pelaksana Seksi Dal Ops', '1972-01-01', NULL),
(104, '196903032008011006', 'bismillah', 'AKHMAD DERAJAT', 6, 'Pelaksana Seksi PJU', '1969-03-03', NULL),
(105, '196012052006041008', 'bismillah', 'DEDE HERMANTO', 6, 'Pelaksana Seksi Dal Ops', '1960-12-05', NULL),
(106, '196505222007011008', 'bismillah', 'DODI KUSPRIADI', 6, 'Pelaksana Seksi LaluLintas jalan', '1965-05-22', NULL),
(107, '196208242007011003', 'bismillah', 'AMAN SETIA NURJAMAN', 6, 'Pelaksana Subag Keuangan', '1962-09-24', NULL),
(108, '196209212007011004', 'bismillah', 'HASBULLAH', 5, 'Pelaksana Seksi  Diseminasi', '1962-09-21', NULL),
(109, '197304082007011017', 'bismillah', 'JAHIDI', 5, 'Pelaksana Seksi Dal Ops', '1973-04-08', NULL),
(110, '196310112014051001', 'bismillah', 'YANA HERYANA', 5, 'Pelaksana Seksi Dal Ops', '1963-10-11', NULL),
(111, '197511102014051001', 'bismillah', 'JAJAT SUDRAJAT', 5, 'Pelaksana Seksi Dal Ops', '1975-04-08', NULL),
(112, '197411132014051001', 'bismillah', 'ENTIS SUTISNA', 5, 'Pelaksana Terminal dan Parkir', '1974-11-13', NULL),
(113, '197709012014051001', 'bismillah', 'WENDY FITRIADI', 5, 'Pelaksana PJU', '1977-09-10', NULL),
(114, '198106262014051001', 'bismillah', 'DENDI RACHMADI', 5, 'Pelaksana PJU', '1981-06-26', NULL),
(115, '198007052014061004', 'bismillah', 'TATANG SUDRAJAT', 5, 'Pelaksana  Kes.Pel', '1980-07-05', NULL),
(116, '198609052014061001', 'bismillah', 'ARIF RACHMAN HAKIM', 5, 'Pelaksana  Kes.Pel', '1986-09-05', NULL),
(117, '197001022007011017', 'bismillah', 'AHYADI', 4, 'Pelaksana Seksi kes  Teksar Darat', '1970-01-02', NULL),
(118, '197002172014051001', 'bismillah', 'SARTA', 3, 'Pelaksana Terminal dan Parkir', '1970-02-17', NULL),
(119, '196301162006041003', 'bismillah', 'TUMIN', 3, 'Pelaksana Seksi Terminal dan parkir', '1963-01-16', NULL),
(120, '196604062007011013', 'bismillah', 'DARWIN', 2, 'Pelaksana Subag Umum', '1966-04-06', NULL),
(121, '198106042014061001', 'bismillah', 'JAENI', 1, 'Pelaksana Subag Umum', '1981-06-04', NULL),
(123, '195404071975011001', 'bisabisa', 'Thomas Alfa Edison 1', 11, 'Programmer', '1986-06-03', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pejabat`
--

CREATE TABLE IF NOT EXISTS `pejabat` (
  `id` int(11) NOT NULL,
  `nip` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `jabatan` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `pejabat`
--

INSERT INTO `pejabat` (`id`, `nip`, `nama`, `jabatan`, `password`, `foto`) VALUES
(1, '196609161991081001', 'Drs. ODI BUDIONO, MM', 'Dishubkominfo Kab. Serang', NULL, NULL),
(2, '111111111111111119', 'Aditia Budiman', 'Kadis', NULL, NULL),
(3, '196207061986031017', 'Thomas ', 'Kabag', NULL, ''),
(4, '196207061986031012', 'Sekretaris Daerah Kabupaten Serang', 'Sekretaris Daerah Kabupaten Serang', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengikut`
--

CREATE TABLE IF NOT EXISTS `pengikut` (
  `id` int(11) NOT NULL,
  `id_spd` int(11) DEFAULT NULL,
  `id_pegawai` int(11) DEFAULT NULL,
  `aktif` int(11) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `pengikut`
--

INSERT INTO `pengikut` (`id`, `id_spd`, `id_pegawai`, `aktif`) VALUES
(83, 47, NULL, 0),
(84, 47, NULL, 0),
(85, 47, NULL, 0),
(86, 47, NULL, 0),
(87, 47, NULL, 0),
(88, 47, NULL, 0),
(92, 48, NULL, 0),
(93, 48, NULL, 0),
(94, 48, NULL, 0),
(95, 48, NULL, 0),
(96, 48, NULL, 0),
(97, 48, NULL, 0),
(109, 53, 13, 0),
(114, 54, 17, 0),
(115, 59, 16, 0),
(116, 59, 14, 0),
(117, 44, 14, 0),
(118, 61, 123, 0),
(119, 62, 18, 0),
(120, 62, 21, 0),
(123, 65, 15, 0),
(124, 65, 16, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `perjalanan`
--

CREATE TABLE IF NOT EXISTS `perjalanan` (
  `id` int(11) NOT NULL,
  `id_spd` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `aktif` int(11) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=404 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `perjalanan`
--

INSERT INTO `perjalanan` (`id`, `id_spd`, `id_pegawai`, `tanggal`, `aktif`) VALUES
(61, 46, 10, '2015-04-08', 1),
(62, 46, 5, '2014-04-08', 0),
(63, 46, 4, '2014-04-09', 0),
(64, 46, 5, '2014-04-09', 0),
(65, 46, 4, '2014-04-10', 0),
(66, 46, 5, '2014-04-10', 0),
(67, 46, 4, '2014-04-11', 0),
(68, 46, 5, '2014-04-11', 0),
(69, 46, 4, '2014-04-12', 0),
(70, 46, 5, '2014-04-12', 0),
(71, 46, 4, '2014-04-08', 0),
(72, 46, 5, '2014-04-08', 0),
(73, 46, 4, '2014-04-09', 0),
(74, 46, 5, '2014-04-09', 0),
(75, 46, 4, '2014-04-10', 0),
(76, 46, 5, '2014-04-10', 0),
(77, 46, 4, '2014-04-11', 0),
(78, 46, 5, '2014-04-11', 0),
(79, 46, 4, '2014-04-12', 0),
(80, 46, 5, '2014-04-12', 0),
(81, 46, 4, '2014-04-08', 0),
(82, 46, 5, '2014-04-08', 0),
(83, 46, 4, '2014-04-09', 0),
(84, 46, 5, '2014-04-09', 0),
(85, 46, 4, '2014-04-10', 0),
(86, 46, 5, '2014-04-10', 0),
(87, 46, 4, '2014-04-11', 0),
(88, 46, 5, '2014-04-11', 0),
(89, 46, 4, '2014-04-12', 0),
(90, 46, 5, '2014-04-12', 0),
(91, 46, 4, '2014-04-08', 0),
(92, 46, 5, '2014-04-08', 0),
(93, 46, 4, '2014-04-09', 0),
(94, 46, 5, '2014-04-09', 0),
(95, 46, 4, '2014-04-10', 0),
(96, 46, 5, '2014-04-10', 0),
(97, 46, 4, '2014-04-11', 0),
(98, 46, 5, '2014-04-11', 0),
(99, 46, 4, '2014-04-12', 0),
(100, 46, 5, '2014-04-12', 0),
(101, 46, 4, '2014-04-08', 0),
(102, 46, 5, '2014-04-08', 0),
(103, 46, 4, '2014-04-09', 0),
(104, 46, 5, '2014-04-09', 0),
(105, 46, 4, '2014-04-10', 0),
(106, 46, 5, '2014-04-10', 0),
(107, 46, 4, '2014-04-11', 0),
(108, 46, 5, '2014-04-11', 0),
(109, 46, 4, '2014-04-12', 0),
(110, 46, 5, '2014-04-12', 0),
(111, 46, 4, '2014-04-08', 0),
(112, 46, 5, '2014-04-08', 0),
(113, 46, 4, '2014-04-09', 0),
(114, 46, 5, '2014-04-09', 0),
(115, 46, 4, '2014-04-10', 0),
(116, 46, 5, '2014-04-10', 0),
(117, 46, 4, '2014-04-11', 0),
(118, 46, 5, '2014-04-11', 0),
(119, 46, 4, '2014-04-12', 0),
(120, 46, 5, '2014-04-12', 0),
(121, 46, 4, '2014-04-08', 0),
(122, 46, 5, '2014-04-08', 0),
(123, 46, 4, '2014-04-09', 0),
(124, 46, 5, '2014-04-09', 0),
(125, 46, 4, '2014-04-10', 0),
(126, 46, 5, '2014-04-10', 0),
(127, 46, 4, '2014-04-11', 0),
(128, 46, 5, '2014-04-11', 0),
(129, 46, 4, '2014-04-12', 0),
(130, 46, 5, '2014-04-12', 0),
(131, 46, 4, '2014-04-08', 0),
(132, 46, 5, '2014-04-08', 0),
(133, 46, 7, '2014-04-08', 0),
(134, 46, 4, '2014-04-09', 0),
(135, 46, 5, '2014-04-09', 0),
(136, 46, 7, '2014-04-09', 0),
(137, 46, 4, '2014-04-10', 0),
(138, 46, 5, '2014-04-10', 0),
(139, 46, 7, '2014-04-10', 0),
(140, 46, 4, '2014-04-11', 0),
(141, 46, 5, '2014-04-11', 0),
(142, 46, 7, '2014-04-11', 0),
(143, 46, 4, '2014-04-12', 0),
(144, 46, 5, '2014-04-12', 0),
(145, 46, 7, '2014-04-12', 0),
(146, 46, 4, '2014-04-08', 0),
(147, 46, 5, '2014-04-08', 0),
(148, 46, 7, '2014-04-08', 0),
(149, 46, 4, '2014-04-09', 0),
(150, 46, 5, '2014-04-09', 0),
(151, 46, 7, '2014-04-09', 0),
(152, 46, 4, '2014-04-10', 0),
(153, 46, 5, '2014-04-10', 0),
(154, 46, 7, '2014-04-10', 0),
(155, 46, 4, '2014-04-11', 0),
(156, 46, 5, '2014-04-11', 0),
(157, 46, 7, '2014-04-11', 0),
(158, 46, 4, '2014-04-12', 0),
(159, 46, 5, '2014-04-12', 0),
(160, 46, 7, '2014-04-12', 0),
(161, 46, 1, '2014-04-08', 1),
(162, 46, 4, '2014-04-08', 1),
(163, 46, 5, '2014-04-08', 1),
(164, 46, 7, '2014-04-08', 1),
(165, 46, 1, '2014-04-09', 1),
(166, 46, 4, '2014-04-09', 1),
(167, 46, 5, '2014-04-09', 1),
(168, 46, 7, '2014-04-09', 1),
(169, 46, 1, '2014-04-10', 1),
(170, 46, 4, '2014-04-10', 1),
(171, 46, 5, '2014-04-10', 1),
(172, 46, 7, '2014-04-10', 1),
(173, 46, 1, '2014-04-11', 1),
(174, 46, 4, '2014-04-11', 1),
(175, 46, 5, '2014-04-11', 1),
(176, 46, 7, '2014-04-11', 1),
(177, 46, 1, '2014-04-12', 1),
(178, 46, 4, '2014-04-12', 1),
(179, 46, 5, '2014-04-12', 1),
(180, 46, 7, '2014-04-12', 1),
(181, 47, 1, '2014-06-02', 1),
(182, 47, 1, '2014-06-03', 1),
(183, 47, 1, '2014-06-04', 1),
(184, 47, 1, '2014-06-05', 1),
(185, 47, 1, '2014-06-06', 0),
(186, 47, 4, '2014-06-02', 0),
(187, 47, 5, '2014-06-02', 0),
(188, 47, 8, '2014-06-02', 0),
(189, 47, 4, '2014-06-03', 0),
(190, 47, 5, '2014-06-03', 0),
(191, 47, 8, '2014-06-03', 0),
(192, 47, 4, '2014-06-04', 0),
(193, 47, 5, '2014-06-04', 0),
(194, 47, 8, '2014-06-04', 0),
(195, 47, 4, '2014-06-05', 0),
(196, 47, 5, '2014-06-05', 0),
(197, 47, 8, '2014-06-05', 0),
(198, 47, 4, '2014-06-06', 0),
(199, 47, 5, '2014-06-06', 0),
(200, 47, 8, '2014-06-06', 0),
(201, 48, 1, '2014-06-09', 0),
(202, 48, 1, '2014-06-10', 0),
(203, 48, 1, '2014-06-11', 0),
(204, 48, 4, '2014-06-09', 0),
(205, 48, 5, '2014-06-09', 0),
(206, 48, 4, '2014-06-10', 0),
(207, 48, 5, '2014-06-10', 0),
(208, 47, 4, '2014-06-02', 0),
(209, 47, 5, '2014-06-02', 0),
(210, 47, 4, '2014-06-03', 0),
(211, 47, 5, '2014-06-03', 0),
(212, 47, 4, '2014-06-04', 0),
(213, 47, 5, '2014-06-04', 0),
(214, 47, 4, '2014-06-05', 0),
(215, 47, 5, '2014-06-05', 0),
(216, 47, 4, '2014-06-06', 0),
(217, 47, 5, '2014-06-06', 0),
(218, 47, 4, '2014-06-02', 0),
(219, 47, 5, '2014-06-02', 0),
(220, 47, 4, '2014-06-03', 0),
(221, 47, 5, '2014-06-03', 0),
(222, 47, 4, '2014-06-04', 0),
(223, 47, 5, '2014-06-04', 0),
(224, 47, 4, '2014-06-05', 0),
(225, 47, 5, '2014-06-05', 0),
(226, 47, 4, '2014-06-06', 0),
(227, 47, 5, '2014-06-06', 0),
(228, 47, 4, '2014-06-02', 0),
(229, 47, 5, '2014-06-02', 0),
(230, 47, 4, '2014-06-03', 0),
(231, 47, 5, '2014-06-03', 0),
(232, 47, 4, '2014-06-04', 0),
(233, 47, 5, '2014-06-04', 0),
(234, 47, 4, '2014-06-05', 0),
(235, 47, 5, '2014-06-05', 0),
(236, 47, 4, '2014-06-06', 0),
(237, 47, 5, '2014-06-06', 0),
(238, 47, 4, '2014-06-02', 0),
(239, 47, 5, '2014-06-02', 0),
(240, 47, 9, '2014-06-02', 0),
(241, 47, 4, '2014-06-03', 0),
(242, 47, 5, '2014-06-03', 0),
(243, 47, 9, '2014-06-03', 0),
(244, 47, 4, '2014-06-04', 0),
(245, 47, 5, '2014-06-04', 0),
(246, 47, 9, '2014-06-04', 0),
(247, 47, 4, '2014-06-05', 0),
(248, 47, 5, '2014-06-05', 0),
(249, 47, 9, '2014-06-05', 0),
(250, 47, 4, '2014-06-06', 0),
(251, 47, 5, '2014-06-06', 0),
(252, 47, 9, '2014-06-06', 0),
(253, 47, 4, '2014-06-02', 0),
(254, 47, 5, '2014-06-02', 0),
(255, 47, 9, '2014-06-02', 0),
(256, 47, 4, '2014-06-03', 0),
(257, 47, 5, '2014-06-03', 0),
(258, 47, 9, '2014-06-03', 0),
(259, 47, 4, '2014-06-04', 0),
(260, 47, 5, '2014-06-04', 0),
(261, 47, 9, '2014-06-04', 0),
(262, 47, 4, '2014-06-05', 0),
(263, 47, 5, '2014-06-05', 0),
(264, 47, 9, '2014-06-05', 0),
(265, 47, 4, '2014-06-02', 0),
(266, 47, 5, '2014-06-02', 0),
(267, 47, 4, '2014-06-03', 0),
(268, 47, 5, '2014-06-03', 0),
(269, 47, 4, '2014-06-04', 0),
(270, 47, 5, '2014-06-04', 0),
(271, 47, 4, '2014-06-05', 0),
(272, 47, 5, '2014-06-05', 0),
(273, 48, 9, '2014-06-02', 1),
(274, 48, 9, '2014-06-03', 1),
(275, 48, 8, '2014-06-02', 0),
(276, 48, 8, '2014-06-03', 0),
(277, 48, 8, '2014-06-02', 0),
(278, 48, 8, '2014-06-03', 0),
(279, 47, 4, '2014-06-02', 0),
(280, 47, 4, '2014-06-03', 0),
(281, 47, 4, '2014-06-04', 0),
(282, 47, 4, '2014-06-05', 0),
(283, 48, 5, '2014-06-02', 0),
(284, 48, 8, '2014-06-02', 0),
(285, 48, 5, '2014-06-03', 0),
(286, 48, 8, '2014-06-03', 0),
(287, 48, 5, '2014-06-02', 0),
(288, 48, 8, '2014-06-02', 0),
(289, 48, 5, '2014-06-03', 0),
(290, 48, 8, '2014-06-03', 0),
(291, 47, 4, '2014-06-02', 1),
(292, 47, 4, '2014-06-03', 1),
(293, 47, 4, '2014-06-04', 1),
(294, 47, 4, '2014-06-05', 1),
(295, 49, 9, '2014-06-09', 1),
(296, 49, 9, '2014-06-10', 1),
(305, 51, 10, '2014-06-02', 1),
(306, 51, 10, '2014-06-03', 1),
(307, 52, 1, '2014-07-22', 1),
(308, 52, 1, '2014-07-23', 1),
(309, 52, 1, '2014-07-24', 1),
(310, 52, 1, '2014-07-25', 1),
(311, 52, 1, '2014-07-26', 1),
(312, 53, 13, '2015-04-08', 1),
(313, 53, 13, '2015-04-09', 1),
(314, 53, 13, '2015-04-10', 1),
(315, 53, 13, '2015-04-11', 1),
(316, 53, 13, '2015-04-12', 1),
(317, 53, 13, '2015-04-13', 1),
(318, 53, 13, '2015-04-14', 1),
(319, 53, 13, '2015-04-15', 1),
(320, 53, 13, '2015-04-16', 1),
(321, 53, 13, '2015-04-17', 1),
(322, 53, 13, '2015-04-18', 1),
(323, 53, 13, '2015-04-19', 1),
(324, 53, 13, '2015-04-20', 1),
(325, 53, 13, '2015-04-21', 1),
(326, 53, 13, '2015-04-22', 1),
(327, 53, 11, '2015-04-08', 1),
(328, 53, 11, '2015-04-09', 1),
(329, 53, 11, '2015-04-10', 1),
(330, 53, 11, '2015-04-11', 1),
(331, 53, 11, '2015-04-12', 1),
(332, 53, 11, '2015-04-13', 1),
(333, 53, 11, '2015-04-14', 1),
(334, 53, 11, '2015-04-15', 1),
(335, 53, 11, '2015-04-16', 1),
(336, 53, 11, '2015-04-17', 1),
(337, 53, 11, '2015-04-18', 1),
(338, 53, 11, '2015-04-19', 1),
(339, 53, 11, '2015-04-20', 1),
(340, 53, 11, '2015-04-21', 1),
(341, 53, 11, '2015-04-22', 1),
(362, 54, 17, '2015-04-13', 1),
(363, 54, 17, '2015-04-14', 1),
(364, 54, 17, '2015-04-15', 1),
(365, 54, 17, '2015-04-16', 1),
(366, 54, 17, '2015-04-17', 1),
(367, 59, 16, '2015-04-30', 1),
(368, 59, 16, '2015-05-01', 1),
(369, 59, 16, '2015-05-02', 1),
(370, 59, 16, '2015-05-03', 1),
(371, 59, 16, '2015-05-04', 1),
(372, 59, 16, '2015-05-05', 1),
(373, 59, 16, '2015-05-06', 1),
(374, 59, 16, '2015-05-07', 1),
(375, 59, 14, '2015-04-30', 1),
(376, 59, 14, '2015-05-01', 1),
(377, 59, 14, '2015-05-02', 1),
(378, 59, 14, '2015-05-03', 1),
(379, 59, 14, '2015-05-04', 1),
(380, 59, 14, '2015-05-05', 1),
(381, 59, 14, '2015-05-06', 1),
(382, 59, 14, '2015-05-07', 1),
(383, 44, 14, '2014-04-03', 1),
(384, 44, 14, '2014-04-04', 1),
(385, 44, 14, '2014-04-05', 1),
(386, 44, 14, '2014-04-06', 1),
(387, 44, 14, '2014-04-07', 1),
(388, 44, 14, '2014-04-08', 1),
(389, 61, 123, '2015-05-20', 1),
(390, 61, 123, '2015-05-21', 1),
(391, 62, 18, '2015-05-21', 1),
(392, 62, 18, '2015-05-22', 1),
(393, 62, 18, '2015-05-23', 1),
(394, 62, 21, '2015-05-21', 1),
(395, 62, 21, '2015-05-22', 1),
(396, 62, 21, '2015-05-23', 1),
(400, 65, 15, '2015-06-01', 1),
(401, 65, 15, '2015-06-02', 1),
(402, 65, 16, '2015-06-01', 1),
(403, 65, 16, '2015-06-02', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `posisi_cetak`
--

CREATE TABLE IF NOT EXISTS `posisi_cetak` (
  `id` int(11) NOT NULL,
  `jenis_surat` int(11) NOT NULL,
  `kolom` varchar(255) NOT NULL,
  `horisontal` int(11) DEFAULT NULL,
  `vertikal` int(11) DEFAULT NULL,
  `font_size` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `posisi_cetak`
--

INSERT INTO `posisi_cetak` (`id`, `jenis_surat`, `kolom`, `horisontal`, `vertikal`, `font_size`) VALUES
(1, 1, 'nomor', 90, 57, NULL),
(2, 1, 'nama_pejabat', 90, 80, NULL),
(3, 1, 'nama_pegawai', 90, 93, NULL),
(4, 1, 'pangkat_golongan', 167, 103, NULL),
(5, 1, 'jabatan', 167, 109, NULL),
(6, 1, 'maksud_perjalanan', 90, 119, NULL),
(7, 1, 'tempat_berangkat', 167, 134, NULL),
(8, 1, 'tempat_tujuan', 167, 140, NULL),
(9, 1, 'lamanya_perjalanan', 167, 153, NULL),
(10, 1, 'tanggal_berangkat', 167, 159, NULL),
(11, 1, 'tanggal_kembali', 167, 165, NULL),
(12, 1, 'alat_angkutan', 167, 178, NULL),
(13, 1, 'nama_pengikut', 10, 180, NULL),
(14, 1, 'umur_pengikut', 80, 180, NULL),
(15, 1, 'instansi', 167, 231, NULL),
(16, 1, 'mata_anggaran', 167, 238, 10),
(17, 1, 'keterangan', 170, 245, NULL),
(18, 1, 'tanggal', 160, 283, NULL),
(20, 2, 'tanggal', 178, 30, NULL),
(21, 2, 'nomor', 178, 37, NULL),
(22, 2, 'uang_sebesar', 50, 72, NULL),
(23, 2, 'dengan_huruf', 44, 79, NULL),
(24, 2, 'untuk_program', 44, 96, NULL),
(25, 2, 'kegiatan', 44, 102, NULL),
(26, 2, 'kode_rekening', 44, 108, NULL),
(27, 2, 'pengguna_anggaran', 13, 148, NULL),
(28, 2, 'pptk', 61, 148, NULL),
(29, 2, 'bendahara', 108, 148, NULL),
(30, 2, 'nama_penerima', 171, 123, 9),
(31, 2, 'pangkat_penerima', 189, 128, 9),
(32, 2, 'satuan_kerja_penerima', 160, 150, NULL),
(33, 3, 'nama', 58, 44, NULL),
(34, 3, 'nip', 58, 50, NULL),
(35, 3, 'jabatan', 58, 56, NULL),
(36, 3, 'alamat', 58, 62, NULL),
(37, 3, 'tujuan', 58, 87, NULL),
(38, 3, 'tgl_berangkat', 58, 94, NULL),
(39, 3, 'tgl_kembali', 58, 100, NULL),
(40, 3, 'tanggal', 130, 258, NULL),
(41, 3, 'nama_ttd', 130, 310, NULL),
(42, 3, 'nip_ttd', 130, 318, NULL),
(51, 4, 'berangkat_dari', 166, 20, NULL),
(52, 4, 'I_tiba_pada_tgl', 47, 67, NULL),
(53, 4, 'I_tiba_jabatan_kepala', 55, 73, NULL),
(54, 4, 'I_tiba_nama_kepala', 47, 87, NULL),
(55, 4, 'I_tiba_nip_kepala', 47, 93, NULL),
(56, 4, 'I_berangkat_dari', 160, 60, NULL),
(57, 4, 'I_berangkat_ke', 160, 67, NULL),
(58, 4, 'pada_tanggal', 166, 26, NULL),
(59, 4, 'I_berangkat_pada_tanggal', 160, 74, NULL),
(60, 4, 'I_berangkat_nip_kepala', 150, 93, NULL),
(61, 4, 'I_berangkat_nama_kepala', 160, 87, NULL),
(62, 4, 'I_berangkat_jabatan_kepala', 165, 79, NULL),
(63, 4, 'I_tiba', 47, 60, NULL),
(64, 4, 'nip_kepala', 137, 53, 11),
(65, 4, 'nama_kepala', 137, 48, NULL),
(66, 4, 'kepala', 166, 41, 10),
(67, 4, 'ke', 166, 32, NULL),
(69, 5, 'nama_pejabat', 68, 85, NULL),
(70, 5, 'nip_pejabat', 68, 91, NULL),
(71, 5, 'jabatan_pejabat', 68, 97, NULL),
(72, 5, 'nama_pegawai', 68, 125, NULL),
(73, 5, 'nip_pegawai', 68, 131, NULL),
(74, 5, 'jabatan_pegawai', 68, 136, NULL),
(75, 5, 'tujuan', 68, 165, NULL),
(76, 5, 'tgl_berangkat', 68, 171, NULL),
(77, 5, 'tgl_kembali', 68, 177, NULL),
(78, 3, 'hasil_kegiatan_1', 30, 122, NULL),
(79, 3, 'hasil_kegiatan_2', 30, 160, NULL),
(80, 3, 'hasil_kegiatan_3', 30, 197, NULL),
(82, 2, 'nip_pengguna_anggaran', 10, 153, 10),
(83, 2, 'nip_pptk', 60, 153, 10),
(84, 2, 'nip_bendahara', 107, 153, 10),
(85, 5, 'alamat_pegawai', 68, 142, NULL),
(86, 5, 'alamat_pejabat', 68, 103, NULL),
(87, 5, 'kegiatan', 43, 196, NULL),
(88, 5, 'tanggal_spt', 160, 261, NULL),
(89, 5, 'nama_yg_memerintah', 150, 303, NULL),
(90, 5, 'nip_yg_memerintah', 150, 310, NULL),
(91, 5, 'nama_yg_diperintah', 50, 303, NULL),
(92, 5, 'nip_yg_diperintah', 50, 310, NULL),
(93, 2, 'untuk', 44, 84, 9),
(94, 2, 'rincian', 50, 75, NULL),
(100, 4, 'II_tiba', 47, 105, NULL),
(101, 4, 'II_tiba_pada_tgl', 47, 111, NULL),
(102, 4, 'II_tiba_jabatan_kepala', 55, 117, NULL),
(103, 4, 'II_tiba_nama_kepala', 47, 130, NULL),
(104, 4, 'II_tiba_nip_kepala', 47, 136, NULL),
(105, 4, 'II_berangkat_dari', 160, 105, NULL),
(106, 4, 'II_berangkat_ke', 160, 111, NULL),
(107, 4, 'II_berangkat_pada_tanggal', 160, 117, NULL),
(108, 4, 'II_berangkat_jabatan_kepala', 165, 122, NULL),
(109, 4, 'II_berangkat_nama_kepala', 160, 130, NULL),
(110, 4, 'II_berangkat_nip_kepala', 150, 136, NULL),
(111, 4, 'III_tiba', 47, 147, NULL),
(112, 4, 'III_tiba_pada_tgl', 47, 153, NULL),
(113, 4, 'III_tiba_jabatan_kepala', 52, 159, NULL),
(114, 4, 'III_tiba_nama_kepala', 47, 174, NULL),
(115, 4, 'III_tiba_nip_kepala', 47, 180, NULL),
(116, 4, 'III_berangkat_dari', 160, 149, NULL),
(117, 4, 'III_berangkat_ke', 160, 154, NULL),
(118, 4, 'III_berangkat_pada_tanggal', 160, 160, NULL),
(119, 4, 'III_berangkat_jabatan_kepala', 170, 166, NULL),
(120, 4, 'III_berangkat_nama_kepala', 160, 175, NULL),
(121, 4, 'III_berangkat_nip_kepala', 149, 180, NULL),
(122, 1, 'tempat_dikeluarkan', 160, 278, NULL),
(123, 1, 'nip_pejabat', 160, 78, NULL),
(124, 1, 'nip_pegawai', 160, 93, NULL),
(125, 1, 'atas_nama_ttd', 133, 289, NULL),
(126, 1, 'ttd_nama_pejabat', 147, 323, NULL),
(127, 1, 'ttd_nip_pejabat', 147, 328, NULL),
(128, 4, 'atas_nama_jabatan', 150, 37, 11),
(129, 2, 'tanggal_ttd', 173, 111, NULL),
(130, 4, 'an', 114, 40, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `refresentatif`
--

CREATE TABLE IF NOT EXISTS `refresentatif` (
  `id` int(11) NOT NULL,
  `jarak` varchar(255) DEFAULT NULL,
  `biaya` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `refresentatif`
--

INSERT INTO `refresentatif` (`id`, `jarak`, `biaya`) VALUES
(1, 'Jarak Tempuh 5 s/d 15 KM', '100000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `riil`
--

CREATE TABLE IF NOT EXISTS `riil` (
  `id` int(11) NOT NULL,
  `id_spd` int(11) DEFAULT NULL,
  `bbm` varchar(255) DEFAULT NULL,
  `tol` varchar(255) DEFAULT NULL,
  `jumlah` varchar(255) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `ppk` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `riil`
--

INSERT INTO `riil` (`id`, `id_spd`, `bbm`, `tol`, `jumlah`, `tanggal`, `ppk`) VALUES
(1, 46, '300000', '100000', '400.000', '2014-04-17', NULL),
(2, 46, '300000', '100000', '400.000', '2014-04-17', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `role`
--

INSERT INTO `role` (`id`, `nama`) VALUES
(1, 'admin'),
(2, 'operator'),
(3, 'pegawai'),
(4, 'tes ok');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_access`
--

CREATE TABLE IF NOT EXISTS `role_access` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `access_id` int(11) NOT NULL,
  `status` int(11) DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `role_access`
--

INSERT INTO `role_access` (`id`, `role_id`, `access_id`, `status`) VALUES
(1, 1, 1, 1),
(2, 1, 2, 1),
(3, 1, 3, 1),
(4, 1, 9, 1),
(5, 1, 17, 1),
(6, 1, 10, 1),
(7, 1, 11, 1),
(8, 1, 13, 1),
(9, 1, 12, 1),
(10, 1, 4, 1),
(11, 1, 6, 1),
(12, 1, 5, 1),
(13, 1, 18, 1),
(14, 1, 20, 1),
(15, 1, 19, 1),
(16, 1, 7, 1),
(17, 1, 21, 1),
(18, 1, 8, 1),
(19, 1, 14, 1),
(20, 1, 16, 1),
(21, 1, 15, 1),
(22, 1, 22, 1),
(23, 1, 24, 1),
(24, 1, 23, 1),
(25, 1, 25, 1),
(26, 1, 27, 1),
(27, 1, 26, 1),
(28, 1, 28, 1),
(29, 1, 29, 1),
(30, 1, 31, 1),
(31, 1, 30, 1),
(32, 1, 32, 1),
(33, 1, 34, 1),
(34, 1, 33, 1),
(35, 1, 41, 0),
(36, 1, 35, 0),
(37, 1, 36, 0),
(38, 1, 42, 0),
(39, 1, 40, 0),
(40, 1, 39, 0),
(41, 1, 38, 0),
(42, 1, 37, 0),
(43, 1, 43, 0),
(44, 1, 44, 0),
(45, 1, 45, 1),
(46, 1, 46, 1),
(47, 1, 47, 1),
(48, 1, 48, 1),
(49, 1, 49, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `setting`
--

INSERT INTO `setting` (`id`, `title`, `key`, `value`) VALUES
(1, 'Font Size', 'font_size', '11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `spd`
--

CREATE TABLE IF NOT EXISTS `spd` (
  `id` int(11) NOT NULL,
  `nomor_spt` varchar(255) DEFAULT NULL,
  `nomor_spd` varchar(255) DEFAULT NULL,
  `tanggal_spt` date DEFAULT NULL,
  `tanggal_spd` date DEFAULT NULL,
  `id_pejabat` int(11) DEFAULT NULL,
  `id_pegawai` int(11) DEFAULT NULL,
  `id_atas_nama` int(11) DEFAULT NULL,
  `maksud` varchar(255) DEFAULT NULL,
  `tujuan` varchar(255) DEFAULT NULL,
  `tgl_pergi` date DEFAULT NULL,
  `tgl_kembali` date DEFAULT NULL,
  `lama` varchar(255) DEFAULT NULL,
  `kendaraan` varchar(255) DEFAULT NULL,
  `tempat_berangkat` varchar(255) DEFAULT NULL,
  `instansi` varchar(255) DEFAULT NULL,
  `biaya` varchar(255) DEFAULT NULL,
  `koring` varchar(255) DEFAULT NULL,
  `pengikut` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `spd`
--

INSERT INTO `spd` (`id`, `nomor_spt`, `nomor_spd`, `tanggal_spt`, `tanggal_spd`, `id_pejabat`, `id_pegawai`, `id_atas_nama`, `maksud`, `tujuan`, `tgl_pergi`, `tgl_kembali`, `lama`, `kendaraan`, `tempat_berangkat`, `instansi`, `biaya`, `koring`, `pengikut`) VALUES
(44, 'Cek 11', 'SPD/2014/11/22', '2014-04-03', '2014-04-03', 1, NULL, NULL, 'Cek 11', 'Cek 11', '2014-04-03', '2014-04-08', '5', 'Mobil', 'Serang', 'DPKD', '2000000', '5.2.15.01', NULL),
(46, '221/2222/33333', 'SPD/0234', '2014-04-05', '2014-04-05', NULL, NULL, NULL, 'Merapatkan rencana kerja', 'Bandung', '2015-04-04', '2015-04-30', '2', 'Mobil', 'Serang', 'BPKD', '2000000', '5.2.15.01', NULL),
(47, 'D/034/23', NULL, '2014-06-02', NULL, NULL, NULL, NULL, 'Mengikut rapat koordinasi', 'Bandung', '2014-06-02', '2014-06-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, 'D/034/25', NULL, '2014-05-30', NULL, NULL, NULL, NULL, 'Mengikuti rapat koordinasi', 'Bandung', '2014-06-02', '2014-06-03', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 'D/034/26', NULL, '2014-05-30', NULL, NULL, NULL, NULL, 'Rapat koordinasi', 'Bandung', '2014-06-09', '2014-06-10', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, 'D/034/27', '800/035-DPKD/2015', '2014-05-30', '2015-04-08', NULL, NULL, NULL, '', 'Bandung', '2015-04-01', '2015-04-30', NULL, '', 'Serang', 'Dinas Perhubungan, Komunikasi, dan Informatika Kabupaten Serang', NULL, '5.2.15.01', NULL),
(52, 'sadfasfasd', '', '2014-07-21', '0000-00-00', NULL, NULL, NULL, '', 'KARAWANG', '2014-07-22', '2014-07-26', '', '', 'Serang', 'Dinas Perhubungan, Komunikasi, dan Informatika Kabupaten Serang', NULL, '5.2.15.01', NULL),
(53, NULL, '01', NULL, '2015-04-09', 1, NULL, NULL, 'Rapat koordinasi', 'Bandung', '2015-04-08', '2015-04-22', '14', 'Mobil Dinas', 'Serang', 'Dinas Perhubungan, Komunikasi, dan Informatika Kabupaten Serang', NULL, '5.2.15.01', NULL),
(54, NULL, '111', NULL, '2015-04-13', 2, NULL, NULL, 'Test Maksud Perjalanan', 'Bandung', '2015-04-13', '2015-04-17', '4', 'Mobil', 'Serang', 'Dinas Perhubungan, Komunikasi, dan Informatika Kabupaten Serang', NULL, '1.07.1.07.01.01.19.5.2.2.15.01', NULL),
(55, NULL, 'SPD/1/2015', NULL, '2015-04-16', 2, NULL, NULL, 'Trip', 'Kec. Sumur Bandung', '2015-04-16', '2015-04-30', '14', 'Mobil Dinas', 'Serang', 'Dinas Perhubungan, Komunikasi, dan Informatika Kabupaten Serang', NULL, '1.07.1.07.01.01.19.5.2.2.15.01', NULL),
(56, NULL, 'SPD/2/2015', NULL, '2015-04-24', 2, NULL, NULL, 'Trip', 'Kec. Karawang', '2015-04-30', '2015-05-07', '7', 'Mobil Dinas', 'Serang', 'Dinas Perhubungan, Komunikasi, dan Informatika Kabupaten Serang', NULL, '1.07.1.07.01.01.19.5.2.2.15.01', NULL),
(57, NULL, 'SPD/2/2015', NULL, '2015-04-24', 2, NULL, NULL, 'Trip', 'Kec. Karawang', '2015-04-30', '2015-05-07', '7', 'Mobil Dinas', 'Serang', 'Dinas Perhubungan, Komunikasi, dan Informatika Kabupaten Serang', NULL, '1.07.1.07.01.01.19.5.2.2.15.01', NULL),
(58, NULL, 'SPD/2/2015', NULL, '2015-04-24', 2, NULL, NULL, 'Trip', 'Kec. Karawang', '2015-04-30', '2015-05-07', '7', 'Mobil Dinas', 'Serang', 'Dinas Perhubungan, Komunikasi, dan Informatika Kabupaten Serang', NULL, '1.07.1.07.01.01.19.5.2.2.15.01', NULL),
(59, NULL, 'SPD/2/2015', NULL, '2015-04-24', 2, NULL, NULL, 'Trip', 'Kec. Karawang', '2015-04-30', '2015-05-07', '7', 'Mobil Dinas', 'Serang', 'Dinas Perhubungan, Komunikasi, dan Informatika Kabupaten Serang', NULL, '1.07.1.07.01.01.19.5.2.2.15.01', NULL),
(60, NULL, 'SPD/2015/V/01', NULL, '2015-05-18', 1, NULL, NULL, 'Koordinasi rapat', 'Bandung', '2015-05-18', '2015-05-21', '3', 'Mobil Dinas', 'Serang', 'Dinas Perhubungan, Komunikasi, dan Informatika Kabupaten Serang', NULL, '5.2.15.01', NULL),
(61, NULL, 'SPD/2015/V/02', NULL, '2015-05-19', 1, NULL, NULL, 'Rapat koordinasi', 'Tangerang Selatan', '2015-05-20', '2015-05-22', '2', 'Mobil Dinas', 'Serang', 'Dinas Perhubungan, Komunikasi, dan Informatika Kabupaten Serang', NULL, '1.07.1.07.01.01.19.5.2.2.15.01', NULL),
(62, NULL, 'SPD/2015/V/03', NULL, '2015-05-18', 1, NULL, NULL, 'Rapat koordinasi', 'Tangerang', '2015-05-21', '2015-05-23', '2', 'Mobil Dinas', 'Serang', 'Dinas Perhubungan, Komunikasi, dan Informatika Kabupaten Serang', NULL, '1.07.1.07.01.01.19.5.2.2.15.01', NULL),
(65, NULL, 'SPD/2015/V/02', NULL, '2015-05-29', 1, NULL, 4, 'Rapat koordinasi', 'Tangerang Selatan', '2015-06-01', '2015-06-02', '2', 'Mobil Dinas', 'Serang', 'Dishubkominfo', NULL, '1.07.1.07.01.01.19.5.2.2.15.01', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `spd_belakang`
--

CREATE TABLE IF NOT EXISTS `spd_belakang` (
  `id` int(11) NOT NULL,
  `id_spd` int(11) NOT NULL,
  `urutan` int(11) NOT NULL,
  `tempat_berangkat` varchar(255) DEFAULT NULL,
  `ke` varchar(255) DEFAULT NULL,
  `tanggal_berangkat` date DEFAULT NULL,
  `nama_kepala_berangkat` varchar(255) DEFAULT NULL,
  `nip_kepala_berangkat` varchar(255) DEFAULT NULL,
  `jabatan_kepala_berangkat` varchar(255) DEFAULT NULL,
  `tempat_tiba` varchar(255) DEFAULT NULL,
  `tanggal_tiba` date DEFAULT NULL,
  `nama_kepala_tiba` varchar(255) DEFAULT NULL,
  `nip_kepala_tiba` varchar(255) DEFAULT NULL,
  `jabatan_kepala_tiba` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `spd_belakang`
--

INSERT INTO `spd_belakang` (`id`, `id_spd`, `urutan`, `tempat_berangkat`, `ke`, `tanggal_berangkat`, `nama_kepala_berangkat`, `nip_kepala_berangkat`, `jabatan_kepala_berangkat`, `tempat_tiba`, `tanggal_tiba`, `nama_kepala_tiba`, `nip_kepala_tiba`, `jabatan_kepala_tiba`) VALUES
(1, 59, 1, 'Serang', NULL, '2015-04-30', '', '', '', 'Kec. Karawang', '2015-04-30', '', '', ''),
(3, 59, 2, 'Test Tempat Berangkat', NULL, '2015-04-23', 'Test Nama Kepala Berangkat', 'Test Nip Kepala Berangkat', 'Test Jabatan Kepala Berangkat', 'Test Tempat Tiba', '2015-04-24', 'Test Nama Kepala Tiba', 'Test Nip Kepala Tiba', 'Test'),
(4, 44, 1, 'Serang', NULL, '2015-05-18', 'Thomas', '1320004637162', 'Kepala Dinas', 'Bandung', '2015-05-18', 'Alfa', '13204992', 'Kepala Bagian'),
(5, 60, 1, 'Serang', NULL, '2015-05-18', NULL, NULL, NULL, 'Bandung', NULL, NULL, NULL, NULL),
(6, 61, 1, 'Serang', NULL, '2015-05-20', '', '', '', 'Tangerang Selatan', '0000-00-00', '', '', ''),
(7, 62, 1, 'Serang', NULL, '2015-05-21', NULL, NULL, NULL, 'Tangerang', NULL, NULL, NULL, NULL),
(8, 62, 2, 'Serang', NULL, '2015-05-20', 'Thomas', '1320004637162', 'Kepala Bagian', 'Jakarta', '2015-05-22', 'Alfa', '13204992', 'Kepala Dinas'),
(9, 63, 1, 'Serang', '', '2015-05-27', '', '', '', 'Tangerang', '0000-11-30', '', '', ''),
(11, 64, 1, 'Tangerang Selatan', NULL, '2015-05-30', NULL, NULL, NULL, 'Tangerang Selatan', '2015-05-29', NULL, NULL, NULL),
(12, 65, 1, 'Tangerang Selatan', 'Serang', '2015-06-02', 'Siti jubaidah', '13200004637161', 'Sales MAnager', 'Tangerang Selatan', '2015-06-01', 'Siti Jubaidah', '13200004637162', 'Sales Manager'),
(16, 65, 2, 'Pandeglang', 'Serang', '2015-05-31', 'Aditia Budiman', '1320004637162', 'Manager', 'Pandeglang', '2015-05-30', 'Aditia Budiman', '1320004637162', 'Manager'),
(17, 65, 3, 'Cilegon', 'Serang', '2015-06-03', 'Indra Andriana', '1320004637161', 'Supervisor', 'Cilegon', '2015-06-02', 'Indra Andriana', '1320004637162', 'Supervisor');

-- --------------------------------------------------------

--
-- Struktur dari tabel `uang_harian`
--

CREATE TABLE IF NOT EXISTS `uang_harian` (
  `id` int(11) NOT NULL,
  `jarak` varchar(255) DEFAULT NULL,
  `id_golongan` int(11) DEFAULT NULL,
  `biaya` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `uang_harian`
--

INSERT INTO `uang_harian` (`id`, `jarak`, `id_golongan`, `biaya`) VALUES
(1, 'Jarak Tempuh 15 s/d 30 KM', 1, '150000'),
(2, 'Jarak Tempuh 15 s/d 30 KM', 2, '200000'),
(3, 'Jarak Tempuh 15 s/d 30 KM', 3, '250000'),
(4, 'Jarak Tempuh 15 s/d 30 KM', 4, '275000'),
(5, 'Jarak Tempuh Lebih dari 30 KM', 1, '150000'),
(6, 'Jarak Tempuh Lebih dari 30 KM', 2, '225000'),
(7, 'Jarak Tempuh Lebih dari 30 KM', 3, '275000'),
(8, 'Jarak Tempuh Lebih dari 30 KM', 4, '325000'),
(9, 'Jarak Tempuh DKI', 1, '350000'),
(10, 'Jarak Tempuh DKI', 2, '375000'),
(11, 'Jarak Tempuh DKI', 3, '425000'),
(12, 'Jarak Tempuh DKI', 4, '475000'),
(13, 'Jarak Tempuh Kurang dari 200 KM', 1, '400000'),
(14, 'Jarak Tempuh Kurang dari 200 KM', 2, '425000'),
(15, 'Jarak Tempuh Kurang dari 200 KM', 3, '475000'),
(16, 'Jarak Tempuh Kurang dari 200 KM', 4, '525000'),
(17, 'Jarak Tempuh Lebih dari 200 KM', 1, '500000'),
(18, 'Jarak Tempuh Lebih dari 200 KM', 2, '525000'),
(19, 'Jarak Tempuh Lebih dari 200 KM', 3, '575000'),
(20, 'Jarak Tempuh Lebih dari 200 KM', 4, '600000'),
(21, 'Jarak Dalam Daerah', 1, '60000'),
(22, 'Jarak Dalam Daerah', 2, '75000'),
(23, 'Jarak Dalam Daerah', 3, '90000'),
(24, 'Jarak Dalam Daerah', 4, '110000'),
(25, 'Jarak Tempuh 5 s/d 15 KM', 1, '100000'),
(26, 'Jarak Tempuh 5 s/d 15 KM', 2, '125000'),
(27, 'Jarak Tempuh 5 s/d 15 KM', 3, '150000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `role_id`, `last_login`, `status`) VALUES
(1, 'admin', 'adminserang', 1, NULL, 1),
(2, 'eja', 'eja', 1, NULL, 1),
(3, 'llaj', 'llaj', 1, NULL, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `visum`
--

CREATE TABLE IF NOT EXISTS `visum` (
  `id` int(11) NOT NULL,
  `nomor` varchar(255) NOT NULL,
  `nip` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `jabatan` varchar(255) DEFAULT NULL,
  `instansi` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `visum`
--

INSERT INTO `visum` (`id`, `nomor`, `nip`, `nama`, `jabatan`, `instansi`) VALUES
(1, '1', '19881203 201202 1 001', 'Drs. Solehudin Al Ghifari, S.Sos, M.Si', 'Kasubag Evaluasi Keuangan Daerah', 'Kementrian Keuangan Jakarta');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access`
--
ALTER TABLE `access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `akomodasi`
--
ALTER TABLE `akomodasi`
  ADD PRIMARY KEY (`id`), ADD KEY `FK_akomodasi_gol` (`id_golongan`);

--
-- Indexes for table `golongan`
--
ALTER TABLE `golongan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `koring`
--
ALTER TABLE `koring`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kwitansi`
--
ALTER TABLE `kwitansi`
  ADD PRIMARY KEY (`id`), ADD KEY `FK_kwitansi_spd` (`id_spd`), ADD KEY `FK_kwitansi_ako` (`akomodasi`), ADD KEY `FK_kwitansi_ref` (`refresentatif`), ADD KEY `FK_kwitansi_bendahara` (`bendahara`), ADD KEY `FK_kwitansi_pelaksana` (`pptk`), ADD KEY `FK_kwitansi_ssh` (`uang_harian`);

--
-- Indexes for table `lhp`
--
ALTER TABLE `lhp`
  ADD PRIMARY KEY (`id`), ADD KEY `FK_lhp_spd` (`id_spd`);

--
-- Indexes for table `login_history`
--
ALTER TABLE `login_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id`), ADD KEY `FK_pegawai_golongan` (`id_golongan`);

--
-- Indexes for table `pejabat`
--
ALTER TABLE `pejabat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengikut`
--
ALTER TABLE `pengikut`
  ADD PRIMARY KEY (`id`), ADD KEY `FK_pengikut_spd` (`id_spd`), ADD KEY `FK_pengikut_pegawai` (`id_pegawai`);

--
-- Indexes for table `perjalanan`
--
ALTER TABLE `perjalanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posisi_cetak`
--
ALTER TABLE `posisi_cetak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `refresentatif`
--
ALTER TABLE `refresentatif`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `riil`
--
ALTER TABLE `riil`
  ADD PRIMARY KEY (`id`), ADD KEY `FK_rill_spd` (`id_spd`), ADD KEY `FK_rill_pejabat` (`ppk`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_access`
--
ALTER TABLE `role_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spd`
--
ALTER TABLE `spd`
  ADD PRIMARY KEY (`id`), ADD KEY `FK_spd_pejabat` (`id_pejabat`), ADD KEY `FK_spd_pegawai` (`id_pegawai`);

--
-- Indexes for table `spd_belakang`
--
ALTER TABLE `spd_belakang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uang_harian`
--
ALTER TABLE `uang_harian`
  ADD PRIMARY KEY (`id`), ADD KEY `FK_ssh_gol` (`id_golongan`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`), ADD KEY `FK_user` (`role_id`);

--
-- Indexes for table `visum`
--
ALTER TABLE `visum`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access`
--
ALTER TABLE `access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `akomodasi`
--
ALTER TABLE `akomodasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `golongan`
--
ALTER TABLE `golongan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `koring`
--
ALTER TABLE `koring`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `kwitansi`
--
ALTER TABLE `kwitansi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `lhp`
--
ALTER TABLE `lhp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `login_history`
--
ALTER TABLE `login_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=124;
--
-- AUTO_INCREMENT for table `pejabat`
--
ALTER TABLE `pejabat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `pengikut`
--
ALTER TABLE `pengikut`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=125;
--
-- AUTO_INCREMENT for table `perjalanan`
--
ALTER TABLE `perjalanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=404;
--
-- AUTO_INCREMENT for table `posisi_cetak`
--
ALTER TABLE `posisi_cetak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=131;
--
-- AUTO_INCREMENT for table `refresentatif`
--
ALTER TABLE `refresentatif`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `riil`
--
ALTER TABLE `riil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `role_access`
--
ALTER TABLE `role_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `spd`
--
ALTER TABLE `spd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `spd_belakang`
--
ALTER TABLE `spd_belakang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `uang_harian`
--
ALTER TABLE `uang_harian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `visum`
--
ALTER TABLE `visum`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `akomodasi`
--
ALTER TABLE `akomodasi`
ADD CONSTRAINT `FK_akomodasi_gol` FOREIGN KEY (`id_golongan`) REFERENCES `golongan` (`id`);

--
-- Ketidakleluasaan untuk tabel `lhp`
--
ALTER TABLE `lhp`
ADD CONSTRAINT `lhp_ibfk_1` FOREIGN KEY (`id_spd`) REFERENCES `spd` (`id`) ON DELETE SET NULL;

--
-- Ketidakleluasaan untuk tabel `pegawai`
--
ALTER TABLE `pegawai`
ADD CONSTRAINT `FK_pegawai_golongan` FOREIGN KEY (`id_golongan`) REFERENCES `golongan` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
