<?php

class PengikutController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/constellation/index';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','tambah'),
				'users'=>array('@'),
			),	
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
$this->render('view',array(
'model'=>$this->loadModel($id),
));
}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
	public function actionCreate()
	{
		$model=new Pengikut;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pengikut']))
		{
			$model->attributes=$_POST['Pengikut'];
			if($model->save())
				$this->redirect(array('spd/view','id'=>$model->id_spd));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	public function actionTambah($id_pegawai,$id_spd)
	{
		$model=new Pengikut;

		if(isset($id_pegawai) AND isset($id_spd))
		{
			$pegawai = Pegawai::model()->findByPk($id_pegawai);
			$spd = Spd::model()->findByPk($id_spd);
			
			//CEK TANGGAL PERJALANAN PENGIKUT	
			$tanggal = $spd->tgl_pergi;
			$error = 0;
		
			while($tanggal <= $spd->tgl_kembali)
			{
				$perjalanan = Perjalanan::model()->findByAttributes(array('id_pegawai'=>$id_pegawai,'tanggal'=>$tanggal,'aktif'=>1));
				if($perjalanan !== null AND $perjalanan->id_spd != $spd->id)
				{
					$error = 1;
					$errorPengikut = 1;
				}
				$tanggal = date('Y-m-d',strtotime(date("Y-m-d", strtotime($tanggal)) . " +1 day"));
			}
			if($error == 1)
			{
				Yii::app()->user->setFlash('danger','Pegawai '.Pegawai::model()->findByPk($id_pegawai)->nama.' sedang dalam perjalanan dinas antara tanggal '.Bantu::tanggal($spd->tgl_pergi).' s.d '.Bantu::tanggal($spd->tgl_kembali));
				$this->redirect(array('spd/view','id'=>$id_spd));
			}
			else
			{
			
				$exist = Pengikut::model()->countByAttributes(array('id_pegawai'=>$id_pegawai,'id_spd'=>$id_spd));
				if($exist==0)
				{
					$model->id_pegawai = $id_pegawai;
					$model->id_spd = $id_spd;
					if($model->save()) 
					{
						//START INSERT PERJALANAN
						$tanggal = $spd->tgl_pergi;
						while($tanggal <= $spd->tgl_kembali)
						{
							//INPUT PERJALANAN PEGAWAI
							$perjalanan = Perjalanan::model()->findByAttributes(array('id_spd'=>$spd->id,'id_pegawai'=>$id_pegawai,'tanggal'=>$tanggal));
					
							if($perjalanan === null)
							{
								$perjalanan = new Perjalanan;
								$perjalanan->id_spd = $spd->id;
								$perjalanan->id_pegawai = $id_pegawai;
								$perjalanan->tanggal = $tanggal;
							}
							
							$perjalanan->aktif = 1;
							$perjalanan->save();
					
							$tanggal = date('Y-m-d',strtotime(date("Y-m-d", strtotime($tanggal)) . " +1 day"));
						}
						Yii::app()->user->setFlash('success','Data berhasil disimpan');
						$this->redirect(array('spd/view','id'=>$model->id_spd));
					}
				} else {
					Yii::app()->user->setFlash('danger','Data gagal ditambahkan, periksa kembali apakah pegawai sudah ditambahkan atau belum');
					$this->redirect(array('spd/view','id'=>$id_spd));
				}
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pengikut']))
		{
			$model->attributes=$_POST['Pengikut'];
			if($model->save()) {
				Yii::app()->user->setFlash('success','Data berhasil diupdate');
				$this->redirect(array('spd/view','id'=>$model->id_spd));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			$model = $this->loadModel($id);
			$id_spd = $model->id_spd;
			$model->delete();
		
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
			{
				Yii::app()->user->setFlash('success','Data berhasil dihapus');
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('spd/view','id'=>$id_spd));
			}
		} else
		throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Pengikut');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
$model=new Pengikut('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['Pengikut']))
$model->attributes=$_GET['Pengikut'];

$this->render('admin',array(
'model'=>$model,
));
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Pengikut::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='pengikut-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
