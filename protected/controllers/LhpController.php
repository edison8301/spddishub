<?php

class LhpController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/constellation/index';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'accessRole'
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','cetak','delete','cetakPdf','cetakBlanko'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','excel','exportExcel'),
				'users'=>array('@'),
			),		
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionAdmin()
	{
		$model=new Lhp('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Lhp']))
			$model->attributes=$_GET['Lhp'];

		$this->render('admin',array(
			'lhp'=>$model,
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new Lhp;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		if(isset($_GET['id_spd']))
		{
			$spd = Spd::model()->findByPk($_GET['id_spd']);
			$model->tanggal_lhp = $spd->tanggal_spd;
		}
		
		if(isset($_POST['Lhp']))
		{
			$model->attributes=$_POST['Lhp'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('spd/view','id'=>$model->id_spd));
			}
		}

		$lhp=new Lhp('search');
		$lhp->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Lhp']))
			$lhp->attributes=$_GET['Lhp'];

		$this->render('create',array(
			'model'=>$model,
			'lhp'=>$lhp
		));
	}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Lhp']))
		{
			$model->attributes=$_POST['Lhp'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('spd/view','id'=>$model->id_spd));
			}
		}

		$lhp=new Lhp('search');
		$lhp->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Lhp']))
			$lhp->attributes=$_GET['Lhp'];
		
		Yii::app()->user->setFlash('info','Silahkan sunting data pada kolom yang disediakan');
		
		$this->render('update',array(
			'model'=>$model,
			'lhp'=>$lhp,
		));
	}
	
	public function actionExcel()
	{
		$this->render('excel');
	}
	
	public function actionExportExcel()
	{
		$this->layout = '//layouts/excel';
		$this->render('_excel',array('border'=>1));
	}
	
	public function actionCetakPdf($id)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Laporan Hasil Perjalanan Dinas');
		$pdf->SetSubject('Data Pegawai');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		
		$pdf->setTopMargin(18);
		$pdf->setLeftMargin(18);
		$pdf->setRightMargin(18);
		
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 12);
		
		$model = $this->loadModel($id);
		
		$hasil_kegiatan_1 = '........................................................................................................................';
		if($model->hasil_kegiatan_1!='')
			$hasil_kegiatan_1 = $model->hasil_kegiatan_1;
			
		$hasil_kegiatan_2 = '........................................................................................................................';
		if($model->hasil_kegiatan_2!='')
			$hasil_kegiatan_2 = $model->hasil_kegiatan_2;
			
		$hasil_kegiatan_3 = '........................................................................................................................';
		if($model->hasil_kegiatan_2!='')
			$hasil_kegiatan_3 = $model->hasil_kegiatan_3;
		
		$html = '';
				
		$html .= '<h1 style="text-align:center;text-decoration:underline;">LAPORAN HASIL PERJALANAN DINAS</h1>';
		
		$html .= '<p>&nbsp;</p>';
		
		$html .= '<p>Petugas yang melaksanakan Perjalanan Dinas: </p>';
		
		$html .= '<table>';
		$html .= '<tr>';
		$html .= '<td style="width:15%">Nama</td>';
		$html .= '<td style="width:85%">: '.$model->getRelationField("pegawai","nama").'</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>NIP</td>';
		$html .= '<td>: '.$model->getRelationField("pegawai","nip").'</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>Jabatan</td>';
		$html .= '<td>: '.$model->getRelationField("pegawai","jabatan").'</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>Alamat</td>';
		$html .= '<td>: Jalan Raya Jakarta Km 4 Serang</td>';
		$html .= '</tr>';
		$html .= '</table>';
		
		$html .= '<p>&nbsp;</p>';
		
		$html .= '<p>Dengan ini melaporkan hasil kegiatan Perjalanan Dinas :</p>';
		
		$html .= '<table>';
		$html .= '<tr>';
		$html .= '<td style="width:15%">Tujuan</td>';
		$html .= '<td style="width:85%">: '.$model->getRelationField("spd","tujuan").'</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>Tgl. Berangkat</td>';
		$html .= '<td>: '.Bantu::tanggal($model->getRelationField("spd","tgl_pergi")).'</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>Tgl. Kembali</td>';
		$html .= '<td>: '.Bantu::tanggal($model->getRelationField("spd","tgl_kembali")).'</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>&nbsp;</td>';
		$html .= '<td>&nbsp;</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>Hasil Kegiatan</td>';
		$html .= '<td>:</td>';
		$html .= '</tr>';
		$html .= '</table>';
		
		$html .= '<p>&nbsp;</p>';
		
		$html .= '<table>';
		$html .= '<tr>';
		$html .= '<td style="width:5%">1. </td>';
		$html .= '<td style="width:95%">'.$hasil_kegiatan_1.'</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td style="width:5%">&nbsp;</td>';
		$html .= '<td style="width:95%">&nbsp;</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>2. </td>';
		$html .= '<td>'.$hasil_kegiatan_2.'</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td style="width:5%">&nbsp;</td>';
		$html .= '<td style="width:95%">&nbsp;</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>3. </td>';
		$html .= '<td>'.$hasil_kegiatan_3.'</td>';
		$html .= '</tr>';
		$html .= '</table>';
		
		$html .= '<p>&nbsp;</p>';
		
		$html .= '<p>Demikian laporan hasil perjalanan dinas ini, agar menjadi maklum</p>';
		
		
		$html .= '<p>&nbsp;</p>';
		
		$html .= '<table>';
		$html .= '<tr>';
		$html .= '<td style="width:50%">&nbsp;</td>';
		$html .= '<td style="width:50%;text-align:center">Serang, '.Bantu::tanggal($model->tanggal_lhp).'</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>&nbsp;</td>';
		$html .= '<td style="text-align:center">&nbsp;</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>&nbsp;</td>';
		$html .= '<td style="text-align:center">Yang Melaksanakan Tugas,</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>&nbsp;</td>';
		$html .= '<td>&nbsp;</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>&nbsp;</td>';
		$html .= '<td>&nbsp;</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>&nbsp;</td>';
		$html .= '<td>&nbsp;</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>&nbsp;</td>';
		$html .= '<td>&nbsp;</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>&nbsp;</td>';
		$html .= '<td>&nbsp;</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>&nbsp;</td>';
		$html .= '<td style="text-align:center;"><u>'.$model->getRelationField("pegawai","nama").'</u></td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>&nbsp;</td>';
		$html .= '<td style="text-align:center">NIP. <span style="text-align:center">'.$model->getRelationField("pegawai","nip").'</span></td>';
		$html .= '</tr>';
		$html .= '</table>';
		
		$pdf->writeHTML($html, true, true, true, true, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }
	
	public function actionCetakBlanko($id,$id_pengikut)
	{
		$pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Laporan Hasil Perjalanan Dinas');
		$pdf->SetTitle('Laporan Hasil Perjalanan Dinas');
		$pdf->SetSubject('Laporan Hasil Perjalanan Dinas');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', Setting::getValue('font_size'));
		
		$lhp = Lhp::model()->findByPk($id);
		$pengikut = Pegawai::model()->findByPk($id_pengikut);
			
		$nama = PosisiCetak::model()->getKolomByJenisSurat(3,'nama');
		$nip = PosisiCetak::model()->getKolomByJenisSurat(3,'nip');
		$jabatan = PosisiCetak::model()->getKolomByJenisSurat(3,'jabatan');
		$alamat = PosisiCetak::model()->getKolomByJenisSurat(3,'alamat');
		$tujuan = PosisiCetak::model()->getKolomByJenisSurat(3,'tujuan');
		$tgl_berangkat = PosisiCetak::model()->getKolomByJenisSurat(3,'tgl_berangkat');
		$tgl_kembali = PosisiCetak::model()->getKolomByJenisSurat(3,'tgl_kembali');
		$tanggal = PosisiCetak::model()->getKolomByJenisSurat(3,'tanggal');
		$nama_ttd = PosisiCetak::model()->getKolomByJenisSurat(3,'nama_ttd');
		$nip_ttd = PosisiCetak::model()->getKolomByJenisSurat(3,'nip_ttd');
		$hasil_kegiatan_1 = PosisiCetak::model()->getKolomByJenisSurat(3,'hasil_kegiatan_1');
		$hasil_kegiatan_2 = PosisiCetak::model()->getKolomByJenisSurat(3,'hasil_kegiatan_2');
		$hasil_kegiatan_3 = PosisiCetak::model()->getKolomByJenisSurat(3,'hasil_kegiatan_3');
			
		$pdf->writeHTMLCell(0,0,$nama->horisontal,$nama->vertikal,'<span style="font-size:'.$nama->font_size.'">'.$lhp->getRelationField('pegawai','nama').'</span>');
		$pdf->writeHTMLCell(0,0,$nip->horisontal,$nip->vertikal,'<span style="font-size:'.$nip->font_size.'">'.$pengikut->getNip().'</span>');
		$pdf->writeHTMLCell(0,0,$jabatan->horisontal,$jabatan->vertikal,'<span style="font-size:'.$jabatan->font_size.'">'.$pengikut->jabatan.'</span>');
		$pdf->writeHTMLCell(0,0,$alamat->horisontal,$alamat->vertikal,'<span style="font-size:'.$alamat->font_size.'">'.'Jl. Raya Jakarta KM. 04 Serang'.'</span>');
		$pdf->writeHTMLCell(0,0,$tujuan->horisontal,$tujuan->vertikal,'<span style="font-size:'.$tujuan->font_size.'">'.$lhp->getRelationField('spd','tujuan').'</span>');
		$pdf->writeHTMLCell(0,0,$tgl_berangkat->horisontal,$tgl_berangkat->vertikal,'<span style="font-size:'.$tgl_berangkat->font_size.'">'.Bantu::tanggal($lhp->getRelationField('spd','tgl_pergi')).'</span>');
		$pdf->writeHTMLCell(0,0,$tgl_kembali->horisontal,$tgl_kembali->vertikal,'<span style="font-size:'.$tgl_kembali->font_size.'">'.Bantu::tanggal($lhp->getRelationField('spd','tgl_kembali')).'</span>');
		$pdf->writeHTMLCell(0,0,$tanggal->horisontal,$tanggal->vertikal,'<span style="font-size:'.$tanggal->font_size.'">'.Bantu::tanggal($lhp->tanggal_lhp).'</span>');
		$pdf->writeHTMLCell(0,0,$nama_ttd->horisontal,$nama_ttd->vertikal,'<span style="font-size:'.$nama_ttd->font_size.'">'.$lhp->getRelationField('pegawai','nama').'</span>');
		$pdf->writeHTMLCell(0,0,$nip_ttd->horisontal,$nip_ttd->vertikal,'<span style="font-size:'.$nip_ttd->font_size.'">'.$lhp->getRelationField('pegawai','nip').'</span>');
		$pdf->writeHTMLCell(0,0,$hasil_kegiatan_1->horisontal,$hasil_kegiatan_1->vertikal,'<span style="font-size:'.$hasil_kegiatan_1->font_size.'">'.$lhp->hasil_kegiatan_1.'</span>');
		$pdf->writeHTMLCell(0,0,$hasil_kegiatan_2->horisontal,$hasil_kegiatan_2->vertikal,'<span style="font-size:'.$hasil_kegiatan_2->font_size.'">'.$lhp->hasil_kegiatan_2.'</span>');
		$pdf->writeHTMLCell(0,0,$hasil_kegiatan_3->horisontal,$hasil_kegiatan_3->vertikal,'<span style="font-size:'.$hasil_kegiatan_3->font_size.'">'.$lhp->hasil_kegiatan_3.'</span>');
		
		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
	}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			$model = $this->loadModel($id);
			// we only allow deletion via POST request
			$model->delete();
			
			if(Yii::app()->controller->action->id == 'admin')
			{
				if(!isset($_GET['ajax']))
					$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			} else {
				$this->redirect(array('spd/view','id'=>$model->id_spd));
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Lhp');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionCetak()
	{
		$model=new Lhp('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Lhp']))
			$model->attributes=$_GET['Lhp'];

		$this->render('cetak',array(
			'model'=>$model,
		));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer the ID of the model to be loaded
	*/
	public function loadModel($id)
	{
		$model=Lhp::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		
		return $model;
	}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='lhp-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
