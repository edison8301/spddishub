<?php

class KwitansiController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/constellation/index';

/**
* @return array action filters
*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'accessRole'
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','delete'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('admin','create','update','cetak','cetakLengkap',
					'cetakBlanko','excel','exportExcel'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionAdmin()
	{
		$model=new Kwitansi('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Kwitansi']))
			$model->attributes=$_GET['Kwitansi'];

		$this->render('admin',array(
			'kwitansi'=>$model,
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new Kwitansi;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Kwitansi']))
		{
			$model->attributes=$_POST['Kwitansi'];
			
			if($model->tanggal == '0000-00-00' OR $model->tanggal == '')
				$model->tanggal = null;
			
			$model->uang_harian = str_replace('.','',$model->uang_harian);
			$model->akomodasi = str_replace('.','',$model->akomodasi);
			$model->refresentatif = str_replace('.','',$model->refresentatif);
			$model->bbm = str_replace('.','',$model->bbm);
			$model->tol = str_replace('.','',$model->tol);
			$model->tiket = str_replace('.','',$model->tiket);
			
			$model->total = 0;
			$model->total += floatval($model->uang_harian);
			$model->total += floatval($model->akomodasi);
			$model->total += floatval($model->refresentatif);
			$model->total += floatval($model->bbm);
			$model->total += floatval($model->tol);
			$model->total += floatval($model->tiket);
			
			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('spd/view','id'=>$model->id_spd));
			}
		}

		$kwitansi = new Kwitansi('search');
		$kwitansi->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Kwitansi']))
			$kwitansi->attributes=$_GET['Kwitansi'];

			
		$akomodasi = new Akomodasi;
		$refresentatif = new Refresentatif;
		$uangHarian = new UangHarian;
		
		$this->render('create',array(
			'model'=>$model,
			//'kwitansi'=>$kwitansi,
			//'akomodasi'=>$akomodasi,
			//'refresentatif'=>$refresentatif,
			//'uangHarian'=>$uangHarian
		));
	}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Kwitansi']))
		{
			$model->attributes=$_POST['Kwitansi'];
			
			if($model->tanggal == '0000-00-00' OR $model->tanggal == '')
				$model->tanggal = null;
			
			$model->uang_harian = str_replace('.','',$model->uang_harian);
			$model->akomodasi = str_replace('.','',$model->akomodasi);
			$model->refresentatif = str_replace('.','',$model->refresentatif);
			$model->bbm = str_replace('.','',$model->bbm);
			$model->tol = str_replace('.','',$model->tol);
			$model->tiket = str_replace('.','',$model->tiket);
			
			$model->total = 0;
			$model->total += floatval($model->uang_harian*$model->spd->lama);
			$model->total += floatval($model->akomodasi);
			$model->total += floatval($model->refresentatif);
			$model->total += floatval($model->bbm);
			$model->total += floatval($model->tol);
			$model->total += floatval($model->tiket);
			
			if($model->save())
			{	
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('spd/view','id'=>$model->id_spd));
			}
		}

		$kwitansi=new Kwitansi('search');
		$kwitansi->unsetAttributes();  // clear any default values
			
		if(isset($_GET['Kwitansi']))
			$kwitansi->attributes=$_GET['Kwitansi'];

		Yii::app()->user->setFlash('info','Silahkan sunting data pada kolom yang disediakan');
		
		$akomodasi = new Akomodasi;
		$refresentatif = new Refresentatif;
		$uangHarian = new UangHarian;
		
		$this->render('update',array(
			'model'=>$model,
			'kwitansi'=>$kwitansi,
			'akomodasi'=>$akomodasi,
			'refresentatif'=>$refresentatif,
			'uangHarian'=>$uangHarian
		));
	}
	
	public function actionExcel()
	{
		$this->render('excel');
	}
	
	public function actionExportExcel()
	{
		$this->layout = '//layouts/excel';
		$this->render('_excel',array('border'=>1));
	}
	
	public function actionCetakLengkap($id)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('RINCIAN BIAYA PERJALANAN DINAS');
		$pdf->SetTitle('RINCIAN BIAYA PERJALANAN DINAS');
		$pdf->SetSubject('RINCIAN BIAYA PERJALANAN DINAS');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		$model = $this->loadModel($id);
		
		
		$tanggal = Bantu::tanggal($model->tanggal);
		if($model->tanggal=='0000-00-00' OR $model->tanggal == null)
			$tanggal = '.......................';
		
		$nomor = '.......................';
		if($model->nomor!=null)
			$nomor = $model->nomor;
			
		$program = '............................................................................................';
		if($model->program!=null)
			$program = $model->program;
			
		$kegiatan = '............................................................................................';
		if($model->kegiatan!=null)
			$kegiatan = $model->kegiatan;
			
		$pa = '.............................';
		$nip_pa = '';
		if($model->pa!=null)
		{
			$pa = $model->getRelationField("paRelation","nama");
			$nip_pa = 'Nip. '.$model->getRelationField("paRelation","nip");
		}
		
		$pptk = '.............................';
		$nip_pptk = '';
		if($model->pptk!=null)
		{
			$pptk = $model->getRelationField("paRelation","nama");
			$nip_pptk = 'Nip. '.$model->getRelationField("paRelation","nip");
		}
		
		$bendahara = '.............................';
		$nip_bendahara = '';
		if($model->bendahara!=null)
		{
			$bendahara = $model->getRelationField("bendaharaRelation","nama");
			$nip_bendahara = 'Nip. '.$model->getRelationField("bendaharaRelation","nip");			
		}
		
		$nama_penerima = '';
		$jabatan_penerima = '';
		if($model->penerima!=null)
		{
			$nama_penerima = $model->getRelationField("penerimaRelation","nama");
			$jabatan_penerima = $model->getRelationField("penerimaRelation","jabatan");
		}
		
		$html = '';
		
		$html .= '<h1 style="text-align:center">PEMERINTAH KABUPATEN SERANG<br>DINASI PERHUHUNGAN, KOMUNIKAS DAN INFORMATIKA</h1>';
		
		$html .= '
			    <table width="100%">
					<tr>
						<td width="70%">&nbsp;</td>
			    		<td width="10%">Tanggal</td>
			    		<td width="20%">: '.$tanggal.'</td>
			    	</tr>
					<tr>
						<td width="70%">&nbsp;</td>
			    		<td width="10%">Nomor</td>
			    		<td width="20%">: '.$nomor.'</td>
			    	</tr>
					<tr>
						<td colspan="3" style="font-size:5">&nbsp;</td>
					</tr>
			    </table>';
			
		$html .= '<h1 style="text-align:center">TANDA BUKTI PEMBAYARAN</h1>';
		
		$html .= '<p>Sudah terima dari Bendahara Pengeluaran Kantor Dinas Perhubungan, Komunikasi dan Informatika Kabupaten Serang</p>';
		
		$html .= '	    
			    <table>
			    <tr>
					<td width="20%">Uang Sebesar</td>
					<td width="80%">: '.Bantu::rp($model->total).'</td>
				</tr>
				<tr>
					<td>Dengan huruf</td>
					<td>: '.Bantu::getTerbilang($model->total,0).'</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>: '.$model->getUntuk().'</td>
				</tr>
				<tr>
					<td>Untuk Program</td>
					<td>: '.$program.'</td>
				</tr>
				<tr>
					<td>Kegiatan</td>
					<td>: '.$kegiatan.'</td>
				</tr>
				<tr>
					<td>Kode Rekening</td>
					<td>: '.$model->getRelationField("spd","koring").'</td>
				</tr>
			    </table>';
		
		$html .= '
			<table>
			<tr>
				<td colspan="3" width="70%">&nbsp;</td>
				<td colspan="2" width="30%" style="text-align:center">Serang, '.$tanggal.'</td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;</td>
				<td colspan="2" style="text-align:center">Yang Menerima</td>
			</tr>
			<tr>
				<td style="text-align:center">Pengguna Anggaran/</td>
				<td style="text-align:center">PPTK</td>
				<td style="text-align:center">Bendahara Pengeluaran</td>
				<td>Nama</td>
				<td style="font-size:8">: '.$nama_penerima.'</td>
			</tr>
			<tr>
				<td style="text-align:center">Kuasa Pengguna Anggaran</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>Pangkat Jabatan</td>
				<td style="font-size:8">: '.$jabatan_penerima.'</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<Td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>Satuan Kerja</td>
				<td>: </td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<Td>&nbsp;</td>
				<td>&nbsp;</td>
				<td colspan="2" style="text-align:center">Tanda Tangan</td>
			</tr>
			<tr>
				<td colspan="5">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align:center">('.$pa.')</td>
				<td style="text-align:center">('.$pptk.')</td>
				<td style="text-align:center">('.$bendahara.')</td>
				<td colspan="2" style="text-align:center">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align:center">'.$nip_pa.'</td>
				<Td>'.$nip_pptk.'</td>
				<td>'.$nip_bendahara.'</td>
				<td colspan="2" style="text-align:center">(..................................)</td>
			</tr>
			</table>';
		   
		$pdf->writeHTML($html, true, false, false, false, '');
		//Close and output PDF document
		//$pdf->copyPage(1);
		$pdf->Output('.pdf', 'I');	
 
    }
	
	public function actionCetakBlanko($id)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Kwitansi Surat Perjalanan Dinas');
		$pdf->SetTitle('Kwitansi Surat Perjalanan Dinas');
		$pdf->SetSubject('Kwitansi Surat Perjalanan Dinas');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetRightMargin(0);	
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', Setting::getValue('font_size'));
		
		$kwitansi = Kwitansi::model()->findByPk($id);
		
		$nomor = PosisiCetak::model()->getKolomByJenisSurat(2,'nomor');
		$tanggal = PosisiCetak::model()->getKolomByJenisSurat(2,'tanggal');
		$tanggal_ttd = PosisiCetak::model()->getKolomByJenisSurat(2,'tanggal_ttd');
		$uang_sebesar = PosisiCetak::model()->getKolomByJenisSurat(2,'uang_sebesar');
		$dengan_huruf = PosisiCetak::model()->getKolomByJenisSurat(2,'dengan_huruf');
		$untuk = PosisiCetak::model()->getKolomByJenisSurat(2,'untuk');
		//$rincian = PosisiCetak::model()->getKolomByJenisSurat(2,'rincian');
		$untuk_program = PosisiCetak::model()->getKolomByJenisSurat(2,'untuk_program');
		$kegiatan = PosisiCetak::model()->getKolomByJenisSurat(2,'kegiatan');
		$kode_rekening = PosisiCetak::model()->getKolomByJenisSurat(2,'kode_rekening');
		$pengguna_anggaran = PosisiCetak::model()->getKolomByJenisSurat(2,'pengguna_anggaran');
		$nip_pengguna_anggaran = PosisiCetak::model()->getKolomByJenisSurat(2,'nip_pengguna_anggaran');
		$pptk = PosisiCetak::model()->getKolomByJenisSurat(2,'pptk');
		$nip_pptk = PosisiCetak::model()->getKolomByJenisSurat(2,'nip_pptk');
		$bendahara = PosisiCetak::model()->getKolomByJenisSurat(2,'bendahara');
		$nip_bendahara = PosisiCetak::model()->getKolomByJenisSurat(2,'nip_bendahara');
		$nama_penerima = PosisiCetak::model()->getKolomByJenisSurat(2,'nama_penerima');
		$pangkat_penerima = PosisiCetak::model()->getKolomByJenisSurat(2,'pangkat_penerima');
		$satuan_kerja_penerima = PosisiCetak::model()->getKolomByJenisSurat(2,'satuan_kerja_penerima');
		
		$pdf->writeHTMLCell(0,0,$nomor->horisontal,$nomor->vertikal,'<span style="font-size:'.$nomor->font_size.'">'.$kwitansi->nomor.'</span>');
		$pdf->writeHTMLCell(0,0,$tanggal->horisontal,$tanggal->vertikal,'<span style="font-size:'.$tanggal->font_size.'">'.Bantu::tanggal($kwitansi->tanggal).'</span>');
		$pdf->writeHTMLCell(0,0,$tanggal_ttd->horisontal,$tanggal_ttd->vertikal,'<span style="font-size:'.$tanggal_ttd->font_size.'">'.Bantu::tanggal($kwitansi->tanggal).'</span>');
		
		$pdf->writeHTMLCell(0,0,$uang_sebesar->horisontal,$uang_sebesar->vertikal,'<span style="font-size:'.$uang_sebesar->font_size.'">'.number_format($kwitansi->total,0,',','.').'</span>');
		$pdf->writeHTMLCell(0,0,$dengan_huruf->horisontal,$dengan_huruf->vertikal,'<span style="font-size:'.$dengan_huruf->font_size.'">'.trim(Bantu::getTerbilang($kwitansi->total,0)).' rupiah</span>');
		$pdf->writeHTMLCell(0,0,$untuk->horisontal,$untuk->vertikal,''.'<span style="font-size:'.$untuk->font_size.'">'.$kwitansi->getUntuk().'</span>');
		//$pdf->writeHTMLCell(0,0,$rincian->horisontal,$rincian->vertikal,''.'<span style="font-size:'.$rincian->font_size.'">'.'dengan rincian : .'</span>');
		$pdf->writeHTMLCell(0,0,$untuk_program->horisontal,$untuk_program->vertikal,'<span style="font-size:'.$untuk_program->font_size.'">'.$kwitansi->program.'</span>');
		$pdf->writeHTMLCell(0,0,$kegiatan->horisontal,$kegiatan->vertikal,'<span style="font-size:'.$kegiatan->font_size.'">'.$kwitansi->kegiatan.'</span>');
		$pdf->writeHTMLCell(0,0,$kode_rekening->horisontal,$kode_rekening->vertikal,'<span style="font-size:'.$kode_rekening->font_size.'">'.$kwitansi->getRelationField('spd','koring').'</span>');
		$pdf->writeHTMLCell(0,0,$pengguna_anggaran->horisontal,$pengguna_anggaran->vertikal,'<span style="font-size:'.$pengguna_anggaran->font_size.'">'.$kwitansi->getRelationField('paRelation','nama').'</span>');
		$pdf->writeHTMLCell(0,0,$nip_pengguna_anggaran->horisontal,$nip_pengguna_anggaran->vertikal,'<span style="font-size:'.$nip_pengguna_anggaran->font_size.'">'.'NIP. '.$kwitansi->getRelationField('paRelation','nip').'</span>');
		$pdf->writeHTMLCell(0,0,$pptk->horisontal,$pptk->vertikal,'<span style="font-size:'.$pptk->font_size.'">'.$kwitansi->getRelationField('pptkRelation','nama').'</span>');
		$pdf->writeHTMLCell(0,0,$nip_pptk->horisontal,$nip_pptk->vertikal,'<span style="font-size:'.$nip_pptk->font_size.'">'.'NIP. '.$kwitansi->getRelationField('pptkRelation','nip').'</span>');
		$pdf->writeHTMLCell(0,0,$bendahara->horisontal,$bendahara->vertikal,'<span style="font-size:'.$bendahara->font_size.'">'.$kwitansi->getRelationField('bendaharaRelation','nama').'</span>');
		$pdf->writeHTMLCell(0,0,$nip_bendahara->horisontal,$nip_bendahara->vertikal,'<span style="font-size:'.$nip_bendahara->font_size.'">'.'NIP. '.$kwitansi->getRelationField('bendaharaRelation','nip').'</span>');
		$pdf->writeHTMLCell(0,0,$nama_penerima->horisontal,$nama_penerima->vertikal,'<span style="font-size:'.$nama_penerima->font_size.'">'.$kwitansi->getRelationField('penerimaRelation','nama').'</span>');
		$pdf->writeHTMLCell(0,0,$pangkat_penerima->horisontal,$pangkat_penerima->vertikal,'<span style="font-size:'.$pangkat_penerima->font_size.'">'.$kwitansi->getRelationField('penerimaRelation','jabatan').'</span>');
		$pdf->writeHTMLCell(0,0,$satuan_kerja_penerima->horisontal,$satuan_kerja_penerima->vertikal,'<span style="font-size:'.$satuan_kerja_penerima->font_size.'">'.'');
			
		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
    }
	
	

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			$model = $this->loadModel($id);
			// we only allow deletion via POST request
			$model->delete();
			
			if(Yii::app()->controller->action->id == 'admin')
			{
				if(!isset($_GET['ajax']))
					$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			} else {
				$this->redirect(array('spd/view','id'=>$model->id_spd));
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Kwitansi');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

	/**
	* Manages all models.
	*/
	public function actionCetak()
	{
		$model=new Kwitansi('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Kwitansi']))
			$model->attributes=$_GET['Kwitansi'];

		$this->render('cetak',array(
			'model'=>$model,
		));
	}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Kwitansi::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='kwitansi-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
