<?php

class ExportController extends Controller{
 
    // no layouts here
    public $layout = '//layouts/plain';
    
    public function actionExportDataPegawai($id)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Data Pegawai');
		$pdf->SetSubject('Data Pegawai');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 14);
		
		
		
		$html='
		<h4 align="center">Detail Pegawai</h4>
		<table border="1" style="padding:5px">
			<tr style="background-color:#ccc; color:#000;">
				<td width="6%" align="center"><b>No</b></td>
				<td align="center"><b>NIP</b></td>
				<td align="center"><b>Nama</b></td>
				<td align="center"><b>Golongan</b></td>
				<td align="center"><b>Jabatan</b></td>
				<td align="center"><b>Tgl Lahir</b></td>	
			</tr>';
			
			$i=1;
			foreach(Pegawai::model()->findAllbyPk($id) as $data)
			{
			    $html .= '<tr>
				<td style="text-align: center;">'.$i.'</td>
				<td>'.$data->nip.'</td>
				<td>'.$data->nama.'</td>
				<td>'.$data->id_golongan.'</td>
				<td>'.$data->jabatan.'</td>
				<td>'.$data->tgl_lahir.'</td>
			    </tr>';
			
			$i++;
			}
		$html .='</table>';
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }

    public function actionExportPegawai()
    {
	        $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Data Pegawai');
		$pdf->SetSubject('Data Pegawai');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 14);
		
		
		
		$html='
		<h4 align="center">Data Pegawai</h4>
		<table border="1" style="padding:5px">
			<tr style="background-color:#ccc; color:#000;">
				<td width="6%" align="center"><b>No</b></td>
				<td align="center"><b>NIP</b></td>
				<td align="center"><b>Nama</b></td>
				<td align="center"><b>Golongan</b></td>
				<td align="center"><b>Jabatan</b></td>
				<td align="center"><b>Tgl Lahir</b></td>	
			</tr>';
			
			$i=1;
			foreach(Pegawai::model()->findAll() as $data)
			{
			    $html .= '<tr>
				<td style="text-align: center;">'.$i.'</td>
				<td>'.$data->nip.'</td>
				<td>'.$data->nama.'</td>
				<td>'.$data->id_golongan.'</td>
				<td>'.$data->jabatan.'</td>
				<td>'.$data->tgl_lahir.'</td>
			    </tr>';
			
			$i++;
			}
		$html .='</table>';
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
	 
    }

    public function actionExportDataPejabat($id)
    {
	        $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Data Pegawai');
		$pdf->SetSubject('Data Pegawai');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 14);
		
		
		
		$html='
		<h4 align="center">Detail Pejabat</h4>
		<table border="1">
			<tr style="background-color:#ccc; color:#000;">
				<td width="6%" align="center"><b>No</b></td>
				<td align="center"><b>NIP</b></td>
				<td align="center"><b>Nama</b></td>
				<td align="center"><b>Jabatan</b></td>	
			</tr>';
			
			$i=1;
			foreach(Pejabat::model()->findAllbyPk($id) as $data)
			{
			    $html .= '<tr>
				<td style="text-align: center;">'.$i.'</td>
				<td>'.$data->nip.'</td>
				<td>'.$data->nama.'</td>
				<td>'.$data->jabatan.'</td>
			    </tr>';
			
			$i++;
			}
		$html .='</table>';
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }

    public function actionExportPejabat()
    {
	        $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Data Pegawai');
		$pdf->SetSubject('Data Pegawai');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 14);
		
		$html='
		<h4 align="center">Data Pejabat</h4>
		<table border="1" style="padding:5px">
			<tr style="background-color:#ccc; color:#000;">
				<td width="6%" align="center"><b>No</b></td>
				<td align="center"><b>NIP</b></td>
				<td align="center"><b>Nama</b></td>
				<td align="center"><b>Jabatan</b></td>	
			</tr>';
			
			$i=1;
			foreach(Pejabat::model()->findAll() as $data)
			{
			    $html .= '<tr>
				<td style="text-align: center;">'.$i.'</td>
				<td>'.$data->nip.'</td>
				<td>'.$data->nama.'</td>
				<td>'.$data->jabatan.'</td>
			    </tr>';
			
			$i++;
			}
		$html .='</table>';
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
	 
    }

    public function actionExportDataSpdSetda($id)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');

	    
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Data Pegawai');
		$pdf->SetSubject('Data Pegawai');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 10);
		
		$txt = <<<EOD
TCPDF Example 003

Custom page header and footer are defined by extending the TCPDF class and overriding the Header() and Footer() methods.
EOD;
		
		$html=' <table style="border-bottom:1px;">
					<tr>
						<td width="15%">
							<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="65px">
						</td>
						<td width="85%">
							<p style="font-size:70px;line-height:101%;">
							&nbsp;<b>PEMERINTAH KABUPATEN SERANG</b><br>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>SEKRETARIAT DAERAH</b><br>
							<i style="font-size:29px;">Jalan Raya Serang-Jakarta Km 4, Kabupaten Serang Telp.(0254)280529</i></p>
						</td>
					</tr>
				</table>';
			
			$i=1;
			foreach(Spd::model()->findAllbyPk($id) as $data)
			{
				$pejabat=Pejabat::model()->findbyPk($data->id_pejabat);
				$pegawai=Pegawai::model()->findbyPk($data->id_pegawai);
				$golongan=Golongan::model()->findbyPk($pegawai->id_golongan);
			    $html .= '
			   
			 	<h3><span align="center" style="text-decoration:underline;line-height:10%;">Surat Perjalanan Dinas</span></h3>
			 	<p align="center">Nomor : '.$data->nomor.'</p>
				<table border="1" style="padding:5px">	
				<tr>
					<td width="6%" align="center">1</td>
					<td width="47%" >Pejabat berwenang yang memberi perintah</td>
					<td width="47%" >'.$pejabat->jabatan.'</td>
				</tr>
				<tr>
					<td width="6%" align="center">2</td>
					<td width="47%" >Nama/NIP pegawain yang diperintahkan</td>
					<td width="47%" ><b>'.$data->pegawai->nama.'</b><br>'.$pegawai->nip.'</td>
				</tr>
				<tr>
					<td width="6%" align="center">3</td>
					<td width="47%" >a. Pangkat dan Golongan Ruang Gaji</td>
					<td width="47%" >'.$golongan->nama.'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >b. Jabatan / Instansi</td>
					<td width="47%" >'.$pegawai->jabatan.'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >c. Tingkat biaya perjalanan dinas</td>
					<td width="47%" >-</td>
				</tr>
				<tr>
					<td width="6%" align="center">4</td>
					<td width="47%" >Maksud perjalanan dinas</td>
					<td width="47%" >'.$data->maksud.'</td>
				</tr>
				<tr>
					<td width="6%" align="center">5</td>
					<td width="47%" >Alat angkutan yang dipergunakan</td>
					<td width="47%" >'.$data->kendaraan.'</td>
				</tr>
				<tr>
					<td width="6%" align="center">6</td>
					<td width="47%" >a. Tempat berangkat</td>
					<td width="47%" >'.$data->tempat_berangkat.'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >b. Tempat tujuan</td>
					<td width="47%" >'.$data->tujuan.'</td>
				</tr>
				<tr>
					<td width="6%" align="center">7</td>
					<td width="47%" >a. Lamanya perjalanan dinas</td>
					<td width="47%" >'.$data->lama.' Hari</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >b. Tanggal berangkat</td>
					<td width="47%" >'.date('d F Y', strtotime($data->tgl_pergi)).'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >c. Tanggal harus kembali / tiba di tempat baru*)</td>
					<td width="47%" >'.date('d F Y', strtotime($data->tgl_kembali)).'</td>
				</tr>
				<tr>
					<td width="6%" align="center">8</td>
					<td width="47%" ><b>Pengikut &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama:</b></td>
					<td width="22%"><b>Tgl. Lahir</b></td>
					<td width="25%"><b>Jabatan</b></td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td>';
				foreach(Pengikut::model()->findAllByAttributes(array('id_spd'=>$id)) as $pengikut)
					{
					$html .='
					
							'.$pengikut->pegawai->nama.'<br>';
					}
					$html .='</td><td>';
				foreach(Pengikut::model()->findAllByAttributes(array('id_spd'=>$id)) as $pengikut)
					{
					$html .='
					
							'.date('d F Y', strtotime($pengikut->pegawai->tgl_lahir)).'<br>';
					}
					$html .='</td><td>';
				foreach(Pengikut::model()->findAllByAttributes(array('id_spd'=>$id)) as $pengikut)
					{
					$html .='
					
							'.$pengikut->pegawai->jabatan.'<br>';
					}
			$html .='
					</td>	
				</tr>
				<tr>
					<td width="6%" align="center">9</td>
					<td width="47%" >
						Pembebanan Anggaran<br>
						a. Instansi<br>
						b. Mata Anggaran
					</td>
					<td width="47%" >
						<br><br>
						'.$data->instansi.'<br>
						'.$data->koring.'
					</td>
				</tr>
				<tr>
					<td width="6%" align="center">10</td>
					<td width="47%" >Keterangan Lain-lain</td>
					<td width="47%" ></td>
				</tr>';
		
		$i++;
			}
		$html .='</table>
		';
		
		foreach(Spd::model()->findAllbyPk($id) as $data){
			$pejabat=Pejabat::model()->findbyPk($data->id_pejabat);
				$html .='
				<div>&nbsp;</div>
				<table>
					<tr>
						<td width="70%">
						</td>
						<td width="30%">
							<p>Di keluarkan di : '.$data->tempat_berangkat.'<br>
							Pada Tanggal : '.date('d F Y', strtotime($data->tanggal)).'</p>
							<p align="center">'.$pejabat->jabatan.'</p>
							<p>&nbsp;</p>
							<p align="center" style="line-height:70%;"><span style="text-decoration:underline;"><b>'.$pejabat->nama.'</b></span></p>
							<p align="center" style="line-height:10%;">'.$pejabat->nip.'</p>
						</td>
					</tr>
				</table>';
		}
		
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }

    public function actionBlankoSpdSetda()
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');

	    
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Data Pegawai');
		$pdf->SetSubject('Data Pegawai');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		$txt = <<<EOD
TCPDF Example 003

Custom page header and footer are defined by extending the TCPDF class and overriding the Header() and Footer() methods.
EOD;
		
		$html=' <table style="border-bottom:1px;">
					<tr>
						<td width="15%">
							<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="65px">
						</td>
						<td width="85%">
							<p style="font-size:70px;line-height:101%;">
							&nbsp;<b>PEMERINTAH KABUPATEN SERANG</b><br>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>SEKRETARIAT DAERAH</b><br>
							<i style="font-size:29px;">Jalan Raya Serang-Jakarta Km 4, Kabupaten Serang Telp.(0254)280529</i></p>
						</td>
					</tr>
				</table>';
			
			
			
			    $html .= '
			   
			 	<h3><span align="center" style="text-decoration:underline;line-height:10%;">Surat Perjalanan Dinas</span></h3>
			 	<p align="center">Nomor : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			 		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			 	</p>
				<table border="1" style="padding:5px">	
				<tr>
					<td width="6%" align="center">1</td>
					<td width="47%" >Pejabat berwenang yang memberi perintah</td>
					<td width="47%" >&nbsp;</td>
				</tr>
				<tr>
					<td width="6%" align="center">2</td>
					<td width="47%" >Nama/NIP pegawain yang diperintahkan</td>
					<td width="47%" ><b>&nbsp;</b><br>&nbsp;</td>
				</tr>
				<tr>
					<td width="6%" align="center">3</td>
					<td width="47%" >a. Pangkat dan Golongan Ruang Gaji</td>
					<td width="47%" >&nbsp;</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >b. Jabatan / Instansi</td>
					<td width="47%" >&nbsp;</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >c. Tingkat biaya perjalanan dinas</td>
					<td width="47%" ></td>
				</tr>
				<tr>
					<td width="6%" align="center">4</td>
					<td width="47%" >Maksud perjalanan dinas</td>
					<td width="47%" >&nbsp;</td>
				</tr>
				<tr>
					<td width="6%" align="center">5</td>
					<td width="47%" >Alat angkutan yang dipergunakan</td>
					<td width="47%" >&nbsp;</td>
				</tr>
				<tr>
					<td width="6%" align="center">6</td>
					<td width="47%" >a. Tempat berangkat</td>
					<td width="47%" >&nbsp;</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >b. Tempat tujuan</td>
					<td width="47%" >&nbsp;</td>
				</tr>
				<tr>
					<td width="6%" align="center">7</td>
					<td width="47%" >a. Lamanya perjalanan dinas</td>
					<td width="47%" >&nbsp;</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >b. Tanggal berangkat</td>
					<td width="47%" >&nbsp;</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >c. Tanggal harus kembali / tiba di tempat baru*)</td>
					<td width="47%" >&nbsp;</td>
				</tr>
				<tr>
					<td width="6%" align="center">8</td>
					<td width="47%" >Pengikut</td>
					<td width="22%">Tgl. Lahir</td>
					<td width="25%">Jabatan</td>
				</tr>';
				
					$html .='
						<tr>
							<td width="6%" align="center"></td>
							<td width="47%" >&nbsp;</td>
							<td width="22%" >&nbsp;</td>
							<td width="25%" >&nbsp;</td>
						</tr>
						<tr>
							<td width="6%" align="center"></td>
							<td width="47%" >&nbsp;</td>
							<td width="22%" >&nbsp;</td>
							<td width="25%" >&nbsp;</td>
						</tr>
						<tr>
							<td width="6%" align="center"></td>
							<td width="47%" >&nbsp;</td>
							<td width="22%" >&nbsp;</td>
							<td width="25%" >&nbsp;</td>
						</tr>
						<tr>
							<td width="6%" align="center"></td>
							<td width="47%" >&nbsp;</td>
							<td width="22%" >&nbsp;</td>
							<td width="25%" >&nbsp;</td>
						</tr>
						';
					
			$html .='	
				
				<tr>
					<td width="6%" align="center">9</td>
					<td width="47%" >
						Pembebanan Anggaran<br>
						a. Instansi<br>
						b. Mata Anggaran
					</td>
					<td width="47%" >
						<br><br>
						&nbsp;<br>
						&nbsp;
					</td>
				</tr>
				<tr>
					<td width="6%" align="center">10</td>
					<td width="47%" >Keterangan Lain-lain</td>
					<td width="47%" ></td>
				</tr>';
		
		
		$html .='</table>
		';
		
	
				$html .='
				<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;
				</div>
				<table>
					<tr>
						<td width="40%">
						</td>
						<td width="50%" align="center">
							<p>Di keluarkan di : &nbsp;<br>
							Pada Tanggal : &nbsp;</p>
							<p>&nbsp;</p>
							<p style="line-height:70%;"><b>&nbsp;</b></p>
							<p style="line-height:10%;">&nbsp;</p>
						</td>
					</tr>
				</table>';
		
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }

    public function actionExportDataSptSetda($id)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'A3', true, 'UTF-8');

	    
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perintah Tugas');
		$pdf->SetTitle('Surat Perintah Tugas');
		$pdf->SetSubject('Surat Perintah Tugas');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		$txt = <<<EOD
TCPDF Example 003

Custom page header and footer are defined by extending the TCPDF class and overriding the Header() and Footer() methods.
EOD;
		
		$html=' <table style="border-bottom:1px;">
					<tr>
						<td width="15%">
							<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="65px">
						</td>
						<td width="85%">
							<p style="font-size:70px;line-height:101%;">
							&nbsp;<b>PEMERINTAH KABUPATEN SERANG</b><br>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>SEKRETARIAT DAERAH</b><br>
							<i style="font-size:29px;">Jalan Raya Serang-Jakarta Km 4, Kabupaten Serang Telp.(0254)280529</i></p>
						</td>
					</tr>
				</table>';
			
			$i=1;
			foreach(Spd::model()->findAllbyPk($id) as $data)
			{
				$pejabat=Pejabat::model()->findbyPk($data->id_pejabat);
				$pegawai=Pegawai::model()->findbyPk($data->id_pegawai);
				$golongan=Golongan::model()->findbyPk($pegawai->id_golongan);
			    $html .= '
			   	<p><span align="center" style="text-decoration:underline;line-height:10%;"><b>SURAT TUGAS</b></span></p>
			 	<p align="center" style="line-height:10%;">Nomor:'.$data->nomor_spt.'</p>


			 	<p style="margin:5px">Yang bertanda tangan dibawah ini:</p>
				<table style="margin:5px">	
					<tr>
						<td width="30%" >Nama</td>
						<td width="70%" >: <b>'.$pejabat->nama.'</b></td>
					</tr>
					<tr>
						<td width="30%" >NIP</td>
						<td width="70%" >: '.$pejabat->nip.'</td>
					</tr>
					<tr>
						<td width="30%" >Jabatan</td>
						<td width="70%" >: '.$pejabat->jabatan.'<br></td>
					</tr>
				</table>
				<p style="margin:5px" align="center"><b>Memberikan Perintah Kepada:</b></p>
				<table style="margin:5px">
					<tr>
						<td width="30%" >Nama</td>
						<td width="70%" >: <b>'.$pegawai->nama.'</b></td>
					</tr>
					<tr>
						<td width="30%" >NIP</td>
						<td width="70%" >: '.$pegawai->nip.'</td>
					</tr>
					<tr>
						<td width="30%" >Jabatan</td>
						<td width="70%" >: '.$pegawai->jabatan.'<br></td>
					</tr>
				</table>
				<p style="margin:5px">Untuk melaksanakan tugas kegiatan Perjalanan Dinas:</p>
				<table style="margin:5px">
					<tr>
						<td width="30%" >Tujuan</td>
						<td width="70%" >: '.$data->tujuan.'</td>
					</tr>
					<tr>
						<td width="30%" >Tanggal Berangkat</td>
						<td width="70%" >: '.date('d F Y', strtotime($data->tgl_pergi)).'</td>
					</tr>
					<tr>
						<td width="30%" >Tanggal kembali</td>
						<td width="70%" >: '.date('d F Y', strtotime($data->tgl_kembali)).'<br></td>
					</tr>
				</table>';
				
					
				$html .='<p style="margin:5px"><b>Pengikut:</b></p>
				<table border="1px" align="center">
					<tr>
						<td width="20%" ><b>NIP</b></td>
						<td width="40%" ><b>Nama</b></td>
						<td width="40%" ><b>Jabatan</b></td>
					</tr>
				';
				foreach(Pengikut::model()->findAllByAttributes(array('id_spd'=>$id)) as $pengikut)
					{
					$html .='
						<tr>
							<td width="20%" >'.$pengikut->pegawai->nip.'</td>
							<td width="40%" >'.$pengikut->pegawai->nama.'</td>
							<td width="40%" >'.$pengikut->pegawai->jabatan.'</td>
						</tr>';
					}
				

		$i++;
			}
		$html .='</table>

		';
		
		foreach(Spd::model()->findAllbyPk($id) as $data){
			$pejabat=Pejabat::model()->findbyPk($data->id_pejabat);
				$html .='
				<p>Kegiatan yang harus dilaksanakan:<br>&nbsp;&nbsp;<i>'.$data->maksud.'</i></p><br>
				<p>Demikian Surat Perintah ini di buat, agar dilaksanakan dengan sebaik-baiknya.</p><br>
				<table>
					<tr>
						<td width="50%">
						</td>
						<td width="50%" align="center">
							<p>Serang, '.date('d F Y', strtotime($data->tanggal)).'<br>
							Yang Memberi Perintah</p>
							<p>&nbsp;</p>
							<p style="line-height:70%;"><span style="text-decoration:underline;"><b>'.$pejabat->nama.'</b></span></p>
							<p style="line-height:10%;">NIP. '.$pejabat->nip.'</p>
						</td>
					</tr>
				</table>';
		}
		
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }

    public function actionExportDataSptSetdaNoPengikut($id)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');

		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perintah Tugas');
		$pdf->SetTitle('Surat Perintah Tugas');
		$pdf->SetSubject('Surat Perintah Tugas');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 12);
				
		$html = '<table style="border-bottom:1px;">';
		$html .= '<tr>';
		$html .= 	'<td width="15%"><img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="65px"></td>';
		$html .= 	'<td width="85%" style="text-align:center">';
		$html .= 		'<p style="font-size:70px;line-height:101%;"><b>PEMERINTAH KABUPATEN SERANG</b><br>';
		$html .= 		'<b>SEKRETARIAT DAERAH</b><br>';
		$html .= 		'<i style="font-size:29px;">Jalan Raya Serang-Jakarta Km 4, Kabupaten Serang Telp.(0254)280529</i></p>';
		$html .= 	'</td>';
		$html .= '</tr>';
		$html .= '</table>';
			
			$i=1;
			foreach(Spd::model()->findAllbyPk($id) as $data)
			{
				$pejabat=Pejabat::model()->findbyPk($data->id_pejabat);
				$pegawai=Pegawai::model()->findbyPk($data->id_pegawai);
				$golongan=Golongan::model()->findbyPk($pegawai->id_golongan);
			    $html .= '
			   	<p><span align="center" style="text-decoration:underline;line-height:10%;"><b>S U R A T &nbsp;&nbsp; T U G A S</b></span></p>
			 	<p align="center" style="line-height:10%;">Nomor: '.$data->nomor_spt.'</p>


			 	<p style="margin:5px">Yang bertanda tangan dibawah ini:</p>
				<table style="margin:5px">	
					<tr>
						<td width="30%" >Nama</td>
						<td width="70%" >: <b>'.$pejabat->nama.'</b></td>
					</tr>
					<tr>
						<td width="30%" >NIP</td>
						<td width="70%" >: '.$pejabat->nip.'</td>
					</tr>
					<tr>
						<td width="30%" >Jabatan</td>
						<td width="70%" >: '.$pejabat->jabatan.'<br></td>
					</tr>
				</table>
				<p style="margin:5px" align="center"><b>Memberikan Perintah Kepada:</b></p>
				<table style="margin:5px">
					<tr>
						<td width="30%" >Nama</td>
						<td width="70%" >: <b>'.$pegawai->nama.'</b></td>
					</tr>
					<tr>
						<td width="30%" >NIP</td>
						<td width="70%" >: '.$pegawai->nip.'</td>
					</tr>
					<tr>
						<td width="30%" >Jabatan</td>
						<td width="70%" >: '.$pegawai->jabatan.'<br></td>
					</tr>
				</table>
				<p style="margin:5px">Untuk melaksanakan tugas kegiatan Perjalanan Dinas:</p>
				<table style="margin:5px">
					<tr>
						<td width="30%" >Tujuan</td>
						<td width="70%" >: '.$data->tujuan.'</td>
					</tr>
					<tr>
						<td width="30%" >Tanggal Berangkat</td>
						<td width="70%" >: '.date('j F Y', strtotime($data->tgl_pergi)).'</td>
					</tr>
					<tr>
						<td width="30%" >Tanggal kembali</td>
						<td width="70%" >: '.date('j F Y', strtotime($data->tgl_kembali)).'<br></td>
					</tr>
				</table>';		

		$i++;
			}
		
		foreach(Spd::model()->findAllbyPk($id) as $data){
			$pejabat=Pejabat::model()->findbyPk($data->id_pejabat);
				$html .='
				<p>Kegiatan yang harus dilaksanakan:<br>&nbsp;&nbsp;<i>'.$data->maksud.'</i></p><br>
				<p>Demikian Surat Perintah ini di buat, agar dilaksanakan dengan sebaik-baiknya.</p><br>
				<table>
					<tr>
						<td width="50%">
						</td>
						<td width="50%" align="center">
							<p>Serang, '.date('j F Y', strtotime($data->tanggal)).'<br>
							Yang Memberi Perintah</p>
							<p>&nbsp;</p>
							<p style="line-height:70%;"><span style="text-decoration:underline;"><b>'.$pejabat->nama.'</b></span></p>
							<p style="line-height:10%;">NIP '.$pejabat->nip.'</p>
						</td>
					</tr>
				</table>';
		}
		
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }



    public function actionExportDataSptDpkd($id)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'A3', true, 'UTF-8');

	    
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perintah Tugas');
		$pdf->SetTitle('Surat Perintah Tugas');
		$pdf->SetSubject('Surat Perintah Tugas');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		$txt = <<<EOD
TCPDF Example 003

Custom page header and footer are defined by extending the TCPDF class and overriding the Header() and Footer() methods.
EOD;
		
		$html=' <table style="border-bottom:1px;">
					<tr>
						<td width="15%">
							<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="65px">
						</td>
						<td width="85%" >
							<p style="font-size:70px;line-height:93%;" >
							&nbsp;&nbsp;<b>PEMERINTAH KABUPATEN SERANG</b><br>
							<b style="font-size:50px;">DINAS PENGELOLAAN KEUANGAN DAERAH</b><br>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							( D P K D )<br>
							<i style="font-size:29px;">Jalan Raya Serang-Jakarta Km 4, Kabupaten Serang Telp.(0254)280529</i></p>
						</td>
					</tr>
				</table>';
			
			$i=1;
			foreach(Spd::model()->findAllbyPk($id) as $data)
			{
				$pejabat=Pejabat::model()->findbyPk($data->id_pejabat);
				$pegawai=Pegawai::model()->findbyPk($data->id_pegawai);
				$golongan=Golongan::model()->findbyPk($pegawai->id_golongan);
			    $html .= '
			   	<p><span align="center" style="text-decoration:underline;line-height:10%;"><b>SURAT TUGAS</b></span></p>
			 	<p align="center" style="line-height:10%;">Nomor:'.$data->nomor_spt.'</p>


			 	<p style="margin:5px">Yang bertanda tangan dibawah ini:</p>
				<table style="margin:5px">	
					<tr>
						<td width="30%" >Nama</td>
						<td width="70%" >: <b>'.$pejabat->nama.'</b></td>
					</tr>
					<tr>
						<td width="30%" >NIP</td>
						<td width="70%" >: '.$pejabat->nip.'</td>
					</tr>
					<tr>
						<td width="30%" >Jabatan</td>
						<td width="70%" >: '.$pejabat->jabatan.'<br></td>
					</tr>
				</table>
				<p style="margin:5px" align="center"><b>Memberikan Perintah Kepada:</b></p>
				<table style="margin:5px">
					<tr>
						<td width="30%" >Nama</td>
						<td width="70%" >: <b>'.$pegawai->nama.'</b></td>
					</tr>
					<tr>
						<td width="30%" >NIP</td>
						<td width="70%" >: '.$pegawai->nip.'</td>
					</tr>
					<tr>
						<td width="30%" >Jabatan</td>
						<td width="70%" >: '.$pegawai->jabatan.'<br></td>
					</tr>
				</table>
				<p style="margin:5px">Untuk melaksanakan tugas kegiatan Perjalanan Dinas:</p>
				<table style="margin:5px">
					<tr>
						<td width="30%" >Tujuan</td>
						<td width="70%" >: '.$data->tujuan.'</td>
					</tr>
					<tr>
						<td width="30%" >Tanggal Berangkat</td>
						<td width="70%" >: '.date('d F Y', strtotime($data->tgl_pergi)).'</td>
					</tr>
					<tr>
						<td width="30%" >Tanggal kembali</td>
						<td width="70%" >: '.date('d F Y', strtotime($data->tgl_kembali)).'<br></td>
					</tr>
				</table>';
				
					
				$html .='<p style="margin:5px"><b>Pengikut:</b></p>
				<table border="1px" align="center">
					<tr>
						<td width="20%" ><b>NIP</b></td>
						<td width="40%" ><b>Nama</b></td>
						<td width="40%" ><b>Jabatan</b></td>
					</tr>
				';
				foreach(Pengikut::model()->findAllByAttributes(array('id_spd'=>$id)) as $pengikut)
					{
					$html .='
						<tr>
							<td width="20%" >'.$pengikut->pegawai->nip.'</td>
							<td width="40%" >'.$pengikut->pegawai->nama.'</td>
							<td width="40%" >'.$pengikut->pegawai->jabatan.'</td>
						</tr>';
					}
				

		$i++;
			}
		$html .='</table>

		';
		
		foreach(Spd::model()->findAllbyPk($id) as $data){
			$pejabat=Pejabat::model()->findbyPk($data->id_pejabat);
				$html .='
				<p>Kegiatan yang harus dilaksanakan:<br>&nbsp;&nbsp;<i>'.$data->maksud.'</i></p><br>
				<p>Demikian Surat Perintah ini di buat, agar dilaksanakan dengan sebaik-baiknya.</p><br>
				<table>
					<tr>
						<td width="50%">
						</td>
						<td width="50%" align="center">
							<p>Serang, '.date('d F Y', strtotime($data->tanggal)).'<br>
							Yang Memberi Perintah</p>
							<p>&nbsp;</p>
							<p style="line-height:70%;"><span style="text-decoration:underline;"><b>'.$pejabat->nama.'</b></span></p>
							<p style="line-height:10%;">NIP. '.$pejabat->nip.'</p>
						</td>
					</tr>
				</table>';
		}
		
		
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }

    public function actionExportDataSptDpkdNoPengikut($id)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'A3', true, 'UTF-8');

	    
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perintah Tugas');
		$pdf->SetTitle('Surat Perintah Tugas');
		$pdf->SetSubject('Surat Perintah Tugas');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		$txt = <<<EOD
TCPDF Example 003

Custom page header and footer are defined by extending the TCPDF class and overriding the Header() and Footer() methods.
EOD;
		
		$html=' <table style="border-bottom:1px;">
					<tr>
						<td width="15%">
							<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="65px">
						</td>
						<td width="85%" >
							<p style="font-size:70px;line-height:93%;" >
							&nbsp;&nbsp;<b>PEMERINTAH KABUPATEN SERANG</b><br>
							<b style="font-size:50px;">DINAS PENGELOLAAN KEUANGAN DAERAH</b><br>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							( D P K D )<br>
							<i style="font-size:29px;">Jalan Raya Serang-Jakarta Km 4, Kabupaten Serang Telp.(0254)280529</i></p>
						</td>
					</tr>
				</table>';
			
			$i=1;
			foreach(Spd::model()->findAllbyPk($id) as $data)
			{
				$pejabat=Pejabat::model()->findbyPk($data->id_pejabat);
				$pegawai=Pegawai::model()->findbyPk($data->id_pegawai);
				$golongan=Golongan::model()->findbyPk($pegawai->id_golongan);
			    $html .= '
			   	<p><span align="center" style="text-decoration:underline;line-height:10%;"><b>SURAT TUGAS</b></span></p>
			 	<p align="center" style="line-height:10%;">Nomor:'.$data->nomor_spt.'</p>


			 	<p style="margin:5px">Yang bertanda tangan dibawah ini:</p>
				<table style="margin:5px">	
					<tr>
						<td width="30%" >Nama</td>
						<td width="70%" >: <b>'.$pejabat->nama.'</b></td>
					</tr>
					<tr>
						<td width="30%" >NIP</td>
						<td width="70%" >: '.$pejabat->nip.'</td>
					</tr>
					<tr>
						<td width="30%" >Jabatan</td>
						<td width="70%" >: '.$pejabat->jabatan.'<br></td>
					</tr>
				</table>
				<p style="margin:5px" align="center"><b>Memberikan Perintah Kepada:</b></p>
				<table style="margin:5px">
					<tr>
						<td width="30%" >Nama</td>
						<td width="70%" >: <b>'.$pegawai->nama.'</b></td>
					</tr>
					<tr>
						<td width="30%" >NIP</td>
						<td width="70%" >: '.$pegawai->nip.'</td>
					</tr>
					<tr>
						<td width="30%" >Jabatan</td>
						<td width="70%" >: '.$pegawai->jabatan.'<br></td>
					</tr>
				</table>
				<p style="margin:5px">Untuk melaksanakan tugas kegiatan Perjalanan Dinas:</p>
				<table style="margin:5px">
					<tr>
						<td width="30%" >Tujuan</td>
						<td width="70%" >: '.$data->tujuan.'</td>
					</tr>
					<tr>
						<td width="30%" >Tanggal Berangkat</td>
						<td width="70%" >: '.date('d F Y', strtotime($data->tgl_pergi)).'</td>
					</tr>
					<tr>
						<td width="30%" >Tanggal kembali</td>
						<td width="70%" >: '.date('d F Y', strtotime($data->tgl_kembali)).'<br></td>
					</tr>
				</table>';
	
		$i++;
			}
		
		foreach(Spd::model()->findAllbyPk($id) as $data){
			$pejabat=Pejabat::model()->findbyPk($data->id_pejabat);
				$html .='
				<p>Kegiatan yang harus dilaksanakan:<br>&nbsp;&nbsp;<i>'.$data->maksud.'</i></p><br>
				<p>Demikian Surat Perintah ini di buat, agar dilaksanakan dengan sebaik-baiknya.</p><br>
				<table>
					<tr>
						<td width="50%">
						</td>
						<td width="50%" align="center">
							<p>Serang, '.date('d F Y', strtotime($data->tanggal)).'<br>
							Yang Memberi Perintah</p>
							<p>&nbsp;</p>
							<p style="line-height:70%;"><span style="text-decoration:underline;"><b>'.$pejabat->nama.'</b></span></p>
							<p style="line-height:10%;">NIP '.$pejabat->nip.'</p>
						</td>
					</tr>
				</table>';
		}
		
		
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }

    public function actionExportDataSpdDpkd($id)
    {
	        $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Data Pegawai');
		$pdf->SetSubject('Data Pegawai');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		$html='
		<table style="border-bottom:1px;">
					<tr>
						<td width="15%">
							<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="65px">
						</td>
						<td width="85%" >
							<p style="font-size:70px;line-height:93%;" >
							&nbsp;&nbsp;<b>PEMERINTAH KABUPATEN SERANG</b><br>
							<b style="font-size:50px;">DINAS PENGELOLAAN KEUANGAN DAERAH</b><br>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							( D P K D )<br>
							<i style="font-size:29px;">Jalan Raya Serang-Jakarta Km 4, Kabupaten Serang Telp.(0254)280529</i></p>
						</td>
					</tr>
				</table>';
			
			$i=1;
			foreach(Spd::model()->findAllbyPk($id) as $data)
			{
				$pejabat=Pejabat::model()->findbyPk($data->id_pejabat);
				$pegawai=Pegawai::model()->findbyPk($data->id_pegawai);
				$golongan=Golongan::model()->findbyPk($pegawai->id_golongan);
			    $html .= '
			   
			 	<h3><span align="center" style="text-decoration:underline;line-height:10%;">Surat Perjalanan Dinas</span></h3>
			 	<p align="center">Nomor : '.$data->nomor.'</p>
				<table border="1" style="padding:5px">	
				<tr>
					<td width="6%" align="center">1</td>
					<td width="47%" >Pejabat berwenang yang memberi perintah</td>
					<td width="47%" >'.$pejabat->jabatan.'</td>
				</tr>
				<tr>
					<td width="6%" align="center">2</td>
					<td width="47%" >Nama/NIP pegawain yang diperintahkan</td>
					<td width="47%" ><b>'.$data->pegawai->nama.'</b><br>'.$pegawai->nip.'</td>
				</tr>
				<tr>
					<td width="6%" align="center">3</td>
					<td width="47%" >a. Pangkat dan Golongan Ruang Gaji</td>
					<td width="47%" >'.$golongan->nama.'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >b. Jabatan / Instansi</td>
					<td width="47%" >'.$pegawai->jabatan.'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >c. Tingkat biaya perjalanan dinas</td>
					<td width="47%" >-</td>
				</tr>
				<tr>
					<td width="6%" align="center">4</td>
					<td width="47%" >Maksud perjalanan dinas</td>
					<td width="47%" >'.$data->maksud.'</td>
				</tr>
				<tr>
					<td width="6%" align="center">5</td>
					<td width="47%" >Alat angkutan yang dipergunakan</td>
					<td width="47%" >'.$data->kendaraan.'</td>
				</tr>
				<tr>
					<td width="6%" align="center">6</td>
					<td width="47%" >a. Tempat berangkat</td>
					<td width="47%" >'.$data->tempat_berangkat.'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >b. Tempat tujuan</td>
					<td width="47%" >'.$data->tujuan.'</td>
				</tr>
				<tr>
					<td width="6%" align="center">7</td>
					<td width="47%" >a. Lamanya perjalanan dinas</td>
					<td width="47%" >'.$data->lama.' Hari</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >b. Tanggal berangkat</td>
					<td width="47%" >'.date('d F Y', strtotime($data->tgl_pergi)).'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >c. Tanggal harus kembali / tiba di tempat baru*)</td>
					<td width="47%" >'.date('d F Y', strtotime($data->tgl_kembali)).'</td>
				</tr>
				<tr>
					<td width="6%" align="center">8</td>
					<td width="47%" >Pengikut</td>
					<td width="22%">Tgl. Lahir</td>
					<td width="25%">Jabatan</td>
				</tr>';
				foreach(Pengikut::model()->findAllByAttributes(array('id_spd'=>$id)) as $pengikut)
					{
					$html .='
						<tr>
							<td width="6%" align="center"></td>
							<td width="47%" >'.$pengikut->pegawai->nama.'</td>
							<td width="22%" >'.date('d F Y', strtotime($pengikut->pegawai->tgl_lahir)).'</td>
							<td width="25%" >'.$pengikut->pegawai->jabatan.'</td>
						</tr>';
					}
			$html .='	
				
				<tr>
					<td width="6%" align="center">9</td>
					<td width="47%" >
						Pembebanan Anggaran<br>
						a. Instansi<br>
						b. Mata Anggaran
					</td>
					<td width="47%" >
						<br><br>
						'.$data->instansi.'<br>
						'.$data->koring.'
					</td>
				</tr>
				<tr>
					<td width="6%" align="center">10</td>
					<td width="47%" >Keterangan Lain-lain</td>
					<td width="47%" ></td>
				</tr>';
		
		$i++;
			}
		$html .='</table>
		';
		
		foreach(Spd::model()->findAllbyPk($id) as $data){
			$pejabat=Pejabat::model()->findbyPk($data->id_pejabat);
				$html .='
				<div>&nbsp;</div>
				<table>
					<tr>
						<td width="50%">
						</td>
						<td width="50%" align="center">
							<p>Di keluarkan di : '.$data->tempat_berangkat.'<br>
							Pada Tanggal : '.date('d F Y', strtotime($data->tanggal)).'</p>
							<p>&nbsp;</p>
							<p style="line-height:70%;"><span style="text-decoration:underline;"><b>'.$pejabat->nama.'</b></span></p>
							<p style="line-height:10%;">'.$pejabat->nip.'</p>
						</td>
					</tr>
				</table>';
		}
		
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }

    public function actionBlankoSpdDpkd()
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');

	    
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Data Pegawai');
		$pdf->SetSubject('Data Pegawai');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		$txt = <<<EOD
TCPDF Example 003

Custom page header and footer are defined by extending the TCPDF class and overriding the Header() and Footer() methods.
EOD;
		

		$html='
		<table style="border-bottom:1px;">
					<tr>
						<td width="15%">
							<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="65px">
						</td>
						<td width="85%" >
							<p style="font-size:70px;line-height:93%;" >
							&nbsp;&nbsp;<b>PEMERINTAH KABUPATEN SERANG</b><br>
							<b style="font-size:50px;">DINAS PENGELOLAAN KEUANGAN DAERAH</b><br>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							( D P K D )<br>
							<i style="font-size:29px;">Jalan Raya Serang-Jakarta Km 4, Kabupaten Serang Telp.(0254)280529</i></p>
						</td>
					</tr>
				</table>';
			
			
			    $html .= '
			   
			 	<h3><span align="center" style="text-decoration:underline;line-height:10%;">Surat Perjalanan Dinas</span></h3>
			 	<p align="center">Nomor : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			 		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			 	</p>
				<table border="1" style="padding:5px">	
				<tr>
					<td width="6%" align="center">1</td>
					<td width="47%" >Pejabat berwenang yang memberi perintah</td>
					<td width="47%" >&nbsp;</td>
				</tr>
				<tr>
					<td width="6%" align="center">2</td>
					<td width="47%" >Nama/NIP pegawain yang diperintahkan</td>
					<td width="47%" ><b>&nbsp;</b><br>&nbsp;</td>
				</tr>
				<tr>
					<td width="6%" align="center">3</td>
					<td width="47%" >a. Pangkat dan Golongan Ruang Gaji</td>
					<td width="47%" >&nbsp;</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >b. Jabatan / Instansi</td>
					<td width="47%" >&nbsp;</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >c. Tingkat biaya perjalanan dinas</td>
					<td width="47%" ></td>
				</tr>
				<tr>
					<td width="6%" align="center">4</td>
					<td width="47%" >Maksud perjalanan dinas</td>
					<td width="47%" >&nbsp;</td>
				</tr>
				<tr>
					<td width="6%" align="center">5</td>
					<td width="47%" >Alat angkutan yang dipergunakan</td>
					<td width="47%" >&nbsp;</td>
				</tr>
				<tr>
					<td width="6%" align="center">6</td>
					<td width="47%" >a. Tempat berangkat</td>
					<td width="47%" >&nbsp;</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >b. Tempat tujuan</td>
					<td width="47%" >&nbsp;</td>
				</tr>
				<tr>
					<td width="6%" align="center">7</td>
					<td width="47%" >a. Lamanya perjalanan dinas</td>
					<td width="47%" >&nbsp;</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >b. Tanggal berangkat</td>
					<td width="47%" >&nbsp;</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >c. Tanggal harus kembali / tiba di tempat baru*)</td>
					<td width="47%" >&nbsp;</td>
				</tr>
				<tr>
					<td width="6%" align="center">8</td>
					<td width="47%" >Pengikut</td>
					<td width="22%">Tgl. Lahir</td>
					<td width="25%">Jabatan</td>
				</tr>';
				
					$html .='
						<tr>
							<td width="6%" align="center"></td>
							<td width="47%" >&nbsp;</td>
							<td width="22%" >&nbsp;</td>
							<td width="25%" >&nbsp;</td>
						</tr>
						<tr>
							<td width="6%" align="center"></td>
							<td width="47%" >&nbsp;</td>
							<td width="22%" >&nbsp;</td>
							<td width="25%" >&nbsp;</td>
						</tr>
						<tr>
							<td width="6%" align="center"></td>
							<td width="47%" >&nbsp;</td>
							<td width="22%" >&nbsp;</td>
							<td width="25%" >&nbsp;</td>
						</tr>
						<tr>
							<td width="6%" align="center"></td>
							<td width="47%" >&nbsp;</td>
							<td width="22%" >&nbsp;</td>
							<td width="25%" >&nbsp;</td>
						</tr>
						';
					
			$html .='	
				
				<tr>
					<td width="6%" align="center">9</td>
					<td width="47%" >
						Pembebanan Anggaran<br>
						a. Instansi<br>
						b. Mata Anggaran
					</td>
					<td width="47%" >
						<br><br>
						&nbsp;<br>
						&nbsp;
					</td>
				</tr>
				<tr>
					<td width="6%" align="center">10</td>
					<td width="47%" >Keterangan Lain-lain</td>
					<td width="47%" ></td>
				</tr>';
		
		
		$html .='</table>
		';
		
	
				$html .='
				<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;
				</div>
				<table>
					<tr>
						<td width="40%">
						</td>
						<td width="50%" align="center">
							<p>Di keluarkan di : &nbsp;<br>
							Pada Tanggal : &nbsp;</p>
							<p>&nbsp;</p>
							<p style="line-height:70%;"><b>&nbsp;</b></p>
							<p style="line-height:10%;">&nbsp;</p>
						</td>
					</tr>
				</table>';
		
		
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }

    public function actionExportDataSpd($id=null)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Surat Perjalanan Dinas');
		$pdf->SetSubject('Surat Perjalanan Dinas');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		
		
		$html='
		<p align="right"><b>Lampiran VI<br>Permenkeu RI No. 113/PMK 05/2012</b></p>
		<table border="1" style="padding:5px;line-height:95%">';
			
			$i=1;
			if($id!=null) {
			foreach(Spd::model()->findAllbyPk($id) as $data)
			{
				//$pejabat=Pejabat::model()->findbyPk($data->id_pejabat);
				//$pegawai=Pegawai::model()->findbyPk($data->id_pegawai);
				//$golongan=Golongan::model()->findbyPk($pegawai->id_golongan);
			    $html .= '
			   
			 	<tr>
			 		<td width="50%" rowspan="2">&nbsp;</td>
			 		<td width="50%" >
			 			<table>
				 			<tr>
								<td style="width:5%">I.</td>
				 				<td style="width:40%">Berangkat dari<br>(Tempat Kedudukan)</td>
				 				<td>: '.$data->tempat_berangkat.'</td>
				 			</tr>
				 			<tr>
								<td style="width:5%">&nbsp;</td>
				 				<td style="width:40%">Ke</td>
				 				<td>: '.$data->tujuan.'</td>
				 			</tr>
							<tr>
								<td style="width:5%"></td>
				 				<td style="width:40%"></td>
				 				<td></td>
				 			</tr>
				 			<tr>
								<td style="width:5%">&nbsp;</td>
				 				<td style="width:40%">Pada Tanggal</td>
				 				<td>: '.date('d F Y', strtotime($data->tgl_pergi)).'</td>
				 			</tr>
			 			</table>
			 		</td>
				</tr>
				<tr>
					<td width="50%" >
			 			<table>
				 			<tr>
				 				<td align="center" colspan="2"> '.$data->getRelationField("pejabat","jabatan").'</td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
							<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
							<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
							<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
							<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><p style="text-decoration:underline;"><b>'.$data->getRelationField("pejabat","nama").'</b></p></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2">NIP. '.$data->getRelationNip("pejabat").'</td>
				 			</tr>
			 			</table>
			 		</td>
			 	</tr>
				<tr>
					<td width="50%" style="line-height:95%">
						<table>
				 			<tr>
								<td style="width:5%">II.</td>
				 				<td>Tiba di</td>
				 				<td>: '.$data->tujuan.'</td>
				 			</tr>
				 			<tr>
								<td style="width:5%"></td>
				 				<td>Pada Tanggal</td>
				 				<td>: '.date('d F Y', strtotime($data->tgl_pergi)).'</td>
				 			</tr>
							<tr>
								<td style="width:5%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
				 			<tr>
								<td style="width:5%"></td>
								<td></td>
								<td></td>
							</tr>
				 			<tr>
								<td style="width:5%"></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td style="width:5%"></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td style="width:5%"></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td style="width:5%"></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td style="width:5%"></td>
								<td></td>
								<td></td>
							</tr>
			 			</table>
					</td>
					<td width="50%" style="line-height:95%">
						<table>
				 			<tr>
				 				<td>Berangkat dari</td>
				 				<td>: '.$data->tujuan.'</td>
				 			</tr>
				 			<tr>
				 				<td>Ke</td>
				 				<td>: '.$data->tempat_berangkat.'</td>
				 			</tr>
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>: '.date('d F Y', strtotime($data->tgl_kembali)).'</td>
				 			</tr>
				 			<tr>
								<td></td>
								<td></td>
							</tr>
				 			<tr>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
							</tr>
			 			</table>
					</td>
				</tr>
				<tr>
					<td width="50%">
						<table>
				 			<tr>
								<td style="width:6%">III.</td>
				 				<td>Tiba di</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
								<td style="width:6%"></td>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td colspan="3" style="width:100%" align="center"><b>_________________________________</b></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
			 			</table>
					</td>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>Berangkat dari</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Ke</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td colspan="2" align="center"><b>_________________________________</b></td>
							</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
			 			</table>
					</td>
				</tr>
				<tr>
					<td width="50%">
						<table>
				 			<tr>
								<td style="width:6%">IV.</td>
				 				<td>Tiba di</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
								<td style="width:6%"></td>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
							<tr>
								<td style="width:6%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td style="width:6%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td style="width:6%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td style="width:6%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td style="width:6%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td colspan="3" style="width:100%" align="center"><b>_________________________________</b></td>
							</tr>
							<tr>
								<td style="width:6%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
			 			</table>
					</td>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>Berangkat dari</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Ke</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td colspan="2" align="center"><b>_________________________________</b></td>
							</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
			 			</table>
					</td>
				</tr>
				<tr>
					<td width="50%">
						<table>
				 			<tr>
								<td style="width:5%">V.</td>
				 				<td>Tiba di</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
								<td style="width:5%"></td>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
							<tr>
								<td style="width:5%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td style="width:5%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td style="width:5%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td style="width:5%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td style="width:5%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td colspan="3" style="width:100%" align="center"><b>_________________________________</b></td>
							</tr>
							<tr>
								<td style="width:5%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
			 			</table>
					</td>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>Berangkat dari</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Ke</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td colspan="2" align="center"><b>_________________________________</b></td>
							</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
			 			</table>
					</td>
				</tr>
				<tr>
					<td width="50%">
						<table >
				 			<tr>
								<td style="width:6%">VI.</td>
				 				<td>Tiba di</td>
				 				<td>: '.$data->tempat_berangkat.'</td>
				 			</tr>
				 			<tr>
								<td style="width:6%"></td>
				 				<td>Pada Tanggal</td>
				 				<td>: '.date('d F Y', strtotime($data->tgl_kembali)).'</td>
				 			</tr>
							<tr>
				 				<td align="center" colspan="3"></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="3" style="width:100%">An. Pejabat yang berwenang/<br>Pejabat lainnya yang di tunjuk</td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="3"></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="3"></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="3"></td>
				 			</tr>
							<tr>
				 				<td align="center" colspan="3"></td>
				 			</tr>
							<tr>
				 				<td align="center" colspan="3"></td>
				 			</tr>
							<tr>
				 				<td align="center" colspan="3"></td>
				 			</tr>
							<tr>
				 				<td align="center" colspan="3"></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="3"><p style="text-decoration:underline;"><b>'.$data->getRelationField("pejabat","nama").'</b></p></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="3">NIP.'.$data->getRelationNip("pejabat").'</td>
				 			</tr>
			 			</table>
					</td>
					<td width="50%">
						<table>
				 			<tr>
				 				<td><p>Telah diperiksa dengan keterangan bahwa perjalanan
				 						tersebut atas perintahnya dan semata-mata untuk
				 						kepentingan jabatan dan waktu yang sesingkat-singkatnya.</p>
				 				</td>
				 			</tr>
				 			<tr>
				 				<td align="center">An. Pejabat yang berwenang/<br>Pejabat lainnya yang di tunjuk</td>
				 			</tr>
				 			<tr>
				 				<td align="center"></td>
				 			</tr>
				 			<tr>
				 				<td align="center"></td>
				 			</tr>
							<tr>
				 				<td align="center"></td>
				 			</tr>
							<tr>
				 				<td align="center"></td>
				 			</tr>
							<tr>
				 				<td align="center"></td>
				 			</tr>
							<tr>
				 				<td align="center"></td>
				 			</tr>
							<tr>
				 				<td align="center"></td>
				 			</tr>
				 			<tr>
				 				<td align="center"><p><b><u>'.$data->getRelationField("pejabat","nama").'</u></b><br>NIP.'.$data->getRelationNip("pejabat").'</p></td>
				 			</tr>

			 			</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						Catatan lain lain:
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table>
							<tr>
								<td width="5%">
									<b>VII.</b>
								</td>
								<td width="95%">
								<b>PERHATIAN</b><br>
								Pejabat yang berwenang menerbitkan SPPD, pegawai yang melakukan perjalanan dinas, para pejabat yang mengesahkan tanggal
								berangkat/tiba, serta bendaharawan bertanggung jawab berdasarkan peraturan-peraturan keuangan Negara apabila negara menderita rugi
								akibat kesalahan, kelalaian dan kealpaannya.
								</td>
							</tr>
						</table>
						
					</td>
				</tr>
				';
				$i++;
			}
		} else {
			$html .= '
			 	<tr>
			 		<td width="50%" rowspan="2">&nbsp;</td>
			 		<td width="50%" >
			 			<table>
				 			<tr>
								<td style="width:5%">I.</td>
				 				<td style="width:40%">Berangkat dari<br>(Tempat Kedudukan)</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
								<td style="width:5%">&nbsp;</td>
				 				<td style="width:40%">Ke</td>
				 				<td>:</td>
				 			</tr>
							<tr>
								<td style="width:5%"></td>
				 				<td style="width:40%"></td>
				 				<td></td>
				 			</tr>
				 			<tr>
								<td style="width:5%">&nbsp;</td>
				 				<td style="width:40%">Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
			 			</table>
			 		</td>
				</tr>
				<tr>
					<td width="50%" >
			 			<table>
				 			<tr>
				 				<td align="center" colspan="2"></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
							<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
							<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
							<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
							<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2">NIP.</td>
				 			</tr>
			 			</table>
			 		</td>
			 	</tr>
				<tr>
					<td width="50%" style="line-height:95%">
						<table>
				 			<tr>
								<td style="width:5%">II.</td>
				 				<td>Tiba di</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
								<td style="width:5%"></td>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
							<tr>
								<td style="width:5%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
				 			<tr>
								<td style="width:5%"></td>
								<td></td>
								<td></td>
							</tr>
				 			<tr>
								<td style="width:5%"></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td style="width:5%"></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td style="width:5%"></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td style="width:5%"></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td style="width:5%"></td>
								<td></td>
								<td></td>
							</tr>
			 			</table>
					</td>
					<td width="50%" style="line-height:95%">
						<table>
				 			<tr>
				 				<td>Berangkat dari</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Ke</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
								<td></td>
								<td></td>
							</tr>
				 			<tr>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
							</tr>
			 			</table>
					</td>
				</tr>
				<tr>
					<td width="50%">
						<table>
				 			<tr>
								<td style="width:6%">III.</td>
				 				<td>Tiba di</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
								<td style="width:6%"></td>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td colspan="3" style="width:100%" align="center"><b>_________________________________</b></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
			 			</table>
					</td>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>Berangkat dari</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Ke</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td colspan="2" align="center"><b>_________________________________</b></td>
							</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
			 			</table>
					</td>
				</tr>
				<tr>
					<td width="50%">
						<table>
				 			<tr>
								<td style="width:6%">IV.</td>
				 				<td>Tiba di</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
								<td style="width:6%"></td>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
							<tr>
								<td style="width:6%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td style="width:6%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td style="width:6%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td style="width:6%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td style="width:6%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td style="width:6%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td colspan="3" style="width:100%" align="center"><b>_________________________________</b></td>
							</tr>
							<tr>
								<td style="width:6%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
			 			</table>
					</td>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>Berangkat dari</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Ke</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td colspan="2" align="center"><b>_________________________________</b></td>
							</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
			 			</table>
					</td>
				</tr>
				<tr>
					<td width="50%">
						<table>
				 			<tr>
								<td style="width:5%">V.</td>
				 				<td>Tiba di</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
								<td style="width:5%"></td>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
							<tr>
								<td style="width:5%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td style="width:5%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td style="width:5%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td style="width:5%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td style="width:5%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td style="width:5%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td colspan="3" style="width:100%" align="center"><b>_________________________________</b></td>
							</tr>
							<tr>
								<td style="width:5%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
			 			</table>
					</td>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>Berangkat dari</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Ke</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td colspan="2" align="center"><b>_________________________________</b></td>
							</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
			 			</table>
					</td>
				</tr>
				<tr>
					<td width="50%">
						<table >
				 			<tr>
								<td style="width:6%">VI.</td>
				 				<td>Tiba di</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
								<td style="width:6%"></td>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
							<tr>
				 				<td align="center" colspan="3"></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="3" style="width:100%">An. Pejabat yang berwenang/<br>Pejabat lainnya yang di tunjuk</td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="3"></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="3"></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="3"></td>
				 			</tr>
							<tr>
				 				<td align="center" colspan="3"></td>
				 			</tr>
							<tr>
				 				<td align="center" colspan="3"></td>
				 			</tr>
							<tr>
				 				<td align="center" colspan="3"></td>
				 			</tr>
							<tr>
				 				<td align="center" colspan="3"></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="3"></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="3">NIP.</td>
				 			</tr>
			 			</table>
					</td>
					<td width="50%">
						<table>
				 			<tr>
				 				<td><p>Telah diperiksa dengan keterangan bahwa perjalanan
				 						tersebut atas perintahnya dan semata-mata untuk
				 						kepentingan jabatan dan waktu yang sesingkat-singkatnya.</p>
				 				</td>
				 			</tr>
				 			<tr>
				 				<td align="center">An. Pejabat yang berwenang/<br>Pejabat lainnya yang di tunjuk</td>
				 			</tr>
				 			<tr>
				 				<td align="center"></td>
				 			</tr>
				 			<tr>
				 				<td align="center"></td>
				 			</tr>
							<tr>
				 				<td align="center"></td>
				 			</tr>
							<tr>
				 				<td align="center"></td>
				 			</tr>
							<tr>
				 				<td align="center"></td>
				 			</tr>
							<tr>
				 				<td align="center"></td>
				 			</tr>
							<tr>
				 				<td align="center"></td>
				 			</tr>
				 			<tr>
				 				<td align="center"></td>
				 			</tr>

			 			</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						Catatan lain lain:
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table>
							<tr>
								<td width="5%">
									<b>VII.</b>
								</td>
								<td width="95%">
								<b>PERHATIAN</b><br>
								Pejabat yang berwenang menerbitkan SPPD, pegawai yang melakukan perjalanan dinas, para pejabat yang mengesahkan tanggal
								berangkat/tiba, serta bendaharawan bertanggung jawab berdasarkan peraturan-peraturan keuangan Negara apabila negara menderita rugi
								akibat kesalahan, kelalaian dan kealpaannya.
								</td>
							</tr>
						</table>
						
					</td>
				</tr>
				';
		}
		$html .='</table>';
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }

    public function actionBlankoSpd()
    {
	       $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Surat Perjalanan Dinas');
		$pdf->SetSubject('Surat Perjalanan Dinas');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		
		
		$html='
		<p align="right"><b>Lampiran VI<br>Permenkeu RI No. 113/PMK 05/2012</b></p>
		<table border="1" style="padding:5px">';
			
			    $html .= '
			   
			 	<tr>
			 		<td width="50%" >
			 		</td>
			 		<td width="50%" >
			 			<table>
				 			<tr>
				 				<td>I.Berangkat dari (Tempat Kedudukan)</td>
				 				<td>: &nbsp;</td>
				 			</tr>
				 			<tr>
				 				<td>Ke</td>
				 				<td>: &nbsp;</td>
				 			</tr>
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>: &nbsp;</td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"> &nbsp;</td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><p><b>&nbsp;</b></p></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2">NIP.&nbsp;</td>
				 			</tr>
			 			</table>
			 		</td>
			 	</tr>
				<tr>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>II.Tiba di</td>
				 				<td>: &nbsp;</td>
				 			</tr>
				 			<tr>
				 				<td>Ke</td>
				 				<td>: &nbsp;</td>
				 			</tr>
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>: &nbsp;</td>
				 			</tr>
			 			</table>
					</td>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>Berangkat dari</td>
				 				<td>: &nbsp;</td>
				 			</tr>
				 			<tr>
				 				<td>Ke</td>
				 				<td>: &nbsp;</td>
				 			</tr>
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>: &nbsp;</td>
				 			</tr>
			 			</table>
					</td>
				</tr>
				<tr>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>III.Tiba di</td>
				 				<td>:</td>
				 			</tr>
				 			
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
			 			</table>
					</td>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>Berangkat dari</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Ke</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
			 			</table>
					</td>
				</tr>
				<tr>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>IV.Tiba di</td>
				 				<td>:</td>
				 			</tr>
				 			
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
			 			</table>
					</td>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>Berangkat dari</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Ke</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
			 			</table>
					</td>
				</tr>
				<tr>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>V.Tiba di</td>
				 				<td>:</td>
				 			</tr>
				 			
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
			 			</table>
					</td>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>Berangkat dari</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Ke</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
			 			</table>
					</td>
				</tr>
				<tr>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>VI.Tiba di</td>
				 				<td>: &nbsp;</td>
				 			</tr>
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>: &nbsp;</td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2">An. Pejabat yang berwenang/<br>Pejabat lainnya yang di tunjuk</td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><p><b>&nbsp;</b></p></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2">NIP.&nbsp;</td>
				 			</tr>
			 			</table>
					</td>
					<td width="50%">
						<table>
				 			<tr>
				 				<td><p>Telah diperiksa dengan keterangan bahwa perjalanan
				 						tersebut atas perintahnya dan semata-mata untuk
				 						kepentingan jabatan dan waktu yang sesingkat-singkatnya.</p>
				 				</td>
				 			</tr>
				 			<tr>
				 				<td align="center">An. Pejabat yang berwenang/<br>Pejabat lainnya yang di tunjuk</td>
				 			</tr>
				 			<tr>
				 				<td align="center"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center"><p><b>&nbsp;</b><br>NIP.&nbsp;</p></td>
				 			</tr>

			 			</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						Catatan lain lain:
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<b>VII. PERHATIAN</b>
						<p>Pejabat yang berwenang menerbitkan SPPD, pegawai yang melakukan perjalanan dinas, para pejabat yang mengesahkan tanggal
						berangkat/tiba, serta bendaharawan bertanggung jawab berdasarkan peraturan-peraturan keuangan Negara apabila negara menderita rugi
						akibat kesalahan, kelalaian dan kealpaannya.</p>
					</td>
				</tr>
				';
		
		
		$html .='</table>
		';
		
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }

    public function actionExportSpdOto1($id)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Surat Perjalanan Dinas');
		$pdf->SetSubject('Surat Perjalanan Dinas');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		$html=' <table>
					<tr>
						<td width="15%">
							
						</td>
						<td width="85%">
							<p style="font-size:70px;line-height:101%;">
							&nbsp;<b></b><br>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b></b><br>
							<i style="font-size:29px;"></i></p>
						</td>
					</tr>
				</table>';
			
			$i=1;
			foreach(Spd::model()->findAllbyPk($id) as $data)
			{
				$pejabat=Pejabat::model()->findbyPk($data->id_pejabat);
				$pegawai=Pegawai::model()->findbyPk($data->id_pegawai);
				//$golongan=Golongan::model()->findbyPk($pegawai->id_golongan);
			    $html .= '
			   
			 	<h3><span align="center" style="text-decoration:underline;line-height:10%;"></span></h3>
			 	<p align="center">Nomor : '.$data->nomor_spd.'</p>
				<table style="padding:5px">	
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" ></td>
					<td width="47%" >'.$data->getRelationField('pejabat','jabatan').'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" ></td>
					<td width="47%" ><b>'.$data->getRelationField('pegawai','nama').'</b><br>'.$data->getRelationField('pegawai','nip').'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" ></td>
					<td width="47%" >'.$data->getRelationRelationField('pegawai','golongan','nama').'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" ></td>
					<td width="47%" >'.$data->getRelationField('pegawai','jabatan').'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" ></td>
					<td width="47%" ></td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" ></td>
					<td width="47%" >'.$data->maksud.'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" ></td>
					<td width="47%" >'.$data->kendaraan.'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" ></td>
					<td width="47%" >'.$data->tempat_berangkat.'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" ></td>
					<td width="47%" >'.$data->tujuan.'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" ></td>
					<td width="47%" >'.$data->lama.'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" ></td>
					<td width="47%" >'.date('d F Y', strtotime($data->tgl_pergi)).'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" ></td>
					<td width="47%" >'.date('d F Y', strtotime($data->tgl_kembali)).'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%"></td>
					<td width="22%"></td>
					<td width="25%"></td>
				</tr>';
				foreach(Pengikut::model()->findAllByAttributes(array('id_spd'=>$id,'aktif'=>1)) as $pengikut)
					{
					$html .='
						<tr>
							<td width="6%" align="center"></td>
							<td width="47%" >'.$data->getRelationField('pegawai','nama').'</td>
							<td width="22%" >'.date('d F Y', strtotime($data->getRelationField('pegawai','tgl_lahir'))).'</td>
							<td width="25%" >'.$data->getRelationField('pegawai','jabatan').'</td>
						</tr>';
					}
			$html .='	
				
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >
						&nbsp;<br>
						&nbsp;<br>
						&nbsp;
					</td>
					<td width="47%" >
						<br><br>
						'.$data->instansi.'<br>
						'.$data->koring.'
					</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" ></td>
					<td width="47%" ></td>
				</tr>';
		
		$i++;
			}
		$html .='</table>
		';
		
		foreach(Spd::model()->findAllbyPk($id) as $data){
			$pejabat=Pejabat::model()->findbyPk($data->id_pejabat);
			$tanggal_spd = date('j F Y', strtotime($data->tanggal_spd));
			if($data->tanggal_spd==null) $tanggal_spd = "";
				$html .='
				<div>&nbsp;</div>
				<table>
					<tr>
						<td width="50%">
						</td>
						<td width="50%" align="center">
							<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;, '.$tanggal_spd.'<br>
							'.$data->getRelationField('pejabat','jabatan').'</p>
							<p>&nbsp;</p>
							<p>&nbsp;</p>
							<p>&nbsp;</p>
							<p style="line-height:70%;"><span style="text-decoration:underline;"><b>'.$data->getRelationField('pejabat','nama').'</b></span></p>
							<p style="line-height:10%;">NIP.'.$data->getRelationField('pejabat','nip').'</p>
						</td>
					</tr>
				</table>';
		}
		
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }
	
	public function actionExportSettingManualKwitansi($id)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Kwitansi Surat Perjalanan Dinas');
		$pdf->SetTitle('Kwitansi Surat Perjalanan Dinas');
		$pdf->SetSubject('Kwitansi Surat Perjalanan Dinas');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', Setting::getValue('font_size'));
		
		$kwitansi = Kwitansi::model()->findByPk($id);
		
		$nomor = PosisiCetak::model()->getKolomByJenisSurat(2,'nomor');
		$tanggal = PosisiCetak::model()->getKolomByJenisSurat(2,'tanggal');
		$uang_sebesar = PosisiCetak::model()->getKolomByJenisSurat(2,'uang_sebesar');
		$dengan_huruf = PosisiCetak::model()->getKolomByJenisSurat(2,'dengan_huruf');
		$untuk = PosisiCetak::model()->getKolomByJenisSurat(2,'untuk');
		$rincian = PosisiCetak::model()->getKolomByJenisSurat(2,'rincian');
		$untuk_program = PosisiCetak::model()->getKolomByJenisSurat(2,'untuk_program');
		$kegiatan = PosisiCetak::model()->getKolomByJenisSurat(2,'kegiatan');
		$kode_rekening = PosisiCetak::model()->getKolomByJenisSurat(2,'kode_rekening');
		$pengguna_anggaran = PosisiCetak::model()->getKolomByJenisSurat(2,'pengguna_anggaran');
		$nip_pengguna_anggaran = PosisiCetak::model()->getKolomByJenisSurat(2,'nip_pengguna_anggaran');
		$pptk = PosisiCetak::model()->getKolomByJenisSurat(2,'pptk');
		$nip_pptk = PosisiCetak::model()->getKolomByJenisSurat(2,'nip_pptk');
		$bendahara = PosisiCetak::model()->getKolomByJenisSurat(2,'bendahara');
		$nip_bendahara = PosisiCetak::model()->getKolomByJenisSurat(2,'nip_bendahara');
		$nama_penerima = PosisiCetak::model()->getKolomByJenisSurat(2,'nama_penerima');
		$pangkat_penerima = PosisiCetak::model()->getKolomByJenisSurat(2,'pangkat_penerima');
		$satuan_kerja_penerima = PosisiCetak::model()->getKolomByJenisSurat(2,'satuan_kerja_penerima');
		
		$pdf->writeHTMLCell(0,0,$nomor->horisontal,$nomor->vertikal,'<span style="font-size:'.$nomor->font_size.'">'.$kwitansi->nomor.'</span>');
		$pdf->writeHTMLCell(0,0,$tanggal->horisontal,$tanggal->vertikal,'<span style="font-size:'.$tanggal->font_size.'">'.Bantu::tanggal($kwitansi->tanggal).'</span>');
		$pdf->writeHTMLCell(0,0,$uang_sebesar->horisontal,$uang_sebesar->vertikal,'<span style="font-size:'.$uang_sebesar->font_size.'">'.Bantu::rp($kwitansi->total).'</span>');
		$pdf->writeHTMLCell(0,0,$dengan_huruf->horisontal,$dengan_huruf->vertikal,'<span style="font-size:'.$dengan_huruf->font_size.'">'.Bantu::getTerbilang($kwitansi->total,0).' rupiah</span>');
		$pdf->writeHTMLCell(0,0,$untuk->horisontal,$untuk->vertikal,''.'<span style="font-size:'.$untuk->font_size.'">'.'Untuk Biaya Perjalanan Dinas Ke '.$kwitansi->getRelationField('spd','tujuan').' selama '.$kwitansi->getRelationField('spd','lama').' ('.trim(Bantu::getTerbilang($kwitansi->getRelationField('spd','lama'),0)).') Hari Tgl. '.date('d',strtotime($kwitansi->getRelationField('spd','tgl_pergi'))).' s/d '.Bantu::tanggal($kwitansi->getRelationField('spd','tgl_kembali')).'</span>');
		$pdf->writeHTMLCell(0,0,$rincian->horisontal,$rincian->vertikal,''.'<span style="font-size:'.$rincian->font_size.'">'.'dengan rincian : Uang Harian '.Bantu::rp($kwitansi->uang_harian).' x '.$kwitansi->getRelationField('spd','lama').' Hari = '.Bantu::rp($kwitansi->uang_harian*$kwitansi->getRelationField('spd','lama')).'</span>');
		$pdf->writeHTMLCell(0,0,$untuk_program->horisontal,$untuk_program->vertikal,'<span style="font-size:'.$untuk_program->font_size.'">'.$kwitansi->program.'</span>');
		$pdf->writeHTMLCell(0,0,$kegiatan->horisontal,$kegiatan->vertikal,'<span style="font-size:'.$kegiatan->font_size.'">'.$kwitansi->kegiatan.'</span>');
		$pdf->writeHTMLCell(0,0,$kode_rekening->horisontal,$kode_rekening->vertikal,'<span style="font-size:'.$kode_rekening->font_size.'">'.$kwitansi->getRelationField('spd','koring').'</span>');
		$pdf->writeHTMLCell(0,0,$pengguna_anggaran->horisontal,$pengguna_anggaran->vertikal,'<span style="font-size:'.$pengguna_anggaran->font_size.'">'.$kwitansi->getRelationField('paRelation','nama').'</span>');
		$pdf->writeHTMLCell(0,0,$nip_pengguna_anggaran->horisontal,$nip_pengguna_anggaran->vertikal,'<span style="font-size:'.$nip_pengguna_anggaran->font_size.'">'.'NIP. '.$kwitansi->getRelationField('paRelation','nip').'</span>');
		$pdf->writeHTMLCell(0,0,$pptk->horisontal,$pptk->vertikal,'<span style="font-size:'.$pptk->font_size.'">'.$kwitansi->getRelationField('pptkRelation','nama').'</span>');
		$pdf->writeHTMLCell(0,0,$nip_pptk->horisontal,$nip_pptk->vertikal,'<span style="font-size:'.$nip_pptk->font_size.'">'.'NIP. '.$kwitansi->getRelationField('pptkRelation','nip').'</span>');
		$pdf->writeHTMLCell(0,0,$bendahara->horisontal,$bendahara->vertikal,'<span style="font-size:'.$bendahara->font_size.'">'.$kwitansi->getRelationField('bendaharaRelation','nama').'</span>');
		$pdf->writeHTMLCell(0,0,$nip_bendahara->horisontal,$nip_bendahara->vertikal,'<span style="font-size:'.$nip_bendahara->font_size.'">'.'NIP. '.$kwitansi->getRelationField('bendaharaRelation','nip').'</span>');
		$pdf->writeHTMLCell(0,0,$nama_penerima->horisontal,$nama_penerima->vertikal,'<span style="font-size:'.$nama_penerima->font_size.'">'.$kwitansi->getRelationField('penerimaRelation','nama').'</span>');
		$pdf->writeHTMLCell(0,0,$pangkat_penerima->horisontal,$pangkat_penerima->vertikal,'<span style="font-size:'.$pangkat_penerima->font_size.'">'.$kwitansi->getRelationField('penerimaRelation','jabatan').'</span>');
		$pdf->writeHTMLCell(0,0,$satuan_kerja_penerima->horisontal,$satuan_kerja_penerima->vertikal,'<span style="font-size:'.$satuan_kerja_penerima->font_size.'">'.'');
			
		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
    }
	
	public function actionExportSettingManualLhp($id)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Laporan Hasil Perjalanan Dinas');
		$pdf->SetTitle('Laporan Hasil Perjalanan Dinas');
		$pdf->SetSubject('Laporan Hasil Perjalanan Dinas');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', Setting::getValue('font_size'));
		
		$lhp = Lhp::model()->findByPk($id);
			
		$nama = PosisiCetak::model()->getKolomByJenisSurat(3,'nama');
		$nip = PosisiCetak::model()->getKolomByJenisSurat(3,'nip');
		$jabatan = PosisiCetak::model()->getKolomByJenisSurat(3,'jabatan');
		$alamat = PosisiCetak::model()->getKolomByJenisSurat(3,'alamat');
		$tujuan = PosisiCetak::model()->getKolomByJenisSurat(3,'tujuan');
		$tgl_berangkat = PosisiCetak::model()->getKolomByJenisSurat(3,'tgl_berangkat');
		$tgl_kembali = PosisiCetak::model()->getKolomByJenisSurat(3,'tgl_kembali');
		$tanggal = PosisiCetak::model()->getKolomByJenisSurat(3,'tanggal');
		$nama_ttd = PosisiCetak::model()->getKolomByJenisSurat(3,'nama_ttd');
		$nip_ttd = PosisiCetak::model()->getKolomByJenisSurat(3,'nip_ttd');
		$hasil_kegiatan_1 = PosisiCetak::model()->getKolomByJenisSurat(3,'hasil_kegiatan_1');
		$hasil_kegiatan_2 = PosisiCetak::model()->getKolomByJenisSurat(3,'hasil_kegiatan_2');
		$hasil_kegiatan_3 = PosisiCetak::model()->getKolomByJenisSurat(3,'hasil_kegiatan_3');
			
		$pdf->writeHTMLCell(0,0,$nama->horisontal,$nama->vertikal,'<span style="font-size:'.$nama->font_size.'">'.$lhp->getRelationField('pegawai','nama').'</span>');
		$pdf->writeHTMLCell(0,0,$nip->horisontal,$nip->vertikal,'<span style="font-size:'.$nip->font_size.'">'.$lhp->getRelationRelationField('spd','pegawai','nip').'</span>');
		$pdf->writeHTMLCell(0,0,$jabatan->horisontal,$jabatan->vertikal,'<span style="font-size:'.$jabatan->font_size.'">'.$lhp->getRelationRelationField('spd','pegawai','jabatan').'</span>');
		$pdf->writeHTMLCell(0,0,$alamat->horisontal,$alamat->vertikal,'<span style="font-size:'.$alamat->font_size.'">'.'Jl. Raya Jakarta KM. 04 Serang'.'</span>');
		$pdf->writeHTMLCell(0,0,$tujuan->horisontal,$tujuan->vertikal,'<span style="font-size:'.$tujuan->font_size.'">'.$lhp->getRelationField('spd','tujuan').'</span>');
		$pdf->writeHTMLCell(0,0,$tgl_berangkat->horisontal,$tgl_berangkat->vertikal,'<span style="font-size:'.$tgl_berangkat->font_size.'">'.Bantu::tanggal($lhp->getRelationField('spd','tgl_pergi')).'</span>');
		$pdf->writeHTMLCell(0,0,$tgl_kembali->horisontal,$tgl_kembali->vertikal,'<span style="font-size:'.$tgl_kembali->font_size.'">'.Bantu::tanggal($lhp->getRelationField('spd','tgl_kembali')).'</span>');
		$pdf->writeHTMLCell(0,0,$tanggal->horisontal,$tanggal->vertikal,'<span style="font-size:'.$tanggal->font_size.'">'.Bantu::tanggal($lhp->tanggal_lhp).'</span>');
		$pdf->writeHTMLCell(0,0,$nama_ttd->horisontal,$nama_ttd->vertikal,'<span style="font-size:'.$nama_ttd->font_size.'">'.$lhp->getRelationField('pegawai','nama').'</span>');
		$pdf->writeHTMLCell(0,0,$nip_ttd->horisontal,$nip_ttd->vertikal,'<span style="font-size:'.$nip_ttd->font_size.'">'.$lhp->getRelationField('pegawai','nip').'</span>');
		$pdf->writeHTMLCell(0,0,$hasil_kegiatan_1->horisontal,$hasil_kegiatan_1->vertikal,'<span style="font-size:'.$hasil_kegiatan_1->font_size.'">'.$lhp->hasil_kegiatan_1.'</span>');
		$pdf->writeHTMLCell(0,0,$hasil_kegiatan_2->horisontal,$hasil_kegiatan_2->vertikal,'<span style="font-size:'.$hasil_kegiatan_2->font_size.'">'.$lhp->hasil_kegiatan_1.'</span>');
		$pdf->writeHTMLCell(0,0,$hasil_kegiatan_3->horisontal,$hasil_kegiatan_3->vertikal,'<span style="font-size:'.$hasil_kegiatan_3->font_size.'">'.$lhp->hasil_kegiatan_3.'</span>');
		
		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }

    public function actionExportSpdOto2($id)
    {
	        $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Surat Perjalanan Dinas');
		$pdf->SetSubject('Surat Perjalanan Dinas');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		
		$html='
		<p align="right"><b></b></p>
		<table style="padding:5px">';
			
			$i=1;
			foreach(Spd::model()->findAllbyPk($id) as $data)
			{
				//$pejabat=Pejabat::model()->findbyPk($data->id_pejabat);
				//$pegawai=Pegawai::model()->findbyPk($data->id_pegawai);
				//$golongan=Golongan::model()->findbyPk($pegawai->id_golongan);
			    $html .= '
			   
			 	<tr>
			 		<td width="50%" >
			 		</td>
			 		<td width="50%" >
			 			<table>
				 			<tr>
				 				<td><p>&nbsp;</p></td>
				 				<td> '.$data->tempat_berangkat.'</td>
				 			</tr>
				 			<tr>
				 				<td>&nbsp;</td>
				 				<td> '.$data->tujuan.'</td>
				 			</tr>
				 			<tr>
				 				<td>&nbsp;</td>
				 				<td> '.date('j F Y', strtotime($data->tgl_pergi)).'</td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"> '.$data->getRelationField("pejabat","jabatan").'</td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><p style="text-decoration:underline;"><b>'.$data->getRelationField("pejabat","nama").'</b></p></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2">NIP.'.$data->getRelationNip("pejabat","nip").'</td>
				 			</tr>
			 			</table>
			 		</td>
			 	</tr>
				<tr>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>&nbsp;</td>
				 				<td> '.$data->tujuan.'</td>
				 			</tr>
				 			<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
				 			<tr>
				 				<td>&nbsp;</td>
				 				<td> '.date('d F Y', strtotime($data->tgl_pergi)).'</td>
				 			</tr>
			 			</table>
					</td>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>&nbsp;</td>
				 				<td> '.$data->tujuan.'</td>
				 			</tr>
				 			<tr>
				 				<td>&nbsp;</td>
				 				<td> '.$data->tempat_berangkat.'</td>
				 			</tr>
				 			<tr>
				 				<td>&nbsp;</td>
				 				<td> '.date('j F Y', strtotime($data->tgl_kembali)).'</td>
				 			</tr>
			 			</table>
					</td>
				</tr>
				<tr>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>&nbsp;</td>
				 				<td>&nbsp;</td>
				 			</tr>
				 			
				 			<tr>
				 				<td>&nbsp;</td>
				 				<td>&nbsp;</td>
				 			</tr>
			 			</table>
					</td>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>&nbsp;</td>
				 				<td>&nbsp;</td>
				 			</tr>
				 			<tr>
				 				<td>&nbsp;</td>
				 				<td>&nbsp;</td>
				 			</tr>
				 			<tr>
				 				<td>&nbsp;</td>
				 				<td>&nbsp;</td>
				 			</tr>
			 			</table>
					</td>
				</tr>
				<tr>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>&nbsp;</td>
				 				<td>&nbsp;</td>
				 			</tr>
				 			
				 			<tr>
				 				<td>&nbsp;</td>
				 				<td>&nbsp;</td>
				 			</tr>
			 			</table>
					</td>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>&nbsp;</td>
				 				<td>&nbsp;</td>
				 			</tr>
				 			<tr>
				 				<td>&nbsp;</td>
				 				<td>&nbsp;</td>
				 			</tr>
				 			<tr>
				 				<td>&nbsp;</td>
				 				<td>&nbsp;</td>
				 			</tr>
			 			</table>
					</td>
				</tr>
				<tr>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>&nbsp;</td>
				 				<td>&nbsp;</td>
				 			</tr>
				 			
				 			<tr>
				 				<td>&nbsp;</td>
				 				<td>&nbsp;</td>
				 			</tr>
			 			</table>
					</td>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>&nbsp;</td>
				 				<td>&nbsp;</td>
				 			</tr>
				 			<tr>
				 				<td>&nbsp;</td>
				 				<td>&nbsp;</td>
				 			</tr>
				 			<tr>
				 				<td>&nbsp;</td>
				 				<td>&nbsp;</td>
				 			</tr>
			 			</table>
					</td>
				</tr>
				<tr>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>&nbsp;</td>
				 				<td>&nbsp;</td>
				 			</tr>
				 			<tr>
				 				<td>&nbsp;</td>
				 				<td> '.date('j F Y', strtotime($data->tgl_kembali)).'</td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2">&nbsp;<br>&nbsp;</td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><p style="text-decoration:underline;"><b>'.$data->getRelationField("pejabat","nama").'</b></p></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2">NIP.'.$data->getRelationNip("pejabat","nip").'</td>
				 			</tr>
			 			</table>
					</td>
					<td width="50%">
						<table>
				 			<tr>
				 				<td><p>
				 						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				 						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				 						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				 						</p>
				 				</td>
				 			</tr>
				 			<tr>
				 				<td align="center">&nbsp;<br>&nbsp;</td>
				 			</tr>
				 			<tr>
				 				<td align="center"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center"><p style="text-decoration:underline;"><b>'.$data->getRelationField("pejabat","nama").'</b><br>NIP.'.$data->getRelationNip("pejabat").'</p></td>
				 			</tr>

			 			</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table>
							<tr>
								<td width="5%">
									<b></b>
								</td>
								<td width="95%">
									<b></b>
								<p></p>
								</td>
							</tr>
						</table>
						
					</td>
				</tr>
				';
		
		$i++;
			}
		$html .='</table>
		';
		
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }

    public function actionExportSpd()
    {
	        $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Rekap Nomor SPD');
		$pdf->SetSubject('Rekap Nomor SPD');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		
		
		$html='

		<table style="border-bottom:1px;">
					<tr>
						<td width="20%">
							<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="80px">
						</td>
						<td width="80%" >
							<p style="font-size:70px;">
							&nbsp;&nbsp;<b>PEMERINTAH KABUPATEN SERANG</b><br>
							<b style="font-size:50px;">DINAS PENGELOLAAN KEUANGAN DAERAH</b><br>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							( D P K D )<br>
							<i style="font-size:29px;">Jalan Raya Serang-Jakarta Km 4, Kabupaten Serang Telp.(0254)280529</i></p>
						</td>
					</tr>
				</table>
		<h4 align="center">Rekap Nomor SPD</h4>
		<table border="1" style="padding:5px">
			<tr style="background-color:#ccc; color:#000;">
				<td width="6%" align="center"><b>No</b></td>
				<td width="23%" align="center"><b>No. SPD</b></td>
				<td width="23%" align="center"><b>Tgl SPD</b></td>
				<td width="24%" align="center"><b>Nama & Pengikut</b></td>
				<td width="23%" align="center"><b>Tujuan</b></td>	
			</tr>';
			
			$i=1;
			foreach(Spd::model()->findAll() as $data)
			{
			    $html .= '<tr>
				<td style="text-align: center;">'.$i.'</td>
				<td>'.$data->nomor_spd.'</td>
				<td>'.date('d F Y', strtotime($data->tanggal)).'</td>
				<td>
					'.$data->pegawai->nama.'<br>';
					foreach(Pengikut::model()->findAllByAttributes(array('id_spd'=>$data->id)) as $row)
					{
						$html .= '&nbsp;&nbsp;'.$row->pegawai->nama.'<br>';
					}
			    $html .= '
				</td>
				<td>'.$data->tujuan.'</td>
			    </tr>';
			
			$i++;
			}
		$html .='</table>';
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
	 
    }

    public function actionExportSpt()
    {
	        $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Rekap Nomor SPD');
		$pdf->SetSubject('Rekap Nomor SPD');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		
		
		$html='

		<table style="border-bottom:1px;">
					<tr>
						<td width="15%">
							<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="65px">
						</td>
						<td width="85%" >
							<p style="font-size:70px;line-height:93%;" >
							&nbsp;&nbsp;<b>PEMERINTAH KABUPATEN SERANG</b><br>
							<b style="font-size:50px;">DINAS PENGELOLAAN KEUANGAN DAERAH</b><br>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							( D P K D )<br>
							<i style="font-size:29px;">Jalan Raya Serang-Jakarta Km 4, Kabupaten Serang Telp.(0254)280529</i></p>
						</td>
					</tr>
				</table>
		<h4 align="center">Rekap Nomor Surat Tugas</h4>
		<table border="1" style="padding:5px">
			<tr>
				<td width="6%" align="center"><b>No</b></td>
				<td width="25%" align="center"><b>No. SPT</b></td>
				<td align="center"><b>Tgl SPD</b></td>
				<td width="30%" align="center"><b>Nama & Pengikut</b></td>
				<td align="center"><b>Tujuan</b></td>	
			</tr>';
			
			$i=1;
			foreach(Spd::model()->findAll() as $data)
			{
			    $html .= '<tr>
				<td style="text-align: center;">'.$i.'</td>
				<td>'.$data->nomor_spt.'</td>
				<td>'.date('d F Y', strtotime($data->tanggal)).'</td>
				<td>
					'.$data->pegawai->nama.'<br>';
					foreach(Pengikut::model()->findAllByAttributes(array('id_spd'=>$data->id)) as $row)
					{
						$html .= '&nbsp;&nbsp;'.$row->pegawai->nama.'<br>';
					}
			    $html .= '
				</td>
				<td>'.$data->tujuan.'</td>
			    </tr>';
			
			$i++;
			}
		$html .='</table>';
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
	 
    }

    public function actionExportSpdDetail()
    {
	        $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Rekap Nomor SPD');
		$pdf->SetSubject('Rekap Nomor SPD');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		
		
		$html='
		<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="30px">
		<p>PEMERINTAH KABUPATEN SERANG
		<br>SKPD : DINAS PENGELOLAAN KEUANGAN DAERAH</p>
		<p>REKAP DETAIL SURAT PERJALANAN DINAS</p>
		<table border="1" style="padding:5px">
			<tr style="background-color:#ccc; color:#000;">
				<td width="6%" align="center"><b>No</b></td>
				<td width="25%" align="center"><b>No. SPD</b></td>
				<td align="center"><b>Tgl SPD</b></td>
				<td width="30%" align="center"><b>Nama & Pengikut</b></td>
				<td align="center"><b>Tujuan</b></td>	
			</tr>';
			
			$i=1;
			foreach(Spd::model()->findAll() as $data)
			{
			    $html .= '<tr>
				<td style="text-align: center;">'.$i.'</td>
				<td>'.$data->nomor.'</td>
				<td>'.date('d F Y', strtotime($data->tanggal)).'</td>
				<td>
					'.$data->pegawai->nama.'<br>';
					foreach(Pengikut::model()->findAllByAttributes(array('id_spd'=>$data->id)) as $row)
					{
						$html .= '&nbsp;&nbsp;'.$row->pegawai->nama.'<br>';
					}
			    $html .= '
				</td>
				<td>'.$data->tujuan.'</td>
			    </tr>';
			
			$i++;
			}
		$html .='</table>';
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
	 
    }

    public function actionRekapSpd()
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Rekap Nomor SPD');
		$pdf->SetSubject('Rekap Nomor SPD');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		
		
		$html='
		<table>
				<tr>
					<td width="10%">
						<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="40px">
					</td>
					<td width="85%" >
						<h2>
							<b>PEMERINTAH KABUPATEN SERANG</b><br>
							<b>DINAS PENGELOLAAN KEUANGAN DAERAH (DPKD)</b>
							<br>
						</h2>
					</td>
				</tr>
				<tr>
					<td width="100%" align="center">
						<h4>REKAP DATA PERJALANAN DINAS</h4>
					</td>
				</tr>
		</table>
		<br>
		';


		$html .= '
		<table border="1" style="padding:5px">
			<thead>
				<tr>
					<td width="6%" align="center">No</td>
					<td width="18%" align="center">No. Lamp. SPD</td>
					<td width="25%" align="center">Tanggal</td>
					<td width="17%" align="center">Nama Pegawai</td>
					<td width="17%" align="center">Tujuan</td>
					<td width="15%" align="center">Jumlah Biaya</td>
				</tr>
			</thead>
			<tbody>
		';
					$criteria = new CDbCriteria;
					$criteria->condition = 'nomor IS NOT NULL';
					$model = Spd::model()->findAll($criteria);

				 	if($model==null) {
						$total_keseluruhan = 0;
						$hasil = 0; 
						echo "<tr><td colspan='6'><i><center>Data SPD pada tanggal tersebut tidak ada</center></i></td></tr>";
					}else{ 
				
							$i=1; $total_keseluruhan = 0;
							foreach ($model as $data) {
							
							$kwitansi = kwitansi::model()->findByAttributes(array('id_spd'=>$data->id));

							if(empty($kwitansi->total)){
								
								$total_kwitansi = 0;
								$hasil = $total_keseluruhan += $total_kwitansi;
								$total = 0;
							}else{
								
								$total_kwitansi = $kwitansi->total;
								$hasil = $total_keseluruhan += $total_kwitansi;
								$total = $kwitansi->total;
							}

			$html .= '	
				<tr>
					<td width="6%" align="center">'.$i.'</td>
					<td width="18%">'.$data->nomor.'</td>
					<td width="25%">'.Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tgl_pergi).' s/d '.Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tgl_kembali).'</td>
					<td width="17%">'.$data->pegawai->nama.'</td>
					<td width="17%">'.$data->tujuan.'</td>
					<td width="15%">Rp.'.number_format($total,0,",",".").'</td>
				</tr>
			';
			$i++; } }
		$html .= '
				<tr>
					<td colspan="4">&nbsp;</td>
					<td><b>Total Jumlah</b></td>
					<td><b>Rp.'.number_format($hasil,0,",",".").'</b></td>
				</tr>
			</tbody>
		</table>
		';

		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
	 
    }

    public function actionRekapSpdFilter($tgl_awal,$tgl_akhir)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Rekap Nomor SPD');
		$pdf->SetSubject('Rekap Nomor SPD');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		
		
		$html='
		<table>
				<tr>
					<td width="10%">
						<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="40px">
					</td>
					<td width="85%" >
						<h2>
							<b>PEMERINTAH KABUPATEN SERANG</b><br>
							<b>DINAS PENGELOLAAN KEUANGAN DAERAH (DPKD)</b>
							<br>
						</h2>
					</td>
				</tr>
				<tr>
					<td width="100%" align="center">
						<h4>REKAP DATA PERJALANAN DINAS</h4>
					</td>
				</tr>
		</table>
		<br>
		';


		$html .= '
		<table border="1" style="padding:5px">
			<thead>
				<tr>
					<td width="6%" align="center">No</td>
					<td width="18%" align="center">No. Lamp. SPD</td>
					<td width="25%" align="center">Tanggal</td>
					<td width="17%" align="center">Nama Pegawai</td>
					<td width="17%" align="center">Tujuan</td>
					<td width="15%" align="center">Jumlah Biaya</td>
				</tr>
			</thead>
			<tbody>
		';
					$criteria = new CDbCriteria;
					$criteria->params = array(':tgl_awal'=>$tgl_awal, ':tgl_akhir'=>$tgl_akhir);
					$criteria->condition = 'tanggal >= :tgl_awal AND tanggal <= :tgl_akhir AND nomor IS NOT NULL';
					$model = Spd::model()->findAll($criteria);

				 	if($model==null) {
						$total_keseluruhan = 0;
						$hasil = 0; 
						echo "<tr><td colspan='6'><i><center>Data SPD pada tanggal tersebut tidak ada</center></i></td></tr>";
					}else{ 
				
							$i=1; $total_keseluruhan = 0;
							foreach ($model as $data) {
							
							$kwitansi = kwitansi::model()->findByAttributes(array('id_spd'=>$data->id));

							if(empty($kwitansi->total)){
								
								$total_kwitansi = 0;
								$hasil = $total_keseluruhan += $total_kwitansi;
								$total = 0;
							}else{
								
								$total_kwitansi = $kwitansi->total;
								$hasil = $total_keseluruhan += $total_kwitansi;
								$total = $kwitansi->total;
							}

			$html .= '	
				<tr>
					<td width="6%" align="center">'.$i.'</td>
					<td width="18%">'.$data->nomor.'</td>
					<td width="25%">'.Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tgl_pergi).' s/d '.Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tgl_kembali).'</td>
					<td width="17%">'.$data->pegawai->nama.'</td>
					<td width="17%">'.$data->tujuan.'</td>
					<td width="15%">Rp.'.number_format($total,0,",",".").'</td>
				</tr>
			';
			$i++; } }
		$html .= '
				<tr>
					<td colspan="4">&nbsp;</td>
					<td><b>Total Jumlah</b></td>
					<td><b>Rp.'.number_format($hasil,0,",",".").'</b></td>
				</tr>
			</tbody>
		</table>
		';

		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
	 
    }

    public function actionExportSptDetail($id)
    {
	        $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Rekap Nomor SPD');
		$pdf->SetSubject('Rekap Nomor SPD');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		
		
		$html='
		<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="30px">
		<p>PEMERINTAH KABUPATEN SERANG
		<br>SKPD : DINAS PENGELOLAAN KEUANGAN DAERAH</p>
		<p>REKAP DETAIL SURAT PERINTAH</p>
		<table border="1" style="padding:5px">
			<tr style="background-color:#ccc; color:#000;">
				<td width="6%" align="center"><b>No</b></td>
				<td width="25%" align="center"><b>No. SPT</b></td>
				<td align="center"><b>Tgl SPD</b></td>
				<td width="30%" align="center"><b>Nama & Pengikut</b></td>
				<td align="center"><b>Tujuan</b></td>	
			</tr>';
			
			$i=1;
			foreach(Spd::model()->findAll() as $data)
			{
			    $html .= '<tr>
				<td style="text-align: center;">'.$i.'</td>
				<td>'.$data->nomor_spt.'</td>
				<td>'.date('d F Y', strtotime($data->tanggal)).'</td>
				<td>
					'.$data->pegawai->nama.'<br>';
					foreach(Pengikut::model()->findAllByAttributes(array('id_spd'=>$data->id)) as $row)
					{
						$html .= '&nbsp;&nbsp;'.$row->pegawai->nama.'<br>';
					}
			    $html .= '
				</td>
				<td>'.$data->tujuan.'</td>
			    </tr>';
			
			$i++;
			}
		$html .='</table>';
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
	 
    }

    public function actionExportKwitansi()
    {
	        $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Data Pegawai');
		$pdf->SetSubject('Data Pegawai');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		
		
		$html='
		<table>
				<tr>			
					<td width="10%">
						<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="50px">
					</td>
					<td width="90%">
						<h3>PEMERINTAH KABUPATEN SERANG<br>
						DINAS PENGELOLAAN KEUANGAN DAERAH (DPKD)</H3>
					</td>
				</tr>
			</table>
		<h4 align="center">Register Nomor Bukti Pembayaran</h4>
		<table border="1" style="padding:3px">
			<tr>
				<td width="6%" ><b>No</b></td>
				<td width="20%"><b>No. Bukti</b></td>
				<td><b>Koring PD</b></td>
				<td width="20%"><b>Tanggal Pergi</b></td>
				<td width="20%"><b>Nama Pegawai</b></td>
				<td><b>Jumlah Biaya</b></td>	
			</tr>';
			
			$i=1;
			foreach(Kwitansi::model()->findAll() as $data)
			{
				$pegawai=Pegawai::model()->findbyPk($data->spd->id_pegawai);
			    $html .= '<tr>
				<td style="text-align: center;">'.$i.'</td>
				<td>'.$data->nomor.'</td>
				<td>'.$data->spd->koring.'</td>
				<td>'.$data->spd->tgl_pergi.'</td>
				<td>'.$pegawai->nama.'</td>
				<td>'.$data->total.'</td>
			    </tr>';
			
			$i++;
			}
		$html .='</table>';
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }

    public function actionExportKwitansiDetail($id)
    {
	        $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Data Pegawai');
		$pdf->SetSubject('Data Pegawai');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		
		
		$html='
			<table>
				<tr>			
					<td width="10%">
						<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="50px">
					</td>
					<td width="90%">
						<h3>PEMERINTAH KABUPATEN SERANG<br>
						DINAS PENGELOLAAN KEUANGAN DAERAH (DPKD)</H3>
					</td>
				</tr>
			</table>				
		<h4>Rekap Detail Bukti Pembayaran</h4>
		<table border="1" style="padding:3px">
			<tr>
				<td width="6%" ><b>No</b></td>
				<td width="20%"><b>No. Bukti</b></td>
				<td><b>Koring PD</b></td>
				<td width="20%"><b>Tanggal Pergi</b></td>
				<td width="20%"><b>Nama Pegawai</b></td>
				<td><b>Jumlah Biaya</b></td>	
			</tr>';
			
			$i=1;
			foreach(Kwitansi::model()->findAll() as $data)
			{
				$pegawai=Pegawai::model()->findbyPk($data->spd->id_pegawai);
			    $html .= '<tr>
				<td style="text-align: center;">'.$i.'</td>
				<td>'.$data->nomor.'</td>
				<td>'.$data->spd->koring.'</td>
				<td>'.$data->spd->tgl_pergi.'</td>
				<td>'.$pegawai->nama.'</td>
				<td>'.$data->total.'</td>
			    </tr>';
			
			$i++;
			}
		$html .='</table>';
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }

    public function actionExportKwitansiDalam($id)
    {
	        $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'A3', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('RINCIAN BIAYA PERJALANAN DINAS');
		$pdf->SetTitle('RINCIAN BIAYA PERJALANAN DINAS');
		$pdf->SetSubject('RINCIAN BIAYA PERJALANAN DINAS');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 10);
		
		$html='
		<p align="right" style="line-height:100%;"><i>Lampiran Vii<br>Permenkeu RI. no.113 /PMK 05/2012</i></p>
		<span align="center" style="text-decoration:underline;"><b>RINCIAN BIAYA PERJALANAN DINAS</br></span><br>';
			
			$i=1;
			foreach(Kwitansi::model()->findAllbyPk($id) as $data)
			{
			    $html .= '
			    	<table width="60%">
			    		<tr>
			    			<td>
			    				Lampiran SPPD Nomor
			    			</td>
			    			<td>
			    				: '.$data->spd->nomor.'
			    			</td>
			    		</tr>
			    		<tr>
			    			<td>
			    				Tanggal
			    			</td>
			    			<td>
			    				: '.date('d F Y', strtotime($data->spd->tanggal)).'
			    			</td>
			    		</tr>
			    	</table>
			    	&nbsp;&nbsp;
			    	<table border="1px;">
			    		<tr>
			    			<td width="6%">
			    				No
			    			</td>
			    			<td width="32%">
			    				Perincian Biaya
			    			</td>
			    			<td width="32%">
			    				Jumlah
			    			</td>
			    			<td width="32%">
			    				Keterangan
			    			</td>
			    		</tr>
			    	';
			    	$i++; }

			    	$i=1;
			foreach(Kwitansi::model()->findAllbyPk($id) as $data)
			{ 
				$akomodasi = $data->akomodasi->biaya*$data->spd->lama;
				$biayaako = number_format($akomodasi, 2,",",".");
				$ref = $data->refresentatif->biaya*$data->spd->lama;
				$biayaref = number_format($ref, 2,",",".");
				$ssh = $data->ssh->biaya*$data->spd->lama;
				$sshawal=$data->ssh->biaya;
				$sshbiaya = number_format($sshawal, 2,",",".");
				$biayassh = number_format($ssh, 2,",",".");
				$bbm = $data->bbm;
				$biayabbm = number_format($bbm, 2,",",".");
				$tol = $data->tol;
				$biayatol = number_format($tol, 2,",",".");
				$tiket = $data->tiket;
				$biayatiket = number_format($tiket, 2,",",".");
				$total = $data->total;
				$jmltotal = number_format($total, 2,",",".");
			    $html .='
			    		<tr>
			    			<td width="6%">
			    				'.$i.'
			    			</td>
			    			<td width="32%">
			    				Uang Harian <br><br> <b>Jumlah</b>
			    			</td>
			    			<td width="32%">
			    				Rp '.$biayassh.' <br><br> <b>Rp '.$jmltotal.'</b>
			    			</td>
			    			<td width="32%">
			    				'.$data->spd->maksud.'
			    			</td>
			    		</tr>
			    		<tr>
			    			<td colspan="4">
			    				<i>Terbilang: </i>
			    			</td>
			    		</tr>';
			$i++;}
			    $html .='</table>';


			    $html .='<table>';
			 foreach(Kwitansi::model()->findAllbyPk($id) as $data)
			{
				$bendahara= Pejabat::model()->findbyPk($data->bendahara);
				$ppk= Pejabat::model()->findbyPk($data->ppk);
				$pegawai= Pegawai::model()->findbyPk($data->spd->id_pegawai);
			    $html .='
			    		<tr>
			    			<td>
			    				<p align="center">Telah Dibayar Sejumlah
			    				<br>Rp '.$jmltotal.'<br>
			    				Bendahara Pengeluaran</p>
			    				<br><br>
			    				<p align="center"><span style="text-decoration:underline;"><b>'.$bendahara->nama.'</b></span><br>NIP.'.$bendahara->nip.'
						 		</p>
			    			</td>
			    			<td>
			    				<br><br><br>
			    				<p align="center">Pejabat Pelaksana Teknis Kegiatan</p>
			    				<br><br>
			    				<p align="center"><span style="text-decoration:underline;"><b>'.$ppk->nama.'</b></span><br>NIP.'.$ppk->nip.'
						 		</p>
			    			</td>
			    			<td>
			    				<p align="center">
			    				'.$data->spd->tempat_berangkat.', '.Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->spd->tanggal).'<br>	
			    				Telah Dibayar Sejumlah
			    				<br>Rp '.$jmltotal.'<br>
			    				Yang Menerima</p>
			    				<br><br>
			    				<p align="center"><span style="text-decoration:underline;"><b>'.$pegawai->nama.'</b></span><br>NIP.'.$pegawai->nip.'
						 		</p>
			    			</td>
			    		</tr>';
			$i++;}
			    $html .='
			    <tr>
			    	<td width="100%">_________________________________________________________________________________________________________</td>
			    </tr>
			    <tr>
			    	<td width="100%" align="center"><b>PERHITUNGAN SPDD RAMPUNG</b></td>
			    </tr>
			    </table>';

			    $html .='
			    <table width="70%">
			    ';
			 foreach(Kwitansi::model()->findAllbyPk($id) as $data)
			{
				$bendahara= Pejabat::model()->findbyPk($data->bendahara);
				$ppk= Pejabat::model()->findbyPk($data->ppk);
				$pegawai= Pegawai::model()->findbyPk($data->spd->id_pegawai);
			    $html .='
			    		<tr>
			    			<td width="40%">
			    				Ditetapkan Sejumlah
			    			</td>
			    			<td width="20%">
			    				:
			    			</td>
			    			<td width="40%">
			    				<b>Rp '.$jmltotal.'</b>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td width="40%">
			    				Yang telah dibayarkan
			    			</td>
			    			<td width="20%">
			    				:
			    			</td>
			    			<td width="40%">
			    				<b>Rp '.$jmltotal.'</b>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td width="40%">
			    				bisa kurang/lebih
			    			</td>
			    			<td width="20%">
			    				:
			    			</td>
			    			<td width="40%">
			    				<b>Rp 0</b>
			    			</td>
			    		</tr>
			    	</table>
			    		';
			
			    $html .='
			    <table>
					<tr>
						<td width="60%">
						</td>
						<td width="40%" align="center">
							<p>Pejabat yang berwenang/<br>pejabat lain yang di tunjuk</p>
							<p>&nbsp;</p>
							<p style="line-height:70%;"><span style="text-decoration:underline;"><b>'.$ppk->nama.'</b></span></p>
							<p style="line-height:10%;">NIP. '.$ppk->nip.'</p>
						</td>
					</tr>
					<tr><td></td></tr>
					<tr>
						<td width="100%">-------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
					</tr>
				</table>
			    ';

			    $i++;}

			   
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }

    public function actionExportKwitansiDalamRef($id)
    {
	        $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('RINCIAN BIAYA PERJALANAN DINAS');
		$pdf->SetTitle('RINCIAN BIAYA PERJALANAN DINAS');
		$pdf->SetSubject('RINCIAN BIAYA PERJALANAN DINAS');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		
		
		$html='
		<p align="right"><i>Lampiran Vii<br>Permenkeu RI. no.113 /PMK 05/2012</i></p>
		<h4 align="center">RINCIAN BIAYA PERJALANAN DINAS</h4>';
			
			$i=1;
			foreach(Kwitansi::model()->findAllbyPk($id) as $data)
			{
			    $html .= '
			    	<table width="60%">
			    		<tr>
			    			<td>
			    				Lampiran SPPD Nomor
			    			</td>
			    			<td>
			    				:'.$data->spd->nomor.'
			    			</td>
			    		</tr>
			    		<tr>
			    			<td>
			    				Tanggal
			    			</td>
			    			<td>
			    				:'.$data->spd->tanggal.'
			    			</td>
			    		</tr>
			    	</table>

			    	<table border="1px;">
			    		<tr>
			    			<td width="6%">
			    				No
			    			</td>
			    			<td width="32%">
			    				Perincian Biaya
			    			</td>
			    			<td width="32%">
			    				Jumlah
			    			</td>
			    			<td width="32%">
			    				Keterangan
			    			</td>
			    		</tr>
			    	';
			    	$i++; }

			    	$i=1;
			foreach(Kwitansi::model()->findAllbyPk($id) as $data)
			{ 
				$akomodasi = $data->akomodasi->biaya*$data->spd->lama;
				$biayaako = number_format($akomodasi, 2,",",".");
				$ref = $data->refresentatif->biaya*$data->spd->lama;
				$biayaref = number_format($ref, 2,",",".");
				$ssh = $data->ssh->biaya*$data->spd->lama;
				$biayassh = number_format($ssh, 2,",",".");
				$bbm = $data->bbm;
				$biayabbm = number_format($bbm, 2,",",".");
				$tol = $data->tol;
				$biayatol = number_format($tol, 2,",",".");
				$tiket = $data->tiket;
				$biayatiket = number_format($tiket, 2,",",".");
				$total = $data->total;
				$jmltotal = number_format($total, 2,",",".");
			    $html .='
			    		<tr>
			    			<td width="6%">
			    				1<br>&nbsp;&nbsp;2
			    			</td>
			    			<td width="32%">
			    				Uang Harian <br>  Refresentasi <br><br> <b>Jumlah</b>
			    			</td>
			    			<td width="32%">
			    				Rp '.$biayassh.' <br>  Rp '.$biayaref.' <br><br> <b>Rp '.$jmltotal.'</b>
			    			</td>
			    			<td width="32%">
			    				'.$data->spd->maksud.'
			    			</td>
			    		</tr>
			    		<tr>
			    			<td colspan="4">
			    				<i>Terbilang: </i>
			    			</td>
			    		</tr>';
			$i++;}
			    $html .='</table>';


			    $html .='<table style="border-bottom:1px;">';
			 foreach(Kwitansi::model()->findAllbyPk($id) as $data)
			{
				$bendahara= Pejabat::model()->findbyPk($data->bendahara);
				$ppk= Pejabat::model()->findbyPk($data->ppk);
				$pegawai= Pegawai::model()->findbyPk($data->spd->id_pegawai);
			    $html .='
			    		<tr>
			    			<td>
			    			</td>
			    			<td>
			    			</td>
			    			<td>
			    				'.$data->spd->tempat_berangkat.','.$data->spd->tanggal.'
			    			</td>
			    		</tr>
			    		<tr>
			    			<td>
			    				<p align="center">Telah Dibayar Sejumlah
			    				<br>Rp '.$jmltotal.'<br>
			    				Bendahara Pengeluaran</p>
			    				<br><br>
			    				<p align="center">'.$bendahara->nama.'<br>NIP.'.$bendahara->nip.'
						 		</p>
			    			</td>
			    			<td>
			    				<br><br><br>
			    				<p align="center">Pejabat Pelaksana Teknis Kegiatan</p>
			    				<br><br>
			    				<p align="center">'.$ppk->nama.'<br>NIP.'.$ppk->nip.'
						 		</p>
			    			</td>
			    			<td>
			    				<p align="center">Telah Dibayar Sejumlah
			    				<br>Rp '.$jmltotal.'<br>
			    				Yang Menerima</p>
			    				<br><br>
			    				<p align="center">'.$pegawai->nama.'<br>NIP.'.$pegawai->nip.'
						 		</p>
			    			</td>
			    		</tr>';
			$i++;}
			    $html .='</table>';

			    $html .='
			    <h4 align="CENTER" ><B>PERHITUNGAN SPDD RAMPUNG</B></h4>
			    <table width="70%">
			    ';
			 foreach(Kwitansi::model()->findAllbyPk($id) as $data)
			{
				$bendahara= Pejabat::model()->findbyPk($data->bendahara);
				$ppk= Pejabat::model()->findbyPk($data->ppk);
				$pegawai= Pegawai::model()->findbyPk($data->spd->id_pegawai);
			    $html .='
			    		<tr>
			    			<td width="40%">
			    				Ditetapkan Sejumlah
			    			</td>
			    			<td width="20%">
			    				:
			    			</td>
			    			<td width="40%">
			    				<b>Rp '.$jmltotal.'</b>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td width="40%">
			    				Yang telah dibayarkan
			    			</td>
			    			<td width="20%">
			    				:
			    			</td>
			    			<td width="40%">
			    				<b>Rp '.$jmltotal.'</b>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td width="40%">
			    				bisa kurang/lebih
			    			</td>
			    			<td width="20%">
			    				:
			    			</td>
			    			<td width="40%">
			    				<b>Rp 0</b>
			    			</td>
			    		</tr>
			    	</table>
			    		';
			
			    $html .='
			    <div align="right">
			    	<p >Pejabat yang berwenang/<br>pejabat lain yang di tunjuk</p>
			    	<br><br>
			    	<p>
			    	'.$pegawai->nama.'<br>NIP.'.$pegawai->nip.'
			    	</p>
			    </div>
			    ';

			    $i++;}


		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }


    public function actionExportKwitansiDalamPptk($id)
    {
	        $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('RINCIAN BIAYA PERJALANAN DINAS');
		$pdf->SetTitle('RINCIAN BIAYA PERJALANAN DINAS');
		$pdf->SetSubject('RINCIAN BIAYA PERJALANAN DINAS');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		
		
		$html='
		<p align="right"><i>Lampiran Vii<br>Permenkeu RI. no.113 /PMK 05/2012</i></p>
		<h4 align="center">RINCIAN BIAYA PERJALANAN DINAS</h4>';
			
			$i=1;
			foreach(Kwitansi::model()->findAllbyPk($id) as $data)
			{
			    $html .= '
			    	<table width="60%">
			    		<tr>
			    			<td>
			    				Lampiran SPPD Nomor
			    			</td>
			    			<td>
			    				:'.$data->spd->nomor.'
			    			</td>
			    		</tr>
			    		<tr>
			    			<td>
			    				Tanggal
			    			</td>
			    			<td>
			    				:'.$data->spd->tanggal.'
			    			</td>
			    		</tr>
			    	</table>

			    	<table border="1px;">
			    		<tr>
			    			<td width="6%">
			    				No
			    			</td>
			    			<td width="32%">
			    				Perincian Biaya
			    			</td>
			    			<td width="32%">
			    				Jumlah
			    			</td>
			    			<td width="32%">
			    				Keterangan
			    			</td>
			    		</tr>
			    	';
			    	$i++; }

			    	$i=1;
			foreach(Kwitansi::model()->findAllbyPk($id) as $data)
			{ 
				$akomodasi = $data->akomodasi->biaya*$data->spd->lama;
				$biayaako = number_format($akomodasi, 2,",",".");
				$ref = $data->refresentatif->biaya*$data->spd->lama;
				$biayaref = number_format($ref, 2,",",".");
				$ssh = $data->ssh->biaya*$data->spd->lama;
				$biayassh = number_format($ssh, 2,",",".");
				$bbm = $data->bbm;
				$biayabbm = number_format($bbm, 2,",",".");
				$tol = $data->tol;
				$biayatol = number_format($tol, 2,",",".");
				$tiket = $data->tiket;
				$biayatiket = number_format($tiket, 2,",",".");
				$total = $data->total;
				$jmltotal = number_format($total, 2,",",".");
			    $html .='
			    		<tr>
			    			<td width="6%">
			    				'.$i.'
			    			</td>
			    			<td width="32%">
			    				Uang Harian <br><br> <b>Jumlah</b>
			    			</td>
			    			<td width="32%">
			    				Rp '.$biayassh.' <br><br> <b>Rp '.$jmltotal.'</b>
			    			</td>
			    			<td width="32%">
			    				'.$data->spd->maksud.'
			    			</td>
			    		</tr>
			    		<tr>
			    			<td colspan="4">
			    				<i>Terbilang: </i>
			    			</td>
			    		</tr>';
			$i++;}
			    $html .='</table>';


			    $html .='<table style="border-bottom:1px;">';
			 foreach(Kwitansi::model()->findAllbyPk($id) as $data)
			{
				$bendahara= Pejabat::model()->findbyPk($data->bendahara);
				$ppk= Pejabat::model()->findbyPk($data->ppk);
				$pegawai= Pegawai::model()->findbyPk($data->spd->id_pegawai);
			    $html .='
			    		<tr>
			    			<td>
			    			</td>
			    			<td>
			    			</td>
			    			<td>
			    				'.$data->spd->tempat_berangkat.','.$data->spd->tanggal.'
			    			</td>
			    		</tr>
			    		<tr>
			    			<td>
			    				<p align="center">Telah Dibayar Sejumlah
			    				<br>Rp '.$jmltotal.'<br>
			    				Bendahara Pengeluaran</p>
			    				<br><br>
			    				<p align="center">'.$bendahara->nama.'<br>NIP.'.$bendahara->nip.'
						 		</p>
			    			</td>
			    			<td>
			    				
			    			</td>
			    			<td>
			    				<p align="center">Telah Dibayar Sejumlah
			    				<br>Rp '.$jmltotal.'<br>
			    				Yang Menerima</p>
			    				<br><br>
			    				<p align="center">'.$pegawai->nama.'<br>NIP.'.$pegawai->nip.'
						 		</p>
			    			</td>
			    		</tr>';
			$i++;}
			    $html .='</table>';

			    $html .='
			    <h4 align="CENTER" ><B>PERHITUNGAN SPDD RAMPUNG</B></h4>
			    <table width="70%">
			    ';
			 foreach(Kwitansi::model()->findAllbyPk($id) as $data)
			{
				$bendahara= Pejabat::model()->findbyPk($data->bendahara);
				$ppk= Pejabat::model()->findbyPk($data->ppk);
				$pegawai= Pegawai::model()->findbyPk($data->spd->id_pegawai);
			    $html .='
			    		<tr>
			    			<td width="40%">
			    				Ditetapkan Sejumlah
			    			</td>
			    			<td width="20%">
			    				:
			    			</td>
			    			<td width="40%">
			    				<b>Rp '.$jmltotal.'</b>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td width="40%">
			    				Yang telah dibayarkan
			    			</td>
			    			<td width="20%">
			    				:
			    			</td>
			    			<td width="40%">
			    				<b>Rp '.$jmltotal.'</b>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td width="40%">
			    				bisa kurang/lebih
			    			</td>
			    			<td width="20%">
			    				:
			    			</td>
			    			<td width="40%">
			    				<b>Rp 0</b>
			    			</td>
			    		</tr>
			    	</table>
			    		';
			
			    $html .='
			    <div align="right">
			    	<p >Pejabat yang berwenang/<br>pejabat lain yang di tunjuk</p>
			    	<br><br>
			    	<p>
			    	'.$pegawai->nama.'<br>NIP.'.$pegawai->nip.'
			    	</p>
			    </div>
			    ';

			    $i++;}


		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }

    public function actionExportKwitansiLuar($id)
    {
	        $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('RINCIAN BIAYA PERJALANAN DINAS');
		$pdf->SetTitle('RINCIAN BIAYA PERJALANAN DINAS');
		$pdf->SetSubject('RINCIAN BIAYA PERJALANAN DINAS');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 10);
		
		
		
		$html='
		<p align="right" style="line-height:100%;"><i>Lampiran Vii<br>Permenkeu RI. no.113 /PMK 05/2012</i></p>
		<span align="center" style="text-decoration:underline;"><b>RINCIAN BIAYA PERJALANAN DINAS</br></span><br>';
			
			$i=1;
			foreach(Kwitansi::model()->findAllbyPk($id) as $data)
			{
				$akomodasi = $data->akomodasi->biaya*$data->spd->lama;
				$biayaako = number_format($akomodasi, 2,",",".");
				$ref = $data->refresentatif->biaya*$data->spd->lama;
				$biayaref = number_format($ref, 2,",",".");
				$ssh = $data->ssh->biaya*$data->spd->lama;
				$sshawal=$data->ssh->biaya;
				$sshbiaya = number_format($sshawal, 2,",",".");
				$biayassh = number_format($ssh, 2,",",".");
				$bbm = $data->bbm;
				$biayabbm = number_format($bbm, 2,",",".");
				$tol = $data->tol;
				$biayatol = number_format($tol, 2,",",".");
				$tiket = $data->tiket;
				$biayatiket = number_format($tiket, 2,",",".");
				$total = $data->total;
				$jmltotal = number_format($total, 2,",",".");
			    $html .= '
			    	<table width="60%">
			    		<tr>
			    			<td>
			    				Lampiran SPPD Nomor
			    			</td>
			    			<td>
			    				:'.$data->spd->nomor.'
			    			</td>
			    		</tr>
			    		<tr>
			    			<td>
			    				Tanggal
			    			</td>
			    			<td>
			    				:'.date('d F Y', strtotime($data->spd->tanggal)).'
			    			</td>
			    		</tr>
			    	</table>

			    	<table border="1px;">
			    		<tr>
			    			<td width="6%">
			    				No
			    			</td>
			    			<td width="42%">
			    				Perincian Biaya
			    			</td>
			    			<td width="32%">
			    				Jumlah
			    			</td>
			    			<td width="22%">
			    				Keterangan
			    			</td>
			    		</tr>
			    	';
			    	$i++; }

			    	$i=1;
			foreach(Kwitansi::model()->findAllbyPk($id) as $data)
			{
			    $html .='
			    		
			    		<tr>
			    			<td width="6%">
			    				1<br><br><br><br><br>
			    				2
			    			</td>
			    			<td width="42%">
			    				Biaya Transport<br>
			    				&nbsp;&nbsp;BBM <br>
			    				&nbsp;&nbsp;TOL<br>
			    				&nbsp;&nbsp;Akomodasi<br>
			    				&nbsp;&nbsp;Tiket<br>
			    				Uang Harian ('.$data->spd->lama.' x @Rp '.$sshbiaya.')<br><br>
			    				<b>Jumlah</b>
			    			</td>
			    			<td width="32%" align="right">
			    				<br><br>
			    				Rp '.$biayabbm.'&nbsp;&nbsp;<br>
			    				Rp '.$biayatol.'&nbsp;&nbsp;<br>
			    				Rp '.$biayaako.'&nbsp;&nbsp;<br>
			    				Rp '.$biayatiket.'&nbsp;&nbsp;<br>
			    				Rp '.$biayassh.'&nbsp;&nbsp;<br><br>
			    				<b>Rp '.$jmltotal.'&nbsp;&nbsp;</b>
			    			</td>
			    			<td width="22%">
			    				'.$data->spd->maksud.'
			    			</td>
			    		</tr>
			    		<tr>
			    			<td colspan="4">
			    				<i>Terbilang: </i>
			    			</td>
			    		</tr>';
			$i++;}
			    $html .='</table>';


			    $html .='<table>';
			 foreach(Kwitansi::model()->findAllbyPk($id) as $data)
			{
				$bendahara= Pejabat::model()->findbyPk($data->bendahara);
				$ppk= Pejabat::model()->findbyPk($data->ppk);
				$pegawai= Pegawai::model()->findbyPk($data->spd->id_pegawai);
			    $html .='
			    		<tr>
			    			<td>
			    				<p align="center">Telah Dibayar Sejumlah
			    				<br>Rp '.$jmltotal.'<br>
			    				Bendahara Pengeluaran</p>
			    				<br><br>
			    				<p align="center"><span style="text-decoration:underline;"><b>'.$bendahara->nama.'</b></span><br>NIP.'.$bendahara->nip.'
						 		</p>
			    			</td>
			    			<td>
			    				<br><br><br>
			    				<p align="center">Pejabat Pelaksana Teknis Kegiatan</p>
			    				<br><br>
			    				<p align="center"><span style="text-decoration:underline;"><b>'.$ppk->nama.'</b></span><br>NIP.'.$ppk->nip.'
						 		</p>
			    			</td>
			    			<td>
			    				<p align="center">
			    				'.$data->spd->tempat_berangkat.', '.Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->spd->tanggal).'<br>	
			    				Telah Dibayar Sejumlah
			    				<br>Rp '.$jmltotal.'<br>
			    				Yang Menerima</p>
			    				<br><br>
			    				<p align="center"><span style="text-decoration:underline;"><b>'.$pegawai->nama.'</b></span><br>NIP.'.$pegawai->nip.'
						 		</p>
			    			</td>
			    		</tr>';
			$i++;}
			    $html .='
			    <tr>
			    	<td width="100%">_________________________________________________________________________________________________________</td>
			    </tr>
			    <tr>
			    	<td width="100%" align="center"><b>PERHITUNGAN SPDD RAMPUNG</b></td>
			    </tr>
			    </table>';

			     $html .='
			    <table width="70%">
			    ';

			 foreach(Kwitansi::model()->findAllbyPk($id) as $data)
			{
				$bendahara= Pejabat::model()->findbyPk($data->bendahara);
				$ppk= Pejabat::model()->findbyPk($data->ppk);
				$pegawai= Pegawai::model()->findbyPk($data->spd->id_pegawai);
			    $html .='
			    		<tr>
			    			<td width="40%">
			    				Ditetapkan Sejumlah
			    			</td>
			    			<td width="20%">
			    				:
			    			</td>
			    			<td width="40%">
			    				<b>Rp '.$jmltotal.'</b>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td width="40%">
			    				Yang telah dibayarkan
			    			</td>
			    			<td width="20%">
			    				:
			    			</td>
			    			<td width="40%">
			    				<b>Rp '.$jmltotal.'</b>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td width="40%">
			    				bisa kurang/lebih
			    			</td>
			    			<td width="20%">
			    				:
			    			</td>
			    			<td width="40%">
			    				<b>Rp 0</b>
			    			</td>
			    		</tr>
			    	</table>
			    		';
			
			    $html .='
			    <table>
					<tr>
						<td width="60%">
						</td>
						<td width="40%" align="center">
							<p>Pejabat yang berwenang/<br>pejabat lain yang di tunjuk</p>
							<p>&nbsp;</p>
							<p style="line-height:70%;"><span style="text-decoration:underline;"><b>'.$ppk->nama.'</b></span></p>
							<p style="line-height:10%;">NIP. '.$ppk->nip.'</p>
						</td>
					</tr>
					<tr><td></td></tr>
					<tr>
						<td width="100%">-------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
					</tr>
				</table>
			    ';

			    $i++;}
//------------------------------//


		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }

    public function actionExportKwitansiLuarRef($id)
    {
	        $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('RINCIAN BIAYA PERJALANAN DINAS');
		$pdf->SetTitle('RINCIAN BIAYA PERJALANAN DINAS');
		$pdf->SetSubject('RINCIAN BIAYA PERJALANAN DINAS');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		
		
		$html='
		<p align="right"><i>Lampiran Vii<br>Permenkeu RI. no.113 /PMK 05/2012</i></p>
		<h4 align="center">RINCIAN BIAYA PERJALANAN DINAS</h4>';
			
			$i=1;
			foreach(Kwitansi::model()->findAllbyPk($id) as $data)
			{
				$akomodasi = $data->akomodasi->biaya*$data->spd->lama;
				$biayaako = number_format($akomodasi, 2,",",".");
				$ref = $data->refresentatif->biaya*$data->spd->lama;
				$biayaref = number_format($ref, 2,",",".");
				$ssh = $data->ssh->biaya*$data->spd->lama;
				$sshawal=$data->ssh->biaya;
				$sshbiaya = number_format($sshawal, 2,",",".");
				$biayassh = number_format($ssh, 2,",",".");
				$bbm = $data->bbm;
				$biayabbm = number_format($bbm, 2,",",".");
				$tol = $data->tol;
				$biayatol = number_format($tol, 2,",",".");
				$tiket = $data->tiket;
				$biayatiket = number_format($tiket, 2,",",".");
				$total = $data->total;
				$jmltotal = number_format($total, 2,",",".");
			    $html .= '
			    	<table width="60%">
			    		<tr>
			    			<td>
			    				Lampiran SPPD Nomor
			    			</td>
			    			<td>
			    				:'.$data->spd->nomor.'
			    			</td>
			    		</tr>
			    		<tr>
			    			<td>
			    				Tanggal
			    			</td>
			    			<td>
			    				:'.$data->spd->tanggal.'
			    			</td>
			    		</tr>
			    	</table>

			    	<table border="1px;">
			    		<tr>
			    			<td width="6%">
			    				No
			    			</td>
			    			<td width="42%">
			    				Perincian Biaya
			    			</td>
			    			<td width="32%">
			    				Jumlah
			    			</td>
			    			<td width="22%">
			    				Keterangan
			    			</td>
			    		</tr>
			    	';
			    	$i++; }

			    	$i=1;
			foreach(Kwitansi::model()->findAllbyPk($id) as $data)
			{
			    $html .='
			    		
			    		<tr>
			    			<td width="6%">
			    				1<br><br><br><br><br>
			    				2
			    			</td>
			    			<td width="42%">
			    				Biaya Transport<br>
			    				&nbsp;&nbsp;BBM <br>
			    				&nbsp;&nbsp;TOL<br>
			    				&nbsp;&nbsp;Akomodasi<br>
			    				&nbsp;&nbsp;Tiket<br>
			    				Uang Harian ('.$data->spd->lama.' x @Rp '.$sshbiaya.')<br><br>
			    				<b>Jumlah</b>
			    			</td>
			    			<td width="32%" align="right">
			    				<br><br>
			    				Rp '.$biayabbm.'&nbsp;&nbsp;<br>
			    				Rp '.$biayatol.'&nbsp;&nbsp;<br>
			    				Rp '.$biayaako.'&nbsp;&nbsp;<br>
			    				Rp '.$biayatiket.'&nbsp;&nbsp;<br>
			    				Rp '.$biayassh.'&nbsp;&nbsp;<br><br>
			    				<b>Rp '.$jmltotal.'&nbsp;&nbsp;</b>
			    			</td>
			    			<td width="22%">
			    				'.$data->spd->maksud.'
			    			</td>
			    		</tr>
			    		<tr>
			    			<td colspan="4">
			    				<i>Terbilang: </i>
			    			</td>
			    		</tr>';
			$i++;}
			    $html .='</table>';


			    $html .='<table style="border-bottom:1px;">';
			 foreach(Kwitansi::model()->findAllbyPk($id) as $data)
			{
				$bendahara= Pejabat::model()->findbyPk($data->bendahara);
				$ppk= Pejabat::model()->findbyPk($data->ppk);
				$pegawai= Pegawai::model()->findbyPk($data->spd->id_pegawai);
			    $html .='
			    		<tr>
			    			<td>
			    			</td>
			    			<td>
			    			</td>
			    			<td>
			    				'.$data->spd->tempat_berangkat.','.$data->spd->tanggal.'
			    			</td>
			    		</tr>
			    		<tr>
			    			<td>
			    				<p align="center">Telah Dibayar Sejumlah
			    				<br>Rp '.$jmltotal.'<br>
			    				Bendahara Pengeluaran</p>
			    				<br><br>
			    				<p align="center">'.$bendahara->nama.'<br>NIP.'.$bendahara->nip.'
						 		</p>
			    			</td>
			    			<td>
			    				<br><br><br>
			    				<p align="center">Pejabat Pelaksana Teknis Kegiatan</p>
			    				<br><br>
			    				<p align="center">'.$ppk->nama.'<br>NIP.'.$ppk->nip.'
						 		</p>
			    			</td>
			    			<td>
			    				<p align="center">Telah Dibayar Sejumlah
			    				<br>Rp '.$jmltotal.'<br>
			    				Yang Menerima</p>
			    				<br><br>
			    				<p align="center">'.$pegawai->nama.'<br>NIP.'.$pegawai->nip.'
						 		</p>
			    			</td>
			    		</tr>';
			$i++;}
			    $html .='</table>';

			    $html .='
			    <h4 align="CENTER" ><B>PERHITUNGAN SPDD RAMPUNG</B></h4>
			    <table width="70%">
			    ';
			 foreach(Kwitansi::model()->findAllbyPk($id) as $data)
			{
				$bendahara= Pejabat::model()->findbyPk($data->bendahara);
				$ppk= Pejabat::model()->findbyPk($data->ppk);
				$pegawai= Pegawai::model()->findbyPk($data->spd->id_pegawai);
			    $html .='
			    		<tr>
			    			<td width="40%">
			    				Ditetapkan Sejumlah
			    			</td>
			    			<td width="20%">
			    				:
			    			</td>
			    			<td width="40%">
			    				<b>Rp '.$jmltotal.'</b>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td width="40%">
			    				Yang telah dibayarkan
			    			</td>
			    			<td width="20%">
			    				:
			    			</td>
			    			<td width="40%">
			    				<b>Rp '.$jmltotal.'</b>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td width="40%">
			    				bisa kurang/lebih
			    			</td>
			    			<td width="20%">
			    				:
			    			</td>
			    			<td width="40%">
			    				<b>Rp 0</b>
			    			</td>
			    		</tr>
			    	</table>
			    		';
			
			    $html .='
			    <div align="right">
			    	<p >Pejabat yang berwenang/<br>pejabat lain yang di tunjuk</p>
			    	<br><br>
			    	<p>
			    	'.$pegawai->nama.'<br>NIP.'.$pegawai->nip.'
			    	</p>
			    </div>
			    ';

			    $i++;}
//------------------------------//


		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }

    public function actionExportKwitansiLuarPptk($id)
    {
	        $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('RINCIAN BIAYA PERJALANAN DINAS');
		$pdf->SetTitle('RINCIAN BIAYA PERJALANAN DINAS');
		$pdf->SetSubject('RINCIAN BIAYA PERJALANAN DINAS');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		
		
		$html='
		<p align="right"><i>Lampiran Vii<br>Permenkeu RI. no.113 /PMK 05/2012</i></p>
		<h4 align="center">RINCIAN BIAYA PERJALANAN DINAS</h4>';
			
			$i=1;
			foreach(Kwitansi::model()->findAllbyPk($id) as $data)
			{
				$akomodasi = $data->akomodasi->biaya*$data->spd->lama;
				$biayaako = number_format($akomodasi, 2,",",".");
				$ref = $data->refresentatif->biaya*$data->spd->lama;
				$biayaref = number_format($ref, 2,",",".");
				$ssh = $data->ssh->biaya*$data->spd->lama;
				$sshawal=$data->ssh->biaya;
				$sshbiaya = number_format($sshawal, 2,",",".");
				$biayassh = number_format($ssh, 2,",",".");
				$bbm = $data->bbm;
				$biayabbm = number_format($bbm, 2,",",".");
				$tol = $data->tol;
				$biayatol = number_format($tol, 2,",",".");
				$tiket = $data->tiket;
				$biayatiket = number_format($tiket, 2,",",".");
				$total = $data->total;
				$jmltotal = number_format($total, 2,",",".");
			    $html .= '
			    	<table width="60%">
			    		<tr>
			    			<td>
			    				Lampiran SPPD Nomor
			    			</td>
			    			<td>
			    				:'.$data->spd->nomor.'
			    			</td>
			    		</tr>
			    		<tr>
			    			<td>
			    				Tanggal
			    			</td>
			    			<td>
			    				:'.$data->spd->tanggal.'
			    			</td>
			    		</tr>
			    	</table>

			    	<table border="1px;">
			    		<tr>
			    			<td width="6%">
			    				No
			    			</td>
			    			<td width="42%">
			    				Perincian Biaya
			    			</td>
			    			<td width="32%">
			    				Jumlah
			    			</td>
			    			<td width="22%">
			    				Keterangan
			    			</td>
			    		</tr>
			    	';
			    	$i++; }

			    	$i=1;
			foreach(Kwitansi::model()->findAllbyPk($id) as $data)
			{
			    $html .='
			    		
			    		<tr>
			    			<td width="6%">
			    				1<br><br><br><br><br>
			    				2
			    			</td>
			    			<td width="42%">
			    				Biaya Transport<br>
			    				&nbsp;&nbsp;BBM <br>
			    				&nbsp;&nbsp;TOL<br>
			    				&nbsp;&nbsp;Akomodasi<br>
			    				&nbsp;&nbsp;Tiket<br>
			    				Uang Harian ('.$data->spd->lama.' x @Rp '.$sshbiaya.')<br><br>
			    				<b>Jumlah</b>
			    			</td>
			    			<td width="32%" align="right">
			    				<br><br>
			    				Rp '.$biayabbm.'&nbsp;&nbsp;<br>
			    				Rp '.$biayatol.'&nbsp;&nbsp;<br>
			    				Rp '.$biayaako.'&nbsp;&nbsp;<br>
			    				Rp '.$biayatiket.'&nbsp;&nbsp;<br>
			    				Rp '.$biayassh.'&nbsp;&nbsp;<br><br>
			    				<b>Rp '.$jmltotal.'&nbsp;&nbsp;</b>
			    			</td>
			    			<td width="22%">
			    				'.$data->spd->maksud.'
			    			</td>
			    		</tr>
			    		<tr>
			    			<td colspan="4">
			    				<i>Terbilang: </i>
			    			</td>
			    		</tr>';
			$i++;}
			    $html .='</table>';


			    $html .='<table style="border-bottom:1px;">';
			 foreach(Kwitansi::model()->findAllbyPk($id) as $data)
			{
				$bendahara= Pejabat::model()->findbyPk($data->bendahara);
				$ppk= Pejabat::model()->findbyPk($data->ppk);
				$pegawai= Pegawai::model()->findbyPk($data->spd->id_pegawai);
			    $html .='
			    		<tr>
			    			<td>
			    			</td>
			    			<td>
			    			</td>
			    			<td>
			    				'.$data->spd->tempat_berangkat.','.$data->spd->tanggal.'
			    			</td>
			    		</tr>
			    		<tr>
			    			<td>
			    				<p align="center">Telah Dibayar Sejumlah
			    				<br>Rp '.$jmltotal.'<br>
			    				Bendahara Pengeluaran</p>
			    				<br><br>
			    				<p align="center">'.$bendahara->nama.'<br>NIP.'.$bendahara->nip.'
						 		</p>
			    			</td>
			    			<td>
			    				
			    			</td>
			    			<td>
			    				<p align="center">Telah Dibayar Sejumlah
			    				<br>Rp '.$jmltotal.'<br>
			    				Yang Menerima</p>
			    				<br><br>
			    				<p align="center">'.$pegawai->nama.'<br>NIP.'.$pegawai->nip.'
						 		</p>
			    			</td>
			    		</tr>';
			$i++;}
			    $html .='</table>';

			    $html .='
			    <h4 align="CENTER" ><B>PERHITUNGAN SPDD RAMPUNG</B></h4>
			    <table width="70%">
			    ';
			 foreach(Kwitansi::model()->findAllbyPk($id) as $data)
			{
				$bendahara= Pejabat::model()->findbyPk($data->bendahara);
				$ppk= Pejabat::model()->findbyPk($data->ppk);
				$pegawai= Pegawai::model()->findbyPk($data->spd->id_pegawai);
			    $html .='
			    		<tr>
			    			<td width="40%">
			    				Ditetapkan Sejumlah
			    			</td>
			    			<td width="20%">
			    				:
			    			</td>
			    			<td width="40%">
			    				<b>Rp '.$jmltotal.'</b>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td width="40%">
			    				Yang telah dibayarkan
			    			</td>
			    			<td width="20%">
			    				:
			    			</td>
			    			<td width="40%">
			    				<b>Rp '.$jmltotal.'</b>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td width="40%">
			    				bisa kurang/lebih
			    			</td>
			    			<td width="20%">
			    				:
			    			</td>
			    			<td width="40%">
			    				<b>Rp 0</b>
			    			</td>
			    		</tr>
			    	</table>
			    		';
			
			    $html .='
			    <div align="right">
			    	<p >Pejabat yang berwenang/<br>pejabat lain yang di tunjuk</p>
			    	<br><br>
			    	<p>
			    	'.$pegawai->nama.'<br>NIP.'.$pegawai->nip.'
			    	</p>
			    </div>
			    ';

			    $i++;}
//------------------------------//


		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }

    public function actionExportLhpSetda($id)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'A3', true, 'UTF-8');

	    
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Data Pegawai');
		$pdf->SetSubject('Data Pegawai');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		$txt = <<<EOD
TCPDF Example 003

Custom page header and footer are defined by extending the TCPDF class and overriding the Header() and Footer() methods.
EOD;
	
		foreach(Lhp::model()->findAllbyPk($id) as $data)
			{
		
		$html=' <table style="border-bottom:1px;">
					<tr>
						<td width="15%">
							<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="65px">
						</td>
						<td width="85%">
							<p style="font-size:70px;line-height:101%;">
							&nbsp;<b>PEMERINTAH KABUPATEN SERANG</b><br>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>SEKRETARIAT DAERAH</b><br>
							<i style="font-size:29px;">Jalan Raya Serang-Jakarta Km 4, Kabupaten Serang Telp.(0254)280529</i></p>
						</td>
					</tr>
				</table>
				<h3 align="center">NOTA DINAS</h3>
				<table>
					<tr>
						<td width="20%">
							Kepada
						</td>
						<td width="80%">
							: Yth.'.$data->kepada.'
						</td>
					</tr>
					<tr>
						<td width="20%">
							Dari
						</td>
						<td width="80%">
							: '.$data->dari.'
						</td>
					</tr>
					<tr>
						<td width="20%">
							Tanggal
						</td>
						<td width="80%">
							: '.date('d F Y', strtotime($data->tanggal)).'
						</td>
					</tr>
					<tr>
						<td width="20%">
							Hal
						</td>
						<td width="80%">
							: '.$data->hal.'
						</td>
					</tr>
					<tr>
						<td width="100%">
						<b>____________________________________________________________________________________________</b>
						</td>
					</tr>
				</table>

				<table>
					<tr>
						<td><br>
							<p><b>1. Dasar Hukum</b></p>
						</td>
					</tr>
					<tr>
						<td width="20%">
							&nbsp;&nbsp;&nbsp;
							Surat Perintah
						</td>
						<td width="80%">
							: '.$data->kepada.'
						</td>
					</tr>
					<tr>
						<td width="20%">
							&nbsp;&nbsp;&nbsp;
							Nomor
						</td>
						<td width="80%">
							: '.$data->nomor.'
						</td>
					</tr>
					<tr>
						<td width="20%">
							&nbsp;&nbsp;&nbsp;
							Tanggal
						</td>
						<td width="80%">
							: '.date('d F Y', strtotime($data->tanggal)).'
						</td>
					</tr>
					<tr><td></td></tr>
				</table>
				&nbsp;&nbsp;&nbsp;
				Memerintahkan kepada:<br>
				';
			}
			$html .='<table>';
			$id_spd=$data->id_spd;$i=1;
			foreach(Spd::model()->findAllbyPk($id_spd) as $pegawai)
					{
					$html .='
						<tr>
							<td width="6%" align="center">'.$i.')</td>
							<td width="30%" >'.$pegawai->pegawai->nama.'</td>
							<td width="70%" >: '.$pegawai->pegawai->jabatan.'</td>
						</tr>';
						$i++;
					}
			$id_spd=$data->id_spd;$i=2;
			foreach(Pengikut::model()->findAllByAttributes(array('id_spd'=>$id_spd)) as $pengikut)
					{
					$html .='
						<tr>
							<td width="6%" align="center">'.$i.')</td>
							<td width="30%" >'.$pengikut->pegawai->nama.'</td>
							<td width="70%" >: '.$pengikut->pegawai->jabatan.'</td>
						</tr>';
						$i++;
					}	
					$html .='
					</table>
					<p><b>2.Maksud dan Tujuan</b><br>&nbsp;&nbsp;<i>'.$data->spd->maksud.'</i></p>
					<p><b>3.Kesimpulan / Rangkuman isi hasil perjalanan</b><br>&nbsp;&nbsp;<i>'.$data->kesimpulan.'</i></p>
					<p>Demikian disampaikan atas perhatiannya diucapkan terimakasih</p>

					';
			$pegawai=Pegawai::model()->findbyPk($data->spd->id_pegawai);
				$html .='<div>&nbsp;</div>
				<table>
					<tr>
						<td width="50%">
						</td>
						<td width="50%" align="center">
							<p>'.$pegawai->jabatan.'</p>
							<p>&nbsp;</p>
							<p style="line-height:70%;"><span style="text-decoration:underline;"><b>'.$pegawai->nama.'</b></span></p>
							<p style="line-height:10%;">'.$pegawai->nip.'</p>
						</td>
					</tr>
				</table>
				<div>&nbsp;</div><div>&nbsp;</div>';
			$html .='	<table>
				<tr>
					<td><b>Unsur Pengikut:</b></td>
				</tr>
				';
				$id_spd=$data->id_spd;$i=1;
			foreach(Pengikut::model()->findAllByAttributes(array('id_spd'=>$id_spd)) as $pengikut)
					{
					$html .='
						<tr>
							<td width="30%" >'.$pengikut->pegawai->nama.'</td>
							<td width="70%" >____________________________</td>
						</tr>';
						$i++;
					}
				$html .='</table>';

		$pdf->writeHTML($html, true, true, true, true, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }


    public function actionExportLhpSetdaNonPengikut($id)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'A3', true, 'UTF-8');

	    
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Data Pegawai');
		$pdf->SetSubject('Data Pegawai');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		$txt = <<<EOD
TCPDF Example 003

Custom page header and footer are defined by extending the TCPDF class and overriding the Header() and Footer() methods.
EOD;
	
		foreach(Lhp::model()->findAllbyPk($id) as $data)
			{
		
		$html=' <table style="border-bottom:1px;">
					<tr>
						<td width="15%">
							<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="65px">
						</td>
						<td width="85%">
							<p style="font-size:70px;line-height:101%;">
							&nbsp;<b>PEMERINTAH KABUPATEN SERANG</b><br>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>SEKRETARIAT DAERAH</b><br>
							<i style="font-size:29px;">Jalan Raya Serang-Jakarta Km 4, Kabupaten Serang Telp.(0254)280529</i></p>
						</td>
					</tr>
				</table>
				<h3 align="center">NOTA DINAS</h3>
				<table>
					<tr>
						<td width="20%">
							Kepada
						</td>
						<td width="80%">
							: Yth.'.$data->kepada.'
						</td>
					</tr>
					<tr>
						<td width="20%">
							Dari
						</td>
						<td width="80%">
							: '.$data->dari.'
						</td>
					</tr>
					<tr>
						<td width="20%">
							Tanggal
						</td>
						<td width="80%">
							: '.date('d F Y', strtotime($data->tanggal)).'
						</td>
					</tr>
					<tr>
						<td width="20%">
							Hal
						</td>
						<td width="80%">
							: '.$data->hal.'
						</td>
					</tr>
					<tr>
						<td width="100%">
						<b>____________________________________________________________________________________________</b>
						</td>
					</tr>
				</table>

				<table>
					<tr>
						<td><br>
							<p><b>1. Dasar Hukum</b></p>
						</td>
					</tr>
					<tr>
						<td width="20%">
							&nbsp;&nbsp;&nbsp;
							Surat Perintah
						</td>
						<td width="80%">
							: '.$data->kepada.'
						</td>
					</tr>
					<tr>
						<td width="20%">
							&nbsp;&nbsp;&nbsp;
							Nomor
						</td>
						<td width="80%">
							: '.$data->nomor.'
						</td>
					</tr>
					<tr>
						<td width="20%">
							&nbsp;&nbsp;&nbsp;
							Tanggal
						</td>
						<td width="80%">
							: '.date('d F Y', strtotime($data->tanggal)).'
						</td>
					</tr>
					<tr><td></td></tr>
				</table>
				&nbsp;&nbsp;&nbsp;
				Memerintahkan kepada:<br>
				';
			}
			$html .='<table>';
			$id_spd=$data->id_spd;$i=1;
			foreach(Spd::model()->findAllbyPk($id_spd) as $pegawai)
					{
					$html .='
						<tr>
							<td width="6%" align="center">'.$i.')</td>
							<td width="30%" >'.$pegawai->pegawai->nama.'</td>
							<td width="70%" >: '.$pegawai->pegawai->jabatan.'</td>
						</tr>';
						$i++;
					}
					$html .='
					</table>
					<p><b>2.Maksud dan Tujuan</b><br>&nbsp;&nbsp;<i>'.$data->spd->maksud.'</i></p>
					<p><b>3.Kesimpulan / Rangkuman isi hasil perjalanan</b><br>&nbsp;&nbsp;<i>'.$data->kesimpulan.'</i></p>
					<p>Demikian disampaikan atas perhatiannya diucapkan terimakasih</p>

					';
			$pegawai=Pegawai::model()->findbyPk($data->spd->id_pegawai);
				$html .='<div>&nbsp;</div>
				<table>
					<tr>
						<td width="50%">
						</td>
						<td width="50%" align="center">
							<p>'.$pegawai->jabatan.'</p>
							<p>&nbsp;</p>
							<p style="line-height:70%;"><span style="text-decoration:underline;"><b>'.$pegawai->nama.'</b></span></p>
							<p style="line-height:10%;">'.$pegawai->nip.'</p>
						</td>
					</tr>
				</table>
				<div>&nbsp;</div><div>&nbsp;</div>';

		$pdf->writeHTML($html, true, true, true, true, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }



    public function actionExportLhpDpkd($id)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'A3', true, 'UTF-8');

	    
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Data Pegawai');
		$pdf->SetSubject('Data Pegawai');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		$txt = <<<EOD
TCPDF Example 003

Custom page header and footer are defined by extending the TCPDF class and overriding the Header() and Footer() methods.
EOD;
	
		foreach(Lhp::model()->findAllbyPk($id) as $data)
			{
		
		$html=' <table style="border-bottom:1px;">
					<tr>
						<td width="15%">
							<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="65px">
						</td>
						<td width="85%" >
							<p style="font-size:70px;line-height:93%;" >
							&nbsp;&nbsp;<b>PEMERINTAH KABUPATEN SERANG</b><br>
							<b style="font-size:50px;">DINAS PENGELOLAAN KEUANGAN DAERAH</b><br>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							( D P K D )<br>
							<i style="font-size:29px;">Jalan Raya Serang-Jakarta Km 4, Kabupaten Serang Telp.(0254)280529</i></p>
						</td>
					</tr>
				</table>
				<h3 align="center">NOTA DINAS</h3>
				<table>
					<tr>
						<td width="20%">
							Kepada
						</td>
						<td width="80%">
							: Yth.'.$data->kepada.'
						</td>
					</tr>
					<tr>
						<td width="20%">
							Dari
						</td>
						<td width="80%">
							: '.$data->dari.'
						</td>
					</tr>
					<tr>
						<td width="20%">
							Tanggal
						</td>
						<td width="80%">
							: '.date('d F Y', strtotime($data->tanggal)).'
						</td>
					</tr>
					<tr>
						<td width="20%">
							Hal
						</td>
						<td width="80%">
							: '.$data->hal.'
						</td>
					</tr>
					<tr>
						<td width="100%">
						<b>____________________________________________________________________________________________</b>
						</td>
					</tr>
				</table>

				<table>
					<tr>
						<td><br>
							<p><b>1. Dasar Hukum</b></p>
						</td>
					</tr>
					<tr>
						<td width="20%">
							&nbsp;&nbsp;&nbsp;
							Surat Perintah
						</td>
						<td width="80%">
							: '.$data->kepada.'
						</td>
					</tr>
					<tr>
						<td width="20%">
							&nbsp;&nbsp;&nbsp;
							Nomor
						</td>
						<td width="80%">
							: '.$data->nomor.'
						</td>
					</tr>
					<tr>
						<td width="20%">
							&nbsp;&nbsp;&nbsp;
							Tanggal
						</td>
						<td width="80%">
							: '.date('d F Y', strtotime($data->tanggal)).'
						</td>
					</tr>
					<tr><td></td></tr>
				</table>
				&nbsp;&nbsp;&nbsp;
				Memerintahkan kepada:<br>
				';
			}
			$html .='<table>';
			$id_spd=$data->id_spd;$i=1;
			foreach(Spd::model()->findAllbyPk($id_spd) as $pegawai)
					{
					$html .='
						<tr>
							<td width="6%" align="center">'.$i.')</td>
							<td width="30%" >'.$pegawai->pegawai->nama.'</td>
							<td width="70%" >: '.$pegawai->pegawai->jabatan.'</td>
						</tr>';
						$i++;
					}
			$id_spd=$data->id_spd;$i=2;
			foreach(Pengikut::model()->findAllByAttributes(array('id_spd'=>$id_spd)) as $pengikut)
					{
					$html .='
						<tr>
							<td width="6%" align="center">'.$i.')</td>
							<td width="30%" >'.$pengikut->pegawai->nama.'</td>
							<td width="70%" >: '.$pengikut->pegawai->jabatan.'</td>
						</tr>';
						$i++;
					}	
					$html .='
					</table>
					<p><b>2.Maksud dan Tujuan</b><br>&nbsp;&nbsp;<i>'.$data->spd->maksud.'</i></p>
					<p><b>3.Kesimpulan / Rangkuman isi hasil perjalanan</b><br>&nbsp;&nbsp;<i>'.$data->kesimpulan.'</i></p>
					<p>Demikian disampaikan atas perhatiannya diucapkan terimakasih</p>

					';
			$pegawai=Pegawai::model()->findbyPk($data->spd->id_pegawai);
				$html .='<div>&nbsp;</div>
				<table>
					<tr>
						<td width="50%">
						</td>
						<td width="50%" align="center">
							<p>'.$pegawai->jabatan.'</p>
							<p>&nbsp;</p>
							<p style="line-height:70%;"><span style="text-decoration:underline;"><b>'.$pegawai->nama.'</b></span></p>
							<p style="line-height:10%;">'.$pegawai->nip.'</p>
						</td>
					</tr>
				</table>
				<div>&nbsp;</div><div>&nbsp;</div>';
			$html .='	<table>
				<tr>
					<td><b>Unsur Pengikut:</b></td>
				</tr>
				';
				$id_spd=$data->id_spd;$i=1;
			foreach(Pengikut::model()->findAllByAttributes(array('id_spd'=>$id_spd)) as $pengikut)
					{
					$html .='
						<tr>
							<td width="30%" >'.$pengikut->pegawai->nama.'</td>
							<td width="70%" >____________________________</td>
						</tr>';
						$i++;
					}
				$html .='</table>';

		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }


    public function actionExportLhpDpkdNonPengikut($id)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'A3', true, 'UTF-8');

	    
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Data Pegawai');
		$pdf->SetSubject('Data Pegawai');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		$txt = <<<EOD
TCPDF Example 003

Custom page header and footer are defined by extending the TCPDF class and overriding the Header() and Footer() methods.
EOD;
	
		foreach(Lhp::model()->findAllbyPk($id) as $data)
			{
		
		$html=' <table style="border-bottom:1px;">
					<tr>
						<td width="15%">
							<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="65px">
						</td>
						<td width="85%" >
							<p style="font-size:70px;line-height:93%;" >
							&nbsp;&nbsp;<b>PEMERINTAH KABUPATEN SERANG</b><br>
							<b style="font-size:50px;">DINAS PENGELOLAAN KEUANGAN DAERAH</b><br>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							( D P K D )<br>
							<i style="font-size:29px;">Jalan Raya Serang-Jakarta Km 4, Kabupaten Serang Telp.(0254)280529</i></p>
						</td>
					</tr>
				</table>
				<h3 align="center">NOTA DINAS</h3>
				<table>
					<tr>
						<td width="20%">
							Kepada
						</td>
						<td width="80%">
							: Yth.'.$data->kepada.'
						</td>
					</tr>
					<tr>
						<td width="20%">
							Dari
						</td>
						<td width="80%">
							: '.$data->dari.'
						</td>
					</tr>
					<tr>
						<td width="20%">
							Tanggal
						</td>
						<td width="80%">
							: '.date('d F Y', strtotime($data->tanggal)).'
						</td>
					</tr>
					<tr>
						<td width="20%">
							Hal
						</td>
						<td width="80%">
							: '.$data->hal.'
						</td>
					</tr>
					<tr>
						<td width="100%">
						<b>____________________________________________________________________________________________</b>
						</td>
					</tr>
				</table>

				<table>
					<tr>
						<td><br>
							<p><b>1. Dasar Hukum</b></p>
						</td>
					</tr>
					<tr>
						<td width="20%">
							&nbsp;&nbsp;&nbsp;
							Surat Perintah
						</td>
						<td width="80%">
							: '.$data->kepada.'
						</td>
					</tr>
					<tr>
						<td width="20%">
							&nbsp;&nbsp;&nbsp;
							Nomor
						</td>
						<td width="80%">
							: '.$data->nomor.'
						</td>
					</tr>
					<tr>
						<td width="20%">
							&nbsp;&nbsp;&nbsp;
							Tanggal
						</td>
						<td width="80%">
							: '.date('d F Y', strtotime($data->tanggal)).'
						</td>
					</tr>
					<tr><td></td></tr>
				</table>
				&nbsp;&nbsp;&nbsp;
				Memerintahkan kepada:<br>
				';
			}
			$html .='<table>';
			$id_spd=$data->id_spd;$i=1;
			foreach(Spd::model()->findAllbyPk($id_spd) as $pegawai)
					{
					$html .='
						<tr>
							<td width="6%" align="center">'.$i.')</td>
							<td width="30%" >'.$pegawai->pegawai->nama.'</td>
							<td width="70%" >: '.$pegawai->pegawai->jabatan.'</td>
						</tr>';
						$i++;
					}
					$html .='
					</table>
					<p><b>2.Maksud dan Tujuan</b><br>&nbsp;&nbsp;<i>'.$data->spd->maksud.'</i></p>
					<p><b>3.Kesimpulan / Rangkuman isi hasil perjalanan</b><br>&nbsp;&nbsp;<i>'.$data->kesimpulan.'</i></p>
					<p>Demikian disampaikan atas perhatiannya diucapkan terimakasih</p>

					';
			$pegawai=Pegawai::model()->findbyPk($data->spd->id_pegawai);
				$html .='<div>&nbsp;</div>
				<table>
					<tr>
						<td width="50%">
						</td>
						<td width="50%" align="center">
							<p>'.$pegawai->jabatan.'</p>
							<p>&nbsp;</p>
							<p style="line-height:70%;"><span style="text-decoration:underline;"><b>'.$pegawai->nama.'</b></span></p>
							<p style="line-height:10%;">'.$pegawai->nip.'</p>
						</td>
					</tr>
				</table>
				<div>&nbsp;</div><div>&nbsp;</div>';

		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }
////////////////Excel/////////////////


    public function actionExportDataPegawaiExcel(){

	     // get a reference to the path of PHPExcel classes 
		$phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes');
	 
		// Turn off our amazing library autoload 
		spl_autoload_unregister(array('YiiBase','autoload'));        
	 
		//
		// making use of our reference, include the main class
		// when we do this, phpExcel has its own autoload registration
		// procedure (PHPExcel_Autoloader::Register();)
		include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
	    
		spl_autoload_register(array('YiiBase','autoload'));        
		
	    $objPHPExcel = new PHPExcel();
	    
	    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);

	    	$objPHPExcel->setActiveSheetIndex(0)
		    ->setCellValue('A1', 'No')
		    ->setCellValue('B1', 'NIP')
		    ->setCellValue('C1', 'Nama')
		    ->setCellValue('D1', 'Golongan')
		    ->setCellValue('E1', 'Jabatan')
		    ->setCellValue('F1', 'Tgl Lahir');
	    $i=2;$no=1;
			foreach(Pegawai::model()->findAll() as $data){
		    
		    $objPHPExcel->setActiveSheetIndex(0)
		    ->setCellValue('A'.$i.'', $no)
		    ->setCellValue('B'.$i.'', $data->nip)
		    ->setCellValue('C'.$i.'', $data->nama)
		    ->setCellValue('D'.$i.'', $data->golongan->nama)
		    ->setCellValue('E'.$i.'', $data->jabatan)
		    ->setCellValue('F'.$i.'', Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tgl_lahir));
			 $i++;$no++;}


	 
	    $objPHPExcel->getActiveSheet()->setTitle('Data Pegawai');
	 
	    $objPHPExcel->setActiveSheetIndex(0);
	     
	    ob_end_clean();
	    ob_start();
	    
	    header('Content-Type: application/vnd.ms-excel');
	    header('Content-Disposition: attachment;filename="Export_Data_Pegawai.xls"');
	    header('Cache-Control: max-age=0');
	    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	    $objWriter->save('php://output');
  	}

  	public function actionExportDataSptExcel(){

	     // get a reference to the path of PHPExcel classes 
		$phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes');
	 
		// Turn off our amazing library autoload 
		spl_autoload_unregister(array('YiiBase','autoload'));        
	 
		//
		// making use of our reference, include the main class
		// when we do this, phpExcel has its own autoload registration
		// procedure (PHPExcel_Autoloader::Register();)
		include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
	    
		spl_autoload_register(array('YiiBase','autoload'));        
		
	    $objPHPExcel = new PHPExcel();

	    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
	    
	    	$objPHPExcel->setActiveSheetIndex(0)
		    ->setCellValue('A1', 'No')
		    ->setCellValue('B1', 'Nomor SPT')
		    ->setCellValue('C1', 'Tanggal')
		    ->setCellValue('D1', 'Pejabat')
		    ->setCellValue('E1', 'Pegawai')
		    ->setCellValue('F1', 'Pengikut')
		    ->setCellValue('G1', 'Maksud')
		    ->setCellValue('H1', 'Tujuan');
	    $i=2;$no=1;
			foreach(Spd::model()->findAll() as $data){
		    
		    $objPHPExcel->setActiveSheetIndex(0)
		    ->setCellValue('A'.$i.'', $no)
		    ->setCellValue('B'.$i.'', $data->nomor_spt)
		    ->setCellValue('C'.$i.'', Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tanggal))
		    ->setCellValue('D'.$i.'', ''.$data->pejabat->nama.' ('.$data->pejabat->jabatan.')')
		    ->setCellValue('E'.$i.'', ''.$data->pegawai->nama.' ('.$data->pegawai->jabatan.')')
		    ->setCellValue('F'.$i.'', $data->pengikut)
		    ->setCellValue('G'.$i.'', $data->maksud)
		    ->setCellValue('H'.$i.'', $data->tujuan);
			 $i++;$no++;}
	 
	    $objPHPExcel->getActiveSheet()->setTitle('Data SPT');
	 
	    $objPHPExcel->setActiveSheetIndex(0);
	     
	    ob_end_clean();
	    ob_start();
	    
	    header('Content-Type: application/vnd.ms-excel');
	    header('Content-Disposition: attachment;filename="Export_Data_SPT.xls"');
	    header('Cache-Control: max-age=0');
	    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	    $objWriter->save('php://output');
  	}

  	public function actionExportDataSpdExcel(){

	     // get a reference to the path of PHPExcel classes 
		$phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes');
	 
		// Turn off our amazing library autoload 
		spl_autoload_unregister(array('YiiBase','autoload'));        
	 
		//
		// making use of our reference, include the main class
		// when we do this, phpExcel has its own autoload registration
		// procedure (PHPExcel_Autoloader::Register();)
		include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
	    
		spl_autoload_register(array('YiiBase','autoload'));        
		
	    $objPHPExcel = new PHPExcel();

	    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);

	    	$objPHPExcel->setActiveSheetIndex(0)
		    ->setCellValue('A1', 'No')
		    ->setCellValue('B1', 'Nomor SPD')
		    ->setCellValue('C1', 'Tanggal')
		    ->setCellValue('D1', 'Pejabat')
		    ->setCellValue('E1', 'Pegawai')
		    ->setCellValue('F1', 'Pengikut')
		    ->setCellValue('G1', 'Maksud')
		    ->setCellValue('H1', 'Tujuan')
		    ->setCellValue('I1', 'Biaya')
		    ->setCellValue('j1', 'Kendaraan')
		    ->setCellValue('K1', 'lama')
		    ->setCellValue('L1', 'Tempat')
		    ->setCellValue('M1', 'Instansi')
		    ->setCellValue('N1', 'Koring')
		    ->setCellValue('O1', 'Pergi')
		    ->setCellValue('P1', 'Kembali');
	    $i=2;$no=1;
			foreach(Spd::model()->findAll() as $data){
		    
		    $objPHPExcel->setActiveSheetIndex(0)
		    ->setCellValue('A'.$i.'', $no)
		    ->setCellValue('B'.$i.'', $data->nomor)
		    ->setCellValue('C'.$i.'', Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tanggal))
		    ->setCellValue('D'.$i.'', ''.$data->pejabat->nama.' ('.$data->pejabat->jabatan.')')
		    ->setCellValue('E'.$i.'', ''.$data->pegawai->nama.' ('.$data->pegawai->jabatan.')')
		    ->setCellValue('F'.$i.'', $data->pengikut)
		    ->setCellValue('G'.$i.'', $data->maksud)
		    ->setCellValue('H'.$i.'', $data->tujuan)
		    ->setCellValue('I'.$i.'', $data->biaya)
		    ->setCellValue('J'.$i.'', $data->kendaraan)
		    ->setCellValue('K'.$i.'', $data->lama)
		    ->setCellValue('L'.$i.'', $data->tempat_berangkat)
		    ->setCellValue('M'.$i.'', $data->instansi)
		    ->setCellValue('N'.$i.'', $data->koring)
		    ->setCellValue('O'.$i.'', Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tgl_pergi))
		    ->setCellValue('P'.$i.'', Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tgl_kembali))
		    ;
			 $i++;$no++;}
	 
	    $objPHPExcel->getActiveSheet()->setTitle('Simple');
	 
	    $objPHPExcel->setActiveSheetIndex(0);
	     
	    ob_end_clean();
	    ob_start();
	    
	    header('Content-Type: application/vnd.ms-excel');
	    header('Content-Disposition: attachment;filename="Export_Data_SPD.xls"');
	    header('Cache-Control: max-age=0');
	    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	    $objWriter->save('php://output');
  	}

  	public function actionExportDataKwitansiExcel(){

	     // get a reference to the path of PHPExcel classes 
		$phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes');
	 
		// Turn off our amazing library autoload 
		spl_autoload_unregister(array('YiiBase','autoload'));        
	 
		//
		// making use of our reference, include the main class
		// when we do this, phpExcel has its own autoload registration
		// procedure (PHPExcel_Autoloader::Register();)
		include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
	    
		spl_autoload_register(array('YiiBase','autoload'));        
		
	    $objPHPExcel = new PHPExcel();
	    
	    	$objPHPExcel->setActiveSheetIndex(0)
		    ->setCellValue('A1', 'No')
		    ->setCellValue('B1', 'Nomor')
		    ->setCellValue('C1', 'Nomor SPD')
		    ->setCellValue('D1', 'Tanggal')
		    ->setCellValue('E1', 'Koring')
		    ->setCellValue('F1', 'Pegawai')
		    ->setCellValue('G1', 'Golongan')
		    ->setCellValue('H1', 'Tujuan')
		    ->setCellValue('I1', 'Maksud')
		    ->setCellValue('j1', 'Pergi')
		    ->setCellValue('K1', 'Kembali')
		    ->setCellValue('L1', 'Uang Harian')
		    ->setCellValue('M1', 'Akomodasi')
		    ->setCellValue('N1', 'BBM')
		    ->setCellValue('O1', 'TOL')
		    ->setCellValue('P1', 'Tiket')
		    ->setCellValue('Q1', 'Total')
		    ->setCellValue('R1', 'PPTK')
		    ->setCellValue('S1', 'Pejabat')
		    ->setCellValue('T1', 'lama')
		    ->setCellValue('U1', 'Ref');
	    $i=2;$no=1;
			foreach(Kwitansi::model()->findAll() as $data){
		    
		    $objPHPExcel->setActiveSheetIndex(0)
		    ->setCellValue('A'.$i.'', $no)
		    ->setCellValue('B'.$i.'', $data->nomor)
		    ->setCellValue('C'.$i.'', $data->spd->nomor)
		    ->setCellValue('D'.$i.'', $data->spd->tanggal)
		    ->setCellValue('E'.$i.'', $data->spd->koring)
		    ->setCellValue('F'.$i.'', ''.$data->spd->pegawai->nama.' ('.$data->spd->pegawai->jabatan.')')
		    ->setCellValue('G'.$i.'', $data->spd->pegawai->golongan->nama)
		    ->setCellValue('H'.$i.'', $data->spd->tujuan)
		    ->setCellValue('I'.$i.'', $data->spd->maksud)
		    ->setCellValue('J'.$i.'', $data->spd->tgl_pergi)
		    ->setCellValue('K'.$i.'', $data->spd->tgl_kembali)
		    ->setCellValue('L'.$i.'', $data->ssh->biaya)
		    ->setCellValue('M'.$i.'', $data->akomodasi->biaya)
		    ->setCellValue('N'.$i.'', $data->bbm)
		    ->setCellValue('O'.$i.'', $data->tol)
		    ->setCellValue('P'.$i.'', $data->tiket)
		    ->setCellValue('Q'.$i.'', $data->total)
		    ->setCellValue('R'.$i.'', $data->ppk->nama)
		    ->setCellValue('S'.$i.'', $data->spd->pejabat->nama)
		    ->setCellValue('T'.$i.'', $data->spd->lama)
		    ->setCellValue('U'.$i.'', $data->refresentatif->biaya)
		    ;
			 $i++;$no++;}
	 
	    $objPHPExcel->getActiveSheet()->setTitle('Simple');
	 
	    $objPHPExcel->setActiveSheetIndex(0);
	     
	    ob_end_clean();
	    ob_start();
	    
	    header('Content-Type: application/vnd.ms-excel');
	    header('Content-Disposition: attachment;filename="Export_Kwitansi.xls"');
	    header('Cache-Control: max-age=0');
	    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	    $objWriter->save('php://output');
  	}
    
}
?>