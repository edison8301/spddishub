<?php

class SpdController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	
	public $layout='//layouts/constellation/index';

	/**
	* @return array action filters
	*/
	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'accessRole',
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','viewSpt','view','view2'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('inputSpt','updateSpt','InputSpd','cetakSpd','cetakSpt',
					'createSpd','delete','laporan','Ekspor','rekap','filter','setting',
					'update','cetakPdfSptSetdaTanpaPengikut','cetakPdfSptSetda',
					'cetakPdfSptDpkd','cetakPdfSptDpkdTanpaPengikut','CetakPdfSpdSetda',
					'cetakPdfSpdDpkd','cetakPdfSpt','excelSpd','excelSpt','cetakPdfSpd',
					'exportExcelSpt','exportExcelSpd','cetakSpdBelakangLengkap','cetakSpdDepanLengkap',
					'cetakSptLengkap','cetakSptBlanko','cetakSpdDepanBlanko','cetakSpdBelakangBlanko'
					
				),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','admin','adminspd','delete','laporan'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/

	public $numberFormat=array('decimals'=>2, 'decimalSeparator'=>',', 'thousandSeparator'=>'');

	public function actionAdmin()
	{
		$model=new Spd('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Spd']))
			$model->attributes=$_GET['Spd'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function actionViewSpt($id)
	{
		$this->render('viewSpt',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionView2($id)
	{
		$this->render('view2',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionExcelSpd()
	{
		$this->render('excelSpd');
	}
	
	public function actionExportExcelSpd()
	{
		$this->layout = '//layouts/excel';
		$this->render('_excelSpd',array('border'=>1));
	}

	
	public function actionExcelSpt()
	{
		$this->render('excelSpt');
	}
	
	public function actionExportExcelSpt()
	{
		$this->layout = '//layouts/excel';
		$this->render('_excelSpt',array('border'=>1));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	
	public function actionInputSpt()
	{
		$model=new Spd;
		$model2=new Pengikut;
		$spd = new Spd;
		
		$model->scenario = 'inputSpt';
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Spd']))
		{
			$model->attributes=$_POST['Spd'];
			if($model->save())
			{
				$errorPengikut = 0;
				
				//START INSERT PENGIKUT
				if(isset($_POST['Pengikut']))
				{
					foreach($_POST['Pengikut'] as $key => $id_pegawai)
					{
						if($id_pegawai!=null)
						{
							//CEK TANGGAL PERJALANAN PENGIKUT	
							$tanggal = $model->tgl_pergi;
							$error = 0;
		
							while($tanggal <= $model->tgl_kembali)
							{
								$perjalanan = Perjalanan::model()->findByAttributes(array('id_pegawai'=>$id_pegawai,'tanggal'=>$tanggal,'aktif'=>1));
		
								if($perjalanan !== null AND $perjalanan->id_spd != $model->id)
								{
									$error = 1;
									$errorPengikut = 1;
								}
			
								$tanggal = date('Y-m-d',strtotime(date("Y-m-d", strtotime($tanggal)) . " +1 day"));
							}
							
							if($error == 1)
							{
								Yii::app()->user->setFlash('error','Pengikut '.Pegawai::model()->findByPk($id_pegawai)->nama.' sedang dalam perjalanan dinas antara tanggal '.$model->tgl_pergi.' dan '.$model->tgl_kembali);
							}
							
							if($error == 0)
							{
								$pengikut = Pengikut::model()->findByAttributes(array('id_spd'=>$model->id,'id_pegawai'=>$id_pegawai));
							
								if($pengikut===null)
								{
									$pengikut = new Pengikut;
									$pengikut->id_spd = $model->id;
									$pengikut->id_pegawai = $id_pegawai;
								} 
							
								$pengikut->aktif = 1;
								$pengikut->save();
							}
						}
					}
				}		
				//END INSERT PENGIKUT
      
				//START INSERT PERJALANAN
				$tanggal = $model->tgl_pergi;
				
				while($tanggal <= $model->tgl_kembali)
				{
					//INPUT PERJALANAN PEGAWAI
					$perjalanan = Perjalanan::model()->findByAttributes(array('id_spd'=>$model->id,'id_pegawai'=>$model->id_pegawai,'tanggal'=>$tanggal));
					
					if($perjalanan === null)
					{
						$perjalanan = new Perjalanan;
						$perjalanan->id_spd = $model->id;
						$perjalanan->id_pegawai = $model->id_pegawai;
						$perjalanan->tanggal = $tanggal;
					}
							
					$perjalanan->aktif = 1;
					$perjalanan->save();
					
					//INPUT PERJALANAN PENGIKUT
					foreach($model->getDataPengikut() as $pengikut)
					{
						$perjalanan = Perjalanan::model()->findByAttributes(array('id_spd'=>$model->id,'id_pegawai'=>$pengikut->id_pegawai,'tanggal'=>$tanggal));
						
						if($perjalanan === null)
						{
							$perjalanan = new Perjalanan;
							$perjalanan->id_spd = $model->id;
							$perjalanan->id_pegawai = $pengikut->id_pegawai;
							$perjalanan->tanggal = $tanggal;
						}
						
						$perjalanan->aktif = 1;
						$perjalanan->save();
					}
					
					$tanggal = date('Y-m-d',strtotime(date("Y-m-d", strtotime($tanggal)) . " +1 day"));
				}
				
				if($errorPengikut)
					$this->redirect(array('updateSpt','id'=>$model->id));
				
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('inputSpt'));
			}
				
						
		}
		
		
		$spd=new Spd('search');
		$spd->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Spd']))
			$spd->attributes=$_GET['Spd'];
			
		$pegawai = new Pegawai;
		$pejabat = new Pejabat;
		
		$this->render('inputSpt',array(
			'model'=>$model,
			'model2'=>$model2,
			'spd'=>$spd,
			'pegawai'=>$pegawai,
			'pejabat'=>$pejabat
		));
	}
	
	public function actionCreate()
	{
		$model=new Spd;
		$model2=new Pengikut;
		$spd = new Spd;
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Spd']))
		{
			$model->attributes=$_POST['Spd'];
			$model->setLama();
			
			if($model->save())
			{
				$errorPengikut = 0;
				
				//START INSERT PENGIKUT
				if(isset($_POST['Pengikut']))
				{
					foreach($_POST['Pengikut'] as $key => $id_pegawai)
					{
						if($id_pegawai!=null)
						{
							//CEK TANGGAL PERJALANAN PENGIKUT	
							$tanggal = $model->tgl_pergi;
							$error = 0;
		
							while($tanggal <= $model->tgl_kembali)
							{
								$perjalanan = Perjalanan::model()->findByAttributes(array('id_pegawai'=>$id_pegawai,'tanggal'=>$tanggal,'aktif'=>1));
		
								if($perjalanan !== null AND $perjalanan->id_spd != $model->id)
								{
									$error = 1;
									$errorPengikut = 1;
								}
			
								$tanggal = date('Y-m-d',strtotime(date("Y-m-d", strtotime($tanggal)) . " +1 day"));
							}
							
							if($error == 1)
							{
								Yii::app()->user->setFlash('error','Pengikut '.Pegawai::model()->findByPk($id_pegawai)->nama.' sedang dalam perjalanan dinas antara tanggal '.$model->tgl_pergi.' dan '.$model->tgl_kembali);
							}
							
							if($error == 0)
							{
								$pengikut = Pengikut::model()->findByAttributes(array('id_spd'=>$model->id,'id_pegawai'=>$id_pegawai));
							
								if($pengikut===null)
								{
									$pengikut = new Pengikut;
									$pengikut->id_spd = $model->id;
									$pengikut->id_pegawai = $id_pegawai;
								} 
							
								$pengikut->aktif = 1;
								$pengikut->save();
							}
						}
					}
				}		
				//END INSERT PENGIKUT
      
				//START INSERT PERJALANAN
				$tanggal = $model->tgl_pergi;
				
				while($tanggal <= $model->tgl_kembali)
				{
					//INPUT PERJALANAN PEGAWAI
					$perjalanan = Perjalanan::model()->findByAttributes(array('id_spd'=>$model->id,'id_pegawai'=>$model->id_pegawai,'tanggal'=>$tanggal));
					
					if($perjalanan === null)
					{
						$perjalanan = new Perjalanan;
						$perjalanan->id_spd = $model->id;
						$perjalanan->id_pegawai = $model->id_pegawai;
						$perjalanan->tanggal = $tanggal;
					}
							
					$perjalanan->aktif = 1;
					$perjalanan->save();
					
					//INPUT PERJALANAN PENGIKUT
					foreach($model->getDataPengikut() as $pengikut)
					{
						$perjalanan = Perjalanan::model()->findByAttributes(array('id_spd'=>$model->id,'id_pegawai'=>$pengikut->id_pegawai,'tanggal'=>$tanggal));
						
						if($perjalanan === null)
						{
							$perjalanan = new Perjalanan;
							$perjalanan->id_spd = $model->id;
							$perjalanan->id_pegawai = $pengikut->id_pegawai;
							$perjalanan->tanggal = $tanggal;
						}
						
						$perjalanan->aktif = 1;
						$perjalanan->save();
					}
					
					$tanggal = date('Y-m-d',strtotime(date("Y-m-d", strtotime($tanggal)) . " +1 day"));
				}
				
				//Insert To SpdBelakang
				
				$spdBelakang = new SpdBelakang;
				$spdBelakang->id_spd = $model->id;
				$spdBelakang->urutan = 1;
				
				$spdBelakang->tempat_tiba = $model->tujuan;
				$spdBelakang->tanggal_tiba = $model->tgl_pergi;
	
				$spdBelakang->tempat_berangkat = $model->tujuan;
				$spdBelakang->ke = $model->tempat_berangkat;
				$spdBelakang->tanggal_berangkat = $model->tgl_kembali;
				
				$spdBelakang->save();
				
				
				if($errorPengikut)
					$this->redirect(array('spd/admin'));
				
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('spd/view','id'=>$model->id));
			}				
		}	
		
		$spd=new Spd('search');
		$spd->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Spd']))
			$spd->attributes=$_GET['Spd'];
			
		$pegawai = new Pegawai;
		$pejabat = new Pejabat;
		
		$this->render('create',array(
			'model'=>$model,
			'model2'=>$model2,
			'spd'=>$spd,
			'pegawai'=>$pegawai,
			'pejabat'=>$pejabat
		));
	}

	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		
		$model->scenario = 'updateSpd';

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Spd']))
		{
			$model->attributes=$_POST['Spd'];
			$model->setLama();
			
			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
			}
			
			$this->redirect(array('spd/view','id'=>$id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Spd');
		$this->render('index',array(
		'dataProvider'=>$dataProvider,
		));
	}

		/**
		* Manages all models.
		*/
		public function actionCetakSpt()
		{
			$spd=new Spd('search');
			$spd->unsetAttributes();  // clear any default values
			
			if(isset($_GET['Spd']))
				$spd->attributes=$_GET['Spd'];

			$this->render('cetakSpt',array(
				'spd'=>$spd,
			));
		}

	public function actionInputSpd()
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 'nomor = ""';
			
		$model=new Spd('search',array(
			'criteria'=>$criteria
		));
			
		$model->unsetAttributes();  // clear any default values
			
		if(isset($_GET['Spd']))
			$model->attributes=$_GET['Spd'];

		$this->render('inputSpd',array(
			'model'=>$model,
		));
	}

	public function actionCetakSpd()
	{
		$model=new Spd('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['Spd']))
			$model->attributes=$_GET['Spd'];

		$this->render('cetakSpd',array(
			'model'=>$model,
		));
	}

		public function actionLaporan()
		{
			$model=new Spd('search');
			$model->unsetAttributes();  // clear any default values

			if(isset($_GET['Spd']))
			$model->attributes=$_GET['Spd'];

			$this->render('laporan',array(
			'model'=>$model,
			));
		}
		
	public function actionCetakSpdBelakangLengkap($id,$id_pengikut)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'A4', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Surat Perjalanan Dinas');
		$pdf->SetSubject('Surat Perjalanan Dinas');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 12);
		
		$model = $this->loadModel($id);
		$pegawai = Pegawai::model()->findByPk($id_pengikut);
		
		$html = '';
		
			
		$html .= '
				<table>
			 	<tr>
			 		<td width="50%" >&nbsp;</td>
				 	<td width="20%">Berangkat dari</td>
					<td width="30%">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>(Tempat Kedudukan)</td>
					<td>: '.$model->tempat_berangkat.'</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>Pada tanggal</td>
					<td>: '.Bantu::tanggal($model->tgl_pergi).'</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>Ke</td>
					<td>: '.$model->tujuan.'</td>
				</tr>';
		
		if($model->atas_nama!='')
			$html .= '<tr>
						<td>&nbsp;</td>
						<td colspan="2" style="text-align:center">an. '.$model->getRelationField("atas_nama","nama").'</td>
					</tr>';
		$html .='
				<tr>
					<td>&nbsp;</td>
					<td>Kepala</td>
					<td>: '.$model->getRelationField("pejabat","jabatan").'</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td colspan="2" style="text-align:center">('.$model->getRelationField("pejabat","nama").')</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td colspan="2" style="text-align:center">&nbsp;'.Bantu::formatNip($model->getRelationField("pejabat","nip")).'</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				</table>';
		
		for($i=1;$i<=3;$i++) {
		
		$spdBelakang = SpdBelakang::model()->findByAttributes(array('id_spd'=>$model->id,'urutan'=>$i));
		
		$tempat_tiba = '....................................................';
		if(!empty($spdBelakang->tempat_tiba))
			$tempat_tiba = $spdBelakang->tempat_tiba;
		
		$tanggal_tiba = '.....................................................';
		if(!empty($spdBelakang->tanggal_tiba))
			$tanggal_tiba = Bantu::tanggal($spdBelakang->tanggal_tiba);
			
		$tempat_berangkat = '.....................................................';
		if(!empty($spdBelakang->tempat_berangkat))
			$tempat_berangkat = $spdBelakang->tempat_berangkat;	
			
		$ke = '.....................................................';
		if(!empty($spdBelakang->ke))
			$ke = $spdBelakang->ke;
			
		$tanggal_berangkat = '.....................................................';
		if(!empty($spdBelakang->tanggal_berangkat))
			$tanggal_berangkat = Bantu::tanggal($spdBelakang->tanggal_berangkat);
		
		$jabatan_kepala_tiba = '&nbsp;';
		if(!empty($spdBelakang->jabatan_kepala_tiba))
			$jabatan_kepala_tiba = $spdBelakang->jabatan_kepala_tiba;
			
		$nama_kepala_tiba = '..................................................';
		if(!empty($spdBelakang->nama_kepala_tiba))
			$nama_kepala_tiba = $spdBelakang->nama_kepala_tiba;	
			
		$nip_kepala_tiba = '..................................................';
		if(!empty($spdBelakang->nip_kepala_tiba))
			$nip_kepala_tiba = 'Nip. '.$spdBelakang->nip_kepala_tiba;	
			
		$jabatan_kepala_berangkat = '&nbsp;';
		if(!empty($spdBelakang->jabatan_kepala_berangkat))
			$jabatan_kepala_berangkat = $spdBelakang->jabatan_kepala_berangkat;
			
		$nama_kepala_berangkat = '................................................';
		if(!empty($spdBelakang->nama_kepala_berangkat))
			$nama_kepala_berangkat = $spdBelakang->nama_kepala_berangkat;	

		$nip_kepala_berangkat = '................................................';
		if(!empty($spdBelakang->nip_kepala_berangkat))
			$nip_kepala_berangkat = 'Nip. '.$spdBelakang->nip_kepala_berangkat;	
		
		$html .= '
			<table>
			<tr>
				<td colspan="6" style="font-size:10;border-top:1px solid #000000">&nbsp;</td>
			</tr>
			<tr>
				<td width="4%">'.$i.'.</td>
				<td width="13%">Tiba di</td>
				<td width="30%">: '.$tempat_tiba.'</td>
				<td width="5%">&nbsp;</td>
				<td width="13%">Berangkat dari</td>
				<td width="30%">: '.$tempat_berangkat.'</td>
				<td width="10%">&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>Pada tanggal</td>
				<td>: '.$tanggal_tiba.'</td>
				<td>&nbsp;</td>
				<td>Ke</td>
				<td>: '.$ke.'</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td style="text-align:center">'.$jabatan_kepala_tiba.'</td>
				<td>&nbsp;</td>
				<td>Pada Tanggal</td>
				<td>: '.$tanggal_berangkat.'</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td style="text-align:center">&nbsp;</td>
				<td>&nbsp;</td>
				<td></td>
				<td style="text-align:center">'.$jabatan_kepala_berangkat.'</td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>Kepala</td>
				<td style="text-align:center">('.$nama_kepala_tiba.')</td>
				<td>&nbsp;</td>
				<td>Kepala</td>
				<td style="text-align:center;">('.$nama_kepala_berangkat.')</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td style="text-align:center">'.$nip_kepala_tiba.'</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td style="text-align:center;">'.$nip_kepala_berangkat.'</td>
			</tr>
			<tr>
				<td colspan="6" style="font-size:10">&nbsp;</td>
			</tr>
			</table>';
		}
			
		
		$html .='
			<table>
			<tr>
				<td colspan="6" style="font-size:10;border-top:1px solid #000000">&nbsp;</td>
			</tr>
			<tr>
				<td width="4%">4. </td>
				<td width="20%">Tiba kembali di</td>
				<td width="23%">&nbsp;</td>
				<td width="5%">&nbsp;</td>
				<td width="48%" rowspan="2" colspan="2">Telah diperiksa dengan keterangan 
					tersebut di atas benar dilaksanakan atas perintahnya semata-mata untuk
					kepentingan dalam waktu yang sesingkat-singkatnya.</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>(tempat kedudukan)</td>
				<td>: ....................................</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td colspan="2" style="text-align:center"> (..................................................)</td>
				<td>&nbsp;</td>
				<td colspan="2" style="text-align:center"> (....................................................)</td>
			</tr>
			<tr>
				<td colspan="6" style="font-size:10;border-bottom:1px solid #000000">&nbsp;</td>
			</tr>
			</table>';
			
		$html .= '<div><span style="text-decoration:underline;font-weight:bold;text-align:justify">PERHATIAN:</span><br>
					Pejabat yang berwenang menerbitkan SPPD pegawai yang melakukan perjalanan dinas para Pejabat yang mengesahkan tanggal
					berangkat / tiba serta bendaharawan bertanggung jawab berdasarkan peraturan-peraturan Keuangan Negara, apabila 
					negara menderita rugi akibat kesalahan, kelalaian dan kealpaannya. (angka 8 Lampiran Surat Menteri
					Keuangan tanggal 30 April 1974 No. B.296/MK/1/4/1974)</div>';
		
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }
	
	public function actionCetakSpdBelakangBlanko($id,$id_pengikut)
	{
		 $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'A4', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Surat Perjalanan Dinas');
		$pdf->SetSubject('Surat Perjalanan Dinas');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetRightMargin(0);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 12);
		
		$pengikut = Pengikut::model()->findByPk($id_pengikut);
		$spd = Spd::model()->findByPk($id);
		$spdBelakang1 = SpdBelakang::model()->findByAttributes(array('id_spd'=>$spd->id,'urutan'=>1));
		$spdBelakang2 = SpdBelakang::model()->findByAttributes(array('id_spd'=>$spd->id,'urutan'=>2));
		$spdBelakang3 = SpdBelakang::model()->findByAttributes(array('id_spd'=>$spd->id,'urutan'=>3));
			
		$berangkat_dari = PosisiCetak::model()->getKolomByJenisSurat(4,'berangkat_dari');
		$pada_tanggal = PosisiCetak::model()->getKolomByJenisSurat(4,'pada_tanggal');
		$ke = PosisiCetak::model()->getKolomByJenisSurat(4,'ke');
		$kepala = PosisiCetak::model()->getKolomByJenisSurat(4,'kepala');
		$nama_kepala = PosisiCetak::model()->getKolomByJenisSurat(4,'nama_kepala');
		$atas_nama_jabatan = PosisiCetak::model()->getKolomByJenisSurat(4,'atas_nama_jabatan');
		$an = PosisiCetak::model()->getKolomByJenisSurat(4,'an');
		
		
		$nip_kepala = PosisiCetak::model()->getKolomByJenisSurat(4,'nip_kepala');
		$I_tiba = PosisiCetak::model()->getKolomByJenisSurat(4,'I_tiba');
		$I_tiba_pada_tanggal = PosisiCetak::model()->getKolomByJenisSurat(4,'I_tiba_pada_tgl');
		$I_tiba_jabatan_kepala = PosisiCetak::model()->getKolomByJenisSurat(4,'I_tiba_jabatan_kepala');
		$I_tiba_nama_kepala = PosisiCetak::model()->getKolomByJenisSurat(4,'I_tiba_nama_kepala');
		$I_tiba_nip_kepala = PosisiCetak::model()->getKolomByJenisSurat(4,'I_tiba_nip_kepala');
		$I_berangkat_dari = PosisiCetak::model()->getKolomByJenisSurat(4,'I_berangkat_dari');
		$I_berangkat_ke = PosisiCetak::model()->getKolomByJenisSurat(4,'I_berangkat_ke');
		$I_berangkat_pada_tanggal = PosisiCetak::model()->getKolomByJenisSurat(4,'I_berangkat_pada_tanggal');
		$I_berangkat_jabatan_kepala = PosisiCetak::model()->getKolomByJenisSurat(4,'I_berangkat_jabatan_kepala');
		$I_berangkat_nama_kepala = PosisiCetak::model()->getKolomByJenisSurat(4,'I_berangkat_nama_kepala');
		$I_berangkat_nip_kepala = PosisiCetak::model()->getKolomByJenisSurat(4,'I_berangkat_nip_kepala');
			
		$II_tiba = PosisiCetak::model()->getKolomByJenisSurat(4,'II_tiba');
		$II_tiba_pada_tanggal = PosisiCetak::model()->getKolomByJenisSurat(4,'II_tiba_pada_tgl');
		$II_tiba_jabatan_kepala = PosisiCetak::model()->getKolomByJenisSurat(4,'II_tiba_jabatan_kepala');
		$II_tiba_nama_kepala = PosisiCetak::model()->getKolomByJenisSurat(4,'II_tiba_nama_kepala');
		$II_tiba_nip_kepala = PosisiCetak::model()->getKolomByJenisSurat(4,'II_tiba_nip_kepala');
		$II_berangkat_dari = PosisiCetak::model()->getKolomByJenisSurat(4,'II_berangkat_dari');
		$II_berangkat_ke = PosisiCetak::model()->getKolomByJenisSurat(4,'II_berangkat_ke');
		$II_berangkat_pada_tanggal = PosisiCetak::model()->getKolomByJenisSurat(4,'II_berangkat_pada_tanggal');
		$II_berangkat_jabatan_kepala = PosisiCetak::model()->getKolomByJenisSurat(4,'II_berangkat_jabatan_kepala');
		$II_berangkat_nama_kepala = PosisiCetak::model()->getKolomByJenisSurat(4,'II_berangkat_nama_kepala');
		$II_berangkat_nip_kepala = PosisiCetak::model()->getKolomByJenisSurat(4,'II_berangkat_nip_kepala');
			
		$III_tiba = PosisiCetak::model()->getKolomByJenisSurat(4,'III_tiba');
		$III_tiba_pada_tanggal = PosisiCetak::model()->getKolomByJenisSurat(4,'III_tiba_pada_tgl');
		$III_tiba_jabatan_kepala = PosisiCetak::model()->getKolomByJenisSurat(4,'III_tiba_jabatan_kepala');
		$III_tiba_nama_kepala = PosisiCetak::model()->getKolomByJenisSurat(4,'III_tiba_nama_kepala');
		$III_tiba_nip_kepala = PosisiCetak::model()->getKolomByJenisSurat(4,'III_tiba_nip_kepala');
		$III_berangkat_dari = PosisiCetak::model()->getKolomByJenisSurat(4,'III_berangkat_dari');
		$III_berangkat_ke = PosisiCetak::model()->getKolomByJenisSurat(4,'III_berangkat_ke');
		$III_berangkat_pada_tanggal = PosisiCetak::model()->getKolomByJenisSurat(4,'III_berangkat_pada_tanggal');
		$III_berangkat_jabatan_kepala = PosisiCetak::model()->getKolomByJenisSurat(4,'III_berangkat_jabatan_kepala');
		$III_berangkat_nama_kepala = PosisiCetak::model()->getKolomByJenisSurat(4,'III_berangkat_nama_kepala');
		$III_berangkat_nip_kepala = PosisiCetak::model()->getKolomByJenisSurat(4,'III_berangkat_nip_kepala');
		

		$pdf->writeHTMLCell(0,0,$berangkat_dari->horisontal,$berangkat_dari->vertikal,'<span style="font-size:'.$berangkat_dari->font_size.'">'.$spd->tempat_berangkat.'</span>');
		$pdf->writeHTMLCell(0,0,$pada_tanggal->horisontal,$pada_tanggal->vertikal,'<span style="font-size:'.$pada_tanggal->font_size.'">'.Bantu::tanggal($spd->tgl_pergi).'</span>');
		$pdf->writeHTMLCell(0,0,$ke->horisontal,$ke->vertikal,'<span style="font-size:'.$ke->font_size.'">'.$spd->tujuan.'</span>');
		$pdf->writeHTMLCell(0,0,$kepala->horisontal,$kepala->vertikal,'<span style="font-size:'.$kepala->font_size.'">'.$spd->getRelationField("pejabat","jabatan").'</span>');
		$pdf->writeHTMLCell(0,0,$nama_kepala->horisontal,$nama_kepala->vertikal,'<span style="font-size:'.$nama_kepala->font_size.'">'.$spd->getRelationField("pejabat","nama").'</span>');
		$pdf->writeHTMLCell(0,0,$nip_kepala->horisontal,$nip_kepala->vertikal,'<span style="font-size:'.$nip_kepala->font_size.'">'.Bantu::formatNip($spd->getRelationField("pejabat","nip")).'</span>');
		$pdf->writeHTMLCell(0,0,$atas_nama_jabatan->horisontal,$atas_nama_jabatan->vertikal,'<span style="font-size:'.$atas_nama_jabatan->font_size.'">'.$spd->getRelationField("atas_nama","jabatan").'</span>');	
		$pdf->writeHTMLCell(0,0,$an->horisontal,$an->vertikal,'<span style="font-size:'.$an->font_size.'">a.n</span>');
			
		if($spdBelakang1 !== null)
		{
			$pdf->writeHTMLCell(0,0,$I_tiba->horisontal,$I_tiba->vertikal,'<span style="font-size:'.$I_tiba->font_size.'">'.$spdBelakang1->tempat_tiba.'</span>');
			$pdf->writeHTMLCell(0,0,$I_tiba_pada_tanggal->horisontal,$I_tiba_pada_tanggal->vertikal,'<span style="font-size:'.$I_tiba_pada_tanggal->font_size.'">'.Bantu::tanggal($spdBelakang1->tanggal_tiba).'</span>');
			$pdf->writeHTMLCell(0,0,$I_tiba_jabatan_kepala->horisontal,$I_tiba_jabatan_kepala->vertikal,'<span style="font-size:'.$I_tiba_jabatan_kepala->font_size.'">'.$spdBelakang1->jabatan_kepala_tiba.'</span>');
			$pdf->writeHTMLCell(0,0,$I_tiba_nama_kepala->horisontal,$I_tiba_nama_kepala->vertikal,'<span style="font-size:'.$I_tiba_nama_kepala->font_size.'">'.$spdBelakang1->nama_kepala_tiba.'</span>');
			$pdf->writeHTMLCell(0,0,$I_tiba_nip_kepala->horisontal,$I_tiba_nip_kepala->vertikal,'<span style="font-size:'.$I_tiba_nip_kepala->font_size.'">'.Bantu::formatNip($spdBelakang1->nip_kepala_tiba).'</span>');
			$pdf->writeHTMLCell(0,0,$I_berangkat_dari->horisontal,$I_berangkat_dari->vertikal,'<span style="font-size:'.$I_berangkat_dari->font_size.'">'.$spdBelakang1->tempat_berangkat.'</span>');
			$pdf->writeHTMLCell(0,0,$I_berangkat_ke->horisontal,$I_berangkat_ke->vertikal,'<span style="font-size:'.$I_berangkat_ke->font_size.'">'.$spdBelakang1->ke.'</span>');
			$pdf->writeHTMLCell(0,0,$I_berangkat_pada_tanggal->horisontal,$I_berangkat_pada_tanggal->vertikal,'<span style="font-size:'.$I_berangkat_pada_tanggal->font_size.'">'.Bantu::tanggal($spdBelakang1->tanggal_berangkat).'</span>');
			$pdf->writeHTMLCell(0,0,$I_berangkat_jabatan_kepala->horisontal,$I_berangkat_jabatan_kepala->vertikal,'<span style="font-size:'.$I_berangkat_jabatan_kepala->font_size.'">'.$spdBelakang1->jabatan_kepala_berangkat.'</span>');
			$pdf->writeHTMLCell(0,0,$I_berangkat_nama_kepala->horisontal,$I_berangkat_nama_kepala->vertikal,'<span style="font-size:'.$I_berangkat_nama_kepala->font_size.'">'.$spdBelakang1->nama_kepala_berangkat.'</span>');
			$pdf->writeHTMLCell(0,0,$I_berangkat_nip_kepala->horisontal,$I_berangkat_nip_kepala->vertikal,'<span style="font-size:'.$I_berangkat_nip_kepala->font_size.'">'.Bantu::formatNip($spdBelakang1->nip_kepala_berangkat).'</span>');	
		}
		
		if($spdBelakang2 !== null) 
		{
				$pdf->writeHTMLCell(0,0,$II_tiba->horisontal,$II_tiba->vertikal,'<span style="font-size:'.$II_tiba->font_size.'">'.$spdBelakang1->tempat_tiba.'</span>');
				$pdf->writeHTMLCell(0,0,$II_tiba_pada_tanggal->horisontal,$II_tiba_pada_tanggal->vertikal,'<span style="font-size:'.$II_tiba_pada_tanggal->font_size.'">'.Bantu::tanggal($spdBelakang2->tanggal_tiba).'</span>');
				$pdf->writeHTMLCell(0,0,$II_tiba_jabatan_kepala->horisontal,$II_tiba_jabatan_kepala->vertikal,'<span style="font-size:'.$II_tiba_jabatan_kepala->font_size.'">'.$spdBelakang2->jabatan_kepala_tiba.'</span>');
				$pdf->writeHTMLCell(0,0,$II_tiba_nama_kepala->horisontal,$II_tiba_nama_kepala->vertikal,'<span style="font-size:'.$II_tiba_nama_kepala->font_size.'">'.$spdBelakang2->nama_kepala_tiba.'</span>');
				$pdf->writeHTMLCell(0,0,$II_tiba_nip_kepala->horisontal,$II_tiba_nip_kepala->vertikal,'<span style="font-size:'.$II_tiba_nip_kepala->font_size.'">'.Bantu::formatNip($spdBelakang2->nip_kepala_tiba).'</span>');
				$pdf->writeHTMLCell(0,0,$II_berangkat_dari->horisontal,$II_berangkat_dari->vertikal,'<span style="font-size:'.$II_berangkat_dari->font_size.'">'.$spdBelakang2->tempat_berangkat.'</span>');
				$pdf->writeHTMLCell(0,0,$II_berangkat_ke->horisontal,$II_berangkat_ke->vertikal,'<span style="font-size:'.$II_berangkat_ke->font_size.'">'.$spdBelakang2->ke.'</span>');
				$pdf->writeHTMLCell(0,0,$II_berangkat_pada_tanggal->horisontal,$II_berangkat_pada_tanggal->vertikal,'<span style="font-size:'.$II_berangkat_pada_tanggal->font_size.'">'.Bantu::tanggal($spdBelakang2->tanggal_berangkat).'</span>');
				$pdf->writeHTMLCell(0,0,$II_berangkat_jabatan_kepala->horisontal,$II_berangkat_jabatan_kepala->vertikal,'<span style="font-size:'.$II_berangkat_jabatan_kepala->font_size.'">'.$spdBelakang2->jabatan_kepala_berangkat.'</span>');
				$pdf->writeHTMLCell(0,0,$II_berangkat_nama_kepala->horisontal,$II_berangkat_nama_kepala->vertikal,'<span style="font-size:'.$II_berangkat_nama_kepala->font_size.'">'.$spdBelakang2->nama_kepala_berangkat.'</span>');
				$pdf->writeHTMLCell(0,0,$II_berangkat_nip_kepala->horisontal,$II_berangkat_nip_kepala->vertikal,'<span style="font-size:'.$II_berangkat_nip_kepala->font_size.'">'.Bantu::formatNip($spdBelakang2->nip_kepala_berangkat).'</span>');	
		}
			
		if($spdBelakang3 !== null)
		{
				$pdf->writeHTMLCell(0,0,$III_tiba->horisontal,$III_tiba->vertikal,'<span style="font-size:'.$III_tiba->font_size.'">'.$spdBelakang3->tempat_tiba.'</span>');
				$pdf->writeHTMLCell(0,0,$III_tiba_pada_tanggal->horisontal,$III_tiba_pada_tanggal->vertikal,'<span style="font-size:'.$III_tiba_pada_tanggal->font_size.'">'.Bantu::tanggal($spdBelakang3->tanggal_tiba).'</span>');
				$pdf->writeHTMLCell(0,0,$III_tiba_jabatan_kepala->horisontal,$III_tiba_jabatan_kepala->vertikal,'<span style="font-size:'.$III_tiba_jabatan_kepala->font_size.'">'.$spdBelakang3->jabatan_kepala_tiba.'</span>');
				$pdf->writeHTMLCell(0,0,$III_tiba_nama_kepala->horisontal,$III_tiba_nama_kepala->vertikal,'<span style="font-size:'.$III_tiba_nama_kepala->font_size.'">'.$spdBelakang3->nama_kepala_tiba.'</span>');
				$pdf->writeHTMLCell(0,0,$III_tiba_nip_kepala->horisontal,$III_tiba_nip_kepala->vertikal,'<span style="font-size:'.$III_tiba_nip_kepala->font_size.'">'.Bantu::formatNip($spdBelakang3->nip_kepala_tiba).'</span>');
				$pdf->writeHTMLCell(0,0,$III_berangkat_dari->horisontal,$III_berangkat_dari->vertikal,'<span style="font-size:'.$III_berangkat_dari->font_size.'">'.$spdBelakang3->tempat_berangkat.'</span>');
				$pdf->writeHTMLCell(0,0,$III_berangkat_ke->horisontal,$III_berangkat_ke->vertikal,'<span style="font-size:'.$III_berangkat_ke->font_size.'">'.$spdBelakang3->ke.'</span>');
				$pdf->writeHTMLCell(0,0,$III_berangkat_pada_tanggal->horisontal,$III_berangkat_pada_tanggal->vertikal,'<span style="font-size:'.$III_berangkat_pada_tanggal->font_size.'">'.Bantu::tanggal($spdBelakang3->tanggal_berangkat).'</span>');
				$pdf->writeHTMLCell(0,0,$III_berangkat_jabatan_kepala->horisontal,$III_berangkat_jabatan_kepala->vertikal,'<span style="font-size:'.$III_berangkat_jabatan_kepala->font_size.'">'.$spdBelakang3->jabatan_kepala_berangkat.'</span>');
				$pdf->writeHTMLCell(0,0,$III_berangkat_nama_kepala->horisontal,$III_berangkat_nama_kepala->vertikal,'<span style="font-size:'.$III_berangkat_nama_kepala->font_size.'">'.$spdBelakang3->nama_kepala_berangkat.'</span>');
				$pdf->writeHTMLCell(0,0,$III_berangkat_nip_kepala->horisontal,$III_berangkat_nip_kepala->vertikal,'<span style="font-size:'.$III_berangkat_nip_kepala->font_size.'">'.Bantu::formatNip($spdBelakang3->nip_kepala_berangkat).'</span>');	
			
		}
		
		//$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
	}

		public function actionRekap()
		{
			
			if(isset($_GET['tgl_awal']) & isset($_GET['tgl_akhir'])) {
				$criteria = new CDbCriteria;
				$criteria->params = array(':tgl_awal'=>$_GET['tgl_awal'], ':tgl_akhir'=>$_GET['tgl_akhir']);
				$criteria->condition = 'tanggal >= :tgl_awal AND tanggal <= :tgl_akhir AND nomor IS NOT NULL';
			
				/*$spd = Spd::model()->findAll($criteria);
				if($spd==null){
					$data= 0;
				}else { $data = Spd::model()->findAll($criteria);}*/
				$model = Spd::model()->findAll($criteria);
			} else {
				$criteria = new CDbCriteria;
				$criteria->condition = 'nomor IS NOT NULL';
				//$spd = Spd::model()->findAll($criteria);
				$model = Spd::model()->findAll($criteria);
			}
			

			$this->render('rekap',array(
				'model'=>$model,
			));
		}

		

		public function actionSetting()
		{
			$model=new Spd('search');
			$model->unsetAttributes();  // clear any default values

			if(isset($_GET['Spd']))
			$model->attributes=$_GET['Spd'];

			$this->render('setting',array(
			'model'=>$model,
			));
		}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer the ID of the model to be loaded
	*/
	public function loadModel($id)
	{
		$model=Spd::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function actionEkspor()
	{
		$data = array(
		    1 => array ('Name', 'Surname'),
		    array('Schwarz', 'Oliver'),
		    array('Test', 'Peter')
		);
		Yii::import('application.extensions.phpexcel.JPhpExcel');
		$xls = new JPhpExcel('UTF-8', false, 'My Test Sheet');
		$xls->addArray($data);
		$xls->generateXML('my-test');

	}
	
	public function actionCetakSpdDepanLengkap($id,$id_pengikut)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perintah Tugas');
		$pdf->SetTitle('Surat Perintah Tugas');
		$pdf->SetSubject('Surat Perintah Tugas');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 12);
		
		$model = $this->loadModel($id);
		$pegawai = Pegawai::model()->findByPk($id_pengikut);
		
		$html = '';
	
		$html .= $this->renderPartial('//layouts/kop',array(),true);
		
		$nama_pejabat = $model->atas_nama != '' ? $model->getRelationField("atas_nama","nama"): $model->getRelationField("pejabat","nama");
		$atas_nama_jabatan = $model->atas_nama != '' ? 'an. '.$model->getRelationField("atas_nama","jabatan"): '';
		$html .= '
				<div style="text-align:center">
					<span style="font-size:16;font-weight:bold;text-decoration:underline">"SURAT PERINTAH PERJALANAN DINAS"</span>
					<br>
					<span style="font-weight:bold">Nomor : '.$model->nomor_spd.'</span>
				</div>
				
				<div>&nbsp;</div>
				
				<table>
					<tr>
						<td colspan="5" style="border-top:1px solid #000000;font-size:5">&nbsp;</td>
					</tr>
					<tr>
						<td width="3%">1.</td>
						<td width="62%" colspan="2">Pejabat berwenang yang memberi Perintah</td>
						<td width="35%" colspan="2">'.$nama_pejabat.'</td>
					</tr>
					<tr>
						<td colspan="5" style="border-bottom:1px solid #000000;font-size:5">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="5" style="font-size:5">&nbsp;</td>
					</tr>
					<tr>
						<td>2.</td>
						<td colspan="2">Nama Pegawai yang diperintahkan</td>
						<td colspan="2">'.$pegawai->nama.'</td>
					</tr>
					<tr>
						<td colspan="5" style="border-bottom:1px solid #000000;font-size:5">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="5" style="font-size:5">&nbsp;</td>
					</tr>
					<tr>
						<td>3.</td>
						<td colspan="2">a. Pangkat dan Golongan</td>
						<td colspan="2">a. '.$pegawai->getRelationField("golongan","nama").'</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td colspan="2">b. Jabatan</td>
						<td colspan="2">b. '.$pegawai->jabatan.'</td>
					</tr>
					<tr>
						<td colspan="5" style="border-bottom:1px solid #000000;font-size:5">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="5" style="font-size:5">&nbsp;</td>
					</tr>
					<tr>
						<td>4.</td>
						<td colspan="2">Maksud Perjalanan</td>
						<td colspan="2">'.$model->maksud.'</td>
					</tr>
					<tr>
						<td colspan="5" style="border-bottom:1px solid #000000;font-size:5">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="5" style="font-size:5">&nbsp;</td>
					</tr>
					<tr>
						<td>5.</td>
						<td colspan="2">a. Tempat berangkat</td>
						<td colspan="2">a. '.$model->tempat_berangkat.'</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td colspan="2">b. Tempat tujuan</td>
						<td colspan="2">b. '.$model->tujuan.'</td>
					</tr>
					<tr>
						<td colspan="5" style="border-bottom:1px solid #000000;font-size:5">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="5" style="font-size:5">&nbsp;</td>
					</tr>
					<tr>
						<td>6.</td>
						<td colspan="2">a. Lamanya perjalanan dinas</td>
						<td colspan="2">a. '.$model->lama.' hari</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td colspan="2">b. Tanggal berangkat</td>
						<td colspan="2">b. '.Bantu::tanggal($model->tgl_pergi).'</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td colspan="2">c. Tanggal harus kembali</td>
						<td colspan="2">c. '.Bantu::tanggal($model->tgl_kembali).'</td>
					</tr>
					<tr>
						<td colspan="5" style="border-bottom:1px solid #000000;font-size:5">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="5" style="font-size:5">&nbsp;</td>
					</tr>
					<tr>
						<td>7.</td>
						<td colspan="2">Alat angkutan yang dipergunakan</td>
						<td colspan="2">'.$model->kendaraan.'</td>
					</tr>
					<tr>
						<td colspan="5" style="border-bottom:1px solid #000000;font-size:5">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="5" style="font-size:5">&nbsp;</td>
					</tr>
					<tr>
						<td width="3%">8.</td>
						<td width="22%">Pengikut</td>
						<td width="40%">Nama</td>
						<td width="15%">Umur</td>
						<td width="20%">Hubungan Keluarga</td>
					</tr>
					<tr>
						<td colspan="5" style="border-bottom:1px solid #000000;font-size:5">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="5" style="font-size:5">&nbsp;</td>
					</tr>
					<tr>
						<td width="3%">&nbsp;</td>
						<td width="62%" colspan="2">1. ....................................................................</td>
						<td width="15%">..................</td>
						<td width="20%">.....................................</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td colspan="2">2. ....................................................................</td>
						<td>..................</td>
						<td>.....................................</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td colspan="2">3. ....................................................................</td>
						<td>..................</td>
						<td>.....................................</td>
					</tr>
					<tr>
						<td colspan="5" style="font-size:5">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="5" style="border-bottom:1px solid #000000;font-size:5">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="5" style="font-size:5">&nbsp;</td>
					</tr>
					<tr>
						<td>9.</td>
						<td colspan="2">Pembebanan Anggaran</td>
						<td colspan="2"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td colspan="2">a. Instansi</td>
						<td colspan="2">a. '.$model->instansi.'</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td colspan="2">b. Mata Anggaran</td>
						<td colspan="2">b. '.$model->koring.'</td>
					</tr>
					<tr>
						<td colspan="5" style="border-bottom:1px solid #000000;font-size:5">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="5" style="font-size:5">&nbsp;</td>
					</tr>
					<tr>
						<td>10.</td>
						<td colspan="2">Keterangan Lain-lain</td>
						<td colspan="2"></td>
					</tr>
					<tr>
						<td colspan="5" style="border-bottom:1px solid #000000;font-size:5">&nbsp;</td>
					</tr>
				</table>';
		
		$html .= '<p style="line-height:10%;">&nbsp;</p>';
		
		$html .= '<table>
					<tr>
						<td width="60%">&nbsp;</td>
						<td width="15%">Dikeluarkan di</td>
						<td width="25%" style="text-align:center">Serang</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>Pada tanggal</td>
						<td style="text-align:center">'.Bantu::tanggal(($model->tanggal_spd)).'</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td colspan="2" style="text-align:center">'.$atas_nama_jabatan.'</td>
						
					</tr>
					
					<tr>
						<td>&nbsp;</td>
						<td colspan="2" style="text-align:center">'.$model->getRelationField("pejabat","jabatan").'</td>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td colspan="2" style="text-align:center"><span style="text-decoration:underline">'.$model->getRelationField("pejabat","nama").'</span></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td colspan="2" style="text-align:center">NIP. '.$model->getRelationField("pejabat","nip").'</td>
					</tr>
				</table>';
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }
	
	public function actionCetakSpdDepanBlanko($id,$id_pengikut)
	{
		 $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'A4', true, 'UTF-8');

		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perintah Tugas');
		$pdf->SetTitle('Surat Perintah Tugas');
		$pdf->SetSubject('Surat Perintah Tugas');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetRightMargin(0);

		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, 0);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 12);
		
		$pengikut = Pegawai::model()->findByPk($id_pengikut);
		$spd = Spd::model()->findByPk($id);
		
		$nomor = PosisiCetak::model()->getKolomByJenisSurat(1,'nomor');
		$nama_pejabat = PosisiCetak::model()->getKolomByJenisSurat(1,'nama_pejabat');
		
		$nama_pegawai = PosisiCetak::model()->getKolomByJenisSurat(1,'nama_pegawai');
		$nip_pegawai = PosisiCetak::model()->getKolomByJenisSurat(1,'nip_pegawai');
		
		$pangkat_golongan = PosisiCetak::model()->getKolomByJenisSurat(1,'pangkat_golongan');
		$jabatan = PosisiCetak::model()->getKolomByJenisSurat(1,'jabatan');
		$maksud_perjalanan = PosisiCetak::model()->getKolomByJenisSurat(1,'maksud_perjalanan');
		$tempat_berangkat = PosisiCetak::model()->getKolomByJenisSurat(1,'tempat_berangkat');
		$tempat_tujuan = PosisiCetak::model()->getKolomByJenisSurat(1,'tempat_tujuan');
		$lamanya_perjalanan = PosisiCetak::model()->getKolomByJenisSurat(1,'lamanya_perjalanan');
		$tanggal_berangkat = PosisiCetak::model()->getKolomByJenisSurat(1,'tanggal_berangkat');
		$tanggal_kembali = PosisiCetak::model()->getKolomByJenisSurat(1,'tanggal_kembali');
		$alat_angkutan = PosisiCetak::model()->getKolomByJenisSurat(1,'alat_angkutan');
		$instansi = PosisiCetak::model()->getKolomByJenisSurat(1,'instansi');
		$umur_pengikut = PosisiCetak::model()->getKolomByJenisSurat(1,'umur_pengikut');
		$mata_anggaran = PosisiCetak::model()->getKolomByJenisSurat(1,'mata_anggaran');
		$tanggal = PosisiCetak::model()->getKolomByJenisSurat(1,'tanggal');
		$tempat_dikeluarkan = PosisiCetak::model()->getKolomByJenisSurat(1,'tempat_dikeluarkan');
		$atas_nama_ttd = PosisiCetak::model()->getKolomByJenisSurat(1,'atas_nama_ttd');
		$ttd_nama_pejabat = PosisiCetak::model()->getKolomByJenisSurat(1,'ttd_nama_pejabat');
		$ttd_nip_pejabat = PosisiCetak::model()->getKolomByJenisSurat(1,'ttd_nip_pejabat');
		
		
		$pdf->writeHTMLCell(0,0,$nomor->horisontal,$nomor->vertikal,'<span style="font-size:'.$nomor->font_size.'">'.$spd->nomor_spd.'</span>');
		
		if($spd->atas_nama!='')
			$pdf->writeHTMLCell(0,0,$nama_pejabat->horisontal,$nama_pejabat->vertikal,'<span style="font-size:'.$nama_pejabat->font_size.'">'.$spd->getRelationField('atas_nama','nama').'</span>');
		else
			$pdf->writeHTMLCell(0,0,$nama_pejabat->horisontal,$nama_pejabat->vertikal,'<span style="font-size:'.$nama_pejabat->font_size.'">'.$spd->getRelationField('pejabat','nama').'</span>');
		
		$pdf->writeHTMLCell(0,0,$nama_pegawai->horisontal,$nama_pegawai->vertikal,'<span style="font-size:'.$nama_pegawai->font_size.'">'.$pengikut->nama.'</span>');
		$pdf->writeHTMLCell(0,0,$nip_pegawai->horisontal,$nip_pegawai->vertikal,'<span style="font-size:'.$nip_pegawai->font_size.'">NIP. '.$pengikut->getNip().'</span>');
		
		$pdf->writeHTMLCell(0,0,$pangkat_golongan->horisontal,$pangkat_golongan->vertikal,'<span style="font-size:'.$pangkat_golongan->font_size.'">'.$pengikut->getRelationField('golongan','nama').'</span>');
		$pdf->writeHTMLCell(0,0,$jabatan->horisontal,$jabatan->vertikal,'<span style="font-size:'.$jabatan->font_size.'">'.$pengikut->jabatan.'</span>');
		$pdf->writeHTMLCell(0,0,$maksud_perjalanan->horisontal,$maksud_perjalanan->vertikal,'<span style="font-size:'.$maksud_perjalanan->font_size.'">'.$spd->maksud.'</span>');
		$pdf->writeHTMLCell(0,0,$tempat_berangkat->horisontal,$tempat_berangkat->vertikal,'<span style="font-size:'.$tempat_berangkat->font_size.'">'.$spd->tempat_berangkat.'</span>');
		$pdf->writeHTMLCell(0,0,$tempat_tujuan->horisontal,$tempat_tujuan->vertikal,'<span style="font-size:'.$tempat_tujuan->font_size.'">'.$spd->tujuan.'</span>');
		$pdf->writeHTMLCell(0,0,$lamanya_perjalanan->horisontal,$lamanya_perjalanan->vertikal,'<span style="font-size:'.$lamanya_perjalanan->font_size.'">'.$spd->lama.' Hari</span>');
		$pdf->writeHTMLCell(0,0,$tanggal_berangkat->horisontal,$tanggal_berangkat->vertikal,'<span style="font-size:'.$tanggal_berangkat->font_size.'">'.Bantu::tanggal($spd->tgl_pergi).'</span>');
		$pdf->writeHTMLCell(0,0,$tanggal_kembali->horisontal,$tanggal_kembali->vertikal,'<span style="font-size:'.$tanggal_kembali->font_size.'">'.Bantu::tanggal($spd->tgl_kembali).'</span>');
		$pdf->writeHTMLCell(0,0,$alat_angkutan->horisontal,$alat_angkutan->vertikal,'<span style="font-size:'.$alat_angkutan->font_size.'">'.$spd->kendaraan.'</span>');
		
		$pdf->writeHTMLCell(0,0,$instansi->horisontal,$instansi->vertikal,'<span style="font-size:'.$instansi->font_size.'">'.$spd->instansi.'</span>');
		$pdf->writeHTMLCell(0,0,$mata_anggaran->horisontal,$mata_anggaran->vertikal,'<span style="font-size:'.$mata_anggaran->font_size.'">'.$spd->koring.'</span>');
		
		$pdf->writeHTMLCell(0,0,$tempat_dikeluarkan->horisontal,$tempat_dikeluarkan->vertikal,'<span style="font-size:'.$tempat_dikeluarkan->font_size.'">Serang</span>');
		$pdf->writeHTMLCell(0,0,$tanggal->horisontal,$tanggal->vertikal,'<span style="font-size:'.$tanggal->font_size.'">'.Bantu::tanggal(date('Y-m-d')).'</span>');
		
		if($spd->atas_nama!=null)
			$pdf->writeHTMLCell(0,0,$atas_nama_ttd->horisontal,$atas_nama_ttd->vertikal,'<span style="font-size:'.$atas_nama_ttd->font_size.'">an. '.$spd->getRelationField("atas_nama","jabatan").'</span>');
		
		$pdf->writeHTMLCell(0,0,$ttd_nama_pejabat->horisontal,$ttd_nama_pejabat->vertikal,'<span style="font-size:'.$ttd_nama_pejabat->font_size.'">'.$spd->getRelationField("pejabat","nama").'</span>');
		$pdf->writeHTMLCell(0,0,$ttd_nip_pejabat->horisontal,$ttd_nip_pejabat->vertikal,'<span style="font-size:'.$ttd_nip_pejabat->font_size.'">'.Bantu::formatNip($spd->getRelationField("pejabat","nip")).'</span>');
		
		
		$pdf->Output('.pdf', 'I');	
	}
	
	public function actionCetakSptLengkap($id,$id_pengikut)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'A4', true, 'UTF-8');

		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perintah Tugas');
		$pdf->SetTitle('Surat Perintah Tugas');
		$pdf->SetSubject('Surat Perintah Tugas');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 12);
		
		$model = $this->loadModel($id);
		$pegawai = Pegawai::model()->findByPk($id_pengikut);
		
		$html = '';
		
		$html .= $this->renderPartial('//layouts/kop',array(),true);
		
		$html .='<div style="text-align:center">
					<span style="font-size:20;font-weight:bold;text-decoration:underline">SURAT PERINTAH TUGAS</span>
				</div>';
				
		$html .= '<p>Yang bertanda tangan dibawah ini:</p>';
		
		$html .= '<table>';
		$html .= '<tr>';
		$html .= 	'<td width="25%" >Nama</td>';
		$html .= 	'<td width="75%" >: '.$model->getRelationField("pejabat","nama").'</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= 	'<td>NIP</td>';
		$html .= 	'<td>: '.$model->getRelationField("pejabat","nip").'</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= 	'<td>Jabatan</td>';
		$html .=	'<td>: '.$model->getRelationField("pejabat","jabatan").'</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= 	'<td>Alamat</td>';
		$html .=	'<td>: Jl. Raya Jakarta Km. 4 Serang Banten</td>';
		$html .= '</tr>';
		$html .= '</table>';
		
		$html .= '<p>&nbsp;</p>';
		
		$html .= '<p>MEMBERI PERINTAH KEPADA :</p>';
		
		$html .= '
				<table style="margin:5px">
					<tr>
						<td width="25%" >Nama</td>
						<td width="75%" >: <b>'.$pegawai->nama.'</b></td>
					</tr>
					<tr>
						<td>NIP</td>
						<td>: '.$pegawai->getNip().'</td>
					</tr>
					<tr>
						<td>Jabatan</td>
						<td>: '.$pegawai->jabatan.'</td>
					</tr>
					<tr>
						<td>Jabatan</td>
						<td>: Jl. Raya Jakarta Km. 4 Serang Banten</td>
					</tr>
				</table>';
			
		$html .= '<p>&nbsp;</p>';
			
		$html .= '<p style="margin:5px">Untuk melaksanakan tugas kegiatan Perjalanan Dinas:</p>';
		$html .= '
				<table>
					<tr>
						<td width="25%" >Tujuan</td>
						<td width="75%" >: '.$model->tujuan.'</td>
					</tr>
					<tr>
						<td>Tanggal Berangkat</td>
						<td>: '.Bantu::tanggal(($model->tgl_pergi)).'</td>
					</tr>
					<tr>
						<td>Tanggal kembali</td>
						<td>: '.Bantu::tanggal(($model->tgl_kembali)).'</td>
					</tr>
				</table>';		

		$html .= '<p>&nbsp;</p>';
		$html .= '<p>Kegiatan yang harus dilaksanakan</p>';
		
		$html .= '<table>';
		$html .= '<tr>';
		$html .= '<td width="100%">1. '.$model->maksud.'</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td width="100%" style="font-size:6">&nbsp;</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>2. ....................................................................................................................</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td width="100%" style="font-size:6">&nbsp;</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>3. ....................................................................................................................</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td width="100%" style="font-size:6">&nbsp;</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>4. ....................................................................................................................</td>';
		$html .= '</tr>';
		$html .= '</table>';
		
		$html .= '<p>&nbsp;</p>';
		
		$html .='<p>Demikian Surat Perintah ini di buat, agar dilaksanakan dengan sebaik-baiknya.</p>';
		
		$html .= '<p>&nbsp;</p>';
		
		$html .= '<table>
					<tr>
						<td width="40%">&nbsp;</td>
						<td width="20%">&nbsp;</td>
						<td width="40%" align="center">Serang, '.Bantu::tanggal(($model->tanggal_spd)).'</td>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr>
						<td style="text-align:center">Yang Diberi Perintah,</td>
						<td>&nbsp;</td>
						<td style="text-align:center">Yang Memberi Perintah,</td>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr>
						<td style="text-align:center"><span style="text-decoration:underline;">'.$pegawai->nama.'</span></td>
						<td>&nbsp;</td>
						<td style="text-align:center"><span style="text-decoration:underline;">'.$model->getRelationField("pejabat","nama").'</span></td>
					</tr>
					<tr>
						<td style="text-align:center">NIP. '.$pegawai->nip.'</td>
						<td>&nbsp;</td>
						<td style="text-align:center">NIP. '.$model->getRelationField("pejabat","nip").'</td>
					</tr>
				</table>';
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }
	
	public function actionCetakSptBlanko($id,$id_pengikut)
	{
		$pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'A4', true, 'UTF-8');

		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perintah Tugas');
		$pdf->SetTitle('Surat Perintah Tugas');
		$pdf->SetSubject('Surat Perintah Tugas');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 12);
		
		$pengikut = Pegawai::model()->findByPk($id_pengikut);
		$spt = Spd::model()->findByPk($id);
			
		$nomor = PosisiCetak::model()->getKolomByJenisSurat(5,'nomor');
		$nama_pejabat = PosisiCetak::model()->getKolomByJenisSurat(5,'nama_pejabat');
		$nip_pejabat = PosisiCetak::model()->getKolomByJenisSurat(5,'nip_pejabat');
		$jabatan_pejabat = PosisiCetak::model()->getKolomByJenisSurat(5,'jabatan_pejabat');
		$alamat_pejabat = PosisiCetak::model()->getKolomByJenisSurat(5,'alamat_pejabat');
		$nama_pegawai = PosisiCetak::model()->getKolomByJenisSurat(5,'nama_pegawai');
		$nip_pegawai = PosisiCetak::model()->getKolomByJenisSurat(5,'nip_pegawai');
		$jabatan_pegawai = PosisiCetak::model()->getKolomByJenisSurat(5,'jabatan_pegawai');
		$alamat_pegawai = PosisiCetak::model()->getKolomByJenisSurat(5,'alamat_pegawai');
		$tujuan = PosisiCetak::model()->getKolomByJenisSurat(5,'tujuan');
		$tgl_berangkat = PosisiCetak::model()->getKolomByJenisSurat(5,'tgl_berangkat');
		$tgl_kembali = PosisiCetak::model()->getKolomByJenisSurat(5,'tgl_kembali');
		$kegiatan = PosisiCetak::model()->getKolomByJenisSurat(5,'kegiatan');
		$tanggal_spt = PosisiCetak::model()->getKolomByJenisSurat(5,'tanggal_spt');
		$nama_yg_memerintah = PosisiCetak::model()->getKolomByJenisSurat(5,'nama_yg_memerintah');
		$nip_yg_memerintah = PosisiCetak::model()->getKolomByJenisSurat(5,'nip_yg_memerintah');
		$nama_yg_diperintah = PosisiCetak::model()->getKolomByJenisSurat(5,'nama_yg_diperintah');
		$nip_yg_diperintah = PosisiCetak::model()->getKolomByJenisSurat(5,'nip_yg_diperintah');
			
		$pdf->writeHTMLCell(0,0,$nama_pejabat->horisontal,$nama_pejabat->vertikal,'<span style="font-size:'.$nama_pejabat->font_size.'">'.$spt->getRelationField('pejabat','nama').'</span>');
		$pdf->writeHTMLCell(0,0,$nip_pejabat->horisontal,$nip_pejabat->vertikal,'<span style="font-size:'.$nip_pejabat->font_size.'">'.$spt->getRelationField('pejabat','nip').'</span>');
		$pdf->writeHTMLCell(0,0,$jabatan_pejabat->horisontal,$jabatan_pejabat->vertikal,'<span style="font-size:'.$jabatan_pejabat->font_size.'">'.$spt->getRelationField('pejabat','jabatan').'</span>');
		$pdf->writeHTMLCell(0,0,$alamat_pejabat->horisontal,$alamat_pejabat->vertikal,'<span style="font-size:'.$alamat_pejabat->font_size.'">'.'Jl. Raya Jakarta KM. 04 Serang'.'</span>');
		$pdf->writeHTMLCell(0,0,$nama_pegawai->horisontal,$nama_pegawai->vertikal,'<span style="font-size:'.$nama_pegawai->font_size.'">'.$pengikut->nama.'</span>');
		$pdf->writeHTMLCell(0,0,$nip_pegawai->horisontal,$nip_pegawai->vertikal,'<span style="font-size:'.$nip_pegawai->font_size.'">'.$pengikut->nip.'</span>');
		$pdf->writeHTMLCell(0,0,$jabatan_pegawai->horisontal,$jabatan_pegawai->vertikal,'<span style="font-size:'.$jabatan_pegawai->font_size.'">'.$pengikut->jabatan.'</span>');
		$pdf->writeHTMLCell(0,0,$alamat_pegawai->horisontal,$alamat_pegawai->vertikal,'<span style="font-size:'.$alamat_pegawai->font_size.'">'.'Jl. Raya Jakarta KM. 04 Serang</span>');
		$pdf->writeHTMLCell(0,0,$tujuan->horisontal,$tujuan->vertikal,'<span style="font-size:'.$tujuan->font_size.'">'.$spt->tujuan.'</span>');
		$pdf->writeHTMLCell(0,0,$tgl_berangkat->horisontal,$tgl_berangkat->vertikal,'<span style="font-size:'.$tgl_berangkat->font_size.'">'.Bantu::tanggal(($spt->tgl_pergi)).'</span>');
		$pdf->writeHTMLCell(0,0,$tgl_kembali->horisontal,$tgl_kembali->vertikal,'<span style="font-size:'.$tgl_kembali->font_size.'">'.Bantu::tanggal(($spt->tgl_kembali)).'</span>');
		$pdf->writeHTMLCell(0,0,$kegiatan->horisontal,$kegiatan->vertikal,'<span style="font-size:'.$kegiatan->font_size.'">'.$spt->maksud.'</span>');
		$pdf->writeHTMLCell(0,0,$tanggal_spt->horisontal,$tanggal_spt->vertikal,'<span style="font-size:'.$tanggal_spt->font_size.'">'.Bantu::tanggal($spt->tanggal_spd).'</span>');
		$pdf->writeHTMLCell(0,0,$nama_yg_memerintah->horisontal,$nama_yg_memerintah->vertikal,'<span style="font-size:'.$nama_yg_memerintah->font_size.'">'.$spt->getRelationField('pejabat','nama').'</span>');
		$pdf->writeHTMLCell(0,0,$nip_yg_memerintah->horisontal,$nip_yg_memerintah->vertikal,'<span style="font-size:'.$nip_yg_memerintah->font_size.'">'.$spt->getRelationField('pejabat','nip').'</span>');
		$pdf->writeHTMLCell(0,0,$nama_yg_diperintah->horisontal,$nama_yg_diperintah->vertikal,'<span style="font-size:'.$nama_yg_diperintah->font_size.'">'.$pengikut->nama.'</span>');
		$pdf->writeHTMLCell(0,0,$nip_yg_diperintah->horisontal,$nip_yg_diperintah->vertikal,'<span style="font-size:'.$nip_yg_diperintah->font_size.'">'.$pengikut->nip.'</span>');
		
		//Close and output PDF document
		$pdf->Output('.pdf', 'I');
	}
	
	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='spd-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
