<?php

class SpdBelakangController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/constellation/index';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
return array(
array('allow',  // allow all users to perform 'index' and 'view' actions
'actions'=>array('index','view'),
'users'=>array('@'),
),
array('allow', // allow authenticated user to perform 'create' and 'update' actions
'actions'=>array('create','update','cetakBlanko'),
'users'=>array('@'),
),
array('allow', // allow admin user to perform 'admin' and 'delete' actions
'actions'=>array('admin','delete'),
'users'=>array('admin'),
),
array('deny',  // deny all users
'users'=>array('*'),
),
);
}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate($id_spd)
	{
		$model=new SpdBelakang;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SpdBelakang']))
		{
			$model->attributes=$_POST['SpdBelakang'];
			$model->id_spd = $id_spd;
			
			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('spd/view','id'=>$model->id_spd));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SpdBelakang']))
		{
			$model->attributes=$_POST['SpdBelakang'];
			if($model->save())
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('spd/view','id'=>$model->id_spd));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			$model = $this->loadModel($id);
			$id_spd = $model->id_spd;
			$model->delete();
		
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
			{
				Yii::app()->user->setFlash('success','Data berhasil dihapus');
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('spd/view','id'=>$id_spd));
			}
		} else
		throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('SpdBelakang');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
$model=new SpdBelakang('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['SpdBelakang']))
$model->attributes=$_GET['SpdBelakang'];

$this->render('admin',array(
'model'=>$model,
));
}

	public function actionCetakSpdBelakangLengkap($id,$id_pengikut)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'A4', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Surat Perjalanan Dinas');
		$pdf->SetSubject('Surat Perjalanan Dinas');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 12);
		
		$model = $this->loadModel($id);
		$pegawai = Pegawai::model()->findByPk($id_pengikut);
		
		$html = '';
		
			
		$html .= '
				<table>
			 	<tr>
			 		<td width="50%" >&nbsp;</td>
				 	<td width="20%">Berangkat dari</td>
					<td width="30%">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>(Tempat Kedudukan)</td>
					<td>: '.$model->tempat_berangkat.'</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>Pada tanggal</td>
					<td>: '.Bantu::tanggal($model->tgl_pergi).'</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>Ke</td>
					<td>: '.$model->tujuan.'</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>Kepala</td>
					<td>: '.$model->getRelationField("pejabat","nama").'</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				</table>';
		
		for($i=1;$i<=3;$i++) {
		
		$html .= '
			<table>
			<tr>
				<td colspan="6" style="font-size:10;border-top:1px solid #000000">&nbsp;</td>
			</tr>
			<tr>
				<td width="4%">'.$i.'.</td>
				<td width="13%">Tiba di</td>
				<td width="30%">: ....................................................</td>
				<td width="5%">&nbsp;</td>
				<td width="13%">Berangkat dari</td>
				<td width="40%">: ......................................................</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>Pada tanggal</td>
				<td>: .....................................................</td>
				<td>&nbsp;</td>
				<td>Ke</td>
				<td>: .....................................................</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>Pada Tanggal</td>
				<td>: .........................................................</td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>Kepala</td>
				<td>: .....................................................</td>
				<td>&nbsp;</td>
				<td>Kepala</td>
				<td>: .....................................................</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td> (..................................................)</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td> (....................................................)</td>
			</tr>
			<tr>
				<td colspan="6" style="font-size:10">&nbsp;</td>
			</tr>
			</table>';
		}
			
		
		$html .='
			<table>
			<tr>
				<td colspan="6" style="font-size:10;border-top:1px solid #000000">&nbsp;</td>
			</tr>
			<tr>
				<td width="4%">4. </td>
				<td width="20%">Tiba kembali di</td>
				<td width="23%">&nbsp;</td>
				<td width="5%">&nbsp;</td>
				<td width="48%" rowspan="2" colspan="2">Telah diperiksa dengan keterangan 
					tersebut di atas benar dilaksanakan atas perintahnya semata-mata untuk
					kepentingan dalam waktu yang sesingkat-singkatnya.</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>(tempat kedudukan)</td>
				<td>: ....................................</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td colspan="2" style="text-align:center"> (..................................................)</td>
				<td>&nbsp;</td>
				<td colspan="2" style="text-align:center"> (....................................................)</td>
			</tr>
			<tr>
				<td colspan="6" style="font-size:10;border-bottom:1px solid #000000">&nbsp;</td>
			</tr>
			</table>';
			
		$html .= '<div><span style="text-decoration:underline;font-weight:bold;text-align:justify">PERHATIAN:</span><br>
					Pejabat yang berwenang menerbitkan SPPD pegawai yang melakukan perjalanan dinas para Pejabat yang mengesahkan tanggal
					berangkat / tiba serta bendaharawan bertanggung jawab berdasarkan peraturan-peraturan Keuangan Negara, apabila 
					negara menderita rugi akibat kesalahan, kelalaian dan kealpaannya. (angka 8 Lampiran Surat Menteri
					Keuangan tanggal 30 April 1974 No. B.296/MK/1/4/1974)</div>';
		
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer the ID of the model to be loaded
	*/
	
	public function loadModel($id)
	{
		$model=SpdBelakang::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='spd-belakang-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
