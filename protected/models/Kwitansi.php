<?php

/**
 * This is the model class for table "kwitansi".
 *
 * The followings are the available columns in table 'kwitansi':
 * @property integer $id
 * @property string $nomor
 * @property integer $id_spd
 * @property integer $id_akomodasi
 * @property integer $id_refresentatif
 * @property integer $id_ssh
 * @property integer $bendahara
 * @property integer $ppk
 * @property string $bbm
 * @property string $tol
 * @property string $tiket
 * @property string $total
 *
 * The followings are the available model relations:
 * @property Akomodasi $idAkomodasi
 * @property Pejabat $bendahara0
 * @property Pejabat $ppk0
 * @property Refresentatif $idRefresentatif
 * @property Spd $idSpd
 * @property Ssh $idSsh
 */
class Kwitansi extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Kwitansi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kwitansi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_spd, lama, penerima, bendahara, pptk, pa', 'numerical', 'integerOnly'=>true),
			array('nomor,spd,akomodasi,uang_harian, bendahara, pptk, pa, penerima','safe'),
			array('nomor, bbm, tol, tiket, total, akomodasi, refresentatif, uang_harian,', 'length', 'max'=>255),
			array('tanggal, program, kegiatan','safe'),
			//array('nomor','nomorUnik'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nomor, id_spd, id_akomodasi, id_refresentatif, id_ssh, bendahara, ppk, bbm, tol, tiket, total', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'penerimaRelation' => array(self::BELONGS_TO, 'Pegawai', 'penerima'),
			'akomodasi' => array(self::BELONGS_TO, 'Akomodasi', 'id_akomodasi'),
			'bendaharaRelation' => array(self::BELONGS_TO, 'Pejabat', 'bendahara'),
			'pptkRelation' => array(self::BELONGS_TO, 'Pejabat', 'pptk'),
			'paRelation' => array(self::BELONGS_TO, 'Pejabat', 'pa'),
			'refresentatif' => array(self::BELONGS_TO, 'Refresentatif', 'id_refresentatif'),
			'spd' => array(self::BELONGS_TO, 'Spd', 'id_spd'),
			'ssh' => array(self::BELONGS_TO, 'Ssh', 'id_ssh'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tanggal' => 'Tanggal Kwitansi',
			'nomor' => 'Nomor Kwitansi',
			'id_spd' => 'Nomor SPD',
			'id_akomodasi' => 'Akomodasi',
			'id_refresentatif' => 'Representatif',
			'id_ssh' => 'Uang Harian',
			'bendahara' => 'Bendahara',
			'pptk' => 'Pejabat Pelaksana Teknis Kegiatan',
			'pa'=>'PA/KPA',
			'bbm' => 'BBM',
			'tol' => 'Tol',
			'tiket' => 'Tiket',
			'total' => 'Total',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = Yii::app()->advancedFilters->createCriteria();
		$criteria->select = "t.*, spd.nomor_spd";
		$criteria->join = "JOIN spd ON spd.id = t.id_spd";
		$criteria->addAdvancedFilterCondition('spd.nomor_spd',$this->id_spd,true);

		$criteria->compare('id',$this->id);
		$criteria->compare('nomor',$this->nomor,true);
		//$criteria->compare('id_spd',$this->id_spd);
		$criteria->compare('penerima',$this->penerima);
		$criteria->compare('program',$this->program);
		$criteria->compare('kegiatan',$this->kegiatan);
		$criteria->compare('akomodasi',$this->akomodasi);
		$criteria->compare('refresentatif',$this->refresentatif);
		$criteria->compare('uang_harian',$this->uang_harian);
		$criteria->compare('bendahara',$this->bendahara);
		$criteria->compare('pptk',$this->pptk);
		$criteria->compare('bbm',$this->bbm,true);
		$criteria->compare('tol',$this->tol,true);
		$criteria->compare('tiket',$this->tiket,true);
		$criteria->compare('total',$this->total,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
                    'defaultOrder'=>'id DESC',
                ),
		));
	}
	
	public function nomorUnik()
	{
		$model = Kwitansi::model()->findByAttributes(array('nomor'=>$this->nomor));
		if($model!==null)
		{
			if($model->id != $this->id)
				$this->addError('nomor','Ganti nomor kwitansi, sudah ada kwitansi yang menggunakan nomor tersebut');
			
		}
	}
	
	public function getRelationField($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return null;
	}
	
	public function getRelationNip($relation)
	{
		if(!empty($this->$relation))
			return $this->$relation->getNip();
		else
			return null;
	}
	
	public function getChart($type)
	{
		$list = array();
		$model = Pegawai::model()->findAll();
		
		if($type=='bar')
		{
			foreach($model as $data)
			{
				$list[] = array('type'=>'column','name'=>$data->nama,'data'=>array(
					$this->getTotal($data->id,01,date('Y')),
					$this->getTotal($data->id,02,date('Y')),
					$this->getTotal($data->id,03,date('Y')),
					$this->getTotal($data->id,04,date('Y')),
					$this->getTotal($data->id,05,date('Y')),
					$this->getTotal($data->id,06,date('Y')),
					$this->getTotal($data->id,08,date('Y')),
					$this->getTotal($data->id,07,date('Y')),
					$this->getTotal($data->id,09,date('Y')),
					$this->getTotal($data->id,10,date('Y')),
					$this->getTotal($data->id,11,date('Y')),
					$this->getTotal($data->id,12,date('Y')),
				));
			}
		}
		
		return $list;
	}
	
	public function getTotal($id,$bulan,$year)
	{
		$criteria = new CDbCriteria;
		$criteria->addCondition('MONTH(tanggal) = :month');
		$criteria->params[':month']=date($bulan);
		$criteria->addCondition('YEAR(tanggal) = :year');
		$criteria->params[':year']=date($year);
		$list = array();
		$jumlah = 0;
		
		$kwitansis = Kwitansi::model()->findAllByAttributes(array('penerima'=>$id),$criteria);
		foreach($kwitansis as $kwitansi)
		{
			$jumlah += $kwitansi->total;
		}
		$list[] = (int)$jumlah;
		
		return $list;
	}
	
	public function getUntuk()
	{
		$untuk = '';
		
		$untuk .= 'Untuk biaya perjalanan dinas ke '.$this->getRelationField('spd','tujuan');
		$untuk .= ' selama '.$this->getRelationField('spd','lama');
		$untuk .= ' ('.ucwords(trim(Bantu::getTerbilang($this->getRelationField('spd','lama'),0))).') hari';
		$untuk .= ' tgl. '.Bantu::tgl($this->getRelationField('spd','tgl_pergi'));
		$untuk .= ' s/d ';
		$untuk .= Bantu::tgl($this->getRelationField('spd','tgl_kembali'));
		$untuk .= ', dengan rincian: ';
		$untuk .= 'Uang Harian '.Bantu::rp($this->uang_harian).' x '.$this->getRelationField('spd','lama').' Hari';
		
		if(!empty($this->akomodasi))
			$untuk .= ' + Akomodasi = '.Bantu::rp($this->akomodasi);
			
		if(!empty($this->refresentatif))
			$untuk .= ' + Refresentatif = '.Bantu::rp($this->refresentatif);
			
		if(!empty($this->bbm))
			$untuk .= ' + BBM = '.Bantu::rp($this->bbm);
			
		if(!empty($this->tol))
			$untuk .= ' + Tol = '.Bantu::rp($this->tol);
			
		if(!empty($this->tiket))
			$untuk .= ' + Tol = '.Bantu::rp($this->tiket);
		
		return $untuk;
	}
}