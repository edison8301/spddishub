<?php

/**
 * This is the model class for table "spd_belakang".
 *
 * The followings are the available columns in table 'spd_belakang':
 * @property integer $id
 * @property integer $id_spd
 * @property integer $urutan
 * @property string $tempat_berangkat
 * @property string $tanggal_berangkat
 * @property string $nama_kepala_berangkat
 * @property string $nip_kepala_berangkat
 * @property string $jabatan_kepala_berangkat
 * @property string $tempat_tiba
 * @property string $tanggal_tiba
 * @property string $nama_kepala_tiba
 * @property string $nip_kepala_tiba
 * @property string $jabatan_kepala_tiba
 */
class SpdBelakang extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SpdBelakang the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'spd_belakang';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_spd, urutan', 'required'),
			array('id_spd, urutan', 'numerical', 'integerOnly'=>true),
			array('tempat_berangkat, nama_kepala_berangkat, ke, nip_kepala_berangkat, jabatan_kepala_berangkat, tempat_tiba, nama_kepala_tiba, nip_kepala_tiba, jabatan_kepala_tiba', 'length', 'max'=>255),
			array('tanggal_berangkat, tanggal_tiba', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_spd, urutan, tempat_berangkat, tanggal_berangkat, nama_kepala_berangkat, nip_kepala_berangkat, jabatan_kepala_berangkat, tempat_tiba, tanggal_tiba, nama_kepala_tiba, nip_kepala_tiba, jabatan_kepala_tiba', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_spd' => 'Id Spd',
			'urutan' => 'Urutan',
			'tempat_berangkat' => 'Berangkat Dari',
			'tanggal_berangkat' => 'Berangkat Pada Tanggal',
			'nama_kepala_berangkat' => 'Nama Kepala Berangkat',
			'nip_kepala_berangkat' => 'Nip Kepala Berangkat',
			'jabatan_kepala_berangkat' => 'Jabatan Kepala Berangkat',
			'tempat_tiba' => 'Tiba di',
			'tanggal_tiba' => 'Tiba Pada Tanggal',
			'nama_kepala_tiba' => 'Nama Kepala Tiba',
			'nip_kepala_tiba' => 'Nip Kepala Tiba',
			'jabatan_kepala_tiba' => 'Jabatan Kepala Tiba',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_spd',$this->id_spd);
		$criteria->compare('urutan',$this->urutan);
		$criteria->compare('tempat_berangkat',$this->tempat_berangkat,true);
		$criteria->compare('tanggal_berangkat',$this->tanggal_berangkat,true);
		$criteria->compare('nama_kepala_berangkat',$this->nama_kepala_berangkat,true);
		$criteria->compare('nip_kepala_berangkat',$this->nip_kepala_berangkat,true);
		$criteria->compare('jabatan_kepala_berangkat',$this->jabatan_kepala_berangkat,true);
		$criteria->compare('tempat_tiba',$this->tempat_tiba,true);
		$criteria->compare('tanggal_tiba',$this->tanggal_tiba,true);
		$criteria->compare('nama_kepala_tiba',$this->nama_kepala_tiba,true);
		$criteria->compare('nip_kepala_tiba',$this->nip_kepala_tiba,true);
		$criteria->compare('jabatan_kepala_tiba',$this->jabatan_kepala_tiba,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}