<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class Import extends CFormModel
{
	public $file;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array('file', 'required'),
			array('file', 'file', 'types'=>'jpg, gif, png, pdf'),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'file'=>'Select File To Import',
		);
	}
}