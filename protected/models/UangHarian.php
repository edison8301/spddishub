<?php

/**
 * This is the model class for table "ssh".
 *
 * The followings are the available columns in table 'ssh':
 * @property integer $id
 * @property string $jarak
 * @property integer $id_golongan
 * @property string $biaya
 *
 * The followings are the available model relations:
 * @property Kwitansi[] $kwitansis
 * @property Golongan $idGolongan
 */
class UangHarian extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Ssh the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'uang_harian';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_golongan', 'numerical', 'integerOnly'=>true),
			array('jarak, biaya', 'length', 'max'=>255),
			array('jarak,biaya','required','message'=>'{attribute} tidak boleh kosong'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, jarak, id_golongan, biaya', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'kwitansi' => array(self::HAS_MANY, 'Kwitansi', 'id_ssh'),
			'golongan' => array(self::BELONGS_TO, 'Golongan', 'id_golongan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'jarak' => 'Jarak',
			'id_golongan' => 'Golongan',
			'biaya' => 'Biaya',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		
		if(isset($_GET['UangHarian']))
			$this->attributes = $_GET['UangHarian'];
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('jarak',$this->jarak,true);
		$criteria->compare('id_golongan',$this->id_golongan);
		$criteria->compare('biaya',$this->biaya,true);
		
		if(Yii::app()->controller->id == 'kwitansi')
			$limit = 3;
		else
			$limit = 10;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => $limit,
			),
		));
	}
}