<?php

/**
 * This is the model class for table "Pengikut".
 *
 * The followings are the available columns in table 'Pengikut':
 * @property integer $id
 * @property integer $id_spd
 * @property integer $id_pegawai
 *
 * The followings are the available model relations:
 * @property Pegawai $idPegawai
 * @property Spd $idSpd
 */
class Pengikut extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Pengikut the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pengikut';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_spd, id_pegawai, aktif', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_spd, id_pegawai', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pegawai' => array(self::BELONGS_TO, 'Pegawai', 'id_pegawai'),
			'spd' => array(self::BELONGS_TO, 'Spd', 'id_spd'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_spd' => 'SPD',
			'id_pegawai' => 'Nama Pegawai',
			'aktif' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_spd',$this->id_spd);
		$criteria->compare('id_pegawai',$this->id_pegawai);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getRelationField($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return null;
	}
	
	public function getRelationRelationField($relation1,$relation2,$field)
	{
		if(!empty($this->$relation1->$relation2->$field))
			return $this->$relation1->$relation2->$field;
		else
			return null;
	}
	
	public function afterDelete()
	{
		$lhp = Lhp::model()->findAllByAttributes(array('id_spd'=>$this->id_spd,'id_pegawai'=>$this->id_pegawai));
		$kwitansi = Kwitansi::model()->findAllByAttributes(array('id_spd'=>$this->id_spd,'penerima'=>$this->id_pegawai));
		$perjalanan = Perjalanan::model()->findAllByAttributes(array('id_pegawai'=>$this->id_pegawai,'id_spd'=>$this->id_spd));
		
		if($lhp!==null) 
		{
			foreach($lhp as $data)
			{
				$data->delete();
			}
		}
		
		if($kwitansi!==null) 
		{
			foreach($kwitansi as $data)
			{
				$data->delete();
			}
		}
		
		if($perjalanan!==null) 
		{
			foreach($perjalanan as $data)
			{
				$data->delete();
			}
		}
	}
}