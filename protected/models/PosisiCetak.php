<?php

/**
 * This is the model class for table "posisi_cetak".
 *
 * The followings are the available columns in table 'posisi_cetak':
 * @property integer $id
 * @property integer $jenis_surat
 * @property string $kolom
 * @property integer $horisontal
 * @property integer $vertikal
 */
class PosisiCetak extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PosisiCetak the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'posisi_cetak';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('jenis_surat, kolom', 'required'),
			array('jenis_surat, horisontal, vertikal, font_size', 'numerical', 'integerOnly'=>true),
			array('kolom', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, jenis_surat, kolom, horisontal, vertikal, font_size', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'jenis_surat' => 'Jenis Surat',
			'kolom' => 'Kolom',
			'horisontal' => 'Horisontal',
			'vertikal' => 'Vertikal',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('jenis_surat',$this->jenis_surat);
		$criteria->compare('kolom',$this->kolom,true);
		$criteria->compare('horisontal',$this->horisontal);
		$criteria->compare('vertikal',$this->vertikal);
		$criteria->compare('font_size',$this->font_size);
		
		if(isset($_GET['jenis_surat']) AND $_GET['jenis_surat'] != null)
			$criteria->compare('jenis_surat',$_GET['jenis_surat']);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getKolomByJenisSurat($jenis_surat,$kolom)
	{
		return PosisiCetak::model()->findByAttributes(array('jenis_surat'=>$jenis_surat,'kolom'=>$kolom));
	}
}