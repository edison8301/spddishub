
<h1><?php echo JBackupTranslator::t('backup','Upload Backup'); ?></h1>

<div class="form">


<?php
    /**
     * @var $form CActiveForm 
     */
    $form = $this->beginWidget($this->module->bootstrap ? 'booster.widgets.TbActiveForm' : 'CActiveForm', array(
            'id' => 'install-form',
            'enableAjaxValidation' => false,
            'htmlOptions'=>array('enctype'=>'multipart/form-data'),
    ));
?>
    <?php 
        if($this->module->bootstrap) {
            echo $form->fileFieldGroup($model,'upload_file');
        } else {
    ?>
        <div class="row">
                <?php echo $form->labelEx($model,'upload_file'); ?>
                <?php echo $form->fileField($model,'upload_file'); ?>
                <?php echo $form->error($model,'upload_file'); ?>
        </div><!-- row -->
    <?php }?>
<?php
	$this->widget('booster.widgets.TbButton', array(
		'buttonType'=>'submit',
		'context'=>'primary',
		'label'=>'Upload',
		'icon'=>'upload',
	));
	$this->endWidget();
?>
</div><!-- form -->